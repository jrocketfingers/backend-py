import asyncio
import dataclasses
import traceback
from datetime import datetime
from typing import Any, Generic, Protocol, TypeVar

import sentry_sdk
from aioinject import Container

from lib.utils import utc_now

_T = TypeVar("_T")


@dataclasses.dataclass
class TaskResult(Generic[_T]):
    result: _T
    next_schedule_at: datetime


class Task(Protocol):
    async def run_once(self) -> TaskResult[Any]: ...


async def run_task_forever(  # pragma: no cover
    container: Container,
    task_cls: type[Task],
    stop: asyncio.Event,
) -> None:
    stop_task = asyncio.create_task(stop.wait())
    while True:
        try:
            async with container.context() as ctx:
                task = await ctx.resolve(task_cls)
                result = await task.run_once()
        except Exception as e:  # noqa: BLE001
            sentry_sdk.capture_exception(e)
            traceback.print_exc()
            await asyncio.sleep(5)
        else:
            delta = result.next_schedule_at.timestamp() - utc_now().timestamp()
            await asyncio.wait(
                [
                    asyncio.create_task(asyncio.sleep(delta)),
                    stop_task,
                ],
                return_when=asyncio.FIRST_COMPLETED,
            )
            if stop.is_set():
                return
