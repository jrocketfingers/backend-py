from collections.abc import Mapping
from typing import Any, Literal, TypeAlias

from lib.models import CamelCaseModel, MinMaxValue, PascalCaseModel

BodyPartType = Literal[
    "Chest",
    "Head",
    "Stomach",
    "LeftArm",
    "RightArm",
    "LeftLeg",
    "RightLeg",
]


class BotHealth(PascalCaseModel):
    body_parts: list[Mapping[BodyPartType, MinMaxValue[int]]]
    energy: MinMaxValue[float]
    hydration: MinMaxValue[float]
    temperature: MinMaxValue[float]


BotDifficultyKey = Literal["easy", "normal", "hard", "impossible"]

BotDifficulty: TypeAlias = dict[str, Any]


class BotConfig(CamelCaseModel):
    appearance: dict[Literal["body", "feet", "hands", "head", "voice"], dict[str, int]]
    chances: dict[str, dict[str, int]]
    difficulty: dict[BotDifficultyKey, BotDifficulty]
    experience: dict[str, Any]
    first_name: list[str]
    generation: dict[str, Any]
    health: BotHealth
    inventory: dict[str, Any]
    last_name: list[str]
    skills: dict[str, Any]


BotCoreConfig = BotDifficulty
