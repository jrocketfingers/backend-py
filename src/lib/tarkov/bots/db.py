from __future__ import annotations

import asyncio
from pathlib import Path

from pydantic import TypeAdapter

from lib.tarkov.bots.models import BotConfig, BotCoreConfig
from lib.tarkov.db import AssetsPath
from lib.utils import read_pydantic_json


async def create_bot_db(assets_path: AssetsPath) -> BotDB:
    bots = await read_bot_configs(
        path=assets_path.joinpath("bot", "bots"),
    )
    core = await read_pydantic_json(
        assets_path.joinpath("bot", "core.json"),
        model=TypeAdapter(BotCoreConfig),
    )
    return BotDB(bots=bots, core=core)


class BotDB:
    def __init__(
        self,
        bots: dict[str, BotConfig],
        core: BotCoreConfig,
    ) -> None:
        self.bots = bots
        self.core = core


async def read_bot_configs(path: Path) -> dict[str, BotConfig]:
    paths = list(path.iterdir())
    tasks = [read_pydantic_json(bot_path, BotConfig) for bot_path in paths]
    result = await asyncio.gather(*tasks)
    return {
        bot_path.stem: config for bot_path, config in zip(paths, result, strict=True)
    }
