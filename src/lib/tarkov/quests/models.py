import enum
from typing import Annotated, Any, Literal

import pydantic
from pydantic import Discriminator

from lib.models import CamelCaseModel, PascalCaseModel
from lib.tarkov.types import QuestId

QuestCondition = Any


class QuestStatus(enum.IntEnum):
    Locked = 0
    AvailableForStart = 1
    Started = 2
    AvailableForFinish = 3
    Success = 4
    Fail = 5
    FailRestartable = 6
    MarkedAsFailed = 7
    Expired = 8
    AvailableAfter = 9


CompareMethod = Literal[">=", ">", "<", "<="]


class _BaseQuestCondition(CamelCaseModel):
    id: str
    index: int
    parent_id: str
    dynamic_locale: bool
    global_quest_counter_id: str
    visibility_conditions: list[Any] = pydantic.Field(default_factory=list)


class QuestStatusCondition(_BaseQuestCondition):
    condition_type: Literal["Quest"] = "Quest"
    available_after: int
    dispersion: int
    status: list[QuestStatus]
    target: str


class LevelCondition(_BaseQuestCondition):
    condition_type: Literal["Level"] = "Level"
    compare_method: CompareMethod
    value: int


class TraderStandingCondition(_BaseQuestCondition):
    condition_type: Literal["TraderStanding"] = "TraderStanding"
    compare_method: CompareMethod
    value: float
    target: str


class TraderLoyaltyCondition(_BaseQuestCondition):
    condition_type: Literal["TraderLoyalty"] = "TraderLoyalty"
    compare_method: CompareMethod
    value: int
    target: str


AnyQuestCondition = Annotated[
    QuestStatusCondition
    | LevelCondition
    | TraderStandingCondition
    | TraderLoyaltyCondition,
    Discriminator("condition_type"),
]


class QuestConditions(PascalCaseModel):
    available_for_finish: list[QuestCondition]
    available_for_start: list[AnyQuestCondition]
    fail: list[QuestCondition]


QuestReward = Any


class QuestRewards(PascalCaseModel):
    fail: list[QuestReward]
    started: list[QuestReward]
    success: list[QuestReward]


QuestType = str


class QuestTemplate(CamelCaseModel):
    id: QuestId = pydantic.Field(alias="_id")
    quest_name: str = pydantic.Field(alias="QuestName")
    name: str
    accept_player_message: str | None = None
    conditions: QuestConditions
    can_show_notifications_in_game: bool
    change_quest_message_text: str
    complete_player_message: str | None = None
    decline_player_message: str
    description: str
    fail_message_text: str
    image: str
    instant_complete: bool
    is_key: bool
    location: str
    note: str
    quest_status: Any | None = None
    restartable: bool
    rewards: QuestRewards
    secret_quest: bool
    side: Literal["Pmc"]
    started_message_text: str
    success_message_text: str
    trader_id: str
    type: QuestType
