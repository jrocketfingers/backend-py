import dataclasses

from lib.tarkov.types import QuestId


@dataclasses.dataclass
class QuestNotFoundError:
    quest_id: QuestId


@dataclasses.dataclass
class QuestIsNotAvailableError:
    quest_id: QuestId


@dataclasses.dataclass
class QuestIsAlreadyAccepted:
    quest_id: QuestId
