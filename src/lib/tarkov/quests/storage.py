from typing import Final

from lib.tarkov.quests.models import QuestTemplate
from lib.tarkov.types import QuestId


class QuestStorage:
    def __init__(self, quests: dict[QuestId, QuestTemplate]) -> None:
        self.quests: Final = quests

    def get(self, quest_id: QuestId) -> QuestTemplate | None:
        return self.quests.get(quest_id)
