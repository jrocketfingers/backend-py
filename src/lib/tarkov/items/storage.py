import itertools
from collections.abc import Sequence
from typing import Final

from lib.tarkov.inventory import HasCartridges, HasChambers
from lib.tarkov.items.models import ItemNode, ItemTemplate
from lib.tarkov.items.props.base import CompoundItemProps
from lib.tarkov.items.props.types import Slot, StackSlot
from lib.tarkov.types import ItemTemplateId


class ItemDB:
    def __init__(
        self,
        items: dict[ItemTemplateId, ItemTemplate],
        nodes: dict[ItemTemplateId, ItemNode],
    ) -> None:
        self.items: Final = items
        self.nodes: Final = nodes

    def get(self, id_: ItemTemplateId) -> ItemTemplate:
        return self.items[id_]

    def get_by_name(self, name: str) -> ItemTemplate:
        return next(  # pragma: no branch
            i for i in self.items.values() if i.name == name
        )

    def linked_item_ids(self, item: ItemTemplate) -> Sequence[ItemTemplateId]:
        slots: list[Sequence[Slot | StackSlot]] = []
        if isinstance(item.props, CompoundItemProps):
            slots.append(item.props.slots)
        if isinstance(item.props, HasChambers):
            slots.append(item.props.chambers)
        if isinstance(item.props, HasCartridges):  # pragma: no cover
            slots.append(item.props.cartridges)

        result = []
        for slot in itertools.chain.from_iterable(slots):
            for slot_filter in slot.props.filters:
                result.extend(slot_filter.filter)
        return tuple(result)
