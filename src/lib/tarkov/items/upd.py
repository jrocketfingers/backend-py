from typing import Literal

from pydantic import ConfigDict
from typing_extensions import TypedDict


class UpdFireMode(TypedDict):
    FireMode: Literal["single", "fullauto", "burst"]


class UpdRepairable(TypedDict, total=False):
    Durability: float
    MaxDurability: float


class UpdFoldable(TypedDict):
    Folded: bool


class UpdResource(TypedDict):
    Value: float


class UpdMedKit(TypedDict):
    HpResource: float


class UpdFoodDrink(TypedDict):
    HpPercent: float


class UpdSight(TypedDict):
    SelectedScope: int
    ScopesCurrentCalibPointIndexes: list[int]
    ScopesSelectedModes: list[int]


class Upd(TypedDict, total=False):
    __pydantic_config__ = ConfigDict(extra="allow")  # type: ignore[misc]

    StackObjectsCount: int
    BuyRestrictionMax: int
    BuyRestrictionCurrent: int
    UnlimitedCount: bool

    Foldable: UpdFoldable
    Repairable: UpdRepairable
    FireMode: UpdFireMode
    Resource: UpdResource
    MedKit: UpdMedKit
    FoodDrink: UpdFoodDrink
    Sight: UpdSight
