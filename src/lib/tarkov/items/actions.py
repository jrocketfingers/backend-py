import dataclasses
from collections.abc import Mapping, Sequence
from typing import Annotated, Any, Generic, Literal, TypeVar

import pydantic
from pydantic import Discriminator, Tag

from lib.models import CamelCaseModel, DatetimeInt, StrictModel
from lib.tarkov.items.models import Item, ItemLocation
from lib.tarkov.profiles.models import (
    HideoutAreaType,
    ProfileHealth,
    Quest,
    Skills,
    TraderInfo,
)
from lib.tarkov.types import ItemId, ItemTemplateId, QuestId

T = TypeVar("T")


class Location(StrictModel):
    id: ItemId
    container: str
    location: ItemLocation | int | None = None


class RequiredItem(StrictModel):
    id: ItemId
    count: int


class ItemOwner(StrictModel):
    id: str
    type: Literal["Trader", "HideoutProduction", "RagFair"]


class ActionBase(StrictModel, Generic[T]):
    action: Annotated[T, pydantic.Field(alias="Action")]


class ExamineAction(ActionBase[Literal["Examine"]]):
    item: ItemId
    from_owner: ItemOwner = pydantic.Field(alias="fromOwner")


class ReadEncyclopediaAction(ActionBase[Literal["ReadEncyclopedia"]]):
    ids: list[ItemTemplateId]


class HideoutUpgradeAction(ActionBase[Literal["HideoutUpgrade"]]):
    area_type: HideoutAreaType = pydantic.Field(alias="areaType")
    items: list[RequiredItem]
    timestamp: DatetimeInt


class HideoutUpgradeCompleteAction(ActionBase[Literal["HideoutUpgradeComplete"]]):
    area_type: HideoutAreaType = pydantic.Field(alias="areaType")
    timestamp: DatetimeInt


class HideoutPutItemsAction(ActionBase[Literal["HideoutPutItemsInAreaSlots"]]):
    area_type: HideoutAreaType = pydantic.Field(alias="areaType")
    items: dict[int, RequiredItem]
    timestamp: DatetimeInt


class HideoutToggleAreaAction(ActionBase[Literal["HideoutToggleArea"]]):
    area_type: HideoutAreaType = pydantic.Field(alias="areaType")
    enabled: bool
    timestamp: DatetimeInt


class MoveAction(ActionBase[Literal["Move"]]):
    item: ItemId
    to: Location


class RecordShootingRangePointsAction(
    ActionBase[Literal["RecordShootingRangePoints"]],
):
    points: int


class TradingConfirmAction(ActionBase[Literal["TradingConfirm"]]):
    type: Literal["buy_from_trader"]
    trader_id: str = pydantic.Field(alias="tid")
    item_id: ItemId
    count: int
    scheme_id: int
    scheme_items: Sequence[RequiredItem]


class SellItem(StrictModel):
    id: ItemId
    count: int = pydantic.Field(ge=1)
    scheme_id: int


class TradingSellToTraderAction(ActionBase[Literal["TradingConfirm"]]):
    type: Literal["sell_to_trader"]
    trader_id: str = pydantic.Field(alias="tid")
    items: list[SellItem]
    price: int


class RagFairOfferBuy(StrictModel):
    id: str
    count: int
    items: list[RequiredItem]


class RagFairBuyOfferAction(ActionBase[Literal["RagFairBuyOffer"]]):
    offers: list[RagFairOfferBuy]


class AddToWishlistAction(ActionBase[Literal["AddToWishList"]]):
    template_id: ItemTemplateId = pydantic.Field(alias="templateId")


class RemoveFromWishlistAction(ActionBase[Literal["RemoveFromWishList"]]):
    template_id: ItemTemplateId = pydantic.Field(alias="templateId")


class SplitAction(ActionBase[Literal["Split"]]):
    split_item: ItemId = pydantic.Field(alias="splitItem")
    count: int = pydantic.Field(ge=1)
    container: Location
    new_item: ItemId = pydantic.Field(alias="newItem")


class MergeAction(ActionBase[Literal["Merge"]]):
    item: ItemId
    with_: ItemId = pydantic.Field(alias="with")


class TransferAction(ActionBase[Literal["Transfer"]]):
    item: ItemId
    with_: ItemId = pydantic.Field(alias="with")
    count: int = pydantic.Field(ge=1)


class RemoveAction(ActionBase[Literal["Remove"]]):
    item: ItemId


class FoldAction(ActionBase[Literal["Fold"]]):
    item: ItemId
    value: bool


class InsureAction(ActionBase[Literal["Insure"]]):
    items: list[ItemId]
    trader_id: str = pydantic.Field(alias="tid")


class EatAction(ActionBase[Literal["Eat"]]):
    item: ItemId
    count: int
    time: DatetimeInt


class BindAction(ActionBase[Literal["Bind"]]):
    item: ItemId
    index: str


class UnbindAction(ActionBase[Literal["Unbind"]]):
    item: ItemId
    index: str


class QuestAcceptAction(ActionBase[Literal["QuestAccept"]]):
    quest_id: QuestId = pydantic.Field(alias="qid")


def discriminator(
    v: Any,  # noqa: ANN401
) -> str:
    if isinstance(v, dict):
        action, action_type = v.get("Action", v.get("action")), v.get("type")
    else:
        action, action_type = v.action, getattr(v, "type", None)

    if action_type:
        return f"{action}-{action_type}"  # pragma: no cover
    return action


AnyAction = Annotated[
    Annotated[MoveAction, Tag("Move")]
    | Annotated[AddToWishlistAction, Tag("AddToWishList")]
    | Annotated[RemoveFromWishlistAction, Tag("RemoveFromWishList")]
    | Annotated[SplitAction, Tag("Split")]
    | Annotated[MergeAction, Tag("Merge")]
    | Annotated[TransferAction, Tag("Transfer")]
    | Annotated[RemoveAction, Tag("Remove")]
    | Annotated[FoldAction, Tag("Fold")]
    | Annotated[EatAction, Tag("Eat")]
    | Annotated[BindAction, Tag("Bind")]
    | Annotated[UnbindAction, Tag("Unbind")]
    | Annotated[InsureAction, Tag("Insure")]
    | Annotated[TradingConfirmAction, Tag("TradingConfirm-buy_from_trader")]
    | Annotated[TradingSellToTraderAction, Tag("TradingConfirm-sell_to_trader")]
    | Annotated[QuestAcceptAction, Tag("QuestAccept")]
    | Annotated[ExamineAction, Tag("Examine")]
    | Annotated[ReadEncyclopediaAction, Tag("ReadEncyclopedia")]
    | Annotated[HideoutUpgradeAction, Tag("HideoutUpgrade")]
    | Annotated[HideoutUpgradeCompleteAction, Tag("HideoutUpgradeComplete")]
    | Annotated[HideoutPutItemsAction, Tag("HideoutPutItemsInAreaSlots")]
    | Annotated[HideoutToggleAreaAction, Tag("HideoutToggleArea")]
    | Annotated[
        RecordShootingRangePointsAction,
        Tag("RecordShootingRangePoints"),
    ]
    | Annotated[RagFairBuyOfferAction, Tag("RagFairBuyOffer")],
    Discriminator(discriminator),
]


class ItemChanges(StrictModel):
    new: set[Item] = pydantic.Field(default_factory=set)
    changed: set[Item] = pydantic.Field(default_factory=set, alias="change")
    deleted: set[Item] = pydantic.Field(default_factory=set, alias="del")


class ProfileChanges(CamelCaseModel):
    id: str = pydantic.Field(alias="_id")
    experience: int = 0
    skills: Skills
    health: ProfileHealth
    items: ItemChanges = pydantic.Field(
        default_factory=lambda: ItemChanges(),
    )
    builds: list[None] = pydantic.Field(default_factory=list)
    equipment_builds: list[None] = pydantic.Field(default_factory=list)
    improvements: dict[str, None] = pydantic.Field(default_factory=dict)
    production: dict[str, Any] = pydantic.Field(default_factory=dict)
    quests: list[Quest] = pydantic.Field(default_factory=list)
    rag_fair_offers: list[None] = pydantic.Field(default_factory=list)
    recipe_unlocked: dict[str, bool] = pydantic.Field(default_factory=dict)
    trader_relations: dict[str, TraderInfo] = pydantic.Field(default_factory=dict)
    weapon_builds: list[None] = pydantic.Field(default_factory=list)


class WarningModel(StrictModel):
    index: int
    errmsg: str
    code: str | None = None
    data: Any


@dataclasses.dataclass(slots=True, frozen=True)
class ItemsMovingResult:
    changes: Mapping[str, ProfileChanges]
    warnings: Sequence[WarningModel]
