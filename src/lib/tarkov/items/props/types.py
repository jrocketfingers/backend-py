import pydantic
from pydantic import ConfigDict

from lib.models import CamelCaseModel, PascalCaseModel, StrictModel, WithPrefix
from lib.tarkov.types import ItemTemplateId

SlotName = str | int


class StatValue(StrictModel):
    value: int


class SlotFilter(PascalCaseModel):
    filter: list[ItemTemplateId]
    excluded_filter: list[ItemTemplateId] = pydantic.Field(default_factory=list)
    shift: int | None = None
    animation_index: int | None = None
    max_stack_count: int | None = None

    locked: bool | None = pydantic.Field(None, alias="locked")
    plate: ItemTemplateId | None = None
    armor_colliders: list[str] | None = pydantic.Field(None, alias="armorColliders")
    armor_plate_colliders: list[str] | None = pydantic.Field(
        None,
        alias="armorPlateColliders",
    )
    blunt_damage_from_soft_armor: bool | None = pydantic.Field(
        None,
        alias="bluntDamageReduceFromSoftArmor",
    )


class SlotFilters(StrictModel):
    filters: list[SlotFilter]


class Slot(StrictModel):
    model_config = ConfigDict(alias_generator=WithPrefix("_"))

    id: str
    merge_slot_with_children: bool = pydantic.Field(alias="_mergeSlotWithChildren")
    name: str
    parent: ItemTemplateId
    props: SlotFilters
    proto: str
    required: bool


class StackSlot(StrictModel):
    model_config = ConfigDict(alias_generator=WithPrefix("_"))
    id: str
    max_count: int
    name: str
    parent: str
    props: SlotFilters
    proto: str


class ContainerSlotProps(CamelCaseModel):
    cells_width: int = pydantic.Field(alias="cellsH")
    cells_height: int = pydantic.Field(alias="cellsV")
    filters: list[SlotFilter]
    is_sorting_table: bool
    max_count: int
    max_weight: int
    min_count: int


class ContainerGrid(StrictModel):
    model_config = ConfigDict(alias_generator=WithPrefix("_"))
    id: str
    name: SlotName
    parent: str
    props: ContainerSlotProps
    proto: str


class EffectsMixin(StrictModel):
    effects_health: dict[str, StatValue] | list[None] = pydantic.Field(
        alias="effects_health",
    )
    effects_damage: dict[str, dict[str, int]] | list[None] = pydantic.Field(
        alias="effects_damage",
    )
