from typing import ClassVar

import pydantic

from lib.models import Vector
from lib.tarkov.items.props.base import CompoundItemProps, SearchableItemProps
from lib.tarkov.items.props.types import EffectsMixin
from lib.tarkov.locations.models import Prefab
from lib.tarkov.types import ItemTemplateId


class BackpackProps(SearchableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448e53e4bdc2d60728b4567")

    grid_layout_name: str
    mouse_penalty: float = pydantic.Field(alias="mousePenalty")
    speed_penalty_percent: float = pydantic.Field(alias="speedPenaltyPercent")
    lean_weapon_against_body: bool
    weapon_ergonomic_penalty: float = pydantic.Field(alias="weaponErgonomicPenalty")


class EquipmentProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("543be5f84bdc2dd4348b456a")

    blocks_earpiece: bool
    blocks_eyewear: bool
    blocks_face_cover: bool
    blocks_headwear: bool


class ArmorEquipmentProps(EquipmentProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57bef4c42459772e8d35a53b")

    armor_material: str
    armor_type: str
    blindness_protection: float
    blunt_throughput: float
    deaf_strength: str
    face_shield_component: bool
    face_shield_mask: str
    has_hinge: bool
    indestructibility: float
    material_type: str
    ricochet_params: Vector[float]
    mouse_penalty: float = pydantic.Field(alias="mousePenalty")
    speed_penalty_percent: float = pydantic.Field(alias="speedPenaltyPercent")
    weapon_ergonomic_penalty: float = pydantic.Field(alias="weaponErgonomicPenalty")

    durability: int
    max_durability: int
    armor_class: str = pydantic.Field(alias="armorClass")
    armor_colliders: list[str] = pydantic.Field(alias="armorColliders")
    armor_plate_colliders: list[str] = pydantic.Field(alias="armorPlateColliders")


class ArmorPlateProps(ArmorEquipmentProps, EffectsMixin):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("644120aa86ffbe10ee032b6f")

    lower_75_prefab: Prefab


class BuiltInInsertsProps(ArmorPlateProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("65649eb40bf0ed77b8044453")


class ArmorProps(ArmorEquipmentProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448e54d4bdc2dcc718b4568")


class VestProps(SearchableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448e5284bdc2dcb718b4567")

    armor_material: str
    armor_type: str
    armor_zone: list[str] = pydantic.Field(alias="armorZone")
    armor_class: int = pydantic.Field(alias="armorClass")
    blunt_throughput: float

    rig_layout_name: str
    mouse_penalty: float = pydantic.Field(alias="mousePenalty")
    speed_penalty_percent: float = pydantic.Field(alias="speedPenaltyPercent")
    weapon_ergonomic_penalty: float = pydantic.Field(alias="weaponErgonomicPenalty")


class VisorProps(ArmorEquipmentProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448e5724bdc2ddf718b4568")


class Headwear(ArmorEquipmentProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5a341c4086f77401f2541505")


class HeadphonesProps(EquipmentProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5645bcb74bdc2ded0b8b4578")

    ambient_volume: int
    compressor_attack: int
    compressor_gain: int
    compressor_release: int
    compressor_treshold: int
    compressor_volume: int
    cutoff_freq: int
    distortion: float
    dry_volume: int
    high_frequencies_gain: float
    resonance: float
    reverb_volume: int
    rolloff_multiplier: float


class ArmBandProps(EquipmentProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5b3f15d486f77432d0509248")


class FaceCoverProps(ArmorEquipmentProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5a341c4686f77469e155819e")


AnyEquipmentProps = (
    BackpackProps
    | EquipmentProps
    | ArmorEquipmentProps
    | ArmorPlateProps
    | BuiltInInsertsProps
    | ArmorProps
    | VestProps
    | VisorProps
    | Headwear
    | HeadphonesProps
    | ArmBandProps
    | FaceCoverProps
)
