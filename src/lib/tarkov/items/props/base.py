from typing import ClassVar

from pydantic import ConfigDict

from lib.models import PascalCaseModel
from lib.tarkov.items.props.types import ContainerGrid, Slot
from lib.tarkov.items.types import Color
from lib.tarkov.locations.models import Prefab
from lib.tarkov.types import ItemTemplateId


class BaseProps(PascalCaseModel):
    model_config = ConfigDict(extra="forbid")

    id: ClassVar[ItemTemplateId] = ItemTemplateId("54009119af1c881c07000029")

    animation_variants_number: int
    background_color: Color
    can_require_on_ragfair: bool
    can_sell_on_ragfair: bool
    conflicting_items: list[ItemTemplateId]
    description: str
    discard_limit: int
    discarding_block: bool
    drop_sound_type: str
    examine_experience: int
    examine_time: int
    examined_by_default: bool
    extra_size_down: int
    extra_size_force_add: bool
    extra_size_left: int
    extra_size_right: int
    extra_size_up: int
    height: int
    hide_entrails: bool
    insurance_disabled: bool
    is_always_available_for_insurance: bool
    is_lockedafter_equip: bool
    is_special_slot_only: bool
    is_unbuyable: bool
    is_unremovable: bool
    is_unsaleable: bool
    is_undiscardable: bool
    is_ungivable: bool
    item_sound: str
    loot_experience: int
    merges_with_children: bool
    name: str
    not_shown_in_slot: bool
    prefab: Prefab
    quest_item: bool
    quest_stash_max_count: int
    rag_fair_commission_modifier: float
    repair_cost: int
    repair_speed: int
    short_name: str
    stack_max_size: int
    stack_objects_count: int
    unlootable: bool
    unlootable_from_side: list[str]
    unlootable_from_slot: str
    use_prefab: Prefab
    weight: float
    width: int


class CompoundItemProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("566162e44bdc2d3f298b4573")

    can_put_into_during_the_raid: bool
    cant_remove_from_slots_during_raid: list[str]
    grids: list[ContainerGrid]
    slots: list[Slot]


class StackableItemProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5661632d4bdc2d903d8b456b")

    stack_max_random: int
    stack_min_random: int


class SearchableItemProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("566168634bdc2d144c8b456c")

    blocks_armor_vest: bool
    search_sound: str
