from typing import ClassVar

from lib.tarkov.items.props.base import BaseProps
from lib.tarkov.types import ItemTemplateId


class BarterItemProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448eb774bdc2d0a728b4567")

    resource: int
    max_resource: int


class BatteryProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864ee62459775490116fc1")


class ElectronicsProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864a66245977548f04a81f")


class LubricantProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864e4c24597754843f8723")


class JewelryProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864a3d24597754843f8721")


class OtherProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("590c745b86f7743cc433c5f2")

    dog_tag_qualities: bool


class BuildingMaterialProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864ada245977548638de91")


class HousehouldGoodsProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864c322459775490116fbf")


class ToolProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864bb7245977548b3b66c2")


class MedicalSuppliesProps(BarterItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("57864c8c245977548867e7f1")


class FuelProps(LubricantProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5d650c3e815116009f6201d2")


AnyBarterProps = (
    BarterItemProps
    | BatteryProps
    | ElectronicsProps
    | LubricantProps
    | JewelryProps
    | OtherProps
    | BuildingMaterialProps
    | HousehouldGoodsProps
    | ToolProps
    | MedicalSuppliesProps
    | FuelProps
)
