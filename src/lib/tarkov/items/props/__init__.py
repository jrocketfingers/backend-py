import typing

from lib.tarkov.items.props.barter import AnyBarterProps
from lib.tarkov.items.props.base import (
    BaseProps,
    CompoundItemProps,
    SearchableItemProps,
    StackableItemProps,
)
from lib.tarkov.items.props.container import (
    HideoutAndContainerProps,
    InventoryProps,
    LockableContainer,
    LootContainerProps,
    MobContainerProps,
    PocketProps,
    RandomLootContainer,
    SimpleContainerProps,
    SortingTableProps,
    StashProps,
    StationaryContainerProps,
)
from lib.tarkov.items.props.equipment import AnyEquipmentProps
from lib.tarkov.items.props.item import (
    AmmoBoxProps,
    AmmoProps,
    CompassProps,
    CultistAmuletProps,
    DrinkProps,
    DrugsProps,
    FoodDrinkProps,
    FoodProps,
    InfoProps,
    KeycardProps,
    KeyMechanicalProps,
    KeyProps,
    MapProps,
    MedicalProps,
    MedKitProps,
    MedsProps,
    MoneyProps,
    PortableRangefinderProps,
    RadioTransmitterProps,
    RepairKitProps,
    SpecItemProps,
    StimulatorProps,
)
from lib.tarkov.items.props.mods import AnyModProps
from lib.tarkov.items.props.weapons import AnyWeaponProps

AnyProps = (
    AnyBarterProps
    | AnyModProps
    | AnyEquipmentProps
    | AnyWeaponProps
    | BaseProps
    | MedsProps
    | KeyProps
    | FoodDrinkProps
    | SpecItemProps
    | CultistAmuletProps
    | CompoundItemProps
    | StackableItemProps
    | SearchableItemProps
    | StashProps
    | StationaryContainerProps
    | MapProps
    | AmmoBoxProps
    | KeyMechanicalProps
    | FoodProps
    | DrinkProps
    | MoneyProps
    | MobContainerProps
    | MedicalProps
    | DrugsProps
    | MedKitProps
    | AmmoProps
    | PocketProps
    | InventoryProps
    | LockableContainer
    | SimpleContainerProps
    | LootContainerProps
    | InfoProps
    | RepairKitProps
    | StimulatorProps
    | KeycardProps
    | CompassProps
    | SortingTableProps
    | PortableRangefinderProps
    | RadioTransmitterProps
    | RandomLootContainer
    | HideoutAndContainerProps
)
props_map = {prop.id: prop for prop in typing.get_args(AnyProps)}
assert len(props_map) == len(typing.get_args(AnyProps))  # noqa: S101


props_map[""] = BaseProps
