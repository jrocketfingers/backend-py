from lib.tarkov.items.models import Item


def is_folded(item: Item) -> bool:
    return item.upd.get("Foldable", {"Folded": False})["Folded"]
