import enum


class ErrorCode(enum.IntEnum):
    none = 0
    player_profile_not_found = 505001
