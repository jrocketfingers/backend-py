import enum
from typing import Literal

import pydantic

from lib.models import CamelCaseModel, DatetimeInt
from lib.tarkov.chat import ChatMember
from lib.tarkov.items.models import Item
from lib.utils._time import utc_now_seconds


class MessageSystemData(CamelCaseModel):
    date: str | None = None
    time: str | None = None
    location: str | None = None
    buyer_nickname: str | None = None
    sold_item: str | None = None
    item_count: int | None = None


class MessageType(enum.IntEnum):
    USER_MESSAGE = 1
    NPC_TRADER = 2
    AUCTION_MESSAGE = 3
    FLEAMARKET_MESSAGE = 4
    ADMIN_MESSAGE = 5
    GROUP_CHAT_MESSAGE = 6
    SYSTEM_MESSAGE = 7
    INSURANCE_RETURN = 8
    GLOBAL_CHAT = 9
    QUEST_START = 10
    QUEST_FAIL = 11
    QUEST_SUCCESS = 12
    MESSAGE_WITH_ITEMS = 13
    INITIAL_SUPPORT = 14
    BTR_ITEMS_DELIVERY = 15


class MessageItems(CamelCaseModel):
    stash: str
    data: list[Item]


class Message(CamelCaseModel):
    id: str = pydantic.Field(alias="_id")
    uid: str
    type: MessageType
    dt: DatetimeInt
    utc_date_time: DatetimeInt | None = pydantic.Field(alias="UtcDateTime")
    template_id: str | None = None
    text: str | None = None
    has_rewards: bool | None = None
    reward_collected: bool
    items: MessageItems
    system_data: MessageSystemData
    # ProfileChangeEvents


class NotificationType(enum.StrEnum):
    RAGFAIR_OFFER_SOLD = "RagfairOfferSold"
    RAGFAIR_RATING_CHANGE = "RagfairRatingChange"
    NEW_MESSAGE = "new_message"
    PING = "ping"
    TRADER_SUPPLY = "TraderSupply"
    TRADER_STANDING = "TraderStanding"
    UNLOCK_TRADER = "UnlockTrader"
    FRIEND_LIST_NEW_REQUEST = "friendListNewRequest"
    FRIEND_LIST_REQUEST_CANCEL = "friendListRequestCancel"
    FRIEND_LIST_REQUEST_DECLINE = "friendListRequestDecline"
    FRIEND_LIST_REQUEST_ACCEPT = "friendListRequestAccept"
    FRIEND_LIST_REMOVE = "youAreRemovedFromFriendList"


class Notification(CamelCaseModel):
    type: NotificationType
    time: DatetimeInt = pydantic.Field(default_factory=utc_now_seconds)


class _ChatMemberNotification(Notification):
    profile: ChatMember


class FleaOfferSoldNotification(Notification):
    type: Literal[NotificationType.RAGFAIR_OFFER_SOLD]
    offer_id: str
    count: int
    handbook_id: str


class FriendListNewRequestNotification(_ChatMemberNotification):
    type: Literal[NotificationType.FRIEND_LIST_NEW_REQUEST]

    id: str = pydantic.Field(alias="_id")


class FriendListRequestCancelledNotification(_ChatMemberNotification):
    type: Literal[NotificationType.FRIEND_LIST_REQUEST_CANCEL]


class FriendListRequestAcceptNotification(_ChatMemberNotification):
    type: Literal[NotificationType.FRIEND_LIST_REQUEST_ACCEPT]


class FriendListRemovetNotification(_ChatMemberNotification):
    type: Literal[NotificationType.FRIEND_LIST_REMOVE]


AnyNotification = (
    FriendListRemovetNotification
    | FriendListRequestAcceptNotification
    | FriendListNewRequestNotification
    | FriendListRequestCancelledNotification
    | FleaOfferSoldNotification
    | Notification
)
