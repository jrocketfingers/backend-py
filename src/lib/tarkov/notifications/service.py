import logging
from collections.abc import AsyncIterator

from pydantic import TypeAdapter

from .abc import NotificationBackend
from .models import AnyNotification


class NotificationService:
    def __init__(self, backend: NotificationBackend) -> None:
        self._backend = backend
        self._adapter = TypeAdapter(AnyNotification)

    async def send(
        self,
        notification: AnyNotification,
        profile_id: str,
    ) -> None:
        await self._backend.send(
            channel_id=profile_id,
            message=notification.model_dump_json(),
        )

    async def subscribe(self, profile_id: str) -> AsyncIterator[AnyNotification]:
        async with self._backend.subscribe(channel_id=profile_id) as pubsub:
            async for message in pubsub:
                logging.debug("Notification message: %s", message)
                yield self._adapter.validate_json(message, strict=False)
