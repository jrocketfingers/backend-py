import asyncio
import contextlib
import dataclasses
import time
from collections.abc import AsyncIterable, AsyncIterator
from typing import final

from lib.tarkov.config import NotificationsConfig

from .abc import NotificationBackend


@dataclasses.dataclass(slots=True, kw_only=True)
class _Channel:
    queue: asyncio.Queue[bytes]
    disconnected_at: float | None = None

    async def iterate(self) -> AsyncIterator[bytes]:
        while True:
            yield await self.queue.get()


@final
class InMemoryBackend(NotificationBackend):
    def __init__(self, config: NotificationsConfig.Broker) -> None:
        self._channels: dict[str, _Channel] = {}
        self._expiration_time = config.expiration_time
        self._cleaned_up_at = time.monotonic()

    def _get_channel(self, channel_id: str) -> _Channel:
        if channel_id not in self._channels:
            self._channels[channel_id] = _Channel(
                queue=asyncio.Queue(),
            )
        return self._channels[channel_id]

    def _cleanup(self) -> None:  # pragma: no cover
        expires_at = self._cleaned_up_at + self._expiration_time
        if expires_at > time.monotonic():
            return

        self._channels = {
            key: channel
            for key, channel in self._channels.items()
            if channel.disconnected_at and channel.disconnected_at > expires_at
        }
        self._cleaned_up_at = time.monotonic()

    @contextlib.asynccontextmanager
    async def subscribe(self, channel_id: str) -> AsyncIterator[AsyncIterable[bytes]]:
        self._cleanup()

        channel = self._get_channel(channel_id)
        channel.disconnected_at = None
        try:
            yield channel.iterate()
        finally:
            channel.disconnected_at = time.monotonic()

    async def send(self, channel_id: str, message: str) -> None:
        channel = self._get_channel(channel_id)
        await channel.queue.put(message.encode())
