from collections.abc import AsyncIterable
from contextlib import AbstractAsyncContextManager
from typing import Protocol


class NotificationBackend(Protocol):
    def subscribe(
        self,
        channel_id: str,
    ) -> AbstractAsyncContextManager[AsyncIterable[bytes]]: ...

    async def send(self, channel_id: str, message: str) -> None: ...
