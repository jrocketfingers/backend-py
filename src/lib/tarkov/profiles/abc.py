from contextlib import AbstractAsyncContextManager
from typing import Protocol, runtime_checkable

from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import AccountProfile
from lib.tarkov.profiles.dto import ProfileContext


@runtime_checkable
class ProfileStorage(Protocol):
    def profile(
        self,
        profile_id: str,
    ) -> AbstractAsyncContextManager[ProfileContext]: ...

    async def readonly(self, profile_id: str) -> ProfileContext: ...

    async def logout(self, profile_id: str) -> None: ...

    async def initialize_profile(
        self,
        profile: AccountProfile,
        dto: ProfileCreateDTO,
    ) -> None: ...
