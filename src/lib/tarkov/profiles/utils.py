from lib.tarkov.inventory import regenerate_item_ids
from lib.tarkov.profiles.models import Inventory


def regenerate_inventory_ids(inventory: Inventory) -> None:
    root_items = {
        item.id: item
        for item in inventory.items
        if item.id
        in (
            inventory.equipment,
            inventory.quest_raid_items,
            inventory.quest_stash_items,
            inventory.stash,
            inventory.sorting_table,
        )
    }
    regenerate_item_ids(*inventory.items)
    inventory.equipment = root_items[inventory.equipment].id
    inventory.quest_raid_items = root_items[inventory.quest_raid_items].id
    inventory.quest_stash_items = root_items[inventory.quest_stash_items].id
    inventory.stash = root_items[inventory.stash].id
    inventory.sorting_table = root_items[inventory.sorting_table].id
