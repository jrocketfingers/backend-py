from pathlib import Path
from typing import Protocol, final

import aiofiles

from lib.tarkov.profiles.models import ProfileStoragePath
from lib.utils import safe_rename


class ProfileStorageBackend(Protocol):
    async def read(self, profile_id: str) -> str: ...

    async def save(self, profile_id: str, content: str) -> None: ...


@final
class FileSystemStorageBackend(ProfileStorageBackend):
    def __init__(
        self,
        storage_path: ProfileStoragePath,
        filename: str = "character.json",
    ) -> None:
        self._storage_path = storage_path
        self._filename = filename

    def _character_path(self, profile_id: str) -> Path:
        return self._storage_path.joinpath(
            profile_id,
            self._filename,
        )

    async def read(self, profile_id: str) -> str:  # pragma: no cover
        async with aiofiles.open(self._character_path(profile_id=profile_id)) as f:
            return await f.read()

    async def save(self, profile_id: str, content: str) -> None:
        path = self._character_path(profile_id=profile_id)
        tmp_path = path.with_suffix(f"{path.suffix}.tmp")

        path.parent.mkdir(parents=True, exist_ok=True)

        with safe_rename(tmp_path, path):
            async with aiofiles.open(tmp_path, "w", encoding="utf8") as f:
                await f.write(content)
