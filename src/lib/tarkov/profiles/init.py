import uuid
from typing import Final

from app.core.domain.items.services import CustomizationDB
from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import AccountProfile
from lib.tarkov.profiles.models import (
    Profile,
    ProfileStorageModel,
    ProfileTemplatesPath,
    ScavProfile,
    TraderInfo,
)
from lib.tarkov.profiles.templates import ProfileTemplates
from lib.tarkov.profiles.utils import regenerate_inventory_ids
from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import Traders


class ProfileInitializer:
    def __init__(  # noqa: PLR0913
        self,
        traders: Traders,
        templates_path: ProfileTemplatesPath,
        scav_template: ScavProfile,
        customization_db: CustomizationDB,
        templates: ProfileTemplates,
    ) -> None:
        self._templates = templates
        self._traders = traders
        self._templates_path = templates_path
        self._scav_template: Final = scav_template
        self._customization_db = customization_db

    async def init(
        self,
        profile: AccountProfile,
        dto: ProfileCreateDTO,
    ) -> ProfileStorageModel:
        template = self._templates.templates[profile.account.game_edition]

        pmc = (
            template.characters.usec if dto.side == "Usec" else template.characters.bear
        )
        pmc = pmc.model_copy(deep=True)

        regenerate_inventory_ids(pmc.inventory)

        pmc.id = profile.profile_id
        pmc.account_id = profile.account_id
        pmc.savage = f"scav{pmc.id}"

        pmc.info.game_version = profile.account.game_edition
        pmc.info.side = dto.side
        pmc.info.nickname = dto.name
        pmc.info.lower_nickname = dto.name.lower()
        pmc.info.voice = self._customization_db.customization[dto.voice_id].name
        pmc.customization.head = dto.head_id

        self._init_trader_standing(pmc, template.traders.initial_standing)

        scav = self._scav_template.model_copy(deep=True)
        regenerate_inventory_ids(scav.inventory)
        scav.id = pmc.savage
        scav.account_id = profile.account_id
        scav.info.nickname = str(uuid.uuid4())
        scav.info.lower_nickname = scav.info.nickname.lower()

        return ProfileStorageModel(
            pmc=pmc,
            scav=scav,
        )

    def _init_trader_standing(self, pmc: Profile, initial_standing: float) -> None:
        for trader in self._traders:
            standing = (
                initial_standing
                if trader.base.id != TraderEnum.lightkeeper
                else max(initial_standing, 0.01)
            )
            pmc.traders_info[trader.base.id] = TraderInfo(
                disabled=False,
                standing=standing,
                unlocked=trader.base.unlocked_by_default,
            )
