from __future__ import annotations

from collections.abc import Mapping

from lib.models import StrictModel
from lib.tarkov.paths import editions_dir
from lib.tarkov.profiles.models import Profile
from lib.utils import read_pydantic_json


class ProfileTemplateCharacters(StrictModel):
    usec: Profile
    bear: Profile


class ProfileTemplateTraders(StrictModel):
    initial_standing: float


class ProfileTemplate(StrictModel):
    characters: ProfileTemplateCharacters
    traders: ProfileTemplateTraders


async def create_profile_templates() -> ProfileTemplates:
    templates = {}
    for path in editions_dir.glob("*.json"):
        templates[path.stem] = await read_pydantic_json(
            path,
            ProfileTemplate,
        )
    return ProfileTemplates(templates=templates)


class ProfileTemplates:
    def __init__(self, templates: Mapping[str, ProfileTemplate]) -> None:
        self.templates = templates
