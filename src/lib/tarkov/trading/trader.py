from collections.abc import Iterator, Sequence

from pydantic import BaseModel

from lib.tarkov.inventory import Inventory
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.trading.models import TraderAssortModel, TraderBase, TraderQuestAssort


class TraderModel(BaseModel):
    base: TraderBase
    assort: TraderAssortModel
    quest_assort: TraderQuestAssort


class Traders:
    def __init__(self, traders: Sequence[TraderModel], item_db: ItemDB) -> None:
        self._traders = {trader.base.id: trader for trader in traders}
        self._item_db = item_db

    def inventory(self, trader: TraderModel) -> Inventory:
        return Inventory.from_sequence(
            trader.assort.items,
            templates=self._item_db.items,
        )

    def get(self, trader_id: str) -> TraderModel | None:
        return self._traders.get(trader_id)

    def __iter__(self) -> Iterator[TraderModel]:
        return iter(self._traders.values())

    def __len__(self) -> int:
        return len(self._traders)
