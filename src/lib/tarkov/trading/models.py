import enum
from typing import Annotated, Literal

import pydantic
from pydantic import BeforeValidator

from lib.models import StrictModel, round_validator
from lib.tarkov.customization import PMCSide
from lib.tarkov.items.models import Item
from lib.tarkov.types import ItemId, ItemTemplateId, QuestId


class CurrencyIds(enum.Enum):
    RUB = ItemTemplateId("5449016a4bdc2d6f028b456f")
    EUR = ItemTemplateId("569668774bdc2da2298b4568")
    USD = ItemTemplateId("5696686a4bdc2da3298b456a")


class Currency(enum.StrEnum):
    RUB = "RUB"
    EUR = "EUR"
    USD = "USD"


class TraderInsurance(StrictModel):
    availability: bool
    excluded_category: list[str]
    max_storage_time: int
    min_payment: int
    min_return_hour: int
    max_return_hour: int


class TraderRepair(StrictModel):
    availability: bool
    # Seems like currency is None if availability is False, fucking BSG
    currency: str | None = None
    currency_coefficient: int
    excluded_category: list[str]
    excluded_id_list: list[str]
    price_rate: Literal[0] | None = None
    quality: float  # Maybe decimal?


class TraderItemsList(StrictModel):
    category: list[str | None]
    id_list: list[str]


class LoyaltyLevel(StrictModel):
    buy_price_coef: int
    exchange_price_coef: int
    heal_price_coef: int
    insurance_price_coef: int
    min_level: int = pydantic.Field(alias="minLevel")
    min_sales_sum: int = pydantic.Field(alias="minSalesSum")
    min_standing: float = pydantic.Field(alias="minStanding")
    repair_price_coef: int


class TraderBase(StrictModel):
    id: str = pydantic.Field(alias="_id")

    available_in_raid: bool = pydantic.Field(alias="availableInRaid")
    avatar: str
    balance_dol: int
    balance_eur: int
    balance_rub: int
    buyer_up: bool
    currency: Currency
    customization_seller: bool
    discount: float
    discount_end: float
    grid_height: int = pydantic.Field(alias="gridHeight")
    insurance: TraderInsurance
    items_buy: TraderItemsList
    items_buy_prohibited: TraderItemsList
    location: str
    loyalty_levels: list[LoyaltyLevel] = pydantic.Field(alias="loyaltyLevels")
    medic: bool
    name: str
    next_resupply: int = pydantic.Field(alias="nextResupply")
    nickname: str
    repair: TraderRepair
    sell_category: list[str]
    sell_modifier_for_prohibited_items: int
    surname: str
    unlocked_by_default: bool = pydantic.Field(alias="unlockedByDefault")


class TraderBarter(StrictModel):
    template_id: ItemTemplateId = pydantic.Field(alias="_tpl")
    # What the actual fuck is going on? Counts are floats now?
    count: Annotated[int, BeforeValidator(round_validator)]
    level: int | None = None
    side: Literal[PMCSide, "Any"] = "Any"
    only_functional: bool | None = pydantic.Field(None, alias="onlyFunctional")


BarterScheme = dict[ItemId, list[list[TraderBarter]]]


class TraderAssortModel(StrictModel):
    items: list[Item]
    barter_scheme: BarterScheme
    loyal_level_items: dict[ItemId, int]


class TraderQuestAssort(StrictModel):
    started: dict[ItemId, QuestId]
    success: dict[ItemId, QuestId]
    fail: dict[ItemId, QuestId]


class TraderEnum(enum.StrEnum):
    prapor = "54cb50c76803fa8b248b4571"
    therapist = "54cb57776803fa99248b456e"
    lightkeeper = "626148251ed3bb5bcc5bd9ed"
