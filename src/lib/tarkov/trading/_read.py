import asyncio
import warnings
from collections.abc import Sequence
from pathlib import Path, PurePath

from lib.tarkov.paths import static_files_dir, static_url
from lib.utils import read_pydantic_json

from .models import TraderAssortModel, TraderBase, TraderQuestAssort
from .trader import TraderModel


async def _read_trader(path: Path) -> TraderModel:
    base = await read_pydantic_json(path.joinpath("base.json"), TraderBase)
    assort = await read_pydantic_json(path.joinpath("assort.json"), TraderAssortModel)
    quest_assort = await read_pydantic_json(
        path.joinpath("questassort.json"),
        TraderQuestAssort,
    )

    avatar_path = Path(
        static_files_dir,
        "trader",
        "avatar",
        PurePath(base.avatar).with_suffix(".png").name,
    )

    if not avatar_path.exists():
        warnings.warn(
            f"Avatar for trader {base.id} at path {avatar_path} does not exist",
            stacklevel=2,
        )

    base.avatar = PurePath(
        static_url,
        avatar_path.relative_to(static_files_dir),
    ).as_posix()

    return TraderModel(
        base=base,
        assort=assort,
        quest_assort=quest_assort,
    )


async def read_trader_data(path: Path) -> Sequence[TraderModel]:
    tasks = (_read_trader(p) for p in path.iterdir() if p.is_dir())
    return await asyncio.gather(*tasks)
