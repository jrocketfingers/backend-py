from typing import Final

from pydantic import TypeAdapter

from lib.tarkov.achievements.models import Achievement, AchievementsFile
from lib.tarkov.paths import achievements_path
from lib.utils import read_pydantic_json

_adapter = TypeAdapter(AchievementsFile)


async def read_achievements_db() -> "AchievementDB":
    return AchievementDB(
        achievements=await read_pydantic_json(achievements_path, _adapter),
    )


class AchievementDB:
    def __init__(self, achievements: list[Achievement]) -> None:
        self.achievements: Final = achievements
