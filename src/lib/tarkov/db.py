import dataclasses
from pathlib import Path
from typing import Any, NewType

import pydantic
from pydantic import ConfigDict, TypeAdapter

from lib.models import CamelCaseModel, PascalCaseModel, StrictModel, WithPrefix
from lib.tarkov.items.models import Item
from lib.tarkov.types import ItemTemplateId
from lib.utils import read_pydantic_json

AssetsPath = NewType("AssetsPath", Path)


class BotWeaponScattering(PascalCaseModel):
    name: str
    priority_scatter_100meter: float
    priority_scatter_10meter: float
    priority_scatter_1meter: float


class ItemPreset(StrictModel):
    model_config = ConfigDict(alias_generator=WithPrefix("_"))

    id: str
    change_weapon_name: bool = pydantic.Field(alias="_changeWeaponName")
    encyclopedia: ItemTemplateId | None = None
    items: list[Item]
    name: str
    parent: str
    type: str


class BotPreset(PascalCaseModel):
    bot_difficulty: str
    coef_if_move: float = pydantic.Field(alias="COEF_IF_MOVE")
    first_contact_add_sec: float = pydantic.Field(alias="FIRST_CONTACT_ADD_SEC")
    hearing_sense: float
    max_aiming_upgrade_by_time: float = pydantic.Field(
        alias="MAX_AIMING_UPGRADE_BY_TIME",
    )
    role: str
    scattering_dist_modifier: float = pydantic.Field(alias="SCATTERING_DIST_MODIF")
    scattering_per_meter: float
    use_this: bool
    visible_angle: float
    visible_distance: float


class ArmorMaterial(PascalCaseModel):
    destructibility: float
    explosion_destructibility: float
    max_repair_degradation: float
    max_repair_kit_degradation: float
    min_repair_degradation: float
    min_repair_kit_degradation: float


class BallisticConfig(PascalCaseModel):
    global_damage_degradation_coefficient: float


class ExpConfig(StrictModel):
    class Heal(CamelCaseModel):
        exp_for_energy: int
        exp_for_heal: int
        exp_for_hydration: int

    class Kill(CamelCaseModel):
        blood_loss_to_litre: float
        bot_exp_on_damage_all_health: int
        bot_head_shot_mult: float
        combo: list[Any]
        long_shot_distance: int
        pmc_exp_on_damage_all_health: int
        pmc_head_shot_mult: float
        victim_bot_level_exp: int
        victim_level_exp: int

    class Level(StrictModel):
        clan_level: int
        exp_table: list[Any]
        mastering1: int
        mastering2: int
        savage_level: int
        trade_level: int

    class MatchEnd(StrictModel):
        readme: str = pydantic.Field(alias="README")
        killed_mult: float = pydantic.Field(alias="killedMult")
        mia_mult: float = pydantic.Field(alias="miaMult")
        left_mult: float = pydantic.Field(alias="leftMult")
        mia_exp_reward: int
        runner_mult: float = pydantic.Field(alias="runnerMult")
        runner_exp_reward: int
        survived_mult: float = pydantic.Field(alias="survivedMult")
        survived_exp_requirement: int
        survived_exp_reward: int
        survived_seconds_requirement: int

    exp_for_locked_door_breach: int = pydantic.Field(alias="expForLockedDoorBreach")
    exp_for_locked_door_open: int = pydantic.Field(alias="expForLockedDoorOpen")
    heal: Heal
    kill: Kill
    level: Level
    loot_attempts: list[Any]
    match_end: MatchEnd
    trigger_mult: int = pydantic.Field(alias="triggerMult")


class GlobalsConfig(PascalCaseModel):
    aim_punch_magnitude: float
    aiming: dict[str, Any]
    armor_materials: dict[str, ArmorMaterial]
    audio_settings: dict[str, Any]
    azimuth_panel_shows_player_orientation: bool
    btr_settings: dict[str, Any] = pydantic.Field(alias="BTRSettings")
    ballistic: BallisticConfig
    base_check_time: float
    base_load_time: float
    base_unload_time: float
    blunt_damage_reduce_from_soft_armor_mod: float
    bots_enabled: bool
    buffer_zone: dict[str, Any]
    coop_settings: dict[str, Any]
    customization: dict[str, Any]
    discard_limits_enabled: bool
    event_settings: dict[str, Any]
    event_type: list[str]
    favorite_items_settings: dict[str, int]
    fence_settings: dict[str, Any]
    fracture_caused_by_bullet_hit: dict[str, Any]
    fracture_caused_by_falling: dict[str, Any]
    game_searching_timeout: int
    global_item_price_modifier: float
    global_loot_chance_modifier: float
    graphic_settings: dict[str, Any]
    hands_overdamage: float
    health: dict[str, Any]
    inertia: dict[str, Any]
    insurance: dict[str, Any]
    items_common_settings: dict[str, Any]
    legs_overdamage: float
    load_time_speed_progress: float
    malfunction: dict[str, Any]
    marksman_accuracy: float
    mastering: list[dict[str, Any]]
    max_bots_alive_on_map: int
    max_loyalty_level_for_all: bool
    overheat: dict[str, Any]
    rag_fair: dict[str, Any]
    repair_settings: dict[str, Any]
    requirement_references: dict[str, Any]
    restrictions_in_raid: list[dict[str, Any]]
    savage_play_cooldown_seconds: int = pydantic.Field(alias="SavagePlayCooldown")
    savage_play_cooldown_develop: int
    savage_play_cooldown_nda_free: int
    sessions_to_show_hot_keys: int
    skill_atrophy: bool
    skill_endurance_weight_threshold: float
    skill_exp_per_level: int
    skill_fatigue_per_point: float
    skill_fatigue_reset: int
    skill_fresh_effectiveness: float
    skill_fresh_points: int
    skill_min_effectiveness: float
    skill_points_before_fatigue: int
    skills_settings: dict[str, Any]
    sprint_speed: dict[str, Any]
    squad_settings: dict[str, Any]
    stamina: dict[str, Any]
    stamina_drain: dict[str, Any]
    stamina_restoration: dict[str, Any]
    stomach_overdamage: float
    tod_sky_date: str = pydantic.Field(alias="TODSkyDate")
    team_searching_timeout: int
    test_value: int
    time_before_deploy: int
    time_before_deploy_local: int
    trading_setting: int
    trading_settings: dict[str, Any]
    trading_unlimited_items: bool
    uncheck_on_shot: bool
    vaulting_settings: dict[str, Any]
    wave_coef_high: float = pydantic.Field(alias="WAVE_COEF_HIGH")
    wave_coef_horde: float = pydantic.Field(alias="WAVE_COEF_HORDE")
    wave_coef_low: float = pydantic.Field(alias="WAVE_COEF_LOW")
    wave_coef_mid: float = pydantic.Field(alias="WAVE_COEF_MID")
    walk_speed: dict[str, Any]
    wall_contusion_absorption: dict[str, Any]
    weapon_fast_draw_settings: dict[str, Any]
    weapon_skill_progress_rate: float
    armor: dict[str, Any] = pydantic.Field(alias="armor")
    content: dict[str, Any] = pydantic.Field(alias="content")
    exp: ExpConfig = pydantic.Field(alias="exp")
    handbook: dict[str, Any] = pydantic.Field(alias="handbook")
    rating: dict[str, Any] = pydantic.Field(alias="rating")
    t_base_lockpicking: int = pydantic.Field(alias="t_base_lockpicking")
    t_base_looting: int = pydantic.Field(alias="t_base_looting")
    tournament: dict[str, Any] = pydantic.Field(alias="tournament")


class Globals(StrictModel):
    bot_weapon_scatterings: list[BotWeaponScattering] = pydantic.Field(
        alias="BotWeaponScatterings",
    )
    item_presets: dict[str, ItemPreset] = pydantic.Field(alias="ItemPresets")
    bot_presets: list[BotPreset]
    config: GlobalsConfig


@dataclasses.dataclass
class ResourceDB:
    globals_: Globals


async def create_resource_db(resources_path: AssetsPath) -> ResourceDB:
    return ResourceDB(
        globals_=await read_pydantic_json(
            resources_path.joinpath("core", "globals.json"),
            TypeAdapter(Globals),
        ),
    )
