from typing import assert_never
from uuid import UUID

from app.core.domain.flea_market.repository import FleaOffersRepository
from lib.tarkov.items.actions import ItemOwner
from lib.tarkov.items.models import Item
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.trading.trader import Traders
from lib.tarkov.types import ItemId, ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id


class ItemLocator:
    def __init__(
        self,
        traders: Traders,
        item_db: ItemDB,
        flea_repository: FleaOffersRepository,
    ) -> None:
        self._traders = traders
        self._item_db = item_db
        self._flea_repository = flea_repository

    async def item_from_owner(
        self,
        id: str,
        owner: ItemOwner,
        *,
        require_real_item: bool,
    ) -> Item | None:
        match owner.type:
            case "Trader":
                return await self._from_trader(item_id=id, trader_id=owner.id)
            case "HideoutProduction":
                if require_real_item:  # pragma: no cover
                    return None
                return await self._from_hideout_production(template_id=id)
            case "RagFair":
                return await self._from_ragfair(offer_id=owner.id, item_id=id)

        assert_never(owner.type)

    async def _from_trader(self, trader_id: str, item_id: str) -> Item | None:
        trader = self._traders.get(trader_id=trader_id)
        if not trader:
            return None
        inventory = self._traders.inventory(trader=trader)
        return inventory.get(item_id=ItemId(item_id))

    async def _from_hideout_production(self, template_id: str) -> Item | None:
        tpl = self._item_db.get(id_=ItemTemplateId(template_id))
        if not tpl:  # pragma: no cover
            return None
        return Item(id=generate_mongodb_id(), template_id=tpl.id)

    async def _from_ragfair(self, offer_id: str, item_id: str) -> Item | None:
        offer = await self._flea_repository.get(id=UUID(offer_id))
        if offer is None:
            return None
        return next((item for item in offer.items if item.id == item_id), None)
