import logging
from collections.abc import Mapping, Sequence
from typing import Any, Protocol, Self

from aioinject import InjectionContext

from app.db.models import AccountProfile
from lib.tarkov.inventory import Inventory, T
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import (
    AddToWishlistHandler,
    BindHandler,
    EatHandler,
    ExamineHandler,
    FleaBuyHandler,
    FoldHandler,
    HideoutPutItemsHandler,
    HideoutToggleAreaHandler,
    HideoutUpgradeCompleteHandler,
    HideoutUpgradeHandler,
    MergeHandler,
    MoveHandler,
    QuestAcceptHandler,
    ReadEncyclopediaHandler,
    RecordShootingRangePointsHandler,
    RemoveFromWishlistHandler,
    RemoveHandler,
    SplitHandler,
    TraderSellItemHandler,
    TradingConfirmHandler,
    TransferHandler,
)
from lib.tarkov.inventory_actions.actions._trading import InsureHandler
from lib.tarkov.inventory_actions.actions.abc import ActionHandler
from lib.tarkov.items.actions import AnyAction, ItemsMovingResult, ProfileChanges
from lib.tarkov.profiles.models import Profile


class ActionHandlersDict(Protocol):
    def __getitem__(self, item: type[T]) -> type[ActionHandler[T]]: ...

    @classmethod
    def from_mapping(
        cls,
        obj: Mapping[type[AnyAction], type[ActionHandler[AnyAction]]],
    ) -> Self:
        return obj  # type: ignore[return-value]


class ActionRouter:

    def __init__(
        self,
        injection_context: InjectionContext,
    ) -> None:
        self._context = injection_context
        self._handlers: Sequence[type[ActionHandler[Any]]] = (
            AddToWishlistHandler,
            BindHandler,
            EatHandler,
            ExamineHandler,
            FleaBuyHandler,
            FoldHandler,
            HideoutPutItemsHandler,
            HideoutToggleAreaHandler,
            HideoutUpgradeCompleteHandler,
            HideoutUpgradeHandler,
            MergeHandler,
            MoveHandler,
            QuestAcceptHandler,
            ReadEncyclopediaHandler,
            RecordShootingRangePointsHandler,
            RemoveFromWishlistHandler,
            RemoveHandler,
            SplitHandler,
            InsureHandler,
            TraderSellItemHandler,
            TradingConfirmHandler,
            TransferHandler,
        )

    async def handle(
        self,
        account_profile: AccountProfile,
        profile: Profile,
        inventory: Inventory,
        actions: Sequence[AnyAction],
    ) -> ItemsMovingResult:
        action_handlers = ActionHandlersDict.from_mapping(
            {handler.action: handler for handler in self._handlers},  # type: ignore[misc]
        )
        logging.debug(actions)
        context = ActionContext(
            account_profile=account_profile,
            profile=profile,
            changes=ProfileChanges(
                id=profile.id,
                health=profile.health,
                skills=profile.skills,
            ),
            inventory=inventory,
        )

        for action in actions:
            handler_cls = action_handlers[type(action)]
            handler = await self._context.resolve(handler_cls)
            result = await handler.handle(action=action, context=context)
            result.unwrap()

        profile.inventory.items = inventory.to_list()
        context.changes.experience = profile.info.experience
        context.changes.trader_relations = profile.traders_info

        return ItemsMovingResult(
            changes={profile.id: context.changes},
            warnings=[],
        )
