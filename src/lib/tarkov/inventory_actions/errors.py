import dataclasses

from lib.tarkov.types import ItemId


class ActionError:
    pass


@dataclasses.dataclass
class TraderNotFoundError(ActionError):
    id: str


@dataclasses.dataclass
class NotEnoughItemsForTradeError(ActionError):
    pass


@dataclasses.dataclass
class NotEnoughItemsError(ActionError):
    pass


@dataclasses.dataclass
class NoSpaceForItemError(ActionError):
    pass


@dataclasses.dataclass
class FleaOfferNotFoundError(ActionError):
    id: str


@dataclasses.dataclass
class InvalidItemError(ActionError):
    id: ItemId
    message: str


@dataclasses.dataclass
class TradingRestrictionLimitError(ActionError):
    item_id: ItemId
    count: int
    current_count: int
    max_count: int
