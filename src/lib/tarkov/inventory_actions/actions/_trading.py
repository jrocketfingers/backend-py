import collections
from collections.abc import Sequence
from typing import final

from result import Err, Ok, Result, is_err

from app.core.domain.accounts.insurance.services import InsuranceService
from app.core.domain.quests.services import QuestsService
from app.core.domain.traders.repositories import (
    TraderAssortRepository,
    TraderRepository,
)
from app.db.models import AccountProfile
from lib.db import DBContext
from lib.errors import Never
from lib.tarkov.inventory import (
    Inventory,
    ItemNotFoundError,
    regenerate_item_ids,
)
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions.abc import ActionHandler
from lib.tarkov.inventory_actions.errors import (
    NoSpaceForItemError,
    NotEnoughItemsError,
    NotEnoughItemsForTradeError,
    TraderNotFoundError,
    TradingRestrictionLimitError,
)
from lib.tarkov.inventory_actions.utils import (
    item_stack_sizes,
    take_item,
    take_required_items,
)
from lib.tarkov.items.actions import (
    InsureAction,
    ItemChanges,
    ProfileChanges,
    QuestAcceptAction,
    RequiredItem,
    SellItem,
    TradingConfirmAction,
    TradingSellToTraderAction,
)
from lib.tarkov.items.models import Item
from lib.tarkov.items.props.item import MoneyProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.items.upd import Upd
from lib.tarkov.profiles.models import InsuredItem, Quest, TraderInfo
from lib.tarkov.quests.errors import (
    QuestIsAlreadyAccepted,
    QuestIsNotAvailableError,
    QuestNotFoundError,
)
from lib.tarkov.quests.models import QuestStatus
from lib.tarkov.quests.storage import QuestStorage
from lib.tarkov.trading.models import CurrencyIds, TraderBarter
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.types import ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id
from lib.utils import utc_now


def _scheme_items_valid(
    count: int,
    scheme: Sequence[TraderBarter],
    scheme_items: Sequence[RequiredItem],
    inventory: Inventory,
) -> bool:
    required_items: dict[ItemTemplateId, int] = collections.defaultdict(int)
    for barter_item in scheme:
        required_items[barter_item.template_id] += barter_item.count * count

    items = [inventory.get(item.id) for item in scheme_items]
    for item, scheme_item in zip(items, scheme_items, strict=True):
        if item is None:
            return False

        if item.upd.get("StackObjectsCount", 1) < scheme_item.count:
            return False

        required_items[item.template_id] -= scheme_item.count

    return all(count == 0 for count in required_items.values())


@final
class TradingConfirmHandler(ActionHandler[TradingConfirmAction]):
    action = TradingConfirmAction

    def __init__(  # noqa: PLR0913
        self,
        traders: Traders,
        item_db: ItemDB,
        repository: TraderRepository,
        db_context: DBContext,
        trader_assort_repository: TraderAssortRepository,
    ) -> None:
        self._traders = traders
        self._item_db = item_db
        self._repository = repository
        self._db_context = db_context
        self._trader_assort_repository = trader_assort_repository

    async def handle(  # noqa: C901
        self,
        *,
        action: TradingConfirmAction,
        context: ActionContext,
    ) -> Result[
        None,
        TraderNotFoundError
        | NotEnoughItemsForTradeError
        | ItemNotFoundError
        | NoSpaceForItemError
        | TradingRestrictionLimitError,
    ]:
        if not (trader := self._traders.get(action.trader_id)):
            return Err(TraderNotFoundError(id=action.trader_id))
        trader_inventory = self._traders.inventory(trader)
        if not (trader_item := trader_inventory.get(item_id=action.item_id)):
            return Err(ItemNotFoundError(id=action.item_id))

        scheme = trader.assort.barter_scheme[action.item_id][action.scheme_id]
        if not _scheme_items_valid(
            count=action.count,
            scheme=scheme,
            scheme_items=action.scheme_items,
            inventory=context.inventory,
        ):
            return Err(NotEnoughItemsForTradeError())

        template = self._item_db.get(trader_item.template_id)
        for stack_count in item_stack_sizes(item_template=template, total=action.count):
            buy_result = await self._buy_one(
                context=context,
                trader_item=trader_item,
                trader_item_children=list(trader_inventory.children(trader_item)),
                count=stack_count,
            )
            if isinstance(buy_result, Err):
                return buy_result  # pragma: no cover

        self._take_required_items(
            scheme_items=action.scheme_items,
            changes=context.changes.items,
            trader=trader,
            trader_info=context.profile.traders_info[trader.base.id],
            inventory=context.inventory,
        )
        if is_err(
            result := await self._update_trader_state(
                account_profile=context.account_profile,
                trader=trader,
                trader_item=trader_item,
                count=action.count,
            ),
        ):
            return result

        return Ok(None)

    def _take_required_items(  # noqa: PLR0913
        self,
        scheme_items: Sequence[RequiredItem],
        changes: ItemChanges,
        trader: TraderModel,
        trader_info: TraderInfo,
        inventory: Inventory,
    ) -> None:
        for scheme_item, item in take_required_items(
            inventory=inventory,
            items=scheme_items,
            changes=changes,
        ):
            item_template = self._item_db.get(item.template_id)
            if (
                isinstance(item_template.props, MoneyProps)
                and item_template.props.type == trader.base.currency
            ):
                trader_info.sales_sum += scheme_item.count

    async def _buy_one(
        self,
        context: ActionContext,
        trader_item: Item,
        trader_item_children: Sequence[Item],
        count: int,
    ) -> Result[None, NoSpaceForItemError]:
        item = trader_item.model_copy(deep=True)
        item.parent_id = None
        item.slot_id = None
        item.location = None
        item.upd = Upd(StackObjectsCount=count)

        children = [i.model_copy(deep=True) for i in trader_item_children]
        regenerate_item_ids(item, *children)

        if (stash := context.inventory.get(context.profile.inventory.stash)) is None:
            raise Never

        location = context.inventory.find_location(
            item=item,
            children=children,
            parent=stash,
            slot_id="hideout",
        )
        if location is None:
            return Err(NoSpaceForItemError())  # pragma: no cover
        context.inventory.add(
            item,
            children=children,
            location=location,
        )
        context.changes.items.new.add(item)
        context.changes.items.new.update(children)
        return Ok(None)

    async def _update_trader_state(
        self,
        account_profile: AccountProfile,
        trader: TraderModel,
        trader_item: Item,
        count: int,
    ) -> Result[None, TradingRestrictionLimitError]:
        if is_err(
            result := await self._update_restrictions(
                trader=trader,
                account_profile=account_profile,
                trader_item=trader_item,
                count=count,
            ),
        ):
            return result
        await self._trader_assort_repository.change_count(
            trader_id=trader.base.id,
            item_id=trader_item.id,
            delta=-count,
        )
        return Ok(None)

    async def _update_restrictions(
        self,
        trader: TraderModel,
        account_profile: AccountProfile,
        trader_item: Item,
        count: int,
    ) -> Result[None, TradingRestrictionLimitError]:
        trader_db = await self._repository.get(trader.base.id)
        if not trader_db:
            raise Never

        if (max_count := trader_item.upd.get("BuyRestrictionMax")) is None:
            return Ok(None)  # pragma: no cover

        restriction = await self._repository.get_or_create_restriction(
            trader=trader_db,
            profile=account_profile,
            item_id=trader_item.id,
        )
        if restriction.bought_count + count > max_count:
            return Err(
                TradingRestrictionLimitError(
                    item_id=trader_item.id,
                    current_count=restriction.bought_count,
                    max_count=max_count,
                    count=count,
                ),
            )
        restriction.bought_count += count
        self._db_context.add(restriction)
        return Ok(None)


@final
class QuestAcceptHandler(ActionHandler[QuestAcceptAction]):
    action = QuestAcceptAction

    def __init__(
        self,
        quest_storage: QuestStorage,
        quest_service: QuestsService,
    ) -> None:
        self._quests = quest_storage
        self._quest_service = quest_service

    async def handle(
        self,
        *,
        action: QuestAcceptAction,
        context: ActionContext,
    ) -> Result[
        None,
        QuestNotFoundError | QuestIsNotAvailableError | QuestIsAlreadyAccepted,
    ]:
        quest = self._quests.get(quest_id=action.quest_id)
        if not quest:
            return Err(QuestNotFoundError(quest_id=action.quest_id))
        if any(quest.id == action.quest_id for quest in context.profile.quests):
            return Err(QuestIsAlreadyAccepted(quest_id=quest.id))
        if not await self._quest_service.is_quest_available(
            profile=context.profile,
            quest=quest,
        ):
            return Err(QuestIsNotAvailableError(quest_id=quest.id))

        context.profile.quests.append(
            Quest(
                id=quest.id,
                start_time=utc_now(),
                status=QuestStatus.Started,
            ),
        )
        return Ok(None)


@final
class TraderSellItemHandler(ActionHandler[TradingSellToTraderAction]):
    action = TradingSellToTraderAction

    def __init__(
        self,
        traders: Traders,
        item_db: ItemDB,
    ) -> None:
        self._traders = traders
        self._item_db = item_db

    async def handle(
        self,
        *,
        action: TradingSellToTraderAction,
        context: ActionContext,
    ) -> Result[
        None,
        TraderNotFoundError
        | ItemNotFoundError
        | NotEnoughItemsForTradeError
        | NoSpaceForItemError,
    ]:
        if not (trader := self._traders.get(trader_id=action.trader_id)):
            return Err(TraderNotFoundError(id=action.trader_id))

        if isinstance(
            take_result := self._take_required_items(
                items=action.items,
                inventory=context.inventory,
                changes=context.changes,
            ),
            Err,
        ):
            return take_result

        stash = context.inventory.get(context.profile.inventory.stash)
        if stash is None:
            raise Never

        if isinstance(
            money_add_result := self._add_money(
                inventory=context.inventory,
                stash=stash,
                currency_id=CurrencyIds[trader.base.currency].value,
                currency_amount=action.price,
                changes=context.changes,
            ),
            Err,
        ):
            return money_add_result

        standing = context.profile.traders_info[trader.base.id]
        standing.sales_sum += action.price

        return Ok(None)

    def _take_required_items(
        self,
        items: Sequence[SellItem],
        inventory: Inventory,
        changes: ProfileChanges,
    ) -> Result[None, ItemNotFoundError | NotEnoughItemsForTradeError]:
        for sell_item in items:
            item = inventory.get(sell_item.id)
            if item is None:
                return Err(ItemNotFoundError(id=sell_item.id))

            count = item.upd.get("StackObjectsCount", 1)
            count -= sell_item.count
            if count < 0:
                return Err(NotEnoughItemsForTradeError())
            if count == 0:
                inventory.remove(item)
                changes.items.deleted.add(item)
            else:  # pragma: no cover
                item.upd["StackObjectsCount"] = count
                changes.items.changed.add(item)
        return Ok(None)

    def _add_money(  # noqa: PLR0913
        self,
        inventory: Inventory,
        currency_id: ItemTemplateId,
        currency_amount: int,
        changes: ProfileChanges,
        stash: Item,
    ) -> Result[None, NoSpaceForItemError]:
        max_stack_size = self._item_db.get(currency_id).props.stack_max_size

        existing_items = (
            item for item in inventory.items.values() if item.template_id == currency_id
        )

        currency_left = currency_amount
        for item in existing_items:
            count = item.upd.get("StackObjectsCount", 1)
            if count >= max_stack_size:
                continue
            diff = min(max_stack_size - count, currency_left)
            currency_left -= diff
            item.upd["StackObjectsCount"] = count + diff
            changes.items.changed.add(item)

        while currency_left > 0:
            count = min(max_stack_size, currency_left)
            currency_left -= count
            money = Item(
                id=generate_mongodb_id(),
                template_id=currency_id,
                upd=Upd(StackObjectsCount=count),
            )
            location = inventory.find_location(
                item=money,
                children=(),
                parent=stash,
                slot_id="hideout",
            )
            if location is None:
                return Err(NoSpaceForItemError())
            inventory.add(
                money,
                children=(),
                location=location,
            )
            changes.items.new.add(money)
        return Ok(None)


class InsureHandler(ActionHandler[InsureAction]):
    action = InsureAction

    def __init__(
        self,
        traders: Traders,
        insurance_service: InsuranceService,
    ) -> None:
        self._traders = traders
        self._insurance_service = insurance_service

    async def handle(
        self,
        *,
        action: InsureAction,
        context: ActionContext,
    ) -> Result[None, TraderNotFoundError | ItemNotFoundError | NotEnoughItemsError]:
        trader = self._traders.get(action.trader_id)
        if not trader:
            return Err(TraderNotFoundError(id=action.trader_id))

        items = []
        for item_id in action.items:
            item = context.inventory.get(item_id)
            if not item:
                return Err(ItemNotFoundError(id=item_id))
            items.append(item)

        prices = self._insurance_service.get_insurance_cost(
            items=items,
            trader=trader,
            profile=context.profile,
        )
        money_required = sum(prices[item.template_id] for item in items)
        take_result = take_item(
            inventory=context.inventory,
            item_template_id=CurrencyIds[trader.base.currency].value,
            count=money_required,
        )
        if isinstance(take_result, Err):
            return take_result
        context.changes.items.changed.update(take_result.ok_value.changed)
        context.changes.items.deleted.update(take_result.ok_value.deleted)

        for item in items:
            context.profile.insured_items.append(
                InsuredItem(trader_id=trader.base.id, item_id=item.id),
            )

        return Ok(None)
