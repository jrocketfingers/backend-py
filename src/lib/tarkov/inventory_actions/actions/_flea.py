import itertools
from typing import Any, final
from uuid import UUID

from result import Err, Ok, Result

from app.core.domain.flea_market.repository import FleaOffersRepository
from app.core.domain.flea_market.services import FleaMarketService
from app.db.models import FleaOffer
from lib.errors import Never
from lib.tarkov.inventory import MoveErrors
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions.abc import ActionHandler
from lib.tarkov.inventory_actions.errors import (
    FleaOfferNotFoundError,
    NoSpaceForItemError,
)
from lib.tarkov.inventory_actions.utils import (
    put_items_into_inventory,
    take_required_items,
)
from lib.tarkov.items.actions import (
    AddToWishlistAction,
    RagFairBuyOfferAction,
    RagFairOfferBuy,
    RemoveFromWishlistAction,
)
from lib.tarkov.items.models import Item
from lib.tarkov.items.storage import ItemDB


@final
class FleaBuyHandler(ActionHandler[RagFairBuyOfferAction]):
    action = RagFairBuyOfferAction

    def __init__(
        self,
        flea_repository: FleaOffersRepository,
        flea_service: FleaMarketService,
        item_db: ItemDB,
    ) -> None:
        self._flea_repository = flea_repository
        self._flea_service = flea_service
        self._item_db = item_db

    async def handle(
        self,
        *,
        action: RagFairBuyOfferAction,
        context: ActionContext,
    ) -> Result[None, Any]:
        stash = context.inventory.get(item_id=context.profile.inventory.stash)
        if stash is None:
            raise Never

        for offer_to_buy in action.offers:
            take_required_items(
                inventory=context.inventory,
                items=offer_to_buy.items,
                changes=context.changes.items,
            )
            offer = await self._flea_repository.get(
                id=UUID(offer_to_buy.id),
                with_for_update=True,
            )
            if offer is None:
                return Err(FleaOfferNotFoundError(id=offer_to_buy.id))

            await self._flea_service.buy_offer(
                offer=offer,
                count=offer_to_buy.count,
            )

            if isinstance(
                add_result := self._add_items(
                    offer_to_buy=offer_to_buy,
                    offer=offer,
                    context=context,
                    stash=stash,
                ),
                Err,
            ):
                return add_result

        return Ok(None)

    def _add_items(
        self,
        offer_to_buy: RagFairOfferBuy,
        offer: FleaOffer,
        context: ActionContext,
        stash: Item,
    ) -> Result[None, NoSpaceForItemError | MoveErrors]:
        parent = next(item for item in offer.items if item.id == offer.root_item_id)
        children = [item for item in offer.items if item.id != offer.root_item_id]
        result = put_items_into_inventory(
            item=parent,
            children=children,
            item_db=self._item_db,
            count=offer_to_buy.count,
            inventory=context.inventory,
            stash=stash,
        )
        if isinstance(result, Err):
            return result

        context.changes.items.new.update(itertools.chain.from_iterable(result.ok_value))
        return Ok(None)


class AddToWishlistHandler(ActionHandler[AddToWishlistAction]):
    action = AddToWishlistAction

    async def handle(
        self,
        *,
        action: AddToWishlistAction,
        context: ActionContext,
    ) -> Ok[None]:
        context.profile.wish_list.add(action.template_id)
        return Ok(None)


class RemoveFromWishlistHandler(ActionHandler[RemoveFromWishlistAction]):
    action = RemoveFromWishlistAction

    async def handle(
        self,
        *,
        action: RemoveFromWishlistAction,
        context: ActionContext,
    ) -> Ok[None]:
        context.profile.wish_list.discard(action.template_id)
        return Ok(None)
