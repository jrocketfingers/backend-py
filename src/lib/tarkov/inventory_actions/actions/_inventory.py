from typing import final

from result import Err, Ok, Result

from lib.errors import Never
from lib.tarkov.inventory import (
    InventoryOperationError,
    ItemIsNotFoldableError,
    ItemNotFoundError,
)
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions.abc import ActionHandler
from lib.tarkov.inventory_actions.errors import InvalidItemError
from lib.tarkov.inventory_actions.services import ItemLocator
from lib.tarkov.items.actions import (
    BindAction,
    EatAction,
    ExamineAction,
    FoldAction,
    Location,
    MergeAction,
    MoveAction,
    ReadEncyclopediaAction,
    RemoveAction,
    SplitAction,
    TransferAction,
    UnbindAction,
)
from lib.tarkov.items.props.item import FoodDrinkProps
from lib.tarkov.items.props.mods import StockProps
from lib.tarkov.items.props.weapons import WeaponProps
from lib.tarkov.items.storage import ItemDB


@final
class MoveHandler(ActionHandler[MoveAction]):
    action = MoveAction

    async def handle(
        self,
        *,
        action: MoveAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (item := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(id=action.item))

        result = context.inventory.move(
            item=item,
            location=action.to,
        )
        if isinstance(result, Err):
            return result

        return Ok(None)


@final
class SplitHandler(ActionHandler[SplitAction]):
    action = SplitAction

    async def handle(
        self,
        *,
        action: SplitAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (item := context.inventory.get(item_id=action.split_item)):
            return Err(ItemNotFoundError(action.split_item))

        result = context.inventory.split(
            item=item,
            new_id=action.new_item,
            count=action.count,
            location=action.container,
        )
        if isinstance(result, Err):
            return result

        context.changes.items.changed.add(item)

        return Ok(None)


@final
class MergeHandler(ActionHandler[MergeAction]):
    action = MergeAction

    async def handle(
        self,
        *,
        action: MergeAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (source := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(action.item))

        if not (target := context.inventory.get(item_id=action.with_)):
            return Err(ItemNotFoundError(action.with_))

        result = context.inventory.merge(
            source=source,
            target=target,
        )
        if isinstance(result, Err):
            return result
        context.changes.items.deleted.add(source)
        context.changes.items.changed.add(target)
        return Ok(None)


@final
class TransferHandler(ActionHandler[TransferAction]):
    action = TransferAction

    async def handle(
        self,
        *,
        action: TransferAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (source := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(action.item))

        if not (target := context.inventory.get(item_id=action.with_)):
            return Err(ItemNotFoundError(action.with_))

        result = context.inventory.transfer(
            source=source,
            target=target,
            count=action.count,
        )
        if isinstance(result, Err):
            return result

        context.changes.items.changed.add(source)
        context.changes.items.changed.add(target)

        return Ok(None)


@final
class RemoveHandler(ActionHandler[RemoveAction]):
    action = RemoveAction

    async def handle(
        self,
        *,
        action: RemoveAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (item := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(action.item))

        if isinstance(result := context.inventory.remove(item), Err):
            return result  # pragma: no cover

        item, children = result.ok_value
        context.changes.items.deleted.add(item)
        context.changes.items.deleted.update(children)
        return Ok(None)


@final
class FoldHandler(ActionHandler[FoldAction]):
    action = FoldAction

    def __init__(self, item_db: ItemDB) -> None:
        self._item_db = item_db

    async def handle(
        self,
        *,
        action: FoldAction,
        context: ActionContext,
    ) -> Result[None, ItemNotFoundError | ItemIsNotFoldableError]:
        if not (item := context.inventory.get(action.item)):
            return Err(ItemNotFoundError(action.item))
        tpl = self._item_db.get(item.template_id)
        if not isinstance(tpl.props, WeaponProps | StockProps):
            return Err(ItemIsNotFoldableError(id=item.id))

        parent = next(
            (
                item
                for item in context.inventory.iter_parents(item)
                if self._item_db.get(item.template_id).props.merges_with_children
            ),
            item,
        )
        children = tuple(context.inventory.children(item=parent))
        context.inventory.remove(item=parent)

        item.upd.setdefault("Foldable", {"Folded": False})["Folded"] = action.value
        if item.parent_id is None or item.slot_id is None:
            raise Never

        if parent.parent_id is None or parent.slot_id is None:
            raise Never

        context.inventory.add(
            item=parent,
            children=children,
            location=Location(
                id=parent.parent_id,
                container=parent.slot_id,
                location=parent.location,
            ),
        )

        return Ok(None)


@final
class ExamineHandler(ActionHandler[ExamineAction]):
    action = ExamineAction

    def __init__(self, item_locator: ItemLocator, item_db: ItemDB) -> None:
        self._item_locator = item_locator
        self._item_db = item_db

    async def handle(
        self,
        *,
        action: ExamineAction,
        context: ActionContext,
    ) -> Result[None, ItemNotFoundError]:
        item = await self._item_locator.item_from_owner(
            id=action.item,
            owner=action.from_owner,
            require_real_item=False,
        )
        if not item:
            return Err(ItemNotFoundError(id=action.item))

        item_tpl = self._item_db.get(id_=item.template_id)

        context.profile.encyclopedia[item_tpl.id] = False
        context.profile.info.experience += item_tpl.props.examine_experience
        return Ok(None)


@final
class ReadEncyclopediaHandler(ActionHandler[ReadEncyclopediaAction]):
    action = ReadEncyclopediaAction

    async def handle(
        self,
        *,
        action: ReadEncyclopediaAction,
        context: ActionContext,
    ) -> Result[None, None]:
        for template_id in action.ids:
            context.profile.encyclopedia[template_id] = True
        return Ok(None)


@final
class EatHandler(ActionHandler[EatAction]):
    action = EatAction

    def __init__(self, item_db: ItemDB) -> None:
        self._item_db = item_db

    async def handle(
        self,
        *,
        action: EatAction,
        context: ActionContext,
    ) -> Result[None, ItemNotFoundError | InvalidItemError]:
        item = context.inventory.get(item_id=action.item)
        if item is None:
            return Err(ItemNotFoundError(id=action.item))

        template = self._item_db.get(item.template_id)
        if not isinstance(template.props, FoodDrinkProps):
            return Err(
                InvalidItemError(id=item.id, message="Item is not food or drink"),
            )
        max_resource = template.props.max_resource

        item.upd.setdefault("FoodDrink", {"HpPercent": max_resource})
        item.upd["FoodDrink"]["HpPercent"] -= action.count
        if item.upd["FoodDrink"]["HpPercent"] <= 0:
            if isinstance(result := context.inventory.remove(item), Err):
                return result  # pragma: no cover
            context.changes.items.deleted.add(item)
        else:
            context.changes.items.changed.add(item)
        return Ok(None)


@final
class BindHandler(ActionHandler[BindAction]):
    action = BindAction

    async def handle(
        self,
        *,
        action: BindAction,
        context: ActionContext,
    ) -> Result[None, ItemNotFoundError]:
        item = context.inventory.get(item_id=action.item)
        if item is None:
            return Err(ItemNotFoundError(id=action.item))
        context.profile.inventory.fast_panel = {
            k: v
            for k, v in context.profile.inventory.fast_panel.items()
            if v != item.id
        }
        context.profile.inventory.fast_panel[action.index] = item.id
        return Ok(None)


@final
class UnbindHandler(ActionHandler[UnbindAction]):
    action = UnbindAction

    async def handle(
        self,
        *,
        action: UnbindAction,
        context: ActionContext,
    ) -> Ok[None]:
        panel = context.profile.inventory.fast_panel
        if panel.get(action.index) == action.item:
            del panel[action.index]
        return Ok(None)
