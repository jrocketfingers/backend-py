from typing import Any, Protocol

from result import Result

from lib.tarkov.inventory import T
from lib.tarkov.inventory_actions import ActionContext


class ActionHandler(Protocol[T]):
    @property
    def action(self) -> type[T]: ...

    async def handle(
        self,
        *,
        action: T,
        context: ActionContext,
    ) -> Result[None, Any]: ...
