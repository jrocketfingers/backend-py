from typing import Any, final

from result import Ok, Result

from app.core.domain.hideout.services import HideoutService, find_area
from app.core.domain.profiles.services import set_counter
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions.abc import ActionHandler
from lib.tarkov.inventory_actions.utils import take_required_items
from lib.tarkov.items.actions import (
    HideoutPutItemsAction,
    HideoutToggleAreaAction,
    HideoutUpgradeAction,
    HideoutUpgradeCompleteAction,
    RecordShootingRangePointsAction,
)
from lib.tarkov.profiles.models import HideoutAreaSlot


@final
class HideoutUpgradeHandler(ActionHandler[HideoutUpgradeAction]):
    action = HideoutUpgradeAction

    def __init__(self, hideout_service: HideoutService) -> None:
        self._hideout_service = hideout_service

    async def handle(
        self,
        *,
        action: HideoutUpgradeAction,
        context: ActionContext,
    ) -> Result[None, None]:
        take_required_items(
            inventory=context.inventory,
            # Just trust the client that it sent correct items
            items=action.items,
            changes=context.changes.items,
        )
        area = find_area(context.profile.hideout.areas, area_type=action.area_type)
        await self._hideout_service.upgrade(area=area)
        return Ok(None)


@final
class HideoutUpgradeCompleteHandler(ActionHandler[HideoutUpgradeCompleteAction]):
    action = HideoutUpgradeCompleteAction

    def __init__(self, hideout_service: HideoutService) -> None:
        self._hideout_service = hideout_service

    async def handle(
        self,
        *,
        action: HideoutUpgradeCompleteAction,
        context: ActionContext,
    ) -> Result[None, None]:
        area = find_area(context.profile.hideout.areas, area_type=action.area_type)
        await self._hideout_service.upgrade_complete(
            area,
            profile=context.profile,
            inventory=context.inventory,
        )
        return Ok(None)


@final
class HideoutPutItemsHandler(ActionHandler[HideoutPutItemsAction]):
    action = HideoutPutItemsAction

    async def handle(
        self,
        *,
        action: HideoutPutItemsAction,
        context: ActionContext,
    ) -> Result[None, Any]:
        area = find_area(context.profile.hideout.areas, area_type=action.area_type)
        for index, required_item in action.items.items():
            slot = next(
                (slot for slot in area.slots if slot.location_index == index),
                None,
            )
            if slot is None:  # pragma: no branch
                slot = HideoutAreaSlot(location_index=index)
                area.slots.append(slot)

            for _, item in take_required_items(
                context.inventory,
                items=[required_item],
                changes=context.changes.items,
            ):
                slot.item.append(item)

        return Ok(None)


@final
class HideoutToggleAreaHandler(ActionHandler[HideoutToggleAreaAction]):
    action = HideoutToggleAreaAction

    async def handle(
        self,
        *,
        action: HideoutToggleAreaAction,
        context: ActionContext,
    ) -> Result[None, Any]:
        area = find_area(context.profile.hideout.areas, area_type=action.area_type)
        area.active = action.enabled
        return Ok(None)


@final
class RecordShootingRangePointsHandler(ActionHandler[RecordShootingRangePointsAction]):
    action = RecordShootingRangePointsAction

    async def handle(
        self,
        *,
        action: RecordShootingRangePointsAction,
        context: ActionContext,
    ) -> Result[None, Any]:
        set_counter(
            profile=context.profile,
            key="ShootingRangePoints",
            value=action.points,
        )
        return Ok(None)
