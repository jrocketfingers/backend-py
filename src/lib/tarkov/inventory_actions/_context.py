import dataclasses

from app.db.models import AccountProfile
from lib.tarkov.inventory import Inventory
from lib.tarkov.items.actions import ProfileChanges
from lib.tarkov.profiles.models import Profile


@dataclasses.dataclass(kw_only=True, slots=True, frozen=True)
class ActionContext:
    account_profile: AccountProfile
    profile: Profile
    inventory: Inventory
    changes: ProfileChanges
