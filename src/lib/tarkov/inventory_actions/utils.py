import dataclasses
from collections.abc import Iterator, Sequence

from result import Err, Ok, Result

from lib.errors import Never
from lib.tarkov.inventory import Inventory, MoveErrors, regenerate_item_ids
from lib.tarkov.inventory_actions.errors import (
    NoSpaceForItemError,
    NotEnoughItemsError,
)
from lib.tarkov.items.actions import ItemChanges, RequiredItem
from lib.tarkov.items.models import Item, ItemTemplate
from lib.tarkov.items.props.base import StackableItemProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.types import ItemTemplateId


@dataclasses.dataclass(slots=True, kw_only=True)
class TakeItemsResult:
    changed: list[Item] = dataclasses.field(default_factory=list)
    deleted: list[Item] = dataclasses.field(default_factory=list)


def take_required_items(
    inventory: Inventory,
    items: Sequence[RequiredItem],
    changes: ItemChanges,
) -> list[tuple[RequiredItem, Item]]:
    result = []
    for required_item in items:
        item = inventory.get(required_item.id)
        if item is None:
            raise Never

        item_count = item.upd.get("StackObjectsCount", 1) - required_item.count
        if item_count <= 0:
            inventory.remove(item)
            changes.deleted.add(item)
        else:
            item.upd["StackObjectsCount"] = item_count
            changes.changed.add(item)

        result.append((required_item, item))
    return result


def take_item(  # noqa: C901
    inventory: Inventory,
    item_template_id: ItemTemplateId,
    count: int,
) -> Result[TakeItemsResult, NotEnoughItemsError]:
    result = TakeItemsResult()

    for item in inventory.items.values():
        if item.template_id != item_template_id:
            continue

        stack_size = item.upd.get("StackObjectsCount", 1)
        count_to_take = min(stack_size, count)
        item.upd["StackObjectsCount"] = stack_size - count_to_take
        if not item.upd["StackObjectsCount"]:
            result.deleted.append(item)
        else:
            result.changed.append(item)
        count -= count_to_take
        if count <= 0:
            break
    if count > 0:
        return Err(NotEnoughItemsError())

    for item in result.deleted:
        inventory.remove(item).unwrap()

    return Ok(result)


def item_stack_sizes(item_template: ItemTemplate, total: int) -> Iterator[int]:
    if not isinstance(item_template.props, StackableItemProps):
        stack_size = 1
    else:
        stack_size = item_template.props.stack_max_size

    while total:
        current_stack_size = min(total, stack_size)
        yield current_stack_size
        total -= current_stack_size


def put_items_into_inventory(  # noqa: PLR0913
    item: Item,
    children: list[Item],
    item_db: ItemDB,
    count: int,
    inventory: Inventory,
    stash: Item,
) -> Result[list[list[Item]], MoveErrors | NoSpaceForItemError]:
    item_template = item_db.items[item.template_id]
    result = []
    for stack_size in item_stack_sizes(item_template, total=count):
        items = [item, *children]
        items = [item.model_copy(deep=True) for item in items]
        parent = next(i for i in items if i.id == item.id)
        children = [i for i in items if i is not parent]

        parent.parent_id = None
        parent.slot_id = None
        parent.location = None
        parent.upd["StackObjectsCount"] = stack_size

        regenerate_item_ids(*items)

        location = inventory.find_location(
            item=parent,
            children=children,
            parent=stash,
            slot_id="hideout",
        )
        if not location:
            return Err(NoSpaceForItemError())

        if isinstance(
            add_result := inventory.add(
                item=parent,
                children=children,
                location=location,
            ),
            Err,
        ):
            return add_result  # pragma: no cover

        result.append(items)

    return Ok(result)
