import collections
import dataclasses
import itertools
from collections.abc import Iterator, Mapping, MutableMapping, Sequence
from typing import (
    Final,
    NamedTuple,
    Protocol,
    Self,
    TypeVar,
    runtime_checkable,
)

from result import Err, Ok, Result

from lib.tarkov.items.actions import (
    Location,
)
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation, ItemTemplate
from lib.tarkov.items.props.base import CompoundItemProps, StackableItemProps
from lib.tarkov.items.props.mods import StockProps
from lib.tarkov.items.props.types import Slot, SlotName, StackSlot
from lib.tarkov.items.props.weapons import WeaponProps
from lib.tarkov.items.upd import Upd
from lib.tarkov.items.utils import is_folded
from lib.tarkov.types import ItemId, ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id

T = TypeVar("T")


@dataclasses.dataclass
class InventoryOperationError:
    pass


@dataclasses.dataclass
class NotAContainerError(InventoryOperationError):
    pass


@dataclasses.dataclass
class SlotAlreadyTakenError(InventoryOperationError):
    slot: str | tuple["str", int] | tuple[int, int]
    state: bool


@dataclasses.dataclass
class ItemNotFoundError(InventoryOperationError):
    id: ItemId


@dataclasses.dataclass
class ItemOutOfBoundsError(InventoryOperationError):
    pass


@dataclasses.dataclass
class ItemIsNotStackableError(InventoryOperationError):
    pass


@dataclasses.dataclass
class StackSizeError(InventoryOperationError):
    msg: str | None = None


@dataclasses.dataclass
class IncompatibleItemsError(InventoryOperationError):
    ids: list[ItemId]


@dataclasses.dataclass
class ItemIsNotFoldableError(InventoryOperationError):
    id: ItemId


MoveErrors = (
    NotAContainerError
    | SlotAlreadyTakenError
    | ItemOutOfBoundsError
    | ItemNotFoundError
)


def remove_orphan_items(items: Sequence[Item]) -> Sequence[Item]:
    item_ids = {item.id for item in items}

    items_by_parent_id = collections.defaultdict(list)
    for item in items:
        items_by_parent_id[item.parent_id].append(item)

    items_to_delete = [
        item
        for item in items
        if item.parent_id not in item_ids and item.parent_id not in ("hideout", None)
    ]
    while items_to_delete:
        item = items_to_delete.pop()
        children = items_by_parent_id.pop(item.parent_id, None)
        if children:
            items_to_delete.extend(children)

    return list(itertools.chain.from_iterable(items_by_parent_id.values()))


def regenerate_item_ids(*items: Item) -> Sequence[Item]:
    parent_ids: set[ItemId] = {item.parent_id for item in items if item.parent_id}

    ids_map: Mapping[ItemId, ItemId] = {
        item.id: generate_mongodb_id() for item in items if item.id in parent_ids
    }
    for item in items:
        # Generate separate id if item has no parent
        item.id = ids_map.get(item.id, generate_mongodb_id())
        item.parent_id = ids_map[item.parent_id] if item.parent_id else item.parent_id
    return items


def item_topological_sort(items: Sequence[Item]) -> Iterator[Item]:
    items_by_parent_id = collections.defaultdict(list)
    for item in items:
        items_by_parent_id[item.parent_id].append(item)

    stack: list[ItemId | None] = [None]
    while stack:
        parent_id = stack.pop()
        if parent_id not in items_by_parent_id:
            continue

        children = items_by_parent_id[parent_id]
        yield from children
        stack.extend(child.id for child in children)


def iter_grid(width: int, height: int) -> Iterator[tuple[int, int]]:
    for w in range(width):
        for h in range(height):
            yield w, h


class Size(NamedTuple):
    width: int
    height: int

    def rotated(self) -> Self:
        return self.__class__(
            width=self.height,
            height=self.width,
        )


@dataclasses.dataclass(slots=True, init=False)
class Grid:
    width: int
    height: int
    map: dict[tuple[int, int], bool]
    is_sorting_table: bool

    def __init__(self, *, height: int, width: int, is_sorting_table: bool) -> None:
        self.height = height
        self.width = width
        self.is_sorting_table = is_sorting_table
        self.map = {
            v: False
            for v in iter_grid(
                width=width,
                height=height,
            )
        }

    def fill(  # noqa: PLR0913
        self,
        *,
        x1: int,
        y1: int,
        x2: int,
        y2: int,
        state: bool,
    ) -> Result[None, SlotAlreadyTakenError]:
        for x in range(x1, x2):
            for y in range(y1, y2):
                if self.map[x, y] is state:
                    return Err(SlotAlreadyTakenError(slot=(x, y), state=state))
                self.map[x, y] = state
        return Ok(None)


@runtime_checkable
class HasChambers(Protocol):
    chambers: Sequence[Slot]


@runtime_checkable
class HasCartridges(Protocol):
    cartridges: Sequence[StackSlot]


CartridgeSlots = dict[int, bool]


@dataclasses.dataclass(slots=True, frozen=True)
class ContainerSlots:
    template_id: ItemTemplateId
    grids: Mapping[SlotName, Grid]
    slots: MutableMapping[str, bool | CartridgeSlots]

    @classmethod
    def from_props(cls, template_id: ItemTemplateId, props: CompoundItemProps) -> Self:
        slots: list[StackSlot | Slot] = [*props.slots]
        if isinstance(props, HasCartridges):
            slots.extend(props.cartridges)
        if isinstance(props, HasChambers):
            slots.extend(props.chambers)

        return cls(
            template_id=template_id,
            grids={
                grid.name: Grid(
                    height=grid.props.cells_height,
                    width=grid.props.cells_width,
                    is_sorting_table=grid.props.is_sorting_table,
                )
                for grid in props.grids
            },
            slots={
                slot.name: CartridgeSlots() if isinstance(slot, StackSlot) else False
                for slot in slots
            },
        )


@dataclasses.dataclass(kw_only=True, slots=True)
class _ExtraSize:
    up: int = 0
    down: int = 0
    left: int = 0
    right: int = 0

    @property
    def height(self) -> int:
        return self.up + self.down

    @property
    def width(self) -> int:
        return self.left + self.right


def calculate_item_size(  # noqa: C901
    root: Item,
    children: Sequence[Item],
    templates: Mapping[ItemTemplateId, ItemTemplate],
    rotation: ItemRotation,
) -> Size:
    root_tpl = templates[root.template_id]
    width = root_tpl.props.width
    height = root_tpl.props.height
    extra_size = _ExtraSize()
    extra_size_force = _ExtraSize()

    root_foldable = (
        root_tpl.props.foldable
        if isinstance(root_tpl.props, WeaponProps | StockProps)
        else None
    )
    if (
        root_foldable
        and isinstance(root_tpl.props, WeaponProps | StockProps)
        and is_folded(root)
    ):
        extra_size_force.right -= root_tpl.props.size_reduce_right

    if root_tpl.props.merges_with_children:
        for child in children:
            child_tpl = templates[child.template_id]
            child_foldable = (
                isinstance(child_tpl.props, StockProps | WeaponProps)
                and child_tpl.props.foldable
            )
            if child_foldable and (  # pragma: no cover
                is_folded(child) or is_folded(root)
            ):
                continue
            if (  # pragma: no cover
                isinstance(root_tpl.props, WeaponProps)
                and root_tpl.props.folded_slot == child.slot_id
                and (is_folded(child) or is_folded(root))
            ):
                continue

            if child_tpl.props.extra_size_force_add:
                extra_size_force.up += child_tpl.props.extra_size_up
                extra_size_force.down += child_tpl.props.extra_size_down
                extra_size_force.left += child_tpl.props.extra_size_left
                extra_size_force.right += child_tpl.props.extra_size_right
            else:
                extra_size.up = max(extra_size.up, child_tpl.props.extra_size_up)
                extra_size.down = max(extra_size.down, child_tpl.props.extra_size_down)
                extra_size.left = max(extra_size.left, child_tpl.props.extra_size_left)
                extra_size.right = max(
                    extra_size.right,
                    child_tpl.props.extra_size_right,
                )

    size = Size(
        width=width + extra_size.width + extra_size_force.width,
        height=height + extra_size.height + extra_size_force.height,
    )
    if rotation == ItemRotation.Vertical:
        return size.rotated()
    return size


class ContainerMap:
    def __init__(
        self,
        templates: Mapping[ItemTemplateId, ItemTemplate],
    ) -> None:
        self._templates: Final = templates
        self.maps: MutableMapping[ItemId, ContainerSlots] = {}

    def _update_grid(  # noqa: PLR0913
        self,
        *,
        item: Item,
        children: Sequence[Item],
        parent_id: ItemId,
        slot_id: str,
        item_location: ItemLocation,
        state: bool,
    ) -> Result[Item, ItemOutOfBoundsError | SlotAlreadyTakenError]:
        grid = self.maps[parent_id].grids[slot_id]
        item_size = calculate_item_size(
            root=item,
            children=children,
            rotation=item_location.rotation,
            templates=self._templates,
        )
        if not grid.is_sorting_table:  # pragma: no branch
            if item_location.y + item_size.height > grid.height:
                return Err(ItemOutOfBoundsError())
            if item_location.x + item_size.width > grid.width:
                return Err(ItemOutOfBoundsError())

        fill_result = grid.fill(
            x1=item_location.x,
            y1=item_location.y,
            x2=item_location.x + item_size.width,
            y2=item_location.y + item_size.height,
            state=state,
        )
        if isinstance(fill_result, Err):
            return fill_result
        return Ok(item)

    def remove(
        self,
        item: Item,
        children: Sequence[Item],
    ) -> Result[Item, SlotAlreadyTakenError | ItemOutOfBoundsError]:
        return self._add_impl(item=item, children=children, state=False)

    def add(
        self,
        item: Item,
        children: Sequence[Item] = (),
    ) -> Result[Item, SlotAlreadyTakenError | ItemOutOfBoundsError]:
        return self._add_impl(item=item, children=children, state=True)

    def find_location(
        self,
        item: Item,
        children: Sequence[Item],
        slot_id: str,
        parent_id: ItemId,
    ) -> ItemLocation | None:
        grid = self.maps[parent_id].grids[slot_id]

        sizes = {
            rotation: calculate_item_size(
                root=item,
                children=children,
                rotation=rotation,
                templates=self._templates,
            )
            for rotation in ItemRotation
        }
        for y, x, (rotation, size) in itertools.product(
            range(grid.height),
            range(grid.width),
            sizes.items(),
        ):
            if x + size.width > grid.width or y + size.height > grid.height:
                continue  # pragma: no cover

            if any(
                grid.map[x + x_delta, y + y_delta]
                for x_delta, y_delta in iter_grid(width=size.width, height=size.height)
            ):
                continue

            return ItemLocation(
                x=x,
                y=y,
                rotation=rotation,
            )
        return None

    def _add_impl(
        self,
        *,
        item: Item,
        children: Sequence[Item],
        state: bool,
    ) -> Result[Item, SlotAlreadyTakenError | ItemOutOfBoundsError]:
        tpl = self._templates[item.template_id]
        if isinstance(tpl.props, CompoundItemProps):
            self.maps[item.id] = ContainerSlots.from_props(
                template_id=tpl.id,
                props=tpl.props,
            )
        if not item.parent_id:
            return Ok(item)  # Root item

        if item.parent_id not in self.maps or not item.slot_id:
            msg = "Parent id is not added to map or item.slot_id is None"
            raise ValueError(msg)

        slots = self.maps[item.parent_id]
        if isinstance(item.location, int):
            return self._cartridge_impl(
                item=item,
                slots=slots,
                slot_id=item.slot_id,
                location=item.location,
                state=state,
            )

        if item.location is None:
            return self._slot_impl(
                item=item,
                slots=slots,
                slot_id=item.slot_id,
                state=state,
            )

        return self._update_grid(
            item=item,
            children=children,
            parent_id=item.parent_id,
            slot_id=item.slot_id,
            item_location=item.location,
            state=state,
        )

    def _cartridge_impl(  # noqa: PLR0913
        self,
        *,
        item: Item,
        slots: ContainerSlots,
        slot_id: str,
        location: int,
        state: bool,
    ) -> Result[Item, SlotAlreadyTakenError]:
        slot = slots.slots[slot_id]
        if not isinstance(slot, dict):
            raise TypeError  # pragma: no cover
        if slot.get(location, False) is state:
            return Err(SlotAlreadyTakenError(slot=(slot_id, location), state=state))
        slot[location] = state
        return Ok(item)

    def _slot_impl(
        self,
        *,
        item: Item,
        slots: ContainerSlots,
        slot_id: str,
        state: bool,
    ) -> Result[Item, SlotAlreadyTakenError]:
        slot = slots.slots[slot_id]
        if slot is state:
            return Err(SlotAlreadyTakenError(slot=slot_id, state=state))
        slots.slots[slot_id] = state
        return Ok(item)


def collect_children(root_id: ItemId, items: Sequence[Item]) -> Iterator[Item]:
    items_by_parent_id: dict[ItemId | None, list[Item]] = collections.defaultdict(list)
    for item in items:
        items_by_parent_id[item.parent_id].append(item)

    deque: collections.deque[Item] = collections.deque()
    for item in items:
        if item.parent_id == root_id:
            deque.append(item)

    while deque:
        item = deque.pop()
        if item.id in items_by_parent_id:
            deque.extend(items_by_parent_id[item.id])
        yield item


class Inventory:
    def __init__(
        self,
        items: dict[ItemId, Item],
        container_map: ContainerMap,
        templates: Mapping[ItemTemplateId, ItemTemplate],
    ) -> None:
        self.items: Final = items
        self._map: Final = container_map
        self._templates: Final = templates

    @classmethod
    def from_sequence(
        cls,
        items: Sequence[Item],
        templates: Mapping[ItemTemplateId, ItemTemplate],
    ) -> Self:
        container_map = ContainerMap(templates=templates)
        sorted_items = list(item_topological_sort(items))
        for item in sorted_items:
            if templates[item.template_id].props.merges_with_children:
                children = tuple(collect_children(item.id, sorted_items))
                container_map.add(item=item, children=children).unwrap()
            else:
                container_map.add(item=item).unwrap()
        return cls(
            container_map=container_map,
            items={item.id: item for item in items},
            templates=templates,
        )

    def to_list(self) -> list[Item]:
        return list(self.items.values())

    def get(self, item_id: ItemId) -> Item | None:
        return self.items.get(item_id)

    def children(self, item: Item) -> Iterator[Item]:
        children = [i for i in self.items.values() if i.parent_id == item.id]
        while True:
            yield from children
            new_children = [
                i
                for i in self.items.values()
                if i.parent_id in {c.id for c in children}
            ]
            if not new_children:
                return
            children = new_children

    def iter_parents(self, item: Item) -> Iterator[Item]:
        while True:
            if item.parent_id not in self.items:
                return

            item = self.items[item.parent_id]
            yield item

    def remove(
        self,
        item: Item,
    ) -> Result[tuple[Item, Sequence[Item]], ItemNotFoundError]:
        if item.id not in self.items:
            return Err(ItemNotFoundError(id=item.id))

        children = list(self.children(item))
        self._remove(item, children=children)
        return Ok((item, children))

    def add(
        self,
        item: Item,
        children: Sequence[Item],
        location: Location | None,
    ) -> Result[tuple[Item, Sequence[Item]], MoveErrors]:
        if location is not None:
            container = self.get(location.id)
            if container is None:
                return Err(ItemNotFoundError(id=location.id))

            container_tpl = self._templates[container.template_id]
            if not isinstance(container_tpl.props, CompoundItemProps):
                return Err(NotAContainerError())

            item.parent_id = location.id
            item.slot_id = location.container
            item.location = location.location
        else:  # Root Item
            item.parent_id = None
            item.slot_id = None
            item.location = None

        self.items[item.id] = item
        for child in children:
            self.items[child.id] = child
        if isinstance(result := self._map.add(item, children), Err):
            return result

        return Ok((item, children))

    def move(
        self,
        item: Item,
        location: Location,
    ) -> Result[Item, MoveErrors]:
        if isinstance(result := self.remove(item), Err):
            return result
        item, children = result.ok_value
        if isinstance(
            add_result := self.add(item=item, children=children, location=location),
            Err,
        ):
            return add_result
        return Ok(item)

    def split(
        self,
        item: Item,
        new_id: ItemId,
        count: int,
        location: Location,
    ) -> Result[Item, ItemIsNotStackableError | StackSizeError | MoveErrors]:
        if not isinstance(
            self._templates[item.template_id].props,
            StackableItemProps,
        ):
            return Err(ItemIsNotStackableError())

        if item.upd.setdefault("StackObjectsCount", 1) <= count:
            return Err(StackSizeError())

        new_item = Item(
            id=new_id,
            template_id=item.template_id,
            upd=Upd(StackObjectsCount=count),
        )
        result = self.add(item=new_item, children=(), location=location)
        if isinstance(result, Err):
            return result

        item.upd["StackObjectsCount"] -= count
        return Ok(new_item)

    def _remove(self, item: Item, children: Sequence[Item]) -> None:
        self._map.remove(item, children)
        del self.items[item.id]
        for child in children:
            del self.items[child.id]

    def _transfer_impl(  # noqa: C901
        self,
        source: Item,
        target: Item,
        count: int | None,
    ) -> Result[
        Item,
        ItemNotFoundError
        | IncompatibleItemsError
        | ItemIsNotStackableError
        | StackSizeError,
    ]:
        if source.id not in self.items:
            return Err(ItemNotFoundError(id=source.id))  # pragma: no cover

        if source.template_id != target.template_id:
            return Err(IncompatibleItemsError(ids=[source.id, target.id]))

        template = self._templates[target.template_id]
        if not isinstance(template.props, StackableItemProps):
            return Err(ItemIsNotStackableError())

        count_taken = source.upd.get("StackObjectsCount", 1)
        if count and count > count_taken:
            return Err(StackSizeError())

        count_taken = count or count_taken
        stack_size = count_taken + target.upd.get(
            "StackObjectsCount",
            1,
        )
        if template.props.stack_max_size < stack_size:
            msg = f"Max stack size of item {source.template_id} is {template.props.stack_max_size}"
            return Err(StackSizeError(msg))

        source.upd["StackObjectsCount"] = (
            source.upd.get("StackObjectsCount", 1) - count_taken
        )
        target.upd["StackObjectsCount"] = stack_size
        if count is None:
            self._remove(source, children=())
        return Ok(target)

    def merge(
        self,
        source: Item,
        target: Item,
    ) -> Result[
        Item,
        ItemNotFoundError
        | IncompatibleItemsError
        | ItemIsNotStackableError
        | StackSizeError,
    ]:
        return self._transfer_impl(source=source, target=target, count=None)

    def transfer(
        self,
        source: Item,
        target: Item,
        count: int,
    ) -> Result[
        Item,
        ItemNotFoundError
        | IncompatibleItemsError
        | ItemIsNotStackableError
        | StackSizeError,
    ]:
        return self._transfer_impl(source=source, target=target, count=count)

    def find_location_in_container(
        self,
        item: Item,
        children: Sequence[Item],
        parent: Item,
        slot_id: str,
    ) -> ItemLocation | None:
        return self._map.find_location(
            item=item,
            children=children,
            parent_id=parent.id,
            slot_id=slot_id,
        )

    def find_location(
        self,
        item: Item,
        children: Sequence[Item],
        parent: Item,
        slot_id: str,
    ) -> Location | None:
        item_location = self._map.find_location(
            item=item,
            children=children,
            parent_id=parent.id,
            slot_id=slot_id,
        )
        if item_location is None:
            return None

        return Location(
            id=parent.id,
            container=slot_id,
            location=item_location,
        )
