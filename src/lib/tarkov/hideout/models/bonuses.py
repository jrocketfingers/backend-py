from enum import StrEnum
from typing import Annotated, Literal

from pydantic import ConfigDict, Discriminator

from lib.models import StrictModel
from lib.tarkov.types import ItemTemplateId
from lib.utils import snake_to_camel


class HideoutBonusType(StrEnum):
    additional_slots = "AdditionalSlots"
    debuff_end_delay = "DebuffEndDelay"
    energy_regeneration = "EnergyRegeneration"
    experience_rate = "ExperienceRate"
    flea_commission = "RagfairCommission"
    fuel_consumption = "FuelConsumption"
    health_regeneration = "HealthRegeneration"
    hydration_regeneration = "HydrationRegeneration"
    insurance_return_time = "InsuranceReturnTime"
    maximum_energy = "MaximumEnergyReserve"
    quest_money_reward = "QuestMoneyReward"
    repair_armor_bonus = "RepairArmorBonus"
    scav_cooldown = "ScavCooldownTimer"
    skill_group_leveling_boost = "SkillGroupLevelingBoost"
    stash_size = "StashSize"
    text = "TextBonus"
    unlock_armor_repair = "UnlockArmorRepair"
    unlock_weapon_modification = "UnlockWeaponModification"
    unlock_weapon_repair = "UnlockWeaponRepair"
    weapon_repair_bonus = "RepairWeaponBonus"


GenericHideoutBonusType = Literal[
    HideoutBonusType.debuff_end_delay,
    HideoutBonusType.energy_regeneration,
    HideoutBonusType.experience_rate,
    HideoutBonusType.flea_commission,
    HideoutBonusType.fuel_consumption,
    HideoutBonusType.health_regeneration,
    HideoutBonusType.hydration_regeneration,
    HideoutBonusType.insurance_return_time,
    HideoutBonusType.maximum_energy,
    HideoutBonusType.quest_money_reward,
    HideoutBonusType.repair_armor_bonus,
    HideoutBonusType.scav_cooldown,
    HideoutBonusType.unlock_armor_repair,
    HideoutBonusType.unlock_weapon_modification,
    HideoutBonusType.unlock_weapon_repair,
    HideoutBonusType.weapon_repair_bonus,
]


class _HideoutBonusBase(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    id: str
    value: int
    passive: bool
    production: bool
    visible: bool
    icon: str | None = None


class HideoutAdditionalSlotsBonus(_HideoutBonusBase):
    type: Literal[HideoutBonusType.additional_slots]
    filter: list[ItemTemplateId]


class HideoutTextBonus(_HideoutBonusBase):
    type: Literal[HideoutBonusType.text]
    icon: str


class HideoutBonusStashSize(_HideoutBonusBase):
    type: Literal[HideoutBonusType.stash_size]
    template_id: ItemTemplateId


class HideoutGenericBonus(_HideoutBonusBase):
    type: GenericHideoutBonusType


class HideoutSkillGroupLevelingBonus(_HideoutBonusBase):
    type: Literal[HideoutBonusType.skill_group_leveling_boost]
    skill_type: str


AnyHideoutAreaBonus = Annotated[
    HideoutGenericBonus
    | HideoutAdditionalSlotsBonus
    | HideoutBonusStashSize
    | HideoutTextBonus
    | HideoutSkillGroupLevelingBonus,
    Discriminator("type"),
]
