from typing import Annotated, Literal

from pydantic import Discriminator

from lib.models import CamelCaseModel, Point
from lib.tarkov.bots.models import BodyPartType
from lib.tarkov.profiles.models import HideoutAreaType


class HideoutQuickTimeEvent(CamelCaseModel):
    type: Literal["ShrinkingCircle"]
    position: Point[float]
    speed: float
    success_range: Point[float]
    key: Literal["Mouse0"]
    start_delay: float
    end_delay: float


class QTEBodyBuffPartRequirement(CamelCaseModel):
    body_part: BodyPartType
    effect_name: Literal["Fracture"]
    excluded: bool
    type: Literal["BodyPartBuff"]


class QTEHealthRequirement(CamelCaseModel):
    energy: int
    hydration: int
    type: Literal["Health"]


HideoutQuickTimeEventRequirement = Annotated[
    QTEBodyBuffPartRequirement | QTEHealthRequirement,
    Discriminator("type"),
]


class _QTERewardBase(CamelCaseModel):
    weight: int
    result: Literal["Exit", "None"]


class QTEMusclePainReward(_QTERewardBase):
    time: int
    type: Literal["MusclePain"]


class LevelMultiplier(CamelCaseModel):
    level: int
    multiplier: float


class QTESkillReward(_QTERewardBase):
    skill_id: str
    level_multipliers: list[LevelMultiplier]
    type: Literal["Skill"]


class QTEArmTraumaReward(_QTERewardBase):
    type: Literal["GymArmTrauma"]


QTEReward = Annotated[
    QTEMusclePainReward | QTESkillReward | QTEArmTraumaReward,
    Discriminator("type"),
]


class QTEEffect(CamelCaseModel):
    energy: int
    hydration: int
    rewards_range: list[QTEReward]


class QTEResults(CamelCaseModel):
    finish_effect: QTEEffect
    single_success_effect: QTEEffect
    single_fail_effect: QTEEffect


class HideoutQuickTimeEvents(CamelCaseModel):
    id: str
    area: HideoutAreaType
    area_level: int
    quick_time_events: list[HideoutQuickTimeEvent]
    requirements: list[HideoutQuickTimeEventRequirement]
    results: QTEResults
