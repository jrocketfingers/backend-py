from datetime import timedelta

import pydantic
from pydantic import ConfigDict

from lib.models import CamelCaseModel
from lib.tarkov.hideout.models.bonuses import AnyHideoutAreaBonus
from lib.tarkov.hideout.models.requirements import AnyHideoutRequirement
from lib.tarkov.profiles.models import HideoutAreaType


class HideoutAreaImprovement(CamelCaseModel):
    id: str
    improvement_time: int | None = None
    requirements: list[AnyHideoutRequirement]
    bonuses: list[AnyHideoutAreaBonus]


class HideoutAreaStage(CamelCaseModel):
    model_config = ConfigDict(ser_json_timedelta="float")

    requirements: list[AnyHideoutRequirement]
    bonuses: list[AnyHideoutAreaBonus]
    slots: int
    construction_time: timedelta
    description: str
    container: str
    auto_upgrade: bool
    display_interface: bool
    improvements: list[HideoutAreaImprovement]


class HideoutAreaTemplate(CamelCaseModel):
    id: str = pydantic.Field(alias="_id")
    type: HideoutAreaType
    enabled: bool
    needs_fuel: bool
    take_from_slot_locked: bool
    craft_gives_exp: bool
    display_level: bool
    requirements: list[AnyHideoutRequirement]
    stages: dict[int, HideoutAreaStage]
    enable_area_requirements: bool
    parent_area: str | None = None
