import operator
from collections.abc import Callable
from typing import assert_never

import uuid_utils

from lib.tarkov.quests.models import CompareMethod
from lib.tarkov.types import ItemId

_length = 24


def generate_mongodb_id() -> ItemId:
    # https://datatracker.ietf.org/doc/html/draft-peabody-dispatch-new-uuid-format-04#name-uuid-version-7
    # We can generate a version 7 UUID and get rid of last 32 bits,
    # giving us 42 bits instead of 74 bits of randomness
    return ItemId(uuid_utils.uuid7().hex[:_length])


def compare_operator(method: CompareMethod) -> Callable[[float, float], bool]:
    if method == ">=":
        return operator.ge
    if method == ">":
        return operator.gt
    if method == "<":
        return operator.lt
    if method == "<=":
        return operator.le

    assert_never(method)
