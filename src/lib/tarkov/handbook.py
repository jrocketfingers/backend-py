from collections.abc import Sequence

from pydantic import ConfigDict

from lib.models import PascalCaseModel
from lib.tarkov.types import ItemTemplateId


class HandbookCategory(PascalCaseModel):
    id: str
    parent_id: str | None
    icon: str
    color: str
    order: str


class HandbookItem(PascalCaseModel):
    id: ItemTemplateId
    parent_id: str
    price: int


class Handbook(PascalCaseModel):
    model_config = ConfigDict(strict=False)

    categories: Sequence[HandbookCategory]
    items: Sequence[HandbookItem]
