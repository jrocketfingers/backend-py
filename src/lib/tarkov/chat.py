from __future__ import annotations

import enum
from typing import TYPE_CHECKING, Self

import pydantic

from lib.models import PascalCaseModel, StrictModel
from lib.tarkov.customization import PMCSide

if TYPE_CHECKING:
    from app.db.models import AccountProfile


class ChatMemberInfo(PascalCaseModel):
    nickname: str
    side: PMCSide
    level: int
    member_category: MemberCategory
    banned: bool = False
    ignored: bool = False
    is_trader: bool = False


class ChatMember(StrictModel):
    """Compact profile schema used by Tarkov in search and friend features."""

    id: str = pydantic.Field(alias="_id")
    account_id: int = pydantic.Field(alias="aid")
    info: ChatMemberInfo = pydantic.Field(alias="Info")

    @classmethod
    def from_profile(cls, profile: AccountProfile) -> Self:
        return cls(
            id=profile.profile_id,
            account_id=profile.account_id,
            info=ChatMemberInfo(
                nickname=profile.name,
                side=profile.side,
                level=profile.level,
                member_category=profile.member_category,
            ),
        )


class MemberCategory(enum.IntEnum):
    default = 0
    developer = 1
    unique_id = 2
    trader = 4
    group = 8
    system = 16
    chat_moderator = 32
    chat_moderator_with_permanent_ban = 64
    unit_test = 128
    sherpa = 256
    emissary = 512
