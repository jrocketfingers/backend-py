import enum
from typing import Literal

import pydantic

from lib.models import PascalCaseModel, StrictModel
from lib.tarkov._node import NodeBase
from lib.tarkov.locations.models import Prefab

PMCSide = Literal["Bear", "Usec"]
AnySide = Literal[PMCSide, "Savage"]


class Side(enum.StrEnum):
    pmc = "Pmc"
    scav = "Scav"


class CustomizationProps(PascalCaseModel):
    body_part: str | None = None
    description: str | None = None
    integrated_armor_vest: bool | None = None
    name: str | None = None
    short_name: str | None = None
    side: list[AnySide] | None = None
    prefab: Prefab | str | None = None

    available_as_default: bool | None = None
    body: str | None = None
    hands: str | None = None
    feet: str | None = None

    watch_position: dict[str, float] | None = None
    watch_prefab: dict[str, str] | None = None
    watch_rotation: dict[str, float] | None = None


class Customization(NodeBase):
    props: CustomizationProps


class Suits(StrictModel):
    id: str = pydantic.Field(alias="_id")
    suites: list[None]
