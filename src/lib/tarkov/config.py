from datetime import timedelta
from pathlib import Path
from typing import Generic, Literal, TypeVar

import pydantic
from pydantic import BaseModel

from lib.models import CamelCaseModel, MinMaxValue, StrictModel
from lib.tarkov.types import ItemTemplateId, QuestId

T = TypeVar("T")


class RandomValue(BaseModel, Generic[T]):
    values: list[T]
    weights: list[float | int] | None = None


class WeatherConfig(BaseModel):
    acceleration: int
    clouds: MinMaxValue[float]
    wind_speed: MinMaxValue[float]
    wind_direction: RandomValue[int]
    wind_gustiness: MinMaxValue[float]
    rain: RandomValue[int]
    rain_intensity: MinMaxValue[float]
    fog: MinMaxValue[float]
    temp: MinMaxValue[int]
    pressure: MinMaxValue[int]


class FileSystemStorageConfig(StrictModel):
    path: Path


class StorageConfig(StrictModel):
    mode: Literal["immediate", "deferred"]
    deferred_interval: timedelta
    filesystem: FileSystemStorageConfig


class QuestConfig(StrictModel):
    usec_only: list[QuestId]
    bear_only: list[QuestId]
    event_quests: set[QuestId]


class NotificationsConfig(StrictModel):
    class Broker(StrictModel):
        expiration_time: int

    backend: Broker


class RaidConfig(CamelCaseModel):
    class Menu(CamelCaseModel):
        ai_amount: str
        ai_difficulty: str
        boss_enabled: bool
        scav_wars: bool
        tagged_and_cursed: bool
        enable_pve: bool
        random_weather: bool
        random_time: bool

    class Save(CamelCaseModel):
        loot: bool

    mia_on_raid_end: bool = pydantic.Field(alias="MIAOnRaidEnd")
    raid_menu_settings: Menu
    save: Save
    car_extracts: list[str]
    coop_extracts: list[str]
    car_extract_base_standing_gain: float
    coop_extract_base_standing_gain: float
    scav_extract_gain: float
    pmc_kill_probability_for_scav_gain: float
    keep_found_in_raid_secure_container_on_death: bool = pydantic.Field(
        alias="keepFiRSecureContainerOnDeath",
    )
    player_scav_hostile_chance_percent: float

    scav_raid_min_time: timedelta


class BotConfig(CamelCaseModel):
    class Durability(CamelCaseModel):
        max_delta: int
        min_delta: int
        min_limit_percent: int
        lowest_max: int | None = None
        highest_max: int | None = None
        lowest_max_percent: int | None = None
        highest_max_percent: int | None = None

    class LootRandomization(CamelCaseModel):
        resource_percent: int
        chance_max_resource_percent: int

    class WalletLoot(CamelCaseModel):
        chance_percent: int
        item_count: MinMaxValue[int]
        stack_size_weight: dict[int, int]
        currency_weight: dict[ItemTemplateId, int]
        wallet_tpl_pool: list[ItemTemplateId]

    preset_batch: dict[str, int]
    bosses: set[str]
    durability: dict[str, dict[Literal["armor", "weapon"], Durability]]
    loot_item_resource_randomization: dict[
        str,
        dict[Literal["food", "meds"], LootRandomization],
    ]
    bot_roles_with_dog_tags: set[str]
    revenge: dict[str, set[str]]
    item_spawn_limits: dict[str, dict[ItemTemplateId, int]]
    equipment: dict[str, object]
    chance_assault_scav_has_player_scav_name: int
    secure_container_ammo_stack_count: int
    show_type_in_nickname: bool
    assault_brain_type: dict[str, dict[str, int]]
    player_scav_brain_type: dict[str, dict[str, int]]
    max_bot_cap: dict[str, int]
    wallet_loot: WalletLoot
    currency_stack_size: dict[str, dict[ItemTemplateId, dict[int, int]]]
