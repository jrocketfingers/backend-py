from pathlib import Path

asset_dir_path = Path("assets")
bots_dir_path = asset_dir_path.joinpath("bot", "bots")
static_files_dir = asset_dir_path.joinpath("static")
editions_dir = asset_dir_path.joinpath("editions")

achievements_path = asset_dir_path.joinpath("achievements.json")


static_url = "/files"
