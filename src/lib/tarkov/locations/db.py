import asyncio
from pathlib import Path
from typing import Final

from lib.tarkov.locations.models import Location, LocationsBase
from lib.utils import read_pydantic_json


async def _read_location(path: Path) -> Location:
    return await read_pydantic_json(path.joinpath("base.json"), Location)


async def read_locations(path: Path) -> list[Location]:
    tasks = []
    for d in path.iterdir():
        if not d.is_dir():
            continue  # pragma: no cover
        tasks.append(_read_location(d))
    return list(await asyncio.gather(*tasks))


class LocationDB:
    def __init__(
        self,
        locations: list[Location],
        base: LocationsBase,
    ) -> None:
        self.locations: Final = {location.id: location for location in locations}
        self.base = base

    def get(self, id: str) -> Location:
        return self.locations[id]

    def full_base(self) -> LocationsBase:
        return LocationsBase(
            locations={loc.id_1: loc for loc in self.locations.values()},
            paths=self.base.paths,
        )
