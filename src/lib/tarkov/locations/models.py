from typing import Any

import pydantic
from pydantic import AliasChoices, ConfigDict

from lib.models import PascalCaseModel, StrictModel
from lib.tarkov.types import ItemTemplateId


class AirdropParameters(PascalCaseModel):
    airdrop_point_deactivate_distance: int
    min_players_count_to_spawn_airdrop: int
    plane_airdrop_chance: float
    plane_airdrop_cooldown_min: int
    plane_airdrop_cooldown_max: int
    plane_airdrop_end: int
    plane_airdrop_max: int
    plane_airdrop_start_min: int
    plane_airdrop_start_max: int
    unsuccessful_try_penalty: int


class Prefab(StrictModel):
    path: str
    rcid: str


class LocationBanner(StrictModel):
    id: str
    pic: Prefab


class LocationBotModifier(PascalCaseModel):
    accuracy_speed: float
    dist_to_activate: int | None = None
    dist_to_persue_axeman_coef: float | None = None
    dist_to_sleep: int | None = None
    gain_sight: float
    khorovod_chance: float | None = None  # lol
    magnet_power: int | None = None
    marksman_accuracy_coef: float = pydantic.Field(alias="MarksmanAccuratyCoef")
    scattering: float
    visible_distance: float


class LocationMinMaxBots(StrictModel):
    wild_spawn_type: str = pydantic.Field(alias="WildSpawnType")
    min: int
    max: int


class LocationExit(PascalCaseModel):
    chance: int
    count: int | None = None
    entry_points: str
    event_available: bool | None = None
    exfiltration_time: int
    exfiltration_type: str | None = None
    id: str
    max_time: int
    min_time: int
    name: str
    passage_requirement: str | None = None
    players_count: int
    required_slot: str | None = None
    requirement_tip: str | None = None


class LocationMaxItemCount(PascalCaseModel):
    template_id: str
    value: int


class BotWave(StrictModel):
    bot_preset: str = pydantic.Field(alias="BotPreset")
    bot_side: str = pydantic.Field(alias="BotSide")
    spawn_points: str = pydantic.Field(alias="SpawnPoints")
    wild_spawn_type: str = pydantic.Field(alias="WildSpawnType")
    is_players: bool = pydantic.Field(alias="isPlayers")
    open_zones: str | None = pydantic.Field(None, alias="OpenZones")
    number: int
    slots_max: int
    slots_min: int
    time_max: int
    time_min: int


class LocationLimit(StrictModel):
    items: list[ItemTemplateId]
    min: int
    max: int


class Location(PascalCaseModel):
    model_config = ConfigDict(title="MapLocation")

    access_keys: list[str]
    airdrop_parameters: list[AirdropParameters] | None = None
    area: float
    average_play_time: int
    average_player_level: int
    banners: list[LocationBanner]
    boss_location_spawn: list[object]
    bot_assault: int
    bot_easy: int
    bot_hard: int
    bot_impossible: int
    bot_location_modifier: LocationBotModifier
    bot_marksman: int
    bot_max: int
    bot_max_player: int | None = None
    bot_max_time_player: int | None = None
    bot_normal: int
    bot_spawn_count_step: int | None = None
    bot_spawn_period_check: int | None = None
    bot_start_player: int | None = None
    bot_spawn_time_off_max: int
    bot_spawn_time_off_min: int
    bot_spawn_time_on_max: int
    bot_spawn_time_on_min: int
    bot_start: int
    bot_stop: int
    description: str | None = None
    disabled_for_scav: bool
    disabled_scav_exits: str  # coma separated values
    doors: list[str] = pydantic.Field(alias="doors")
    enabled: bool
    enable_coop: bool | None = pydantic.Field(
        None,
        validation_alias=AliasChoices("EnableCoop", "EnabledCoop"),
    )
    escape_time_limit: int
    escape_time_limit_coop: int = -1
    exit_access_time: int | None = pydantic.Field(None, alias="exit_access_time")
    exit_count: int | None = pydantic.Field(None, alias="exit_count")
    exit_time: int | None = None
    exit_zones: dict[str, str] = pydantic.Field(default_factory=dict)
    exits: list[LocationExit] = pydantic.Field(alias="exits")
    filter_ex: list[str] | None = pydantic.Field(None, alias="filter_ex")
    generate_local_loot_cache: bool = False
    global_loot_chance_modifier: float = 1
    global_container_chance_modifier: float = 1
    icon_x: int
    icon_y: int
    id: str
    id_1: str = pydantic.Field(alias="_Id")
    insurance: bool = False
    is_secret: bool
    limits: list[LocationLimit] = pydantic.Field(alias="limits")
    locked: bool
    loot: list[object]
    matching_min_seconds: int = pydantic.Field(0, alias="matching_min_seconds")
    match_maker_min_players_by_wait_time: list[Any] = pydantic.Field(
        default_factory=list,
    )
    max_bot_per_zone: int
    max_coop_group: int = -1
    max_dist_to_free_point: int
    max_item_count_location: list[LocationMaxItemCount] = pydantic.Field(
        default_factory=list,
        alias="maxItemCountInLocation",
    )
    max_players: int
    min_dist_to_exit_point: int | None = None
    min_dist_to_free_point: int
    min_max_bots: list[LocationMinMaxBots]
    min_players: int
    min_player_lvl_access_keys: int = -1
    name: str
    new_spawn: bool
    non_wave_group_scenario: Any | None = None
    occlusion_culling_enabled: bool = pydantic.Field(alias="OcculsionCullingEnabled")
    old_spawn: bool
    open_zones: str  # csv
    offline_new_spawn: bool | None = None
    offline_old_spawn: bool | None = None
    players_request_count: int = -1
    required_player_level_max: int = -1
    required_player_level_min: int = -1
    pmc_max_players_in_group: int = -1
    preview: Prefab | None = None
    required_player_level: int | None = None
    rules: str
    safe_location: bool | None = None
    sav_summon_seconds: int | None = pydantic.Field(None, alias="sav_summon_seconds")
    scav_max_players_in_group: int = -1
    scene: Prefab
    spawn_point_params: list[dict[str, Any]]
    tmp_location_field_remove_me: int = pydantic.Field(
        0,
        alias="tmp_location_field_remove_me",
    )  # lmao
    unix_date_time: int
    users_gather_seconds: int = pydantic.Field(0, alias="users_gather_seconds")
    users_spawn_seconds_n2: int = pydantic.Field(0, alias="users_spawn_seconds_n2")
    users_spawn_seconds_n: int = pydantic.Field(0, alias="users_spawn_seconds_n")
    users_summon_seconds: int = pydantic.Field(0, alias="users_summon_seconds")
    waves: list[BotWave]


class LocationPath(PascalCaseModel):
    source: str
    destination: str


class LocationsBase(StrictModel):
    locations: dict[str, Location]
    paths: list[LocationPath]
