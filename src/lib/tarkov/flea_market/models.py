import pydantic

from lib.models import CamelCaseModel, DatetimeInt
from lib.tarkov.chat import MemberCategory
from lib.tarkov.items.models import Item
from lib.tarkov.types import ItemTemplateId


class FleaMarketOfferUser(CamelCaseModel):
    id: str
    nickname: str | None = None
    rating: float | None = None
    member_type: MemberCategory
    avatar: str | None = None
    is_rating_growing: bool | None = None


class FleaMarketOfferRequirement(CamelCaseModel):
    template_id: ItemTemplateId = pydantic.Field(alias="_tpl")
    count: int
    only_functional: bool


class FleaMarketOffer(CamelCaseModel):
    id: str = pydantic.Field(alias="_id")
    int_id: int
    items: list[Item]
    requirements: list[FleaMarketOfferRequirement]
    root: str
    items_cost: int
    requirements_cost: int
    start_time: DatetimeInt
    end_time: DatetimeInt
    sell_in_one_piece: bool
    loyalty_level: int
    buy_restriction_max: int | None = None
    buy_restriction_current: int | None = None
    locked: bool
    unlimited_count: bool
    summary_cost: int
    user: FleaMarketOfferUser
    current_item_count: int
    priority: bool
    not_available: bool
