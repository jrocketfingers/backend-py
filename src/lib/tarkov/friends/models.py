from lib.models import PascalCaseModel
from lib.tarkov.chat import ChatMember


class FriendList(PascalCaseModel):
    friends: list[ChatMember]
    ignore: list[str]
    in_ignore_list: list[str]
