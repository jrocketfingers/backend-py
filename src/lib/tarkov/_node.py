from typing import Literal, TypeVar

from pydantic import BaseModel, ConfigDict

from lib.models import WithPrefix
from lib.tarkov.types import ItemTemplateId

TProps = TypeVar("TProps")


class NodeBase(BaseModel):
    model_config = ConfigDict(
        populate_by_name=True,
        alias_generator=WithPrefix("_"),
        extra="forbid",
    )

    id: ItemTemplateId
    name: str
    parent: ItemTemplateId
    type: Literal["Node", "Item"]
    proto: str | None = None
