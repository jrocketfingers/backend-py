from lib.models import CamelCaseModel


class UserBuilds(CamelCaseModel):
    weapon_builds: list[None]
    equipment_builds: list[None]
    magazine_builds: list[None]
