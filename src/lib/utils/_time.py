from datetime import UTC, datetime


def utc_now() -> datetime:
    return datetime.now(tz=UTC)


def utc_now_seconds() -> datetime:
    return datetime.now(tz=UTC).replace(microsecond=0)


utc_min = datetime.fromtimestamp(0, tz=UTC)
