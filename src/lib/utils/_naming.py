def snake_to_camel(name: str) -> str:
    first, *rest = name.split("_")
    return first + "".join(map(str.capitalize, rest))


def snake_to_pascal(value: str) -> str:
    return "".join(map(str.capitalize, value.split("_")))
