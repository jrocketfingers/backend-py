import contextlib
import logging.config
import shutil
import typing
from collections.abc import Iterator
from pathlib import Path
from typing import Any, TypeVar

import aiofiles
import orjson
import yaml
from pydantic import BaseModel, TypeAdapter

from lib.types import PathOrStr

T = TypeVar("T")
TBaseModel = TypeVar("TBaseModel", bound=BaseModel)


async def read_json(path: PathOrStr) -> Any:  # noqa: ANN401
    async with aiofiles.open(path, encoding="utf8") as f:
        return orjson.loads(await f.read())


@typing.overload
async def read_pydantic_json(path: PathOrStr, model: TypeAdapter[T]) -> T: ...


@typing.overload
async def read_pydantic_json(
    path: PathOrStr,
    model: type[TBaseModel],
) -> TBaseModel: ...


async def read_pydantic_json(
    path: PathOrStr,
    model: TypeAdapter[T] | type[TBaseModel],
) -> T | TBaseModel:
    logging.debug("read_pydantic_json: %s", path)
    async with aiofiles.open(path, encoding="utf8") as f:
        contents = orjson.loads(await f.read())
    # https://github.com/pydantic/pydantic/issues/8133
    try:
        if isinstance(model, TypeAdapter):
            return model.validate_python(contents, strict=False)
        return model.model_validate(contents, strict=False)
    except Exception as e:
        e.add_note(f"Path: {path}")
        raise


async def read_pydantic_yaml(
    path: PathOrStr,
    model: type[TBaseModel],
) -> TBaseModel:
    async with aiofiles.open(path, encoding="utf8") as f:
        contents = yaml.load(await f.read(), Loader=yaml.CSafeLoader)
    return model.model_validate(contents, strict=False)


@contextlib.contextmanager
def safe_rename(
    src: PathOrStr,
    dest: PathOrStr,
) -> Iterator[None]:
    try:
        yield
        shutil.move(src, dest)
    finally:
        Path(src).unlink(missing_ok=True)
