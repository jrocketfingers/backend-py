import contextlib
import enum
import math
from datetime import datetime
from typing import Annotated, Generic, TypeVar

from pydantic import BaseModel, ConfigDict, PlainSerializer

from lib.utils import snake_to_camel, snake_to_pascal

T = TypeVar("T")

DatetimeFloat = Annotated[
    datetime,
    PlainSerializer(lambda d: d.timestamp(), return_type=float, when_used="json"),
]
DatetimeInt = Annotated[
    datetime,
    PlainSerializer(lambda d: int(d.timestamp()), return_type=int, when_used="json"),
]


def round_validator(obj: object) -> object:
    if not isinstance(obj, float):
        return obj
    return math.ceil(obj)


class CoerceEnumValues:
    def __init__(self, enum_cls: type[enum.Enum]) -> None:
        self.enum_cls = enum_cls

    def __call__(self, obj: object) -> object:
        if isinstance(obj, str):
            with contextlib.suppress(KeyError):
                return self.enum_cls[obj]

        return obj


class NullIf:
    def __init__(self, value: object) -> None:
        self._value = value

    def __call__(self, obj: object) -> object:
        if obj == self._value:  # pragma: no cover
            return None

        return obj


class StrictModel(BaseModel):
    model_config = ConfigDict(
        populate_by_name=True,
        extra="forbid",
        strict=True,
    )


class Point(StrictModel, Generic[T]):
    x: T
    y: T


class Vector(StrictModel, Generic[T]):
    x: T
    y: T
    z: T


class RGBAColor(StrictModel):
    r: int
    g: int
    b: int
    a: int


class MinMaxValue(StrictModel, Generic[T]):
    min: T
    max: T


class WithPrefix:
    def __init__(self, prefix: str) -> None:
        self._prefix = prefix

    def __call__(self, value: str) -> str:
        return self._prefix + value


class CamelCaseModel(StrictModel):
    model_config = ConfigDict(
        alias_generator=snake_to_camel,
    )


class PascalCaseModel(StrictModel):
    model_config = ConfigDict(
        alias_generator=snake_to_pascal,
    )
