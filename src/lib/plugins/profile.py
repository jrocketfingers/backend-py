from typing import Protocol, runtime_checkable

from lib.tarkov.profiles.dto import ProfileContext


@runtime_checkable
class OnProfileLoadPlugin(Protocol):
    """Called when profile is loaded from the storage."""

    async def on_profile_load(
        self,
        profile_context: ProfileContext,
    ) -> None: ...


@runtime_checkable
class OnProfileCreatePlugin(Protocol):
    """Called when profile is first created by the user."""

    async def on_profile_create(
        self,
        profile_context: ProfileContext,
    ) -> None: ...


@runtime_checkable
class OnProfileChangePlugin(Protocol):
    """Called when profile was potentially changed."""

    async def on_profile_change(
        self,
        profile_context: ProfileContext,
    ) -> None: ...


ProfilePlugin = OnProfileLoadPlugin | OnProfileCreatePlugin | OnProfileChangePlugin
