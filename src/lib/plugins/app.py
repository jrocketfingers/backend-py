from typing import Protocol, runtime_checkable


@runtime_checkable
class AppOnInitPlugin(Protocol):
    async def on_app_init(self) -> None: ...


AppPlugin = AppOnInitPlugin
