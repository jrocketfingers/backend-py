from typing import TypeVar, assert_never, final

from aioinject import Container, InjectionContext, Scoped
from aioinject.context import context_var

from lib.plugins.app import AppOnInitPlugin, AppPlugin
from lib.plugins.profile import (
    OnProfileChangePlugin,
    OnProfileCreatePlugin,
    OnProfileLoadPlugin,
    ProfilePlugin,
)
from lib.tarkov.profiles.dto import ProfileContext

Plugin = ProfilePlugin | AppPlugin

TPlugin = TypeVar("TPlugin", bound=Plugin)


async def _resolve(plugin: type[TPlugin]) -> TPlugin:  # Dirty hack
    context = context_var.get()
    assert isinstance(context, InjectionContext)  # noqa: S101 # pragma: no branch
    return await context.resolve(plugin)


@final
class AppPluginRegistry(AppOnInitPlugin):
    def __init__(self) -> None:
        self._on_init: list[type[AppOnInitPlugin]] = []

    def register(self, plugin: type[AppPlugin]) -> None:
        if issubclass(plugin, AppOnInitPlugin):  # pragma: no branch
            self._on_init.append(plugin)

    async def on_app_init(self) -> None:
        for plugin_cls in self._on_init:
            plugin = await _resolve(plugin_cls)
            await plugin.on_app_init()


@final
class ProfilePluginRegistry(
    OnProfileCreatePlugin,
    OnProfileLoadPlugin,
    OnProfileChangePlugin,
):
    def __init__(self) -> None:
        self._on_load: list[type[OnProfileLoadPlugin]] = []
        self._on_create: list[type[OnProfileCreatePlugin]] = []
        self._on_change: list[type[OnProfileChangePlugin]] = []

    def register(self, plugin: type[ProfilePlugin]) -> None:
        if issubclass(plugin, OnProfileLoadPlugin):  # pragma: no branch
            self._on_load.append(plugin)
        if issubclass(plugin, OnProfileCreatePlugin):  # pragma: no branch
            self._on_create.append(plugin)
        if issubclass(plugin, OnProfileChangePlugin):  # pragma: no branch
            self._on_change.append(plugin)

    async def on_profile_create(self, context: ProfileContext) -> None:
        for plugin_cls in self._on_create:
            plugin = await _resolve(plugin_cls)
            await plugin.on_profile_create(profile_context=context)

    async def on_profile_load(self, context: ProfileContext) -> None:
        for plugin_cls in self._on_load:
            plugin = await _resolve(plugin_cls)
            await plugin.on_profile_load(profile_context=context)

    async def on_profile_change(self, context: ProfileContext) -> None:
        for plugin_cls in self._on_change:
            plugin = await _resolve(plugin_cls)
            await plugin.on_profile_change(profile_context=context)


class PluginRegistry:
    def __init__(self, container: Container) -> None:
        self._container = container
        self.profile = ProfilePluginRegistry()
        self.app = AppPluginRegistry()

    def register(self, plugin: type[Plugin]) -> None:
        self._container.register(Scoped(plugin))
        if issubclass(
            plugin,
            OnProfileLoadPlugin | OnProfileCreatePlugin | OnProfileChangePlugin,
        ):
            self.profile.register(plugin)
        elif issubclass(plugin, AppOnInitPlugin):
            self.app.register(plugin)
        else:
            assert_never(plugin)
