from collections.abc import Sequence
from http import HTTPStatus
from typing import Annotated, Literal

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, Request, post
from litestar.datastructures import State
from pydantic import BaseModel
from result import Err

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.profiles._schema import (
    ProfileCreateResponseSchema,
    ProfileCreateSchema,
    ProfileNicknameValidateSchema,
    ProfileSearchSchema,
    ProfileSelectRequestSchema,
    ProfileStatusesSchema,
    ProfileStatusSchema,
)
from app.adapters.api.schema import (
    Error,
    NotificationChannelSchema,
    Success,
)
from app.adapters.api.ws import get_ws_channel
from app.core.domain.accounts.commands import ValidateNicknameCommand
from app.core.domain.accounts.errors import UsernameIsTaken, UsernameTooShort
from app.core.domain.accounts.queries import ProfileSearchQuery
from app.core.domain.customization.query import AccountCustomizationQuery
from app.core.domain.profiles.commands import ProfileCreateCommand, ProfileLogoutCommand
from app.core.domain.profiles.dto import ProfileCreateDTO
from app.core.domain.profiles.queries import ProfileListQuery, ProfileStatusQuery
from app.db.models import Account, AccountProfile
from lib.tarkov.chat import ChatMember
from lib.tarkov.profiles.models import Profile, ScavProfile


class ProfilesController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/game/profile/list",
        status_code=HTTPStatus.OK,
        name="profile_list",
        type_encoders={
            BaseModel: lambda model: model.model_dump(
                mode="json",
                by_alias=True,
                exclude_none=True,
            ),
        },
    )
    @inject
    async def profile_list(
        self,
        account: Account,
        query: Annotated[ProfileListQuery, Inject],
    ) -> Success[tuple[ScavProfile, Profile] | tuple[()]]:
        result = await query.execute(account=account)
        return Success(data=result)

    @post(
        "/client/game/profile/select",
        name="profile_select",
        status_code=HTTPStatus.OK,
    )
    async def profile_select(
        self,
        data: ProfileSelectRequestSchema,  # noqa: ARG002
        request: Request[None, None, State],
        account: Account,
    ) -> Success[NotificationChannelSchema]:
        return Success(data=get_ws_channel(request=request, account=account))

    @post(
        "/client/profile/status",
        name="profile_status",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def profile_status(
        self,
        account: Account,
        query: Annotated[ProfileStatusQuery, Inject],
    ) -> Success[ProfileStatusesSchema]:
        statuses = await query.execute(account=account)
        data = ProfileStatusesSchema(
            max_pve_count_exceeded=statuses.max_pve_count_exceeded,
            profiles=[
                ProfileStatusSchema(
                    profile_id=profile.profile_id,
                    profile_token=profile.profile_token,
                    status=profile.status,
                    sid=profile.session_id,
                    ip=profile.ip,
                    port=profile.port,
                )
                for profile in statuses.profiles
            ],
        )
        return Success(data=data)

    @post(
        "/client/game/profile/search",
        name="profile_search",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def profile_search(
        self,
        query_: Annotated[ProfileSearchQuery, Inject],
        data: ProfileSearchSchema,
        account_profile: AccountProfile,
    ) -> Success[Sequence[ChatMember]]:
        profiles = await query_.execute(nickname=data.nickname, profile=account_profile)
        result = [ChatMember.from_profile(profile) for profile in profiles]
        return Success(data=result)

    @post(
        "/client/account/customization",
        name="account_customization",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def account_customization(
        self,
        query: Annotated[AccountCustomizationQuery, Inject],
    ) -> Success[Sequence[str]]:
        return Success(data=await query.execute())

    @post(
        "/client/game/profile/nickname/reserved",
        name="profile_nickname_reserved",
        status_code=HTTPStatus.OK,
    )
    async def profile_nickname_reserved(self) -> Success[Literal[""]]:
        return Success(data="")

    @post(
        "/client/game/profile/nickname/validate",
        name="profile_nickname_validate",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def profile_nickname_validate(
        self,
        data: ProfileNicknameValidateSchema,
        command: Annotated[ValidateNicknameCommand, Inject],
    ) -> Success[dict[str, str]] | Error:
        result = await command.execute(data.username)
        if isinstance(result, Err):
            match result.err_value:
                case UsernameIsTaken():
                    return Error(
                        error_code=255,
                        error_message="The nickname is already in use",
                    )
                case UsernameTooShort():  # pragma: no branch
                    return Error(
                        error_code=256,
                        error_message="The nickname is too short",
                    )

        return Success(data={"status": "ok"})

    @post(
        "/client/game/profile/create",
        name="profile_create",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def profile_create(
        self,
        data: ProfileCreateSchema,
        account: Account,
        command: Annotated[ProfileCreateCommand, Inject],
    ) -> Success[ProfileCreateResponseSchema]:
        result = await command.execute(
            account=account,
            dto=ProfileCreateDTO(
                name=data.nickname,
                side=data.side,
                voice_id=data.voice_id,
                head_id=data.head_id,
            ),
        )
        return Success(data=ProfileCreateResponseSchema(profile_id=str(result.id)))

    @post(
        "/client/game/logout",
        name="client_game_logout",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def logout(
        self,
        account: Account,
        command: Annotated[ProfileLogoutCommand, Inject],
    ) -> Success[None]:
        await command.execute(account=account)
        return Success(data=None)
