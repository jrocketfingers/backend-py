import pydantic
from pydantic import ConfigDict

from lib.models import StrictModel
from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.models import ProfileStatus
from lib.utils import snake_to_camel


class ProfileSelectRequestSchema(StrictModel):
    uid: str


class ProfileStatusSchema(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    profile_id: str = pydantic.Field(alias="profileid")
    profile_token: str | None = pydantic.Field(alias="profileToken")
    status: ProfileStatus
    sid: str
    ip: str
    port: int


class ProfileStatusesSchema(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    max_pve_count_exceeded: bool
    profiles: list[ProfileStatusSchema]


class ProfileCreateSchema(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    side: PMCSide
    nickname: str
    head_id: str
    voice_id: str


class ProfileCreateResponseSchema(StrictModel):
    profile_id: str = pydantic.Field(alias="uid")


class ProfileNicknameValidateSchema(StrictModel):
    username: str = pydantic.Field(alias="nickname")


class ProfileSearchSchema(StrictModel):
    nickname: str
