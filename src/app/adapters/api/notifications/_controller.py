import asyncio
from http import HTTPStatus
from typing import Annotated

from aioinject import Container, Inject
from aioinject.ext.litestar import inject
from litestar import Controller, Request, WebSocket, post, websocket
from litestar.datastructures import State
from sqlalchemy.orm import joinedload

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.notifications._websocket import (
    receive_notifications,
)
from app.adapters.api.schema import NotificationChannelSchema, Success
from app.adapters.api.ws import get_ws_channel
from app.core.domain.accounts.repositories import AccountRepository
from app.db.models import Account
from lib.tarkov.notifications.service import NotificationService


class NotificationController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/notifier/channel/create",
        status_code=HTTPStatus.OK,
        name="notification_channel_create",
    )
    async def notification_channel_create(
        self,
        request: Request[None, None, State],
        account: Account,
    ) -> Success[NotificationChannelSchema]:
        return Success(data=get_ws_channel(request=request, account=account))

    @websocket(
        "/ws/{session_id:str}",
        name="notifications_wss",
    )
    @inject
    async def ws(  # pragma: no cover
        self,
        session_id: str,
        socket: WebSocket[None, None, State],
        notification_service: Annotated[NotificationService, Inject],
        container: Annotated[Container, Inject],
    ) -> None:
        async with container.context() as ctx:
            account_repo = await ctx.resolve(AccountRepository)
            account = await account_repo.get(
                session_id=session_id,
                options=(joinedload(Account.profile),),
            )
            if not account or not account.profile:
                raise ValueError
            profile_id = str(account.profile.id)
        await socket.accept()
        timeout = asyncio.Timeout(None)
        async with asyncio.TaskGroup() as tg:
            tg.create_task(
                receive_notifications(
                    socket=socket,
                    notification_service=notification_service,
                    profile_id=profile_id,
                    timeout=timeout,
                ),
            )
            try:
                async for _ in socket.iter_data("binary"):
                    pass
            finally:
                timeout.reschedule(-1)
