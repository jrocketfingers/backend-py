import asyncio
import contextlib
import logging

from litestar import WebSocket
from litestar.datastructures import State

from lib.tarkov.notifications.service import NotificationService


async def receive_notifications(  # pragma: no cover
    socket: WebSocket[None, None, State],
    notification_service: NotificationService,
    profile_id: str,
    timeout: asyncio.Timeout,
) -> None:
    with contextlib.suppress(TimeoutError):
        async with timeout:
            async for message in notification_service.subscribe(profile_id=profile_id):
                text = message.model_dump_json(by_alias=True, exclude_none=True)
                logging.debug("Sending WebSocket event to %s: %s", profile_id, text)
                await socket.send_text(text)
