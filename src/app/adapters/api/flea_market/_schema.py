import enum
from typing import Annotated

from pydantic import AfterValidator, BeforeValidator

from app.core.domain.models import FleaMarketSort, FleaMarketSortDirection
from lib.models import CamelCaseModel, NullIf
from lib.tarkov.chat import MemberCategory
from lib.tarkov.types import ItemTemplateId

IntOrNull = Annotated[int | None, AfterValidator(NullIf(0))]


class FleaSearchCurrencyIds(enum.IntEnum):
    RUB = 1
    USD = 2
    EUR = 3


class FleaMarketSearchSchema(CamelCaseModel):
    condition_from: IntOrNull
    condition_to: IntOrNull
    price_from: IntOrNull
    price_to: IntOrNull
    quantity_from: IntOrNull
    quantity_to: IntOrNull
    page: int
    limit: int
    sort_type: FleaMarketSort
    sort_direction: FleaMarketSortDirection
    currency: Annotated[FleaSearchCurrencyIds | None, BeforeValidator(NullIf(0))]
    one_hour_expiration: bool
    remove_bartering: bool
    offer_owner_type: MemberCategory
    only_functional: bool
    update_offer_count: bool
    handbook_id: str
    linked_search_id: Annotated[ItemTemplateId | None, AfterValidator(NullIf(""))]
    needed_search_id: str
    build_items: dict[object, object]
    build_count: int
    tm: int
    reload: int
