from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post
from pydantic import BaseModel

from app.adapters.api.flea_market._schema import FleaMarketSearchSchema
from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.flea_market.dto import (
    FleaMarketSearchDTO,
    FleaMarketSearchResultSchema,
)
from app.core.domain.flea_market.queries import FleaMarketSearchQuery
from app.core.dto import MinMaxDTO
from lib.tarkov.trading.models import CurrencyIds


class FleaMarketController(Controller):
    middleware = (ZLibMiddleware,)

    @post(
        "/client/ragfair/find",
        name="client_ragfair_find",
        status_code=HTTPStatus.OK,
        type_encoders={
            BaseModel: lambda model: model.model_dump(
                mode="json",
                by_alias=True,
                exclude_none=True,
            ),
        },
    )
    @inject
    async def find(
        self,
        data: FleaMarketSearchSchema,
        query_: Annotated[FleaMarketSearchQuery, Inject],
    ) -> Success[FleaMarketSearchResultSchema]:
        dto = FleaMarketSearchDTO(
            price=MinMaxDTO(
                min=data.price_from,
                max=data.price_to,
            ),
            quantity=MinMaxDTO(
                min=data.quantity_from,
                max=data.quantity_to,
            ),
            condition=MinMaxDTO(
                min=data.condition_from,
                max=data.condition_to,
            ),
            sort=data.sort_type,
            sort_direction=data.sort_direction,
            page_size=data.limit,
            page=data.page,
            handbook_id=data.handbook_id,
            exclude_bartering_offers=data.remove_bartering,
            include_categories=data.update_offer_count,
            currency_id=(
                CurrencyIds[data.currency.name].value if data.currency else None
            ),
            linked_search_id=data.linked_search_id,
        )
        result = await query_.execute(dto=dto)
        return Success(data=result)
