import asyncio
import contextlib
from asyncio import TaskGroup
from collections.abc import AsyncIterator

from app.core.di import create_container
from app.core.domain.tasks import task_classes
from lib.tasks import run_task_forever


@contextlib.asynccontextmanager
async def tasks_lifespan(_: object) -> AsyncIterator[None]:  # pragma: no cover
    container = create_container()
    stop = asyncio.Event()
    async with TaskGroup() as tg:
        for task_cls in task_classes:
            tg.create_task(
                run_task_forever(container=container, task_cls=task_cls, stop=stop),
            )
        yield
        stop.set()
