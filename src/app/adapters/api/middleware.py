import contextlib
import logging
import time
import zlib

from litestar.datastructures import MutableScopeHeaders
from litestar.enums import ScopeType
from litestar.middleware import MiddlewareProtocol
from litestar.types import (
    ASGIApp,
    HTTPReceiveMessage,
    HTTPResponseStartEvent,
    Message,
    Receive,
    Scope,
    Send,
    WebSocketReceiveMessage,
)

from lib.utils.http import http_is_err


async def _unattached_send(_: Message) -> None:
    raise NotImplementedError


class StripUnityContentEncoding(MiddlewareProtocol):
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(
        self,
        scope: Scope,
        receive: Receive,
        send: Send,
    ) -> None:
        if scope["type"] != ScopeType.HTTP:  # pragma: no cover
            await self.app(scope, receive, send)
            return

        request_headers = MutableScopeHeaders(scope)
        should_strip_header = request_headers.get(
            "user-agent",
            "",
        ).startswith("UnityPlayer")

        async def send_wrapper(message: Message) -> None:
            if message["type"] == "http.response.start" and should_strip_header:
                headers = MutableScopeHeaders.from_message(message)
                del headers["content-encoding"]
            await send(message)

        await self.app(scope, receive, send_wrapper)


class ZLibResponder:
    def __init__(self, app: ASGIApp) -> None:
        self.app = app
        self.send: Send = _unattached_send
        self.initial_message: HTTPResponseStartEvent | None = None

    async def __call__(
        self,
        scope: Scope,
        receive: Receive,
        send: Send,
    ) -> None:
        self.send = send
        await self.app(scope, receive, self.send_zlib)

    async def send_zlib(self, message: Message) -> None:
        if message["type"] == "http.response.start":
            self.initial_message = message
            return

        if (  # pragma: no branch
            self.initial_message and message["type"] == "http.response.body"
        ):
            if not http_is_err(self.initial_message["status"]):
                message["body"] = zlib.compress(message["body"])
                headers = MutableScopeHeaders(self.initial_message)
                headers["Content-Encoding"] = "deflate"
                headers["Content-Length"] = str(len(message["body"]))
            await self.send(self.initial_message)

        await self.send(message)


class ZLibMiddleware(MiddlewareProtocol):
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(
        self,
        scope: Scope,
        receive: Receive,
        send: Send,
    ) -> None:
        if scope["type"] != ScopeType.HTTP:  # pragma: no cover
            await self.app(scope, receive, send)
            return

        async def receive_wrapper() -> HTTPReceiveMessage | WebSocketReceiveMessage:
            message = await receive()
            if message["type"] == "http.request":  # pragma: no branch
                with contextlib.suppress(zlib.error):
                    message["body"] = zlib.decompress(message["body"])
            return message

        responder = ZLibResponder(self.app)
        await responder(scope, receive_wrapper, send)


class PerfMiddleware(MiddlewareProtocol):
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        t = time.perf_counter()
        await self.app(scope, receive, send)
        elapsed_ms = (time.perf_counter() - t) * 1000
        logging.info("%s %.4fms", scope.get("path"), elapsed_ms)
