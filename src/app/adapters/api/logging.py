import logging
from http import HTTPStatus
from logging import LogRecord
from typing import Protocol, final

from litestar.exceptions import HTTPException
from litestar.logging import LoggingConfig

from app.settings import LoggingSettings
from lib.settings import get_settings


class ExceptionFilter(Protocol):
    def filter_(self, exc_type: type[BaseException], exc: BaseException) -> bool: ...


@final
class NotFoundHTTPExceptionFilter(ExceptionFilter):
    def filter_(
        self,
        exc_type: type[BaseException],  # noqa: ARG002
        exc: BaseException,
    ) -> bool:  # pragma: no cover
        if not isinstance(exc, HTTPException):
            return True

        return exc.status_code != HTTPStatus.NOT_FOUND


class LoggingExceptionFilter(logging.Filter):
    def __init__(
        self,
        filters_: tuple[ExceptionFilter, ...] = (),
    ) -> None:
        super().__init__()
        self.filters = filters_

    def filter(self, record: LogRecord) -> bool:  # pragma: no cover
        if not record.exc_info:
            return True
        exc_type, exc, _ = record.exc_info
        if exc_type is None or exc is None:
            return True
        return all(f.filter_(exc_type, exc) for f in self.filters)


def get_logging_config() -> LoggingConfig:
    settings = get_settings(LoggingSettings)
    logging_config = LoggingConfig(
        log_exceptions="always",
        filters={
            "exception_filter": {
                "()": LoggingExceptionFilter,
                "filters_": (NotFoundHTTPExceptionFilter(),),
            },
        },
        root={"level": settings.level.name},
    )
    logging_config.handlers["queue_listener"].setdefault("filters", []).append(
        "exception_filter",
    )
    return logging_config
