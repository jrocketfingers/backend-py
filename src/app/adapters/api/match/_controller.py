from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, Request, post
from litestar.datastructures import State

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.startup.queries import ClientGameConfigQuery
from app.db.models import Account
from lib.tarkov.backend_info import ClientGameConfig


class MatchController(Controller):
    middleware = (ZLibMiddleware,)
    path = "/client/match"

    @post(
        "/group/exit_from_menu",
        name="match_group_exit_from_menu",
        status_code=HTTPStatus.OK,
    )
    async def group_exit_from_menu(
        self,
        account: Account,  # noqa: ARG002
    ) -> Success[None]:
        return Success(data=None)

    @post(
        "/group/current",
        name="match_group_current",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def match_group_current(
        self,
        query: Annotated[ClientGameConfigQuery, Inject],
        request: Request[None, None, State],
        account: Account,
    ) -> Success[ClientGameConfig]:
        return Success(
            data=await query.execute(account=account, base_url=str(request.base_url)),
        )
