from lib.models import CamelCaseModel, PascalCaseModel
from lib.tarkov.bots.models import BodyPartType
from lib.tarkov.customization import Side
from lib.tarkov.profiles.models import Health


class BSGLoggingSchema(CamelCaseModel):
    verbosity: int
    send_to_server: bool = False


class AKIVersionSchema(PascalCaseModel):
    version: str


class AKIReleaseInfo(CamelCaseModel):
    beta_disclaimer_text: str | None
    beta_disclaimer_accept_text: str
    server_mods_loaded_text: str
    server_mods_loaded_debug_text: str
    client_mods_loaded_text: str
    client_mods_loaded_debug_text: str
    illegal_plugins_loaded_text: str
    illegal_plugins_exception_text: str
    release_summary_text: str | None
    is_beta: bool | None
    is_moddable: bool | None
    is_modded: bool
    beta_disclaimer_timeout_delay: int


class HealthSyncSchema(PascalCaseModel):
    health: dict[BodyPartType, Health]
    is_alive: bool
    hydration: float | None = None
    energy: float | None = None
    temperature: float | None = None


class RaidTimeRequestSchema(PascalCaseModel):
    side: Side
    location: str


class RaidTimeResponseSchema(PascalCaseModel):
    class ExitChange(PascalCaseModel):
        name: str
        min_time: int | None = None
        max_time: int | None = None
        chance: int | None = None

    raid_time_minutes: int
    new_survive_time_seconds: int
    original_survival_time_seconds: int
    exit_changes: list[ExitChange]
