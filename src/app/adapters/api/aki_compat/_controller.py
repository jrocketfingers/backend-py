from http import HTTPStatus
from typing import Annotated, Literal, assert_never

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, get, post
from litestar.exceptions import HTTPException
from result import Err

from app.adapters.api.aki_compat.schema import (
    AKIReleaseInfo,
    AKIVersionSchema,
    BSGLoggingSchema,
    HealthSyncSchema,
    RaidTimeRequestSchema,
    RaidTimeResponseSchema,
)
from app.adapters.api.middleware import ZLibMiddleware
from app.core.domain.aki_compat.queries import RaidTimeQuery
from app.core.domain.bots.errors import NoRaidInfoError
from app.core.domain.bots.queries import (
    BotDifficultyQuery,
    BotMaxCapQuery,
    BotPresetLimitQuery,
)
from app.db.models import AccountProfile
from lib.tarkov.bots.models import BotDifficulty, BotDifficultyKey
from lib.tarkov.config import RaidConfig
from lib.tarkov.profiles.abc import ProfileStorage
from lib.utils import utc_now


class AkiCompatabilityController(Controller):  # pragma: no cover
    middleware = (ZLibMiddleware,)

    @get("/singleplayer/bundles")
    async def bundles(self) -> list[None]:
        return []

    @get("/singleplayer/release")
    async def release(self) -> AKIReleaseInfo:
        return AKIReleaseInfo(
            beta_disclaimer_timeout_delay=30,
            beta_disclaimer_accept_text="",
            beta_disclaimer_text="",
            client_mods_loaded_debug_text="",
            client_mods_loaded_text="",
            illegal_plugins_exception_text="",
            illegal_plugins_loaded_text="",
            server_mods_loaded_debug_text="",
            server_mods_loaded_text="",
            release_summary_text="",
            is_beta=False,
            is_moddable=True,
            is_modded=False,
        )

    @get("/singleplayer/settings/version")
    async def version(self) -> AKIVersionSchema:
        return AKIVersionSchema(
            version="Game Version",
        )

    @get("/singleplayer/enableBSGlogging")
    async def bsg_logging(self) -> BSGLoggingSchema:
        return BSGLoggingSchema(verbosity=2)

    @post("/singleplayer/log")
    async def log(self) -> None:
        return None

    @post("/singleplayer/settings/getRaidTime", status_code=HTTPStatus.OK)
    @inject
    async def raid_time(
        self,
        query: Annotated[RaidTimeQuery, Inject],
        data: RaidTimeRequestSchema,
    ) -> RaidTimeResponseSchema:
        result = await query.execute(
            side=data.side,
            location_id=data.location,
        )
        return RaidTimeResponseSchema(
            raid_time_minutes=int(result.time_left.total_seconds() / 60),
            new_survive_time_seconds=60,
            original_survival_time_seconds=int(
                result.original_required_survival_time.total_seconds(),
            ),
            exit_changes=[],
        )

    @post("/player/health/sync", status_code=HTTPStatus.OK)
    @inject
    async def health_sync(  # pragma: no cover
        self,
        account_profile: AccountProfile,
        data: HealthSyncSchema,
        profile_storage: Annotated[ProfileStorage, Inject],
    ) -> None:
        async with profile_storage.profile(
            profile_id=str(account_profile.id),
        ) as context:
            health = context.pmc.health

            health.body_parts.chest.health = data.health["Chest"]
            health.body_parts.head.health = data.health["Head"]
            health.body_parts.stomach.health = data.health["Stomach"]
            health.body_parts.left_arm.health = data.health["LeftArm"]
            health.body_parts.right_arm.health = data.health["RightArm"]
            health.body_parts.left_leg.health = data.health["LeftLeg"]
            health.body_parts.right_leg.health = data.health["RightLeg"]

            health.temperature.current = data.temperature or health.temperature.current
            health.hydration.current = data.hydration or health.hydration.current
            health.energy.current = data.energy or health.energy.current

            health.update_time = utc_now()

    @get("/singleplayer/settings/raid/menu")
    @inject
    async def raid_settings(
        self,
        config: Annotated[RaidConfig, Inject],
    ) -> RaidConfig:
        return config

    @get("/singleplayer/settings/bot/difficulty/{bot_type:str}/{difficulty:str}")
    @inject
    async def bot_difficulty(
        self,
        bot_type: str,
        difficulty: Literal[BotDifficultyKey, "core"],
        query: Annotated[BotDifficultyQuery, Inject],
    ) -> BotDifficulty:
        return await query.execute(bot_type=bot_type, bot_difficulty=difficulty)

    @get("/singleplayer/settings/bot/limit/{role:str}", name="aki_bot_generation_limit")
    @inject
    async def bot_limit(
        self,
        role: str,
        query: Annotated[BotPresetLimitQuery, Inject],
    ) -> int:
        return await query.execute(role=role)

    @get("/singleplayer/settings/bot/maxCap", name="aki_bot_max_cap")
    @inject
    async def bot_max_cap(
        self,
        account_profile: AccountProfile,
        query: Annotated[BotMaxCapQuery, Inject],
    ) -> int:
        result = await query.execute(account_profile=account_profile)
        if isinstance(result, Err):
            match result.err_value:
                case NoRaidInfoError():
                    raise HTTPException(status_code=HTTPStatus.BAD_REQUEST)
                case _:
                    assert_never(result.err_value)
        return result.ok_value
