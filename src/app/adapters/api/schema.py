from typing import Any, Generic, Literal, TypeVar

import pydantic
from pydantic import BaseModel, ConfigDict

from lib.models import StrictModel
from lib.utils import snake_to_camel

T = TypeVar("T")


class BaseSchema(BaseModel):
    model_config = ConfigDict(
        from_attributes=True,
        populate_by_name=True,
        alias_generator=snake_to_camel,
    )


class Success(StrictModel, Generic[T]):
    data: T | None
    err: Literal[0] = 0
    errmsg: None = None


class Error(StrictModel):
    model_config = ConfigDict(populate_by_name=True)

    data: Any = None
    error_code: int = pydantic.Field(alias="err")
    error_message: str | None = pydantic.Field(default=None, alias="errmsg")


class Elements(StrictModel, Generic[T]):
    elements: T


class NotificationChannelSchema(StrictModel):
    server: str
    channel_id: str
    url: str
    notifier_server: str = pydantic.Field(alias="notifierServer")
    ws: str
