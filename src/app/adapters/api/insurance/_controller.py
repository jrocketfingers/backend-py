from collections.abc import Mapping
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.insurance.schema import InsuranceCostRequestSchema
from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.accounts.insurance.dto import InsuranceCostDTO
from app.core.domain.accounts.insurance.queries import InsuranceCostQuery
from app.db.models import AccountProfile
from lib.tarkov.types import ItemTemplateId


class InsuranceController(Controller):

    path = "/client/insurance"
    middleware = (ZLibMiddleware,)

    @post(
        "/items/list/cost",
        name="insurance_items_cost",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def insurance_cost(
        self,
        data: InsuranceCostRequestSchema,
        account_profile: AccountProfile,
        query_: Annotated[InsuranceCostQuery, Inject],
    ) -> Success[Mapping[str, Mapping[ItemTemplateId, int]]]:
        result = await query_.execute(
            profile=account_profile,
            dto=InsuranceCostDTO(trader_ids=data.traders, item_ids=data.items),
        )
        return Success(data=result)
