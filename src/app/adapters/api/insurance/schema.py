from lib.models import StrictModel
from lib.tarkov.types import ItemId


class InsuranceCostRequestSchema(StrictModel):
    traders: list[str]
    items: list[ItemId]
