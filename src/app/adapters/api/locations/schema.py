from lib.models import CamelCaseModel


class LocationLocalLootRequestSchema(CamelCaseModel):
    location_id: str
    variant_id: int
