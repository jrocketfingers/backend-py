from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post
from pydantic import BaseModel

from app.adapters.api.locations.schema import LocationLocalLootRequestSchema
from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.locations.queries import GenerateLocationQuery, LocationsQuery
from lib.tarkov.locations.models import Location, LocationsBase


class LocationController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/locations",
        status_code=HTTPStatus.OK,
        name="client_locations",
        type_encoders={
            BaseModel: lambda model: model.model_dump(
                mode="json",
                by_alias=True,
                exclude_none=True,
            ),
        },
    )
    @inject
    async def client_locations(
        self,
        query: Annotated[LocationsQuery, Inject],
    ) -> Success[LocationsBase]:
        return Success(data=await query.execute())

    @post(
        "/client/location/getLocalloot",
        name="locations_get_loot",
        status_code=HTTPStatus.OK,
        type_encoders={
            BaseModel: lambda model: model.model_dump(
                mode="json",
                by_alias=True,
                exclude_none=True,
            ),
        },
    )
    @inject
    async def get_location_loot(
        self,
        data: LocationLocalLootRequestSchema,
        query: Annotated[GenerateLocationQuery, Inject],
    ) -> Success[Location]:
        return Success(data=await query.execute(location_id=data.location_id))
