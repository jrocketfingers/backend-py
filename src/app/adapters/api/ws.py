from urllib.parse import urljoin

from litestar import Request
from litestar.datastructures import State

from app.adapters.api.schema import NotificationChannelSchema
from app.db.models import Account


def get_port(port: int | None, scheme: str) -> int:
    if port is None:  # pragma: no branch
        port = {
            "http": 80,
            "https": 443,
        }[scheme]
    return port


def get_ws_channel(
    request: Request[None, None, State],
    account: Account,
) -> NotificationChannelSchema:
    port = get_port(port=request.base_url.port, scheme=request.base_url.scheme)
    server = f"{request.base_url.hostname}:{port}"
    profile_id = str(account.session_id)
    route_path = request.app.route_reverse("notifications_wss", session_id=profile_id)

    return NotificationChannelSchema(
        server=server,
        channel_id=profile_id,
        url="",
        notifier_server=urljoin(server, route_path),
        ws=urljoin(str(request.base_url.with_replacements(scheme="wss")), route_path),
    )
