from collections.abc import Mapping, Sequence
from typing import Self

import pydantic

from app.core.domain.traders.dto import TraderAssortDTO
from lib.models import CamelCaseModel, DatetimeInt, StrictModel
from lib.tarkov.items.models import Item
from lib.tarkov.trading.models import BarterScheme
from lib.tarkov.types import ItemId, ItemTemplateId


class TraderPricesSchema(CamelCaseModel):
    supply_next_time: DatetimeInt
    prices: Mapping[ItemTemplateId, int]
    currency_courses: Mapping[str, int]


class TraderAssortSchema(StrictModel):
    next_resupply: DatetimeInt = pydantic.Field(alias="nextResupply")
    items: Sequence[Item]
    barter_scheme: BarterScheme
    loyal_level_items: Mapping[ItemId, int]

    @classmethod
    def from_dto(cls, dto: TraderAssortDTO) -> Self:
        return cls(
            next_resupply=dto.next_resupply,
            items=dto.items,
            barter_scheme=dto.barter_scheme,
            loyal_level_items=dto.loyal_level_items,
        )
