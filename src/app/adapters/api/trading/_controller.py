from collections.abc import Sequence
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post
from litestar.exceptions import HTTPException
from pydantic import BaseModel
from result import Err

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.adapters.api.trading.schema import TraderAssortSchema, TraderPricesSchema
from app.core.domain.errors import NotFoundError
from app.core.domain.traders.queries import (
    CustomizationStorageQuery,
    TraderAssortQuery,
    TraderPricesQuery,
    TraderSettingsQuery,
)
from app.db.models import AccountProfile
from lib.tarkov.customization import Suits
from lib.tarkov.trading.models import TraderBase


class TraderController(Controller):
    path = ""
    middleware = (ZLibMiddleware,)

    @post(
        "/client/trading/api/traderSettings",
        name="trading_trader_settings",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def trading_trader_settings(
        self,
        query: Annotated[TraderSettingsQuery, Inject],
    ) -> Success[Sequence[TraderBase]]:
        settings = await query.execute()
        return Success(data=settings)

    @post(
        "/client/trading/customization/storage",
        name="trading_customization_storage",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def trading_customization_storage(
        self,
        query: Annotated[CustomizationStorageQuery, Inject],
        account_profile: AccountProfile,
    ) -> Success[Suits]:
        result = await query.execute(account_profile=account_profile)
        return Success(data=result)

    @post(
        "/client/items/prices/{trader_id:str}",
        name="trader_item_prices",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def trader_item_prices(
        self,
        trader_id: str,
        query: Annotated[TraderPricesQuery, Inject],
    ) -> Success[TraderPricesSchema]:
        result = await query.execute(
            trader_id=trader_id,
        )
        if isinstance(result, Err):
            match result.err_value:
                case NotFoundError():  # pragma: no branch
                    raise HTTPException(status_code=HTTPStatus.NOT_FOUND)

        dto = result.ok_value
        return Success(
            data=TraderPricesSchema(
                supply_next_time=dto.supply_next_time,
                prices=dto.prices,
                currency_courses=dto.currency_courses,
            ),
        )

    @post(  # pragma: no branch
        "/client/trading/api/getTraderAssort/{trader_id:str}",
        name="trader_assort",
        status_code=HTTPStatus.OK,
        type_encoders={
            BaseModel: lambda model: model.model_dump(
                mode="json",
                by_alias=True,
                exclude_none=True,
            ),
        },
    )
    @inject
    async def trader_assort(
        self,
        trader_id: str,
        account_profile: AccountProfile,
        query: Annotated[TraderAssortQuery, Inject],
    ) -> Success[TraderAssortSchema]:
        result = await query.execute(profile=account_profile, trader_id=trader_id)
        if isinstance(result, Err):
            match result.err_value:
                case NotFoundError():  # pragma: no branch
                    raise HTTPException(status_code=HTTPStatus.NOT_FOUND)
        return Success(data=TraderAssortSchema.from_dto(dto=result.ok_value))
