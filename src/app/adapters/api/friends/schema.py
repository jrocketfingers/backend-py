import pydantic
from pydantic import BaseModel

from lib.models import CamelCaseModel, DatetimeInt, StrictModel
from lib.tarkov.chat import ChatMember


class FriendRequestSendSchema(BaseModel):
    profile_id: str = pydantic.Field(alias="to")


class FriendRequestCancelSchema(CamelCaseModel):
    profile_id: str


class FriendAcceptRequestSchema(CamelCaseModel):
    profile_id: str


class FriendDeleteRequestSchema(StrictModel):
    friend_id: str


class SendFriendRequestResponse(CamelCaseModel):
    status: int
    request_id: str
    retry_after: int


class FriendRequestResponseSchema(StrictModel):
    id: str = pydantic.Field(alias="_id")
    from_: str = pydantic.Field(alias="from")
    to: str
    date: DatetimeInt
    profile: ChatMember
