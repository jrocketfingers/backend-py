import uuid
from http import HTTPStatus
from typing import Annotated, assert_never

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post
from result import Err

from app.adapters.api.friends.schema import (
    FriendAcceptRequestSchema,
    FriendDeleteRequestSchema,
    FriendRequestCancelSchema,
    FriendRequestResponseSchema,
    FriendRequestSendSchema,
    SendFriendRequestResponse,
)
from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Error, Success
from app.core.domain.friends.commands import (
    FriendAcceptRequestCommand,
    FriendCancelRequestCommand,
    FriendDeleteCommand,
    FriendSendRequestCommand,
)
from app.core.domain.friends.errors import FriendRequestNotFound, ProfileNotFound
from app.core.domain.friends.queries import (
    FriendListQuery,
    FriendRequestInboxQuery,
    FriendRequestOutboxQuery,
)
from app.db.models import AccountProfile
from lib.tarkov.chat import ChatMember
from lib.tarkov.errors import ErrorCode
from lib.tarkov.friends.models import FriendList


class FriendsController(Controller):
    path = "/client/friend"
    middleware = (ZLibMiddleware,)

    @post(
        "/list",
        status_code=HTTPStatus.OK,
        name="friend_list",
    )
    @inject
    async def friend_list(
        self,
        query: Annotated[FriendListQuery, Inject],
        account_profile: AccountProfile,
    ) -> Success[FriendList]:
        return Success(data=await query.execute(profile=account_profile))

    @post(
        "/request/list/outbox",
        status_code=HTTPStatus.OK,
        name="friend_request_outbox",
    )
    @inject
    async def friend_request_outbox(
        self,
        query_: Annotated[FriendRequestOutboxQuery, Inject],
        account_profile: AccountProfile,
    ) -> Success[list[FriendRequestResponseSchema]]:
        friend_requests = await query_.execute(user=account_profile)
        return Success(
            data=[
                FriendRequestResponseSchema(
                    id=str(request.id),
                    from_=str(request.sender.profile_id),
                    to=str(request.recipient.profile_id),
                    date=request.created_at,
                    profile=ChatMember.from_profile(request.recipient),
                )
                for request in friend_requests
            ],
        )

    @post(
        "/request/list/inbox",
        status_code=HTTPStatus.OK,
        name="friend_request_inbox",
    )
    @inject
    async def friend_request_inbox(
        self,
        query_: Annotated[FriendRequestInboxQuery, Inject],
        account_profile: AccountProfile,
    ) -> Success[list[FriendRequestResponseSchema]]:
        friend_requests = await query_.execute(user=account_profile)
        return Success(
            data=[
                FriendRequestResponseSchema(
                    id=str(request.id),
                    from_=str(request.sender.profile_id),
                    to=str(request.recipient.profile_id),
                    date=request.created_at,
                    profile=ChatMember.from_profile(request.sender),
                )
                for request in friend_requests
            ],
        )

    @post(
        "/request/send",
        status_code=HTTPStatus.OK,
        name="friend_request_send",
    )
    @inject
    async def friend_request_send(
        self,
        data: FriendRequestSendSchema,
        command: Annotated[FriendSendRequestCommand, Inject],
        account_profile: AccountProfile,
    ) -> Success[SendFriendRequestResponse] | Error:
        result = await command.execute(
            sender=account_profile,
            recipient_profile_id=data.profile_id,
        )
        if isinstance(result, Err):
            match result.err_value:
                case ProfileNotFound():
                    return Error(
                        error_code=ErrorCode.player_profile_not_found,
                    )
                case _:  # pragma: no cover
                    assert_never(result.err_value)

        return Success(
            data=SendFriendRequestResponse(
                status=0,
                request_id=str(result.ok_value.id),
                retry_after=1,
            ),
        )

    @post(
        "/request/cancel",
        status_code=HTTPStatus.OK,
        name="friend_request_cancel",
    )
    @inject
    async def friend_request_cancel(
        self,
        data: FriendRequestCancelSchema,
        command: Annotated[FriendCancelRequestCommand, Inject],
        account_profile: AccountProfile,
    ) -> Success[SendFriendRequestResponse] | Error:
        result = await command.execute(
            sender=account_profile,
            recipient_profile_id=data.profile_id,
        )
        if isinstance(result, Err):
            match result.err_value:
                case ProfileNotFound():
                    return Error(
                        error_code=ErrorCode.player_profile_not_found,
                    )
                case _:  # pragma: no cover
                    assert_never(result.err_value)

        return Success(
            data=SendFriendRequestResponse(
                status=0,
                request_id=str(uuid.uuid4()),
                retry_after=60,
            ),
        )

    @post(
        "/request/accept",
        status_code=HTTPStatus.OK,
        name="friend_request_accept",
    )
    @inject
    async def friend_request_accept(
        self,
        data: FriendAcceptRequestSchema,
        command: Annotated[FriendAcceptRequestCommand, Inject],
        account_profile: AccountProfile,
    ) -> Success[bool] | Error:
        result = await command.execute(
            recipient=account_profile,
            sender_profile_id=data.profile_id,
        )
        if isinstance(result, Err):
            match result.err_value:
                case ProfileNotFound() | FriendRequestNotFound():
                    return Error(
                        error_code=ErrorCode.player_profile_not_found,
                    )

        return Success(data=True)

    @post(
        "/delete",
        status_code=HTTPStatus.OK,
        name="friend_delete",
    )
    @inject
    async def friend_delete(
        self,
        data: FriendDeleteRequestSchema,
        command: Annotated[FriendDeleteCommand, Inject],
        account_profile: AccountProfile,
    ) -> Success[bool]:
        await command.execute(
            profile=account_profile,
            friend_profile_id=data.friend_id,
        )
        return Success(data=True)
