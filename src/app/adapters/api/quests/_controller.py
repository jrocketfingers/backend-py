from collections.abc import Sequence
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.quests.queries import QuestListQuery
from app.db.models import AccountProfile
from lib.tarkov.quests.models import QuestTemplate


class QuestController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/quest/list",
        name="quest_list",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def quest_list(
        self,
        query: Annotated[QuestListQuery, Inject],
        account_profile: AccountProfile,
    ) -> Success[Sequence[QuestTemplate]]:
        result = await query.execute(account_profile=account_profile)
        return Success(data=result)

    @post(
        "/client/repeatalbeQuests/activityPeriods",
        name="quest_repeatable_list",
        status_code=HTTPStatus.OK,
    )
    async def quest_repeatable_list(self) -> Success[list[None]]:
        return Success(data=[])
