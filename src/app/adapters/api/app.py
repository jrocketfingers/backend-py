from collections.abc import Iterable

from aioinject.ext.litestar import AioInjectPlugin
from litestar import Litestar, get
from litestar.contrib.pydantic import PydanticPlugin
from litestar.di import Provide
from litestar.openapi import OpenAPIConfig
from litestar.static_files import create_static_files_router
from litestar.types import ControllerRouterHandler

from app import telemetry
from app.adapters.api import (
    achievements,
    aki_compat,
    flea_market,
    friends,
    hideout,
    insurance,
    items,
    locales,
    locations,
    mail,
    match,
    notifications,
    presets,
    profiles,
    quests,
    startup,
    trading,
)
from app.adapters.api.dependencies import get_account, get_account_profile
from app.adapters.api.logging import get_logging_config
from app.adapters.api.middleware import PerfMiddleware, StripUnityContentEncoding
from app.adapters.api.standalone import tasks_lifespan
from app.core.di import create_container
from app.settings import AppSettings
from lib.settings import get_settings
from lib.tarkov.paths import static_files_dir, static_url

_routers: Iterable[ControllerRouterHandler] = [
    achievements.AchievementsController,
    aki_compat.AkiCompatabilityController,
    flea_market.FleaMarketController,
    friends.FriendsController,
    hideout.HideoutController,
    insurance.InsuranceController,
    items.ItemsController,
    locales.LocalesController,
    locations.LocationController,
    mail.MailController,
    match.MatchController,
    notifications.NotificationController,
    presets.PresetsController,
    profiles.ProfilesController,
    quests.QuestController,
    startup.StartupController,
    trading.TraderController,
]


@get("/health")
async def healthcheck() -> None:
    return None


def create_app() -> Litestar:
    telemetry.setup_telemetry()
    container = create_container()
    app_settings = get_settings(AppSettings)
    lifespans = []
    if app_settings.standalone:  # pragma: no cover
        lifespans.append(tasks_lifespan)

    return Litestar(
        plugins=[
            AioInjectPlugin(container=container),
            PydanticPlugin(prefer_alias=True),
        ],
        dependencies={
            "account": Provide(get_account),
            "account_profile": Provide(get_account_profile),
        },
        route_handlers=[
            healthcheck,
            *_routers,
            create_static_files_router(path=static_url, directories=[static_files_dir]),
        ],
        openapi_config=OpenAPIConfig(
            title="Tarkov API",
            version="0.0.1",
        ),
        lifespan=lifespans,
        middleware=[StripUnityContentEncoding, PerfMiddleware],
        logging_config=get_logging_config(),
    )
