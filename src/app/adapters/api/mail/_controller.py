from collections.abc import Sequence
from http import HTTPStatus

from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success


class MailController(Controller):
    path = "/client/mail"
    middleware = (ZLibMiddleware,)

    @post(
        "/dialog/list",
        status_code=HTTPStatus.OK,
        name="mail_dialogue_list",
    )
    @inject
    async def mail_dialogue_list(self) -> Success[Sequence[None]]:
        return Success(data=[])
