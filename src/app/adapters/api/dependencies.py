from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar.exceptions import HTTPException
from litestar.params import Parameter
from sqlalchemy.orm import joinedload

from app.core.domain.accounts.repositories import AccountRepository
from app.db.models import Account, AccountProfile


@inject
async def get_account(
    service: Annotated[AccountRepository, Inject],
    session_id: Annotated[str | None, Parameter(cookie="PHPSESSID")] = None,
    session_id_header: Annotated[str | None, Parameter(header="PHPSESSID_")] = None,
) -> Account:
    session_id = session_id or session_id_header
    if not session_id:
        raise HTTPException(status_code=HTTPStatus.UNAUTHORIZED)

    account = await service.get(
        session_id=session_id,
        options=(joinedload(Account.profile).joinedload(AccountProfile.raid_info),),
    )
    if account is None:
        raise HTTPException(status_code=HTTPStatus.UNAUTHORIZED)
    return account


async def get_account_profile(account: Account) -> AccountProfile:
    if account.profile is None:
        raise HTTPException(status_code=HTTPStatus.BAD_REQUEST)
    return account.profile
