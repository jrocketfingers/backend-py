import asyncio
import signal

from app import telemetry
from app.core.di import create_container
from app.core.domain.traders.tasks import UpdateTraderAssortTask
from lib.tasks import run_task_forever


async def main() -> None:
    stop = asyncio.Event()

    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, stop.set)
    loop.add_signal_handler(signal.SIGTERM, stop.set)

    telemetry.setup_telemetry()
    task_classes = (UpdateTraderAssortTask,)
    container = create_container()
    async with (
        container,
        asyncio.TaskGroup() as tg,
    ):
        for cls in task_classes:
            tg.create_task(
                run_task_forever(
                    container=container,
                    task_cls=cls,
                    stop=stop,
                ),
            )


asyncio.run(main())
