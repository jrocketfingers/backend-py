import logging
from datetime import timedelta

from app.core.domain.hideout.services import HideoutService
from app.settings import AppSettings
from lib.plugins.app import AppOnInitPlugin


class DevHideoutPlugin(AppOnInitPlugin):  # pragma: no cover
    def __init__(self, hideout_service: HideoutService, settings: AppSettings) -> None:
        self._hideout_service = hideout_service
        self._app_settings = settings

    async def on_app_init(self) -> None:
        if not self._app_settings.dev_mode:
            return

        for area in self._hideout_service.areas:
            area.requirements = []
            for stage in area.stages.values():
                stage.requirements = []
                stage.construction_time = timedelta(seconds=5)
            logging.debug(
                "%s patching %s",
                self.__class__.__qualname__,
                repr(area.type),
            )
