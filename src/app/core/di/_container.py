import contextlib
import functools
import importlib
import pkgutil
from collections.abc import AsyncIterator, Iterable, Sequence
from pathlib import Path
from types import ModuleType

import aioinject
from aioinject import InjectionContext, Object, Provider, Singleton, Transient
from aioinject.context import context_var
from pydantic import BaseModel
from pydantic_settings import BaseSettings

from app import db
from app.settings import AppSettings
from lib.plugins.registry import PluginRegistry
from lib.settings import get_settings
from lib.tarkov.config import (
    BotConfig,
    NotificationsConfig,
    QuestConfig,
    RaidConfig,
    StorageConfig,
    WeatherConfig,
)
from lib.tarkov.db import AssetsPath, create_resource_db
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.paths import asset_dir_path
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.profiles.backend import FileSystemStorageBackend, ProfileStorageBackend
from lib.tarkov.profiles.init import ProfileInitializer
from lib.tarkov.profiles.models import ProfileStoragePath
from lib.tarkov.profiles.storage import ProfileStorageImpl
from lib.types import PathOrStr
from lib.utils import read_pydantic_yaml

from . import _modules
from ._extensions import OnAppInit

_config_files: Iterable[tuple[PathOrStr, type[BaseModel]]] = [
    ("bot.yaml", BotConfig),
    ("notifications.yaml", NotificationsConfig),
    ("quests.yaml", QuestConfig),
    ("raid.yaml", RaidConfig),
    ("storage.yaml", StorageConfig),
    ("weather.yaml", WeatherConfig),
]
_settings_classes: Iterable[type[BaseSettings]] = (AppSettings,)


async def _profile_storage_path(config: StorageConfig) -> ProfileStoragePath:
    return ProfileStoragePath(config.filesystem.path)  # pragma: no cover


async def _profile_storage_backend(
    storage_path: ProfileStoragePath,
) -> ProfileStorageBackend:
    return FileSystemStorageBackend(
        storage_path=storage_path,
    )


@contextlib.asynccontextmanager
async def _create_profile_storage(
    storage_backend: ProfileStorageBackend,
    initializer: ProfileInitializer,
    item_db: ItemDB,
    config: StorageConfig,
    plugins: PluginRegistry,
) -> AsyncIterator[ProfileStorage]:
    async with ProfileStorageImpl(
        backend=storage_backend,
        initializer=initializer,
        item_db=item_db,
        config=config,
        plugins=plugins,
    ).context() as storage:
        yield storage


async def _injection_context() -> InjectionContext:
    context = context_var.get()
    assert isinstance(context, InjectionContext)  # noqa: S101
    return context


def _autodiscover(module: ModuleType, attr_name: str) -> Sequence[Provider[object]]:
    result = []
    for submodule_info in pkgutil.walk_packages(module.__path__, f"{module.__name__}."):
        if submodule_info.ispkg:  # pragma: no cover
            continue
        submodule = importlib.import_module(submodule_info.name)
        module_providers = getattr(submodule, attr_name, None)
        if module_providers is None:  # pragma: no cover
            msg = f"Module {submodule_info.name} does not have {attr_name} attribute"
            raise ValueError(msg)
        result.extend(module_providers)
    return result


@functools.cache
def create_container() -> aioinject.Container:
    container = aioinject.Container(extensions=[OnAppInit()])
    container.register(Object(container, type_=aioinject.Container))

    for provider in _autodiscover(_modules, "providers"):
        container.register(provider)

    for provider in db.providers:
        container.register(provider)

    container.register(Object(asset_dir_path, AssetsPath))
    container.register(Singleton(create_resource_db))
    container.register(Singleton(_profile_storage_path))
    container.register(Singleton(_profile_storage_backend))
    container.register(Singleton(_create_profile_storage))
    container.register(Transient(_injection_context))

    for path, model in _config_files:
        container.register(
            Singleton(
                functools.partial(
                    read_pydantic_yaml,
                    path=Path("./config").joinpath(path),
                    model=model,
                ),
                type_=model,
            ),
        )

    for settings_cls in _settings_classes:
        container.register(Object(get_settings(settings_cls), type_=settings_cls))

    container.register(Singleton(PluginRegistry))

    return container
