import aioinject

from app.core.domain.accounts.commands import ValidateNicknameCommand
from app.core.domain.accounts.queries import ProfileSearchQuery
from app.core.domain.accounts.repositories import AccountRepository, ProfileRepository
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(AccountRepository),
    aioinject.Scoped(ProfileRepository),
    aioinject.Scoped(ValidateNicknameCommand),
    aioinject.Scoped(ProfileSearchQuery),
]
