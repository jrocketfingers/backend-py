import aioinject

from app.core.domain.inventory.commands import InventoryOperationsCommand
from lib.tarkov.inventory_actions.router import ActionRouter
from lib.tarkov.inventory_actions.services import ItemLocator
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(InventoryOperationsCommand),
    aioinject.Scoped(ActionRouter),
    aioinject.Scoped(ItemLocator),
]
