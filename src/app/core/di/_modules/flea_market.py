import aioinject

from app.core.domain.flea_market.queries import FleaMarketSearchQuery
from app.core.domain.flea_market.repository import FleaOffersRepository
from app.core.domain.flea_market.services import FleaMarketService
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(FleaMarketSearchQuery),
    aioinject.Scoped(FleaOffersRepository),
    aioinject.Scoped(FleaMarketService),
]
