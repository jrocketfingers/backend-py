import aioinject

from app.core.domain.customization.query import (
    AccountCustomizationQuery,
    AllCustomizationQuery,
)
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(AccountCustomizationQuery),
    aioinject.Scoped(AllCustomizationQuery),
]
