import aioinject
from pydantic import TypeAdapter

from app.core.domain.hideout.queries import (
    HideoutAreasQuery,
    HideoutQTEListQuery,
    HideoutRecipesQuery,
    HideoutScavCaseRecipesQuery,
    HideoutSettingsQuery,
)
from app.core.domain.hideout.services import HideoutService
from lib.tarkov.db import AssetsPath
from lib.tarkov.hideout.models.area import HideoutAreaTemplate
from lib.tarkov.hideout.models.qte import HideoutQuickTimeEvents
from lib.tarkov.hideout.models.recipes import HideoutRecipe, ScavCaseRecipe
from lib.tarkov.hideout.models.settings import HideoutSettings
from lib.tarkov.items.storage import ItemDB
from lib.types import Providers
from lib.utils import read_pydantic_json


async def create_hideout_service(
    path: AssetsPath,
    item_db: ItemDB,
) -> HideoutService:
    hideout_path = path.joinpath("hideout")
    areas = TypeAdapter(list[HideoutAreaTemplate])
    qte = TypeAdapter(list[HideoutQuickTimeEvents])
    recipes = TypeAdapter(list[HideoutRecipe])
    scav_case = TypeAdapter(list[ScavCaseRecipe])
    return HideoutService(
        area_templates=await read_pydantic_json(
            hideout_path.joinpath("areas.json"),
            areas,
        ),
        quick_time_events=await read_pydantic_json(
            hideout_path.joinpath("qte.json"),
            qte,
        ),
        settings=await read_pydantic_json(
            hideout_path.joinpath("settings.json"),
            HideoutSettings,
        ),
        recipes=await read_pydantic_json(
            hideout_path.joinpath("production.json"),
            recipes,
        ),
        scav_case_recipes=await read_pydantic_json(
            hideout_path.joinpath("scavcase.json"),
            scav_case,
        ),
        item_db=item_db,
    )


providers: Providers = [
    aioinject.Singleton(create_hideout_service),
    aioinject.Scoped(HideoutAreasQuery),
    aioinject.Scoped(HideoutQTEListQuery),
    aioinject.Scoped(HideoutSettingsQuery),
    aioinject.Scoped(HideoutRecipesQuery),
    aioinject.Scoped(HideoutScavCaseRecipesQuery),
]
