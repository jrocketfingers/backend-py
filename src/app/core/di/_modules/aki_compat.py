from aioinject import Scoped

from app.core.domain.aki_compat.commands import RaidConfigurationCommand
from app.core.domain.aki_compat.queries import RaidTimeQuery
from app.core.domain.aki_compat.services import RaidService
from lib.types import Providers

providers: Providers = [
    Scoped(RaidConfigurationCommand),
    Scoped(RaidTimeQuery),
    Scoped(RaidService),
]
