import aioinject

from app.core.domain.accounts.insurance.queries import InsuranceCostQuery
from app.core.domain.accounts.insurance.services import InsuranceService
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(InsuranceCostQuery),
    aioinject.Scoped(InsuranceService),
]
