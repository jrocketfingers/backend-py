import aioinject

from app.core.domain.traders.queries import (
    CustomizationStorageQuery,
    TraderAssortQuery,
    TraderPricesQuery,
    TraderSettingsQuery,
)
from app.core.domain.traders.repositories import (
    TraderAssortRepository,
    TraderRepository,
)
from app.core.domain.traders.tasks import UpdateTraderAssortTask
from lib.tarkov.db import AssetsPath
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.trading import read_trader_data
from lib.tarkov.trading.trader import Traders
from lib.types import Providers


async def create_traders(assets_path: AssetsPath, item_db: ItemDB) -> Traders:
    return Traders(
        traders=await read_trader_data(assets_path.joinpath("traders")),
        item_db=item_db,
    )


providers: Providers = [
    aioinject.Scoped(CustomizationStorageQuery),
    aioinject.Scoped(TraderAssortQuery),
    aioinject.Scoped(TraderAssortRepository),
    aioinject.Scoped(TraderPricesQuery),
    aioinject.Scoped(TraderRepository),
    aioinject.Scoped(TraderSettingsQuery),
    aioinject.Scoped(UpdateTraderAssortTask),
    aioinject.Singleton(create_traders),
]
