import aioinject
from pydantic import TypeAdapter

from app.core.domain.quests.queries import QuestListQuery
from app.core.domain.quests.services import QuestsService
from lib.tarkov.db import AssetsPath
from lib.tarkov.quests.models import QuestTemplate
from lib.tarkov.quests.storage import QuestStorage
from lib.tarkov.types import QuestId
from lib.types import Providers
from lib.utils import read_pydantic_json


async def create_quest_storage(path: AssetsPath) -> QuestStorage:
    adapter = TypeAdapter(dict[QuestId, QuestTemplate])
    return QuestStorage(
        quests=await read_pydantic_json(
            path.joinpath("quests.json"),
            adapter,
        ),
    )


providers: Providers = [
    aioinject.Singleton(create_quest_storage),
    aioinject.Scoped(QuestListQuery),
    aioinject.Scoped(QuestsService),
]
