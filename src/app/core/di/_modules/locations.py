import aioinject

from app.core.domain.locations.queries import GenerateLocationQuery, LocationsQuery
from app.core.domain.locations.services import LocationService
from lib.tarkov.db import AssetsPath
from lib.tarkov.locations.db import LocationDB, read_locations
from lib.tarkov.locations.models import LocationsBase
from lib.types import Providers
from lib.utils import read_pydantic_json


async def create_location_db(asset_path: AssetsPath) -> LocationDB:
    return LocationDB(
        locations=await read_locations(asset_path.joinpath("locations")),
        base=await read_pydantic_json(
            asset_path.joinpath("locations", "base.json"),
            LocationsBase,
        ),
    )


providers: Providers = [
    aioinject.Singleton(create_location_db),
    aioinject.Scoped(LocationService),
    aioinject.Scoped(LocationsQuery),
    aioinject.Scoped(GenerateLocationQuery),
]
