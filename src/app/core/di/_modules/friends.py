import aioinject

from app.core.domain.friends.commands import (
    FriendAcceptRequestCommand,
    FriendCancelRequestCommand,
    FriendDeleteCommand,
    FriendSendRequestCommand,
)
from app.core.domain.friends.queries import (
    FriendListQuery,
    FriendRequestInboxQuery,
    FriendRequestOutboxQuery,
)
from app.core.domain.friends.repository import FriendRepository, FriendRequestRepository
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(FriendListQuery),
    aioinject.Scoped(FriendRequestInboxQuery),
    aioinject.Scoped(FriendRequestOutboxQuery),
    aioinject.Scoped(FriendSendRequestCommand),
    aioinject.Scoped(FriendCancelRequestCommand),
    aioinject.Scoped(FriendAcceptRequestCommand),
    aioinject.Scoped(FriendDeleteCommand),
    aioinject.Scoped(FriendRequestRepository),
    aioinject.Scoped(FriendRepository),
]
