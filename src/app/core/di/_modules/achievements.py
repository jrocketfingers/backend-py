import aioinject

from app.core.domain.achievements.queries import (
    AchievementListQuery,
    AchievementStatisticQuery,
)
from lib.tarkov.achievements.db import read_achievements_db
from lib.types import Providers

providers: Providers = [
    aioinject.Singleton(read_achievements_db),
    aioinject.Scoped(AchievementListQuery),
    aioinject.Scoped(AchievementStatisticQuery),
]
