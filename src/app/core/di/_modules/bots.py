from aioinject import Scoped, Singleton

from app.core.domain.bots.queries import (
    BotDifficultyQuery,
    BotMaxCapQuery,
    BotPresetLimitQuery,
)
from app.core.domain.bots.services import BotService
from lib.tarkov.bots.db import create_bot_db
from lib.types import Providers

providers: Providers = [
    Singleton(create_bot_db),
    Scoped(BotService),
    Scoped(BotDifficultyQuery),
    Scoped(BotMaxCapQuery),
    Scoped(BotPresetLimitQuery),
]
