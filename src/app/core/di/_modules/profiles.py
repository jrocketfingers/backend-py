import aioinject

from app.core.domain.items.services import CustomizationDB
from app.core.domain.profiles.commands import ProfileCreateCommand, ProfileLogoutCommand
from app.core.domain.profiles.queries import ProfileListQuery, ProfileStatusQuery
from lib.tarkov.db import AssetsPath
from lib.tarkov.profiles.init import ProfileInitializer
from lib.tarkov.profiles.models import ProfileTemplatesPath, ScavProfile
from lib.tarkov.profiles.templates import ProfileTemplates, create_profile_templates
from lib.tarkov.trading.trader import Traders
from lib.types import Providers
from lib.utils import read_pydantic_json


async def _profile_initializer(
    assets_path: AssetsPath,
    traders: Traders,
    customization_db: CustomizationDB,
    templates: ProfileTemplates,
) -> ProfileInitializer:
    return ProfileInitializer(
        traders=traders,
        templates_path=ProfileTemplatesPath(assets_path.joinpath("editions")),
        scav_template=await read_pydantic_json(
            assets_path.joinpath("core", "playerScav.json"),
            ScavProfile,
        ),
        customization_db=customization_db,
        templates=templates,
    )


providers: Providers = [
    aioinject.Singleton(_profile_initializer),
    aioinject.Singleton(create_profile_templates),
    aioinject.Scoped(ProfileListQuery),
    aioinject.Scoped(ProfileStatusQuery),
    aioinject.Scoped(ProfileCreateCommand),
    aioinject.Scoped(ProfileLogoutCommand),
]
