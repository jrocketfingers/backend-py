from aioinject import Singleton

from lib.tarkov.config import NotificationsConfig
from lib.tarkov.notifications.abc import NotificationBackend
from lib.tarkov.notifications.backend import InMemoryBackend
from lib.tarkov.notifications.service import NotificationService
from lib.types import Providers


async def _in_memory_backend(config: NotificationsConfig) -> InMemoryBackend:
    return InMemoryBackend(config=config.backend)


providers: Providers = [
    Singleton(_in_memory_backend, type_=NotificationBackend),
    Singleton(NotificationService),
]
