import contextlib
from collections.abc import AsyncIterator
from typing import final

from aioinject import Container
from aioinject.extensions import LifespanExtension

from app.core.plugins import register_plugins
from lib.plugins.registry import PluginRegistry


@final
class OnAppInit(LifespanExtension):
    @contextlib.asynccontextmanager
    async def lifespan(
        self,
        container: Container,
    ) -> AsyncIterator[None]:
        async with container.context() as ctx:
            plugins = await ctx.resolve(PluginRegistry)
            register_plugins(plugins)
            await plugins.app.on_app_init()
        yield
