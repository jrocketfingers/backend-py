from app.core.dev.plugins import DevHideoutPlugin
from app.core.domain.profiles.plugins import (
    ProfileFixHideoutPlugin,
    ProfileFixTradersInfo,
    UpdateProfileInfoPlugin,
)
from lib.plugins.registry import PluginRegistry


def register_plugins(plugins: PluginRegistry) -> None:
    plugins.register(ProfileFixTradersInfo)
    plugins.register(ProfileFixHideoutPlugin)
    plugins.register(UpdateProfileInfoPlugin)
    plugins.register(DevHideoutPlugin)
