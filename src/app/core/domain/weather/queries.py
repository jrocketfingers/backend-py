from app.core.domain.weather.dto import WeatherDTO
from app.core.domain.weather.services import WeatherService
from lib.utils import utc_now


class WeatherQuery:
    def __init__(self, weather_service: WeatherService) -> None:
        self._service = weather_service

    async def execute(self) -> WeatherDTO:
        return self._service.generate_weather(
            time_=utc_now(),
        )
