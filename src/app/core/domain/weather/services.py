import random
from datetime import datetime, timedelta

from app.core.domain.weather.dto import WeatherDetailsDTO, WeatherDTO
from lib.models import MinMaxValue
from lib.tarkov.config import WeatherConfig
from lib.utils import scale


def betavariate_float(value: MinMaxValue[float]) -> float:
    return scale(
        value=random.betavariate(1, 3),
        a=value.min,
        b=value.max,
    )


def accelerated_time(time_: datetime, factor: int) -> datetime:
    delta = (
        datetime.min
        + timedelta(hours=time_.hour, minutes=time_.minute, seconds=time_.second)
        * factor
    )
    return time_.replace(
        hour=delta.hour,
        minute=delta.minute,
        second=delta.second,
    )


class WeatherService:
    def __init__(self, config: WeatherConfig) -> None:
        self.config = config

    def generate_weather(self, time_: datetime) -> WeatherDTO:
        accelerated = accelerated_time(time_, factor=self.config.acceleration)
        datetime_str = accelerated.strftime("%Y-%m-%d %H:%M:%S")
        date_str = accelerated.strftime("%Y-%m-%d")
        time_str = accelerated.strftime("%H:%M:%S")

        return WeatherDTO(
            acceleration=self.config.acceleration,
            date=date_str,
            time=time_str,
            weather=WeatherDetailsDTO(
                date=date_str,
                time=datetime_str,
                cloud=betavariate_float(self.config.clouds),
                wind_speed=betavariate_float(self.config.wind_speed),
                wind_direction=random.choices(
                    self.config.wind_direction.values,
                    weights=self.config.wind_direction.weights,
                )[0],
                wind_gustiness=random.uniform(
                    self.config.wind_gustiness.min,
                    self.config.wind_gustiness.max,
                ),
                rain=random.choices(
                    self.config.rain.values,
                    weights=self.config.rain.weights,
                )[0],
                rain_intensity=betavariate_float(self.config.rain_intensity),
                fog=betavariate_float(self.config.fog),
                temp=random.randint(
                    self.config.temp.min,
                    self.config.temp.max,
                ),
                pressure=random.randint(
                    self.config.pressure.min,
                    self.config.pressure.max,
                ),
            ),
        )
