import dataclasses
from collections.abc import Mapping, Sequence

from app.core.domain.models import FleaMarketSort, FleaMarketSortDirection
from app.core.dto import MinMaxDTO
from app.db.models import FleaOffer
from lib.models import CamelCaseModel
from lib.tarkov.flea_market.models import FleaMarketOffer
from lib.tarkov.types import ItemTemplateId


@dataclasses.dataclass(kw_only=True, slots=True)
class FleaMarketSearchDTO:
    condition: MinMaxDTO[int | None]
    price: MinMaxDTO[int | None]
    quantity: MinMaxDTO[int | None]

    include_categories: bool

    handbook_id: str
    exclude_bartering_offers: bool
    currency_id: ItemTemplateId | None
    linked_search_id: ItemTemplateId | None

    page: int
    page_size: int
    sort: FleaMarketSort
    sort_direction: FleaMarketSortDirection


class FleaMarketSearchResultSchema(CamelCaseModel):
    categories: Mapping[str, int] | None
    offers: list[FleaMarketOffer]
    offers_count: int
    selected_category: str


@dataclasses.dataclass(frozen=True, kw_only=True, slots=True)
class FleaSearchResult:
    items: Sequence[FleaOffer]
    total_count: int
    categories: Mapping[str, int] | None
