from app.core.domain.flea_market.dto import (
    FleaMarketSearchDTO,
    FleaMarketSearchResultSchema,
)
from app.core.domain.flea_market.repository import FleaOffersRepository
from app.core.domain.items.services import HandbookDB
from app.db.models import FleaOffer
from lib.tarkov.flea_market.models import FleaMarketOffer, FleaMarketOfferUser
from lib.tarkov.trading.trader import Traders


class FleaMarketSearchQuery:
    def __init__(
        self,
        traders: Traders,
        handbook_db: HandbookDB,
        offers_repository: FleaOffersRepository,
    ) -> None:
        self._traders = traders
        self._handbook_db = handbook_db
        self._offers_repo = offers_repository

    async def execute(self, dto: FleaMarketSearchDTO) -> FleaMarketSearchResultSchema:
        result = await self._offers_repo.search(search=dto)
        offers = [self._map_offer(offer) for offer in result.items]
        return FleaMarketSearchResultSchema(
            categories=result.categories,
            offers=offers,
            offers_count=result.total_count,
            selected_category=dto.handbook_id,
        )

    def _map_offer(self, offer: FleaOffer) -> FleaMarketOffer:  # pragma: no cover
        return FleaMarketOffer(
            items=offer.items,
            requirements=offer.requirements,
            id=str(offer.id),
            int_id=offer.int_id,
            root=offer.root_item_id,
            items_cost=offer.items_cost,
            requirements_cost=offer.requirement_items_cost,
            start_time=offer.start_time,
            end_time=offer.end_time,
            sell_in_one_piece=offer.sell_in_one_piece,
            loyalty_level=offer.required_loyalty_level,
            locked=False,
            unlimited_count=False,
            summary_cost=offer.requirement_items_cost,
            user=FleaMarketOfferUser(
                id=offer.user_id,
                nickname=None,
                rating=None,
                member_type=offer.user_type,
                avatar=None,
                is_rating_growing=None,
            ),
            current_item_count=offer.count,
            priority=offer.priority,
            not_available=False,
        )
