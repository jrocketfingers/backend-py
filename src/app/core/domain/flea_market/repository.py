from collections.abc import Mapping, Sequence
from typing import Any, assert_never
from uuid import UUID

from sqlalchemy import (
    Dialect,
    Select,
    func,
    literal,
    literal_column,
    select,
)
from sqlalchemy.dialects.postgresql.base import PGDialect
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import InstrumentedAttribute
from sqlalchemy.sql.functions import _FunctionGenerator

from app.core.domain.flea_market.dto import FleaMarketSearchDTO, FleaSearchResult
from app.core.domain.items.services import HandbookDB
from app.core.domain.models import FleaMarketSort, FleaMarketSortDirection
from app.core.dto import MinMaxDTO
from app.db.models import FleaOffer
from lib.tarkov.items.storage import ItemDB


def _unnest_func(dialect: Dialect) -> _FunctionGenerator:  # pragma: no cover
    if isinstance(dialect, PGDialect):
        return func.json_array_elements_text
    return func.json_each


class FleaOffersRepository:
    def __init__(
        self,
        session: AsyncSession,
        handbook_db: HandbookDB,
        item_db: ItemDB,
    ) -> None:
        self._session = session
        self._handbook_db = handbook_db
        self._item_db = item_db

    async def get(self, id: UUID, *, with_for_update: bool = False) -> FleaOffer | None:
        return await self._session.get(
            FleaOffer,
            ident=id,
            with_for_update=with_for_update,
        )

    async def delete(self, offer: FleaOffer) -> None:
        await self._session.delete(offer)

    async def search(
        self,
        search: FleaMarketSearchDTO,
    ) -> FleaSearchResult:
        stmt = select(FleaOffer)
        stmt = self._sort(stmt, sort=search.sort, direction=search.sort_direction)
        stmt = self._common_filters(stmt=stmt, search=search)
        categories_stmt = stmt
        stmt = self._filter(stmt=stmt, search=search)
        count_stmt = select(func.count("*")).select_from(
            stmt.order_by(None).with_only_columns(FleaOffer.id).subquery(),
        )
        stmt = stmt.limit(search.page_size).offset(search.page * search.page_size)
        return FleaSearchResult(
            items=(await self._session.scalars(stmt)).all(),
            total_count=await self._session.scalar(count_stmt) or 0,
            categories=(
                await self._categories(categories_stmt)
                if search.include_categories
                else None
            ),
        )

    async def _categories(self, stmt: Select[tuple[FleaOffer]]) -> Mapping[str, int]:
        category_stmt = (
            stmt.with_only_columns(
                FleaOffer.root_item_template_id,
                func.count(FleaOffer.id),
            )
            .group_by(FleaOffer.root_item_template_id)
            .order_by(None)
        )
        return dict(
            row._t for row in await self._session.execute(category_stmt)  # noqa: SLF001
        )

    def _common_filters(
        self,
        stmt: Select[tuple[FleaOffer]],
        search: FleaMarketSearchDTO,
    ) -> Select[tuple[FleaOffer]]:
        """Apply filters to both categories count and items."""
        if search.exclude_bartering_offers:
            stmt = stmt.where(FleaOffer.is_bartering.is_(False))

        if search.linked_search_id is not None:
            item = self._item_db.get(search.linked_search_id)
            stmt = stmt.where(
                FleaOffer.root_item_template_id.in_(
                    self._item_db.linked_item_ids(item=item),
                ),
            )

        return stmt

    def _filter(
        self,
        stmt: Select[tuple[FleaOffer]],
        search: FleaMarketSearchDTO,
    ) -> Select[tuple[FleaOffer]]:

        # Seems like client would filter out categories themselves,
        # so this filter can be applied later
        if search.handbook_id:  # Handbook id could be an empty string ("")
            stmt = stmt.where(
                select(literal(1))
                .select_from(
                    _unnest_func(self._session.bind.dialect)(FleaOffer.category_ids),
                )
                .where(literal_column("value") == search.handbook_id)
                .exists(),
            )
        if search.currency_id is not None:
            stmt = stmt.where(FleaOffer.currency_id == search.currency_id)

        min_max_filters: Sequence[
            tuple[MinMaxDTO[int | None], InstrumentedAttribute[int | float]]
        ] = [
            (search.quantity, FleaOffer.count),
            (search.price, FleaOffer.summary_cost),
            (search.condition, FleaOffer.condition),
        ]
        for filter_, attr in min_max_filters:
            if filter_.min is not None:
                stmt = stmt.where(filter_.min <= attr)
            if filter_.max is not None:
                stmt = stmt.where(attr <= filter_.max)
        return stmt

    def _sort(
        self,
        stmt: Select[tuple[FleaOffer]],
        sort: FleaMarketSort,
        direction: FleaMarketSortDirection,
    ) -> Select[tuple[FleaOffer]]:  # pragma: no cover
        model_field: InstrumentedAttribute[Any]
        match sort:
            case FleaMarketSort.id:
                model_field = FleaOffer.id
            case FleaMarketSort.category:
                model_field = FleaOffer.id
            case FleaMarketSort.rating:
                model_field = FleaOffer.id
            case FleaMarketSort.offer_title:
                model_field = FleaOffer.id
            case FleaMarketSort.price:
                model_field = FleaOffer.requirement_items_cost
            case FleaMarketSort.expiry:
                model_field = FleaOffer.end_time
            case _:
                assert_never(sort)

        field = (
            model_field.desc()
            if direction == FleaMarketSortDirection.asc
            else model_field.asc()
        )

        return stmt.order_by(field, FleaOffer.id)
