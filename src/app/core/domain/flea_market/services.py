from collections.abc import AsyncIterator
from datetime import datetime

from app.core.domain.flea_market.repository import FleaOffersRepository
from app.core.domain.items.services import HandbookDB
from app.core.domain.traders.repositories import TraderAssortRepository
from app.db.models import FleaOffer
from lib.errors import Never
from lib.tarkov.chat import MemberCategory
from lib.tarkov.flea_market.models import FleaMarketOfferRequirement
from lib.tarkov.items.models import Item
from lib.tarkov.items.props.item import MoneyProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.notifications.models import FleaOfferSoldNotification, NotificationType
from lib.tarkov.notifications.service import NotificationService
from lib.tarkov.trading.trader import TraderModel, Traders


class FleaMarketService:
    def __init__(  # noqa: PLR0913
        self,
        handbook_db: HandbookDB,
        traders: Traders,
        item_db: ItemDB,
        assort_repository: TraderAssortRepository,
        flea_repository: FleaOffersRepository,
        notification_service: NotificationService,
    ) -> None:
        self._handbook_db = handbook_db
        self._traders = traders
        self._item_db = item_db
        self._assort_repository = assort_repository
        self._flea_repository = flea_repository
        self._notification_service = notification_service

    async def buy_offer(self, offer: FleaOffer, count: int) -> None:
        if count <= 0:
            raise ValueError
        if offer.count < count:
            raise ValueError

        offer.count -= count
        if offer.count == 0:
            await self._flea_repository.delete(offer)

        if not (offer.user_type & MemberCategory.trader):
            await self._notification_service.send(
                FleaOfferSoldNotification(
                    type=NotificationType.RAGFAIR_OFFER_SOLD,
                    offer_id=str(offer.id),
                    count=count,
                    handbook_id=offer.root_item_template_id,
                ),
                profile_id=offer.user_id,
            )

    def can_sell_on_flea(self, item: Item) -> bool:
        item_template = self._item_db.get(item.template_id)
        return item_template.props.can_sell_on_ragfair

    async def make_trader_offers(
        self,
        trader_model: TraderModel,
        now: datetime,
        next_resupply_time: datetime,
    ) -> AsyncIterator[FleaOffer]:
        inventory = self._traders.inventory(trader=trader_model)
        for item_id, barter_schemes in trader_model.assort.barter_scheme.items():
            item = inventory.get(item_id)
            if not item:
                raise Never
            if not self.can_sell_on_flea(item):
                continue
            children = inventory.children(item)
            items = [item, *children]

            assort = await self._assort_repository.get(
                trader_id=trader_model.base.id,
                item_id=item.id,
            )
            if assort is None:
                raise Never

            for barter_scheme in barter_schemes:
                yield self._create_offer(
                    item=item,
                    items=items,
                    count=assort.count,
                    requirements=[
                        FleaMarketOfferRequirement(
                            template_id=requirement.template_id,
                            count=requirement.count,
                            only_functional=True,
                        )
                        for requirement in barter_scheme
                    ],
                    required_loyalty_level=trader_model.assort.loyal_level_items.get(
                        item.id,
                        1,
                    ),
                    seller=trader_model,
                    start_time=now,
                    end_time=next_resupply_time,
                )

    def _create_offer(  # noqa: PLR0913
        self,
        item: Item,
        items: list[Item],
        count: int,
        requirements: list[FleaMarketOfferRequirement],
        required_loyalty_level: int,
        seller: TraderModel,
        start_time: datetime,
        end_time: datetime,
    ) -> FleaOffer:
        items_cost_sum = sum(
            self._handbook_db.items[i.template_id].price for i in items
        )
        is_bartering = not all(
            isinstance(self._item_db.get(requirement.template_id).props, MoneyProps)
            for requirement in requirements
        )
        currency_id = (
            None
            if is_bartering
            else next(requirement.template_id for requirement in requirements)
        )

        return FleaOffer(
            root_item_id=item.id,
            root_item_template_id=item.template_id,
            items=items,
            items_cost=items_cost_sum,
            requirement_items_cost=sum(
                self._handbook_db.items[req.template_id].price * req.count
                for req in requirements
            ),
            category_ids=[
                c.id
                for c in self._handbook_db.parent_categories(
                    item_id=item.template_id,
                )
            ],
            currency_id=currency_id,
            requirements=[
                FleaMarketOfferRequirement(
                    template_id=req.template_id,
                    count=req.count,
                    only_functional=True,
                )
                for req in requirements
            ],
            sell_in_one_piece=False,
            required_loyalty_level=required_loyalty_level,
            is_bartering=is_bartering,
            count=count,
            condition=100,
            priority=False,
            user_type=MemberCategory.trader,
            user_id=seller.base.id,
            start_time=start_time,
            end_time=end_time,
            summary_cost=items_cost_sum,
        )
