from collections.abc import Iterable, Sequence
from typing import Final, assert_never

from lib.errors import Never
from lib.tarkov.hideout.models.area import HideoutAreaTemplate
from lib.tarkov.hideout.models.bonuses import (
    AnyHideoutAreaBonus,
    HideoutAdditionalSlotsBonus,
    HideoutBonusStashSize,
    HideoutBonusType,
    HideoutGenericBonus,
    HideoutSkillGroupLevelingBonus,
    HideoutTextBonus,
)
from lib.tarkov.hideout.models.qte import HideoutQuickTimeEvents
from lib.tarkov.hideout.models.recipes import HideoutRecipe, ScavCaseRecipe
from lib.tarkov.hideout.models.settings import HideoutSettings
from lib.tarkov.inventory import Inventory
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.models import (
    AdditionalSlotsBonus,
    AnyBonus,
    Bonus,
    HideoutArea,
    HideoutAreaType,
    Profile,
    SkillGroupLevelingBonus,
    StashSizeBonus,
    TextBonus,
)
from lib.utils import utc_min, utc_now


def find_area(areas: Iterable[HideoutArea], area_type: HideoutAreaType) -> HideoutArea:
    return next(area for area in areas if area.type == area_type)  # pragma: no branch


def map_bonus(bonus: AnyHideoutAreaBonus) -> AnyBonus:
    match bonus:
        case HideoutAdditionalSlotsBonus():
            return AdditionalSlotsBonus(
                id=bonus.id,
                filter=bonus.filter.copy(),
                type=bonus.type,
            )
        case HideoutTextBonus():
            return TextBonus(
                id=bonus.id,
                icon=bonus.icon,
                type=bonus.type,
            )
        case HideoutBonusStashSize():
            return StashSizeBonus(
                id=bonus.id,
                template_id=bonus.template_id,
                type=bonus.type,
            )
        case HideoutGenericBonus():
            return Bonus(
                id=bonus.id,
                type=bonus.type,
                passive=bonus.passive,
                production=bonus.production,
                visible=bonus.visible,
                icon=bonus.icon,
            )
        case HideoutSkillGroupLevelingBonus():
            return SkillGroupLevelingBonus(
                id=bonus.id,
                type=bonus.type,
                skill_type=bonus.skill_type,
            )
    assert_never(bonus)


class HideoutService:
    def __init__(  # noqa: PLR0913
        self,
        area_templates: list[HideoutAreaTemplate],
        quick_time_events: list[HideoutQuickTimeEvents],
        settings: HideoutSettings,
        recipes: list[HideoutRecipe],
        scav_case_recipes: list[ScavCaseRecipe],
        item_db: ItemDB,
    ) -> None:
        self.areas: Final = area_templates
        self.quick_time_events: Final = quick_time_events
        self.settings: Final = settings
        self.recipes: Final = recipes
        self.scav_case_recipes: Final = scav_case_recipes

        self._item_db = item_db

    def area_template(self, type: HideoutAreaType) -> HideoutAreaTemplate:
        return next(  # pragma: no branch
            area for area in self.areas if area.type == type
        )

    async def upgrade(self, area: HideoutArea) -> None:
        area_tpl = self.area_template(type=area.type)
        stage = area_tpl.stages[area.level + 1]

        area.constructing = True
        area.complete_time = utc_now() + stage.construction_time

    async def upgrade_complete(
        self,
        area: HideoutArea,
        profile: Profile,
        inventory: Inventory,
    ) -> None:
        area.level += 1
        area.constructing = False
        area.complete_time = utc_min

        area_tpl = self.area_template(type=area.type)
        stage = area_tpl.stages[area.level]
        await self._apply_bonuses(
            bonuses=stage.bonuses,
            profile=profile,
            inventory=inventory,
        )

    async def _apply_bonuses(
        self,
        bonuses: Sequence[AnyHideoutAreaBonus],
        profile: Profile,
        inventory: Inventory,
    ) -> None:
        for bonus in bonuses:
            if isinstance(bonus, HideoutBonusStashSize):
                stash = inventory.get(profile.inventory.stash)
                if not stash:
                    raise Never
                stash, children = inventory.remove(stash).unwrap()
                stash.template_id = bonus.template_id
                inventory.add(stash, children=children, location=None).unwrap()

            if isinstance(bonus, HideoutGenericBonus):

                match bonus.type:
                    case HideoutBonusType.maximum_energy:
                        profile.health.energy.maximum += bonus.value

            profile.bonuses.append(map_bonus(bonus))
