from collections.abc import Sequence

from app.core.domain.hideout.services import HideoutService
from lib.tarkov.hideout.models.area import HideoutAreaTemplate
from lib.tarkov.hideout.models.qte import HideoutQuickTimeEvents
from lib.tarkov.hideout.models.recipes import HideoutRecipe, ScavCaseRecipe
from lib.tarkov.hideout.models.settings import HideoutSettings


class HideoutAreasQuery:
    def __init__(self, service: HideoutService) -> None:
        self._service = service

    async def execute(self) -> Sequence[HideoutAreaTemplate]:
        return self._service.areas


class HideoutQTEListQuery:
    def __init__(self, service: HideoutService) -> None:
        self._service = service

    async def execute(self) -> Sequence[HideoutQuickTimeEvents]:
        return self._service.quick_time_events


class HideoutSettingsQuery:
    def __init__(self, service: HideoutService) -> None:
        self._service = service

    async def execute(self) -> HideoutSettings:
        return self._service.settings


class HideoutRecipesQuery:
    def __init__(self, service: HideoutService) -> None:
        self._service = service

    async def execute(self) -> Sequence[HideoutRecipe]:
        return self._service.recipes


class HideoutScavCaseRecipesQuery:
    def __init__(self, service: HideoutService) -> None:
        self._service = service

    async def execute(self) -> Sequence[ScavCaseRecipe]:
        return self._service.scav_case_recipes
