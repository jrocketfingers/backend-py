from result import Err, Ok, Result

from app.core.domain.accounts.dto import ProfileFindDTO
from app.core.domain.accounts.repositories import ProfileRepository
from app.core.domain.friends.errors import FriendRequestNotFound, ProfileNotFound
from app.core.domain.friends.repository import FriendRepository, FriendRequestRepository
from app.db.models import AccountProfile, FriendRequest
from app.db.models._friends import Friends
from lib.db import DBContext
from lib.tarkov.chat import ChatMember
from lib.tarkov.notifications.models import (
    FriendListNewRequestNotification,
    FriendListRemovetNotification,
    FriendListRequestAcceptNotification,
    FriendListRequestCancelledNotification,
    NotificationType,
)
from lib.tarkov.notifications.service import NotificationService


class FriendSendRequestCommand:
    def __init__(
        self,
        friend_request_repository: FriendRequestRepository,
        profile_repository: ProfileRepository,
        notification_service: NotificationService,
        db_context: DBContext,
    ) -> None:
        self._friend_request_repository = friend_request_repository
        self._profile_repository = profile_repository
        self._notification_service = notification_service
        self._db_context = db_context

    async def execute(
        self,
        sender: AccountProfile,
        recipient_profile_id: str,
    ) -> Result[FriendRequest, ProfileNotFound]:
        recipient = await self._profile_repository.find(
            dto=ProfileFindDTO(profile_id=recipient_profile_id),
        )
        if recipient is None:
            return Err(ProfileNotFound())

        request = await self._friend_request_repository.find(
            sender=sender,
            recipient=recipient,
        )
        if request is not None:
            return Ok(request)

        request = FriendRequest(sender=sender, recipient=recipient)
        self._db_context.add(request)
        await self._notification_service.send(
            notification=FriendListNewRequestNotification(
                id=str(request.id),
                profile=ChatMember.from_profile(sender),
                type=NotificationType.FRIEND_LIST_NEW_REQUEST,
            ),
            profile_id=str(recipient.id),
        )
        return Ok(request)


class FriendCancelRequestCommand:
    def __init__(
        self,
        friend_request_repository: FriendRequestRepository,
        profile_repository: ProfileRepository,
        notification_service: NotificationService,
    ) -> None:
        self._friend_request_repository = friend_request_repository
        self._profile_repository = profile_repository
        self._notification_service = notification_service

    async def execute(
        self,
        sender: AccountProfile,
        recipient_profile_id: str,
    ) -> Result[None, ProfileNotFound]:
        recipient = await self._profile_repository.find(
            dto=ProfileFindDTO(profile_id=recipient_profile_id),
        )
        if recipient is None:
            return Err(ProfileNotFound())

        request = await self._friend_request_repository.find(
            sender=sender,
            recipient=recipient,
        )
        if request is None:
            return Ok(None)

        await self._friend_request_repository.delete(request)
        await self._notification_service.send(
            notification=FriendListRequestCancelledNotification(
                profile=ChatMember.from_profile(sender),
                type=NotificationType.FRIEND_LIST_REQUEST_CANCEL,
            ),
            profile_id=str(recipient.id),
        )
        return Ok(None)


class FriendAcceptRequestCommand:
    def __init__(
        self,
        friend_request_repository: FriendRequestRepository,
        friend_repository: FriendRepository,
        profile_repository: ProfileRepository,
        notification_service: NotificationService,
    ) -> None:
        self._friend_request_repository = friend_request_repository
        self._friend_repository = friend_repository
        self._profile_repository = profile_repository
        self._notification_service = notification_service

    async def execute(
        self,
        recipient: AccountProfile,
        sender_profile_id: str,
    ) -> Result[Friends, FriendRequestNotFound | ProfileNotFound]:
        sender = await self._profile_repository.find(
            dto=ProfileFindDTO(profile_id=sender_profile_id),
        )
        if sender is None:
            return Err(ProfileNotFound())

        request = await self._friend_request_repository.find(
            sender=sender,
            recipient=recipient,
        )
        if request is None:
            return Err(FriendRequestNotFound())
        await self._friend_request_repository.delete(request)

        friends = self._friend_repository.create(
            sender=sender,
            recipient=recipient,
        )
        await self._notification_service.send(
            notification=FriendListRequestAcceptNotification(
                type=NotificationType.FRIEND_LIST_REQUEST_ACCEPT,
                profile=ChatMember.from_profile(recipient),
            ),
            profile_id=str(sender.id),
        )

        return Ok(friends)


class FriendDeleteCommand:
    def __init__(
        self,
        friend_request_repository: FriendRequestRepository,
        friend_repository: FriendRepository,
        profile_repository: ProfileRepository,
        notification_service: NotificationService,
    ) -> None:
        self._friend_request_repository = friend_request_repository
        self._friend_repository = friend_repository
        self._profile_repository = profile_repository
        self._notification_service = notification_service

    async def execute(
        self,
        profile: AccountProfile,
        friend_profile_id: str,
    ) -> Ok[None]:
        if not (
            friend := await self._profile_repository.find(
                dto=ProfileFindDTO(profile_id=friend_profile_id),
            )
        ):
            return Ok(None)  # pragma: no cover

        friends = await self._friend_repository.get(profile=profile, friend=friend)
        if friends is None:
            return Ok(None)  # pragma: no cover

        await self._friend_repository.delete(friends)
        await self._notification_service.send(
            profile_id=str(friend.id),
            notification=FriendListRemovetNotification(
                type=NotificationType.FRIEND_LIST_REMOVE,
                profile=ChatMember.from_profile(profile),
            ),
        )
        return Ok(None)
