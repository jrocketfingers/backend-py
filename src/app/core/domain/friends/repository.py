from collections.abc import Sequence
from uuid import UUID

from sqlalchemy import and_, delete, func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.core.domain.friends.filter import FriendRequestFilter
from app.db.models import AccountProfile, FriendRequest
from app.db.models._friends import Friends


class FriendRequestRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(self, id: UUID) -> FriendRequest | None:
        return await self._session.get(FriendRequest, ident=id)

    async def delete(self, request: FriendRequest) -> None:
        await self._session.delete(request)

    async def find(
        self,
        sender: AccountProfile,
        recipient: AccountProfile,
    ) -> FriendRequest | None:
        stmt = select(FriendRequest).where(
            FriendRequest.sender_id == sender.id,
            FriendRequest.recipient_id == recipient.id,
        )
        return await self._session.scalar(stmt)

    async def list(
        self,
        filter_: FriendRequestFilter,
        options: Sequence[ORMOption] | None = None,
    ) -> Sequence[FriendRequest]:
        stmt = select(FriendRequest).order_by(FriendRequest.id)

        if filter_.recipient:
            stmt = stmt.where(FriendRequest.recipient == filter_.recipient)
        if filter_.sender:
            stmt = stmt.where(FriendRequest.sender == filter_.sender)

        if options:
            stmt = stmt.options(*options)
        return (await self._session.scalars(stmt)).all()


class FriendRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    def create(
        self,
        sender: AccountProfile,
        recipient: AccountProfile,
    ) -> Friends:
        a = Friends(sender=sender, recipient=recipient)
        b = Friends(sender=recipient, recipient=sender)

        self._session.add(a)
        self._session.add(b)
        return a

    async def friends(self, profile: AccountProfile) -> Sequence[AccountProfile]:
        stmt = (
            select(AccountProfile)
            .join(
                Friends,
                onclause=and_(
                    Friends.sender_id == profile.id,
                    Friends.recipient_id == AccountProfile.id,
                ),
            )
            .order_by(func.lower(AccountProfile.name))
            .where(AccountProfile.id != profile.id)
        )
        return (await self._session.scalars(stmt)).all()

    async def get(
        self,
        profile: AccountProfile,
        friend: AccountProfile,
    ) -> Friends | None:
        stmt = select(Friends).where(
            Friends.sender == profile,
            Friends.recipient == friend,
        )
        return await self._session.scalar(stmt)

    async def delete(self, friends: Friends) -> None:
        stmt = delete(Friends).where(
            Friends.recipient_id == friends.sender_id,
            Friends.sender_id == friends.recipient_id,
        )
        await self._session.execute(stmt)
        await self._session.delete(friends)
