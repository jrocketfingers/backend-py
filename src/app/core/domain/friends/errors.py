import dataclasses


@dataclasses.dataclass
class ProfileNotFound:
    pass


@dataclasses.dataclass
class FriendRequestNotFound:
    pass
