from collections.abc import Sequence

from sqlalchemy.orm import joinedload

from app.core.domain.friends.filter import FriendRequestFilter
from app.core.domain.friends.repository import FriendRepository, FriendRequestRepository
from app.db.models import AccountProfile, FriendRequest
from lib.tarkov.chat import ChatMember
from lib.tarkov.friends.models import FriendList


class FriendListQuery:
    def __init__(self, friend_repository: FriendRepository) -> None:
        self._friend_repository = friend_repository

    async def execute(self, profile: AccountProfile) -> FriendList:
        friends = await self._friend_repository.friends(profile=profile)
        return FriendList(
            friends=[ChatMember.from_profile(friend) for friend in friends],
            ignore=[],
            in_ignore_list=[],
        )


class FriendRequestInboxQuery:
    def __init__(self, friend_request_repository: FriendRequestRepository) -> None:
        self._friend_request_repository = friend_request_repository

    async def execute(self, user: AccountProfile) -> Sequence[FriendRequest]:
        return await self._friend_request_repository.list(
            filter_=FriendRequestFilter(
                recipient=user,
            ),
            options=(joinedload(FriendRequest.sender),),
        )


class FriendRequestOutboxQuery:
    def __init__(self, friend_request_repository: FriendRequestRepository) -> None:
        self._friend_request_repository = friend_request_repository

    async def execute(self, user: AccountProfile) -> Sequence[FriendRequest]:
        return await self._friend_request_repository.list(
            filter_=FriendRequestFilter(
                sender=user,
            ),
            options=(joinedload(FriendRequest.recipient),),
        )
