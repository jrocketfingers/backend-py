import dataclasses

from app.db.models import AccountProfile


@dataclasses.dataclass
class FriendRequestFilter:
    sender: AccountProfile | None = None
    recipient: AccountProfile | None = None
