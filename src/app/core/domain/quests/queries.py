from collections.abc import Sequence

from app.core.domain.quests.services import QuestsService
from app.db.models import AccountProfile
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.quests.models import QuestTemplate


class QuestListQuery:
    def __init__(
        self,
        quests_service: QuestsService,
        profile_storage: ProfileStorage,
    ) -> None:
        self._service = quests_service
        self._profile_storage = profile_storage

    async def execute(self, account_profile: AccountProfile) -> Sequence[QuestTemplate]:
        profile = await self._profile_storage.readonly(
            profile_id=str(account_profile.id),
        )
        return await self._service.available_quests(profile=profile.pmc)
