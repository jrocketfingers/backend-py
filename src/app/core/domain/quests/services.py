from collections.abc import Sequence
from typing import assert_never

from app.core.domain.traders.services import calculate_loyalty_level
from lib.tarkov.config import QuestConfig
from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.models import Profile
from lib.tarkov.quests.models import (
    AnyQuestCondition,
    LevelCondition,
    QuestStatusCondition,
    QuestTemplate,
    TraderLoyaltyCondition,
    TraderStandingCondition,
)
from lib.tarkov.quests.storage import QuestStorage
from lib.tarkov.trading.trader import Traders
from lib.tarkov.types import QuestId
from lib.tarkov.utils import compare_operator


def _quest_is_accessible_to_side(
    quest: QuestTemplate,
    side: PMCSide,
    config: QuestConfig,
) -> bool:
    if side == "Bear" and quest.id in config.usec_only:
        return False
    if side == "Usec" and quest.id in config.bear_only:
        return False
    return True


class QuestsService:
    def __init__(
        self,
        config: QuestConfig,
        quests: QuestStorage,
        traders: Traders,
    ) -> None:
        self._config = config
        self._quests = quests
        self._traders = traders

    async def available_quests(
        self,
        profile: Profile,
    ) -> Sequence[QuestTemplate]:
        quest_ids = {q.id for q in profile.quests}
        return [
            quest
            for quest in self._quests.quests.values()
            if await self.is_quest_available(
                profile=profile,
                quest=quest,
                profile_quest_ids=quest_ids,
            )
        ]

    async def is_quest_available(
        self,
        profile: Profile,
        quest: QuestTemplate,
        profile_quest_ids: set[QuestId] | None = None,
    ) -> bool:
        if quest.id in self._config.event_quests:
            return False

        if profile_quest_ids is None:
            profile_quest_ids = {q.id for q in profile.quests}

        if quest.id in profile_quest_ids:
            return True

        if not _quest_is_accessible_to_side(
            quest=quest,
            side=profile.info.side,
            config=self._config,
        ):
            return False

        return all(
            self._condition_fulfilled(profile=profile, condition=condition)
            for condition in quest.conditions.available_for_start
        )

    def _condition_fulfilled(  # noqa: C901
        self,
        profile: Profile,
        condition: AnyQuestCondition,
    ) -> bool:
        if isinstance(condition, LevelCondition):
            return compare_operator(condition.compare_method)(
                profile.info.level,
                condition.value,
            )
        if isinstance(condition, TraderStandingCondition):
            trader_info = profile.traders_info[condition.target]
            return compare_operator(condition.compare_method)(
                trader_info.standing,
                condition.value,
            )
        if isinstance(condition, TraderLoyaltyCondition):
            trader_info = profile.traders_info[condition.target]
            trader = self._traders.get(trader_id=condition.target)
            if not trader:
                raise ValueError  # pragma: no cover
            loyalty_level = calculate_loyalty_level(
                player_level=profile.info.level,
                current_standing=trader_info,
                loyalty_levels=trader.base.loyalty_levels,
            )
            return compare_operator(condition.compare_method)(
                loyalty_level.number,
                condition.value,
            )
        if isinstance(condition, QuestStatusCondition):
            previous_quest = next(
                (q for q in profile.quests if q.id == condition.target),
                None,
            )
            if not previous_quest:
                return False
            return previous_quest.status in condition.status

        assert_never(condition)
