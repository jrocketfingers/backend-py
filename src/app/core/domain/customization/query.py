from collections.abc import Mapping, Sequence

from app.core.domain.items.services import CustomizationDB
from lib.tarkov.customization import Customization


class AccountCustomizationQuery:
    def __init__(self, db: CustomizationDB) -> None:
        self._db = db

    async def execute(self) -> Sequence[str]:
        return [
            key for key, value in self._db.customization.items() if value.props.side
        ]


class AllCustomizationQuery:
    def __init__(self, customization_db: CustomizationDB) -> None:
        self._customization_db = customization_db

    async def execute(self) -> Mapping[str, Customization]:
        return self._customization_db.customization
