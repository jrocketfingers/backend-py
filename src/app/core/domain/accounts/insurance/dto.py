import dataclasses

from lib.tarkov.types import ItemId


@dataclasses.dataclass
class InsuranceCostDTO:
    trader_ids: list[str]
    item_ids: list[ItemId]
