import math
from collections.abc import Mapping, Sequence

from app.core.domain.items.services import HandbookDB
from app.core.domain.traders.services import calculate_loyalty_level
from lib.tarkov.items.models import Item
from lib.tarkov.profiles.models import Profile
from lib.tarkov.trading.trader import TraderModel
from lib.tarkov.types import ItemTemplateId


class InsuranceService:
    def __init__(self, handbook_db: HandbookDB) -> None:
        self._handbook_db = handbook_db

    def get_insurance_cost(
        self,
        items: Sequence[Item],
        trader: TraderModel,
        profile: Profile,
    ) -> Mapping[ItemTemplateId, int]:
        loyalty_level = calculate_loyalty_level(
            player_level=profile.info.level,
            current_standing=profile.traders_info[trader.base.id],
            loyalty_levels=trader.base.loyalty_levels,
        )
        result = {}
        for item in items:
            if item.template_id not in self._handbook_db.items:
                continue

            handbook_item = self._handbook_db.items[item.template_id]
            result[item.template_id] = math.ceil(
                handbook_item.price
                * (loyalty_level.loyalty.insurance_price_coef / 100),
            )
        return result
