from collections.abc import Mapping

from app.core.domain.accounts.insurance.dto import InsuranceCostDTO
from app.core.domain.accounts.insurance.services import InsuranceService
from app.db.models import AccountProfile
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.trading.trader import Traders
from lib.tarkov.types import ItemTemplateId


class InsuranceCostQuery:
    def __init__(
        self,
        profile_storage: ProfileStorage,
        traders: Traders,
        service: InsuranceService,
    ) -> None:
        self._profile_storage = profile_storage
        self._traders = traders
        self._service = service

    async def execute(
        self,
        profile: AccountProfile,
        dto: InsuranceCostDTO,
    ) -> Mapping[str, Mapping[ItemTemplateId, int]]:
        result: dict[str, Mapping[ItemTemplateId, int]] = {}
        profile_ctx = await self._profile_storage.readonly(
            profile_id=str(profile.id),
        )
        items = [
            item
            for item_id in dto.item_ids
            if (item := profile_ctx.inventory.get(item_id))
        ]

        for trader_id in dto.trader_ids:
            if not (trader := self._traders.get(trader_id=trader_id)):
                continue  # pragma: no cover

            result[trader.base.id] = self._service.get_insurance_cost(
                items=items,
                trader=trader,
                profile=profile_ctx.pmc,
            )
        return result
