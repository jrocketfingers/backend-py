from collections.abc import Sequence

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.core.domain.accounts.dto import ProfileFindDTO
from app.db.models import Account
from app.db.models._account import AccountProfile
from lib.tarkov.customization import PMCSide
from lib.tarkov.utils import generate_mongodb_id


class AccountRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(
        self,
        session_id: str,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Account | None:
        stmt = select(Account).where(Account.session_id == session_id)
        if options:  # pragma: no branch
            stmt = stmt.options(*options)
        return await self._session.scalar(stmt)


class ProfileRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def find(self, dto: ProfileFindDTO) -> AccountProfile | None:
        stmt = select(AccountProfile).limit(2)
        if dto.name:
            stmt = stmt.where(AccountProfile.name == dto.name)
        if dto.profile_id:
            stmt = stmt.where(AccountProfile.profile_id == dto.profile_id)

        return (await self._session.scalars(stmt)).one_or_none()

    async def create(
        self,
        account: Account,
        name: str,
        side: PMCSide,
    ) -> AccountProfile:
        profile = AccountProfile(
            account=account,
            name=name,
            side=side,
            profile_id=generate_mongodb_id(),
        )
        self._session.add(profile)
        await self._session.flush()
        return profile

    async def search(
        self,
        nickname: str,
        profile: AccountProfile,
        options: Sequence[ORMOption] | None = None,
    ) -> Sequence[AccountProfile]:
        stmt = (
            select(AccountProfile)
            .order_by(AccountProfile.name)
            .where(
                AccountProfile.id != profile.id,
                AccountProfile.name.icontains(nickname),
            )
        )
        if options:
            stmt = stmt.options(*options)

        return (await self._session.scalars(stmt)).all()
