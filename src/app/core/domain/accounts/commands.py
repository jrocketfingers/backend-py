from typing import Final

from result import Err, Ok, Result

from app.core.domain.accounts.dto import ProfileFindDTO
from app.core.domain.accounts.errors import UsernameIsTaken, UsernameTooShort
from app.core.domain.accounts.repositories import ProfileRepository


class ValidateNicknameCommand:
    MIN_USERNAME_LENGTH: Final[int] = 3

    def __init__(self, repository: ProfileRepository) -> None:
        self._repository = repository

    async def execute(
        self,
        name: str,
    ) -> Result[str, UsernameTooShort | UsernameIsTaken]:
        if len(name) < self.MIN_USERNAME_LENGTH:
            return Err(UsernameTooShort())

        profile = await self._repository.find(dto=ProfileFindDTO(name=name))
        if profile is not None:
            return Err(UsernameIsTaken())

        return Ok(name)
