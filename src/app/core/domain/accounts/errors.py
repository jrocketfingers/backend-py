import dataclasses


@dataclasses.dataclass
class UsernameTooShort:
    pass


@dataclasses.dataclass
class UsernameIsTaken:
    pass
