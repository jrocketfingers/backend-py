from collections.abc import Sequence

from sqlalchemy.orm import joinedload

from app.core.domain.accounts.repositories import ProfileRepository
from app.db.models import AccountProfile


class ProfileSearchQuery:
    def __init__(self, profile_repository: ProfileRepository) -> None:
        self._profile_repository = profile_repository

    async def execute(
        self,
        nickname: str,
        profile: AccountProfile,
    ) -> Sequence[AccountProfile]:
        return await self._profile_repository.search(
            nickname=nickname,
            profile=profile,
            options=(joinedload(AccountProfile.account),),
        )
