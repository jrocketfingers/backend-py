import dataclasses


@dataclasses.dataclass(slots=True, kw_only=True)
class ProfileFindDTO:
    profile_id: str | None = None
    name: str | None = None
