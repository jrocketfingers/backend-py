from collections.abc import Mapping

from app.core.domain.items.services import HandbookDB
from lib.tarkov.handbook import Handbook
from lib.tarkov.items.models import ItemNode, ItemTemplate
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.types import ItemTemplateId


class GetClientItemsQuery:
    def __init__(self, item_db: ItemDB) -> None:
        self._item_db = item_db

    async def execute(self) -> Mapping[ItemTemplateId, ItemNode | ItemTemplate]:
        return self._item_db.nodes | self._item_db.items  # type: ignore[operator]


class HandbookQuery:
    def __init__(self, handbook_db: HandbookDB) -> None:
        self._handbook_db = handbook_db

    async def execute(self) -> Handbook:
        return Handbook(
            items=list(self._handbook_db.items.values()),
            categories=list(self._handbook_db.categories.values()),
        )
