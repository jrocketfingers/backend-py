from collections.abc import Iterator, Mapping, Sequence
from typing import Literal

from lib.tarkov.customization import Customization
from lib.tarkov.handbook import HandbookCategory, HandbookItem
from lib.tarkov.types import ItemTemplateId
from lib.utils.unset import Unset


class CustomizationDB:
    def __init__(
        self,
        customization: Mapping[str, Customization],
    ) -> None:
        self._customization = customization

    @property
    def customization(self) -> Mapping[str, Customization]:
        return self._customization


class HandbookDB:
    def __init__(
        self,
        categories: Sequence[HandbookCategory],
        items: Sequence[HandbookItem],
    ) -> None:
        self.items = {item.id: item for item in items}
        self.categories = {category.id: category for category in categories}
        self._item_prices: Mapping[ItemTemplateId, int] | Literal[Unset.unset] = (
            Unset.unset
        )

    @property
    def item_prices(self) -> Mapping[ItemTemplateId, int]:
        if self._item_prices is Unset.unset:
            self._item_prices = {id: item.price for id, item in self.items.items()}
        return self._item_prices

    def parent_categories(
        self,
        item_id: ItemTemplateId,
    ) -> Iterator[HandbookItem | HandbookCategory]:
        item = self.items[item_id]
        yield item

        parent_id = item.parent_id
        while category := self.categories.get(parent_id):
            yield category

            if category.parent_id is None:
                return

            parent_id = category.parent_id

    def item(self, template_id: ItemTemplateId) -> HandbookItem | None:
        return self.items.get(template_id)
