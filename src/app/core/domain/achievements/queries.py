from collections.abc import Mapping, Sequence

from lib.tarkov.achievements.db import AchievementDB
from lib.tarkov.achievements.models import Achievement


class AchievementListQuery:
    def __init__(self, db: AchievementDB) -> None:
        self._db = db

    async def execute(self) -> Sequence[Achievement]:
        return self._db.achievements


class AchievementStatisticQuery:
    def __init__(self, db: AchievementDB) -> None:
        self._db = db

    async def execute(self) -> Mapping[str, float]:
        return {achievement.id: 0 for achievement in self._db.achievements}
