from lib.tarkov.profiles.models import CounterValue, Profile


def set_counter(profile: Profile, key: str, value: int) -> CounterValue:
    generator = (c for c in profile.stats.eft.overall_counters.items if key in c.key)
    counter = next(generator, None)
    if counter is None:  # pragma: no branch
        counter = CounterValue(key=[key], value=value)
        profile.stats.eft.overall_counters.items.append(counter)
    counter.value = value
    return counter
