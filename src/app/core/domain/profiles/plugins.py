import logging
from typing import final

from app.core.domain.accounts.dto import ProfileFindDTO
from app.core.domain.accounts.repositories import ProfileRepository
from lib.plugins.profile import (
    OnProfileChangePlugin,
    OnProfileCreatePlugin,
    OnProfileLoadPlugin,
)
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.profiles.models import HideoutArea, HideoutAreaType, TraderInfo
from lib.tarkov.trading.trader import Traders
from lib.utils import utc_min


@final
class ProfileFixTradersInfo(OnProfileLoadPlugin, OnProfileCreatePlugin):
    def __init__(self, traders: Traders) -> None:
        self._traders = traders

    async def _impl(self, profile_context: ProfileContext) -> None:
        for trader in self._traders:
            profile_context.pmc.traders_info.setdefault(
                trader.base.id,
                TraderInfo(
                    disabled=False,
                    standing=0,
                    unlocked=trader.base.unlocked_by_default,
                ),
            )

    on_profile_create = _impl
    on_profile_load = _impl


@final
class ProfileFixHideoutPlugin(OnProfileLoadPlugin, OnProfileCreatePlugin):
    async def _impl(self, profile_context: ProfileContext) -> None:
        existing_area_types = {area.type for area in profile_context.pmc.hideout.areas}
        for area_type in HideoutAreaType:
            if area_type in existing_area_types:
                continue
            area = HideoutArea(
                type=area_type,
                level=0,
                active=True,
                passive_bonuses_enabled=True,
                complete_time=utc_min,
                constructing=False,
                slots=[],
                last_recipe="",
            )
            profile_context.pmc.hideout.areas.append(area)
            logging.debug(
                "Fixing area %s for profile %s",
                area_type,
                profile_context.pmc.id,
            )

    on_profile_create = _impl
    on_profile_load = _impl


@final
class UpdateProfileInfoPlugin(OnProfileChangePlugin):
    def __init__(self, profile_repository: ProfileRepository) -> None:
        self._profile_repository = profile_repository

    async def on_profile_change(
        self,
        profile_context: ProfileContext,
    ) -> None:
        profile = await self._profile_repository.find(
            dto=ProfileFindDTO(profile_id=profile_context.pmc.id),
        )
        assert profile  # noqa: S101
        profile.level = profile_context.pmc.info.level
        profile.member_category = profile_context.pmc.info.member_category
