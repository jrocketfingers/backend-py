from app.core.domain.profiles.dto import ProfileStatusDTO, ProfileStatusesDTO
from app.db.models import Account
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.profiles.models import Profile, ScavProfile


class ProfileListQuery:
    def __init__(self, profile_storage: ProfileStorage) -> None:
        self._profile_storage = profile_storage

    async def execute(
        self,
        account: Account,
    ) -> tuple[ScavProfile, Profile] | tuple[()]:
        if account.profile is None:
            return ()

        profiles = await self._profile_storage.readonly(
            profile_id=str(account.profile.id),
        )
        return (
            profiles.scav,
            profiles.pmc,
        )


class ProfileStatusQuery:
    def __init__(self, storage: ProfileStorage) -> None:
        self._storage = storage

    async def execute(
        self,
        account: Account,
    ) -> ProfileStatusesDTO:
        if account.profile is None:
            return ProfileStatusesDTO(profiles=[], max_pve_count_exceeded=False)
        async with self._storage.profile(
            profile_id=str(account.profile.id),
        ) as profiles:
            return ProfileStatusesDTO(
                max_pve_count_exceeded=False,
                profiles=[
                    ProfileStatusDTO(
                        profile_id=profile.id,
                        profile_token=None,
                        status="Free",
                        session_id="",
                        ip="",
                        port=0,
                    )
                    for profile in profiles.profiles()
                ],
            )
