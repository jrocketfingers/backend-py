from app.core.domain.accounts.repositories import ProfileRepository
from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import Account, AccountProfile
from lib.tarkov.profiles.abc import ProfileStorage


class ProfileCreateCommand:
    def __init__(
        self,
        repository: ProfileRepository,
        profile_storage: ProfileStorage,
    ) -> None:
        self._repository = repository
        self._storage = profile_storage

    async def execute(
        self,
        account: Account,
        dto: ProfileCreateDTO,
    ) -> AccountProfile:
        profile = await self._repository.create(
            account=account,
            name=dto.name,
            side=dto.side,
        )
        await self._storage.initialize_profile(
            profile=profile,
            dto=dto,
        )
        return profile


class ProfileLogoutCommand:
    def __init__(self, profile_storage: ProfileStorage) -> None:
        self._storage = profile_storage

    async def execute(self, account: Account) -> None:
        if not account.profile:
            return

        await self._storage.logout(profile_id=str(account.profile.id))
