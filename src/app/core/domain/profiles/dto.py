import dataclasses
from collections.abc import Sequence

from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.models import ProfileStatus


@dataclasses.dataclass(slots=True, frozen=True, kw_only=True)
class ProfileStatusDTO:
    profile_id: str
    profile_token: str | None = None
    status: ProfileStatus
    session_id: str
    ip: str
    port: int


@dataclasses.dataclass(slots=True, frozen=True, kw_only=True)
class ProfileStatusesDTO:
    max_pve_count_exceeded: bool
    profiles: Sequence[ProfileStatusDTO]


@dataclasses.dataclass(slots=True, frozen=True, kw_only=True)
class ProfileCreateDTO:
    name: str
    side: PMCSide
    head_id: str
    voice_id: str
