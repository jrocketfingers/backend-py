from pydantic_core import Url

from app.core.domain.locales.services import LocaleService
from app.core.domain.startup.dto import ClientConfigDTO
from app.db.models import Account
from lib.tarkov.backend_info import BackendInfo, ClientGameConfig
from lib.tarkov.db import AssetsPath, Globals, ResourceDB
from lib.utils import read_json, utc_now


class GetGlobalsQuery:
    def __init__(
        self,
        resource_db: ResourceDB,
    ) -> None:
        self._db = resource_db

    async def execute(self) -> Globals:
        return self._db.globals_


class ClientSettingsQuery:
    def __init__(self, assets_path: AssetsPath) -> None:
        self._assets_path = assets_path

    async def execute(self) -> object:
        return await read_json(
            self._assets_path.joinpath("core", "client.settings.json"),
        )


class ClientGameConfigQuery:
    def __init__(self, locale_service: LocaleService) -> None:
        self._locale_service = locale_service

    async def execute(
        self,
        base_url: str,
        account: Account,
    ) -> ClientGameConfig:
        base_url = base_url.rstrip("/")

        config = ClientConfigDTO(
            languages=self._locale_service.available_languages,
            account_id=str(account.id),
            active_profile_id=str(account.profile.id) if account.profile else None,
        )

        return ClientGameConfig(
            languages=config.languages,
            account_id=config.account_id,
            active_profile_id=config.active_profile_id,
            utc_time=int(utc_now().timestamp()),
            total_in_game=0,
            backend=BackendInfo(
                lobby=Url(base_url),
                trading=Url(base_url),
                messaging=Url(base_url),
                main=Url(base_url),
                rag_fair=Url(base_url),
            ),
        )
