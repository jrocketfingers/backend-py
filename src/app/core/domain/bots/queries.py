from result import Err, Ok, Result

from app.core.domain.bots.errors import NoRaidInfoError
from app.core.domain.bots.services import BotService
from app.db.models import AccountProfile
from lib.tarkov.bots.models import BotDifficulty
from lib.tarkov.config import BotConfig


class BotDifficultyQuery:
    def __init__(self, bot_service: BotService) -> None:
        self._bot_service = bot_service

    async def execute(self, bot_type: str, bot_difficulty: str) -> BotDifficulty:
        return self._bot_service.git_difficulty(
            bot_type=bot_type,
            difficulty=bot_difficulty,
        )


class BotMaxCapQuery:
    def __init__(self, config: BotConfig) -> None:
        self._config = config

    async def execute(
        self,
        account_profile: AccountProfile,
    ) -> Result[int, NoRaidInfoError]:
        if not account_profile.raid_info:
            return Err(NoRaidInfoError())

        return Ok(self._config.max_bot_cap[account_profile.raid_info.locations.lower()])


class BotPresetLimitQuery:
    def __init__(self, config: BotConfig) -> None:
        self._config = config

    async def execute(self, role: str) -> int:
        return self._config.preset_batch.get(role, 30)
