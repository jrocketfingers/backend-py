from pydantic import Field

from lib.models import CamelCaseModel
from lib.tarkov.customization import Side


class RaidTimeAndWeatherSchema(CamelCaseModel):
    is_random_time: bool
    is_random_weather: bool
    cloudiness_type: str
    rain_type: str
    wind_type: str
    fog_type: str
    time_flow_type: str
    hour_of_day: int


class RaidBotSettings(CamelCaseModel):
    is_scav_wars: bool
    bot_amount: str


class RaidWavesSettings(CamelCaseModel):
    bot_amount: str
    bot_difficulty: str
    is_bosses: bool
    is_tagged_and_cursed: bool


class RaidConfigurationSchema(CamelCaseModel):
    bot_settings: RaidBotSettings
    can_show_group_preview: bool = Field(alias="CanShowGroupPreview")
    key_id: str
    location: str
    max_group_count: int = Field(alias="MaxGroupCount")
    metabolism_disabled: bool
    players_spawn_place: str
    raid_mode: str
    side: Side
    time_and_weather_settings: RaidTimeAndWeatherSchema
    time_variant: str
    waves_settings: RaidWavesSettings
