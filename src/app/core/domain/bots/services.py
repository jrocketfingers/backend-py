from lib.tarkov.bots.db import BotDB
from lib.tarkov.bots.models import BotDifficulty


class BotService:
    def __init__(self, bot_db: BotDB) -> None:
        self._bot_db = bot_db

    def git_difficulty(
        self,
        bot_type: str,
        difficulty: str,
    ) -> BotDifficulty:
        if difficulty == "core":
            return self._bot_db.core
        bot_type = bot_type.removeprefix("spt")
        return self._bot_db.bots[bot_type.lower()].difficulty[difficulty]  # type: ignore[index]
