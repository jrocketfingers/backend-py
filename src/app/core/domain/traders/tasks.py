import random
from collections.abc import Sequence
from datetime import datetime, timedelta
from typing import final

from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.domain.flea_market.services import FleaMarketService
from app.core.domain.items.services import HandbookDB
from app.db.models import FleaOffer, Trader, TraderAssort, TraderPlayerRestriction
from lib.errors import Never
from lib.tarkov.chat import MemberCategory
from lib.tarkov.trading.trader import Traders
from lib.tasks import Task, TaskResult
from lib.utils import utc_min, utc_now


@final
class UpdateTraderAssortTask(Task):
    def __init__(
        self,
        traders: Traders,
        handbook_db: HandbookDB,
        session: AsyncSession,
        flea_service: FleaMarketService,
    ) -> None:
        self._traders = traders
        self._handbook_db = handbook_db
        self._flea_service = flea_service
        self._session = session

    async def run_once(self) -> TaskResult[Sequence[Trader]]:
        return await self.execute(now=utc_now(), update_flea=True)

    async def execute(
        self,
        now: datetime,
        *,
        update_flea: bool,
    ) -> TaskResult[Sequence[Trader]]:
        now = now or utc_now()

        traders = list(await self._session.scalars(select(Trader)))
        traders.extend(self._init_traders(traders))
        self._session.add_all(traders)
        for trader in traders:
            if trader.next_resupply > now:
                continue  # pragma: no cover
            delta_seconds = random.randrange(
                int(timedelta(hours=1).total_seconds()),
                int(timedelta(hours=4).total_seconds()),
            )
            trader.next_resupply = now + timedelta(seconds=delta_seconds)
            await self._remove_restrictions(trader=trader)
            await self._update_items_count(trader=trader)
            if update_flea:
                await self._add_flea_offers(
                    trader=trader,
                    now=now,
                    next_resupply_time=trader.next_resupply,
                )
        await self._session.flush()
        return TaskResult(
            result=traders,
            next_schedule_at=min(t.next_resupply for t in traders),
        )

    async def _remove_restrictions(
        self,
        trader: Trader,
    ) -> None:
        stmt = delete(TraderPlayerRestriction).where(
            TraderPlayerRestriction.trader == trader,
        )
        await self._session.execute(stmt)

    async def _update_items_count(
        self,
        trader: Trader,
    ) -> None:
        await self._session.execute(
            delete(TraderAssort).where(TraderAssort.trader == trader),
        )
        trader_model = self._traders.get(trader.id)
        if not trader_model:
            raise ValueError  # pragma: no cover

        items = [
            item
            for item in trader_model.assort.items
            if item.id in trader_model.assort.barter_scheme
        ]
        assort = [
            TraderAssort(
                trader=trader,
                item_id=item.id,
                count=item.upd.get("StackObjectsCount", 0),
            )
            for item in items
        ]
        self._session.add_all(assort)

    def _init_traders(self, traders: list[Trader]) -> Sequence[Trader]:
        existing_ids = {t.id for t in traders}
        return [
            Trader(
                id=trader.base.id,
                next_resupply=utc_min,
            )
            for trader in self._traders
            if trader.base.id not in existing_ids
        ]

    async def _add_flea_offers(
        self,
        trader: Trader,
        now: datetime,
        next_resupply_time: datetime,
    ) -> None:
        trader_model = self._traders.get(trader_id=trader.id)
        if not trader_model:
            raise Never

        await self._session.execute(
            delete(FleaOffer).where(
                FleaOffer.user_id == trader.id,
                FleaOffer.user_type == MemberCategory.trader,
            ),
        )

        async for offer in self._flea_service.make_trader_offers(
            trader_model=trader_model,
            now=now,
            next_resupply_time=next_resupply_time,
        ):
            self._session.add(offer)
