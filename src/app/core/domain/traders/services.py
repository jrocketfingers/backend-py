from collections.abc import (
    Mapping,
    Sequence,
)

from app.core.domain.traders.dto import LoyaltyLevelDTO
from lib.tarkov.inventory import remove_orphan_items
from lib.tarkov.items.models import Item
from lib.tarkov.profiles.models import Quest, TraderInfo
from lib.tarkov.quests.models import QuestStatus
from lib.tarkov.trading.models import LoyaltyLevel
from lib.tarkov.trading.trader import TraderModel
from lib.tarkov.types import QuestId


def filter_assort_items(
    level: int,
    traders_info: Mapping[str, TraderInfo],
    trader: TraderModel,
    items: Sequence[Item],
    quests: Sequence[Quest],
) -> Sequence[Item]:
    loyalty_level = calculate_loyalty_level(
        player_level=level,
        current_standing=traders_info[trader.base.id],
        loyalty_levels=trader.base.loyalty_levels,
    )

    result = []
    for item in items:
        loyalty_requirement = trader.assort.loyal_level_items.get(item.id, 0)
        if loyalty_requirement > loyalty_level.number:
            continue
        if not _quest_condition_fulfilled(
            quests={q.id: q for q in quests},
            trader=trader,
            item=item,
        ):
            continue

        result.append(item)
    return remove_orphan_items(result)


def calculate_loyalty_level(
    player_level: int,
    current_standing: TraderInfo,
    loyalty_levels: Sequence[LoyaltyLevel],
) -> LoyaltyLevelDTO:
    current = len(loyalty_levels)
    for level in reversed(loyalty_levels):
        if (
            player_level >= level.min_level
            and current_standing.standing >= level.min_standing
            and current_standing.sales_sum >= level.min_sales_sum
        ):
            return LoyaltyLevelDTO(
                number=current,
                loyalty=level,
            )
        current -= 1
    return LoyaltyLevelDTO(
        number=1,
        loyalty=loyalty_levels[0],
    )


def _quest_condition_fulfilled(
    quests: Mapping[QuestId, Quest],
    trader: TraderModel,
    item: Item,
) -> bool:
    quest_assorts = (
        (trader.quest_assort.success, QuestStatus.Success),
        (trader.quest_assort.fail, QuestStatus.Fail),
    )
    for quest_assort, required_status in quest_assorts:
        if item.id in quest_assort:
            quest = quests.get(quest_assort[item.id])
            if not quest:
                return False
            return quest.status == required_status

    if required_started_quest_id := trader.quest_assort.started.get(
        item.id,
    ):
        quest = quests.get(required_started_quest_id)
        if quest is None:
            return False
        return quest.status not in (QuestStatus.Locked, QuestStatus.AvailableForStart)

    return True
