from collections.abc import Sequence

from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.db.models import AccountProfile, Trader, TraderAssort, TraderPlayerRestriction
from lib.tarkov.types import ItemId


class TraderRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(self, id_: str, options: Sequence[ORMOption] = ()) -> Trader | None:
        stmt = select(Trader).where(Trader.id == id_).options(*options)
        return await self._session.scalar(stmt)

    async def player_restrictions(
        self,
        trader_id: str,
        profile: AccountProfile,
    ) -> Sequence[TraderPlayerRestriction]:
        stmt = select(TraderPlayerRestriction).where(
            TraderPlayerRestriction.trader_id == trader_id,
            TraderPlayerRestriction.profile == profile,
        )
        return (await self._session.scalars(stmt)).all()

    async def get_or_create_restriction(
        self,
        trader: Trader,
        profile: AccountProfile,
        item_id: ItemId,
    ) -> TraderPlayerRestriction:
        restriction = await self._session.get(
            TraderPlayerRestriction,
            (trader.id, profile.id, item_id),
            with_for_update=True,
        )
        if restriction is None:
            restriction = TraderPlayerRestriction(
                trader=trader,
                item_id=item_id,
                profile=profile,
            )
            self._session.add(restriction)
        return restriction


class TraderAssortRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(
        self,
        trader_id: str,
        item_id: str,
    ) -> TraderAssort | None:
        return await self._session.get(
            TraderAssort,
            ident=(trader_id, item_id),
        )

    async def change_count(
        self,
        trader_id: str,
        item_id: str,
        delta: int,
    ) -> None:
        stmt = (
            update(TraderAssort)
            .values(count=TraderAssort.count + delta)
            .where(
                TraderAssort.trader_id == trader_id,
                TraderAssort.item_id == item_id,
            )
            .returning(TraderAssort.item_id)
        )
        result = await self._session.execute(stmt)
        result.scalars().one()  # Simply assert that exactly one row was updated
