import dataclasses
from collections.abc import Mapping, Sequence
from datetime import datetime

from lib.tarkov.items.models import Item
from lib.tarkov.trading.models import BarterScheme, LoyaltyLevel
from lib.tarkov.types import ItemId, ItemTemplateId


@dataclasses.dataclass(kw_only=True, frozen=True, slots=True)
class TraderPricesDTO:
    supply_next_time: datetime
    prices: Mapping[ItemTemplateId, int]
    currency_courses: Mapping[str, int]


@dataclasses.dataclass(kw_only=True, frozen=True, slots=True)
class TraderAssortDTO:
    next_resupply: datetime
    items: Sequence[Item]
    barter_scheme: BarterScheme
    loyal_level_items: Mapping[ItemId, int]


@dataclasses.dataclass(kw_only=True, frozen=True, slots=True)
class LoyaltyLevelDTO:
    number: int
    loyalty: LoyaltyLevel
