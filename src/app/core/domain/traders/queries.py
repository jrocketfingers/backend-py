from collections.abc import Mapping, Sequence

from result import Err, Ok, Result
from sqlalchemy.orm import selectinload

from app.core.domain.errors import NotFoundError
from app.core.domain.items.services import HandbookDB
from app.core.domain.traders.dto import TraderAssortDTO, TraderPricesDTO
from app.core.domain.traders.repositories import TraderRepository
from app.core.domain.traders.services import filter_assort_items
from app.db.models import AccountProfile, Trader, TraderPlayerRestriction
from lib.tarkov.customization import Suits
from lib.tarkov.items.models import Item
from lib.tarkov.items.upd import Upd
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.trading.models import CurrencyIds, TraderBase
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.types import ItemId


class TraderSettingsQuery:
    def __init__(self, traders: Traders) -> None:
        self._traders = traders

    async def execute(self) -> Sequence[TraderBase]:
        bases = [trader.base for trader in self._traders]
        bases.sort(key=lambda base: base.id)
        return bases


class CustomizationStorageQuery:
    async def execute(self, account_profile: AccountProfile) -> Suits:
        return Suits(
            id=str(account_profile.id),
            suites=[],
        )


class TraderPricesQuery:
    def __init__(
        self,
        handbook_db: HandbookDB,
        repository: TraderRepository,
        profile_storage: ProfileStorage,
    ) -> None:
        self._handbook = handbook_db
        self._repository = repository
        self._profile_storage = profile_storage

    async def execute(
        self,
        trader_id: str,
    ) -> Result[TraderPricesDTO, NotFoundError]:
        if not (trader := await self._repository.get(id_=trader_id)):
            return Err(NotFoundError())
        dto = TraderPricesDTO(
            supply_next_time=trader.next_resupply,
            prices=self._handbook.item_prices,
            currency_courses={
                currency.value: handbook.price
                for currency in CurrencyIds
                if (handbook := self._handbook.item(currency.value))
            },
        )
        return Ok(dto)


class TraderAssortQuery:
    def __init__(
        self,
        traders: Traders,
        repository: TraderRepository,
        profile_storage: ProfileStorage,
    ) -> None:
        self._traders = traders
        self._repository = repository
        self._profile_storage = profile_storage

    async def execute(
        self,
        profile: AccountProfile,
        trader_id: str,
    ) -> Result[TraderAssortDTO, NotFoundError]:
        trader_model = self._traders.get(trader_id)
        if trader_model is None:
            return Err(NotFoundError())

        trader = await self._repository.get(
            trader_id,
            options=(selectinload(Trader.assort),),
        )
        if trader is None:
            return Err(NotFoundError())

        profiles = await self._profile_storage.readonly(
            profile_id=str(profile.id),
        )
        restrictions = {
            r.item_id: r
            for r in await self._repository.player_restrictions(
                trader_id=trader.id,
                profile=profile,
            )
        }

        items = filter_assort_items(
            level=profiles.pmc.info.level,
            traders_info=profiles.pmc.traders_info,
            trader=trader_model,
            items=await self._assort_items(
                trader=trader,
                trader_model=trader_model,
                restrictions=restrictions,
            ),
            quests=profiles.pmc.quests,
        )

        result = TraderAssortDTO(
            next_resupply=trader.next_resupply,
            items=items,
            barter_scheme=trader_model.assort.barter_scheme,
            loyal_level_items=trader_model.assort.loyal_level_items,
        )
        return Ok(result)

    async def _assort_items(
        self,
        trader: Trader,
        trader_model: TraderModel,
        restrictions: Mapping[ItemId, TraderPlayerRestriction],
    ) -> Sequence[Item]:
        result = []
        assort_by_item_id = {assort.item_id: assort for assort in trader.assort}
        for item in trader_model.assort.items:
            if item.id not in trader_model.assort.barter_scheme:
                result.append(item)
                continue

            upd = Upd(StackObjectsCount=assort_by_item_id[item.id].count)
            if item.id in restrictions:
                upd["BuyRestrictionCurrent"] = restrictions[item.id].bought_count

            item_copy = item.model_copy()
            item_copy.upd = item.upd | upd
            result.append(item_copy)

        return result
