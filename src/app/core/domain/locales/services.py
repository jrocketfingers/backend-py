from collections.abc import Mapping

from lib.tarkov.locales import Locale


class LocaleService:
    def __init__(
        self,
        locales: dict[str, Locale],
    ) -> None:
        self._locales = locales

    @property
    def locales(self) -> Mapping[str, Locale]:
        return self._locales

    @property
    def available_languages(self) -> Mapping[str, str]:
        return {loc.name: loc.verbose_name for loc in self._locales.values()}
