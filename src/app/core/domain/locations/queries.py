from app.core.domain.locations.services import LocationService
from lib.tarkov.locations.db import LocationDB
from lib.tarkov.locations.models import Location, LocationsBase


class LocationsQuery:
    def __init__(self, location_service: LocationDB) -> None:
        self._service = location_service

    async def execute(self) -> LocationsBase:
        return self._service.full_base()


class GenerateLocationQuery:
    def __init__(self, service: LocationService) -> None:
        self._service = service

    async def execute(self, location_id: str) -> Location:
        return self._service.generate_location(location_id=location_id)
