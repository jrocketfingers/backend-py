from lib.tarkov.locations.db import LocationDB
from lib.tarkov.locations.models import Location


class LocationService:
    def __init__(self, locations: LocationDB) -> None:
        self._locations = locations

    def generate_location(self, location_id: str) -> Location:
        return self._locations.get(location_id)
