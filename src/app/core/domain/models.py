import enum


class FleaMarketSort(enum.IntEnum):
    id = 0
    category = 2
    rating = 3
    offer_title = 4
    price = 5
    expiry = 6


class FleaMarketSortDirection(enum.IntEnum):
    desc = 0
    asc = 1
