import dataclasses
from datetime import timedelta


class RaidExitChange:
    pass


@dataclasses.dataclass(slots=True, kw_only=True)
class RaidTimeDTO:
    time_left: timedelta
    original_required_survival_time: timedelta
    new_required_survival_time: timedelta
    exit_changes: list[RaidExitChange]
