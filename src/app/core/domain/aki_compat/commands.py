from app.core.domain.bots.schema import RaidConfigurationSchema
from app.db.models import AccountProfile, ProfileRaidInfo


class RaidConfigurationCommand:
    async def execute(
        self,
        configuration: RaidConfigurationSchema,
        account_profile: AccountProfile,
    ) -> None:
        account_profile.raid_info = ProfileRaidInfo(
            locations=configuration.location,
        )
