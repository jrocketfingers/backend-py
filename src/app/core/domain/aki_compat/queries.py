from app.core.domain.aki_compat.dto import RaidTimeDTO
from app.core.domain.aki_compat.services import RaidService
from lib.tarkov.customization import Side


class RaidTimeQuery:
    def __init__(self, service: RaidService) -> None:
        self._service = service

    async def execute(self, location_id: str, side: Side) -> RaidTimeDTO:
        return self._service.raid_time(location_id=location_id, side=side)
