import random
from datetime import timedelta

from app.core.domain.aki_compat.dto import RaidTimeDTO
from lib.tarkov.config import RaidConfig
from lib.tarkov.customization import Side
from lib.tarkov.db import ResourceDB
from lib.tarkov.locations.db import LocationDB


class RaidService:
    def __init__(
        self,
        locations: LocationDB,
        resource_db: ResourceDB,
        config: RaidConfig,
    ) -> None:
        self._locations = locations
        self._resource_db = resource_db
        self._config = config

    def raid_time(self, location_id: str, side: Side) -> RaidTimeDTO:
        location = self._locations.get(location_id)
        location_escape_time_limit = timedelta(minutes=location.escape_time_limit)
        result = RaidTimeDTO(
            time_left=location_escape_time_limit,
            original_required_survival_time=self.survival_time_requirement,
            new_required_survival_time=self.survival_time_requirement,
            exit_changes=[],
        )
        if side == Side.scav:
            result.time_left = timedelta(
                seconds=random.uniform(
                    self._config.scav_raid_min_time.total_seconds(),
                    result.time_left.total_seconds(),
                ),
            )
            required_survival_time_seconds = max(
                result.original_required_survival_time.total_seconds()
                - (
                    location_escape_time_limit.total_seconds()
                    - result.time_left.total_seconds()
                ),
                0,
            )
            result.new_required_survival_time = timedelta(
                seconds=required_survival_time_seconds,
            )

        return result

    @property
    def survival_time_requirement(self) -> timedelta:
        return timedelta(
            seconds=self._resource_db.globals_.config.exp.match_end.survived_seconds_requirement,
        )
