from lib.tarkov.presets.models import UserBuilds


class UserBuildsQuery:
    async def execute(self) -> UserBuilds:
        # Not implemented
        return UserBuilds(weapon_builds=[], equipment_builds=[], magazine_builds=[])
