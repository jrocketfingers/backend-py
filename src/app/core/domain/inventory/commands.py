from collections.abc import Sequence

from app.db.models import AccountProfile
from lib.tarkov.inventory_actions.router import ActionRouter
from lib.tarkov.items.actions import AnyAction, ItemsMovingResult
from lib.tarkov.profiles.abc import ProfileStorage


class InventoryOperationsCommand:
    def __init__(
        self,
        storage: ProfileStorage,
        router: ActionRouter,
    ) -> None:
        self._storage = storage
        self._router = router

    async def execute(
        self,
        account_profile: AccountProfile,
        actions: Sequence[AnyAction],
    ) -> ItemsMovingResult:
        async with self._storage.profile(
            profile_id=str(account_profile.id),
        ) as profiles:
            return await self._router.handle(
                account_profile=account_profile,
                profile=profiles.pmc,
                inventory=profiles.inventory,
                actions=actions,
            )
