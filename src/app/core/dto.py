import dataclasses
from typing import Generic, TypeVar

_T = TypeVar("_T")


@dataclasses.dataclass(frozen=True, kw_only=True, slots=True)
class MinMaxDTO(Generic[_T]):
    min: _T
    max: _T
