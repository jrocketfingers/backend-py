from __future__ import annotations

from datetime import datetime

from sqlalchemy import (
    CheckConstraint,
    ForeignKey,
    PrimaryKeyConstraint,
)
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column, relationship
from sqlalchemy.sql.elements import conv

from app.db import Base
from app.db.models import AccountProfile
from lib.tarkov.types import ItemId


class Trader(Base):
    __tablename__ = "trader"

    id: Mapped[str] = mapped_column(primary_key=True)
    next_resupply: Mapped[datetime]

    assort: Mapped[list[TraderAssort]] = relationship(back_populates="trader")


class TraderAssort(MappedAsDataclass, Base):
    __tablename__ = "trader_assort"

    __table_args__ = (
        PrimaryKeyConstraint("trader_id", "item_id"),
        CheckConstraint("count >= 0", name=conv("check_count_gte_0")),
    )

    trader_id: Mapped[str] = mapped_column(ForeignKey("trader.id"), init=False)
    item_id: Mapped[str]
    count: Mapped[int]

    trader: Mapped[Trader] = relationship(back_populates="assort")


class TraderPlayerRestriction(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "trader_player_restriction"
    __table_args__ = (
        PrimaryKeyConstraint("trader_id", "profile_id", "item_id"),
        CheckConstraint("bought_count >= 0", name=conv("check_bought_count_gte_0")),
    )
    trader_id: Mapped[str] = mapped_column(ForeignKey("trader.id"), init=False)
    profile_id: Mapped[int] = mapped_column(ForeignKey("profile.id"), init=False)
    item_id: Mapped[ItemId]
    bought_count: Mapped[int] = mapped_column(default=0)

    trader: Mapped[Trader] = relationship()
    profile: Mapped[AccountProfile] = relationship()
