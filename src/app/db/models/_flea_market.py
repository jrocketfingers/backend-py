import random
from datetime import datetime
from uuid import UUID

from pydantic import TypeAdapter
from sqlalchemy import JSON
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column
from uuid_utils.compat import uuid7

from app.db import Base
from app.db.types import IntFlagType, PydanticType
from lib.tarkov.chat import MemberCategory
from lib.tarkov.flea_market.models import FleaMarketOfferRequirement
from lib.tarkov.items.models import Item
from lib.tarkov.types import ItemId, ItemTemplateId


def _int_id() -> int:
    return random.randint(1, 2**31 - 1)


class FleaOffer(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "flea_market_offer"

    id: Mapped[UUID] = mapped_column(default_factory=uuid7, primary_key=True)
    int_id: Mapped[int] = mapped_column(default_factory=_int_id, init=False)
    root_item_id: Mapped[ItemId]
    root_item_template_id: Mapped[ItemTemplateId] = mapped_column(index=True)
    category_ids: Mapped[list[str]] = mapped_column(JSON)
    items: Mapped[list[Item]] = mapped_column(PydanticType(TypeAdapter(list[Item])))
    requirements: Mapped[list[FleaMarketOfferRequirement]] = mapped_column(
        PydanticType(TypeAdapter(list[FleaMarketOfferRequirement])),
    )
    currency_id: Mapped[ItemTemplateId | None]
    items_cost: Mapped[int]
    requirement_items_cost: Mapped[int]
    start_time: Mapped[datetime]
    end_time: Mapped[datetime]
    is_bartering: Mapped[bool]
    sell_in_one_piece: Mapped[bool]
    required_loyalty_level: Mapped[int]
    summary_cost: Mapped[int]
    count: Mapped[int]
    priority: Mapped[bool]
    condition: Mapped[float]

    user_id: Mapped[str]
    user_type: Mapped[MemberCategory] = mapped_column(IntFlagType(MemberCategory))
