from __future__ import annotations

import uuid

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column, relationship

from app.db import Base
from app.db.types import IntFlagType
from lib.tarkov.chat import MemberCategory
from lib.tarkov.customization import PMCSide


class Account(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "account"

    id: Mapped[int] = mapped_column(primary_key=True, init=False)
    session_id: Mapped[str] = mapped_column(unique=True)
    username: Mapped[str]
    game_edition: Mapped[str]

    profile: Mapped[AccountProfile | None] = relationship(
        back_populates="account",
        uselist=False,
        default=None,
    )


class AccountProfile(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "profile"

    id: Mapped[uuid.UUID] = mapped_column(
        primary_key=True,
        default_factory=uuid.uuid4,
        init=False,
    )
    account_id: Mapped[int] = mapped_column(
        ForeignKey("account.id"),
        init=False,
        unique=True,
    )
    name: Mapped[str] = mapped_column(unique=True)
    side: Mapped[PMCSide]
    level: Mapped[int] = mapped_column(default=1)
    member_category: Mapped[MemberCategory] = mapped_column(
        IntFlagType(MemberCategory),
        default=MemberCategory.default,
    )
    profile_id: Mapped[str]

    account: Mapped[Account] = relationship(back_populates="profile")
    raid_info: Mapped[ProfileRaidInfo | None] = relationship(
        back_populates="profile",
        default=None,
        cascade="all, delete-orphan",
    )


class ProfileRaidInfo(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "profile_raid_info"

    profile_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("profile.id"),
        primary_key=True,
        init=False,
    )
    profile: Mapped[AccountProfile] = relationship(
        back_populates="raid_info",
        default=None,
    )
    locations: Mapped[str]
