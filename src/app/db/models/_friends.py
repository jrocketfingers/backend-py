from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING
from uuid import UUID

from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column, relationship
from uuid_utils.compat import uuid7

from app.db import Base
from lib.utils import utc_now

if TYPE_CHECKING:
    from app.db.models import AccountProfile


class FriendRequest(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "friend_request"
    __table_args__ = (
        UniqueConstraint(
            "sender_id",
            "recipient_id",
            name="sender_recipient_uq",
        ),
    )

    id: Mapped[UUID] = mapped_column(default_factory=uuid7, primary_key=True)
    created_at: Mapped[datetime] = mapped_column(default_factory=utc_now)
    sender_id: Mapped[UUID] = mapped_column(ForeignKey("profile.id"), init=False)
    recipient_id: Mapped[UUID] = mapped_column(ForeignKey("profile.id"), init=False)

    sender: Mapped[AccountProfile] = relationship(
        foreign_keys=[sender_id],
        repr=False,
    )
    recipient: Mapped[AccountProfile] = relationship(
        foreign_keys=[recipient_id],
        repr=False,
    )


class Friends(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "friends"
    __table_args__ = (
        UniqueConstraint(
            "sender_id",
            "recipient_id",
            name="friends_users_uq",
        ),
    )

    id: Mapped[UUID] = mapped_column(default_factory=uuid7, primary_key=True)
    created_at: Mapped[datetime] = mapped_column(default_factory=utc_now)
    sender_id: Mapped[UUID] = mapped_column(ForeignKey("profile.id"), init=False)
    recipient_id: Mapped[UUID] = mapped_column(ForeignKey("profile.id"), init=False)

    sender: Mapped[AccountProfile] = relationship(
        foreign_keys=[sender_id],
        repr=False,
    )
    recipient: Mapped[AccountProfile] = relationship(
        foreign_keys=[recipient_id],
        repr=False,
    )
