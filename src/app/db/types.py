import enum
from datetime import UTC, datetime
from typing import TypeVar

from pydantic import TypeAdapter
from sqlalchemy import JSON, DateTime, Dialect, Integer, TypeDecorator
from sqlalchemy.dialects.sqlite.base import SQLiteDialect

T = TypeVar("T")
TIntFlag = TypeVar("TIntFlag", bound=enum.IntFlag)


class PydanticType(TypeDecorator[T]):
    impl = JSON
    cache_ok = True

    def __init__(self, pydantic_type: TypeAdapter[T] | type[T]) -> None:
        super().__init__()
        if not isinstance(pydantic_type, TypeAdapter):  # pragma: no cover
            pydantic_type = TypeAdapter(pydantic_type)
        self._type_adapter = pydantic_type

    def process_bind_param(
        self,
        value: T | None,
        dialect: Dialect,  # noqa: ARG002
    ) -> object | None:
        if value is None:  # pragma: no cover
            return None
        return self._type_adapter.dump_python(value)

    def process_result_value(
        self,
        value: dict[object, object] | None,
        dialect: Dialect,  # noqa: ARG002
    ) -> T | None:
        if value is None:  # pragma: no cover
            return None
        return self._type_adapter.validate_python(value)


class IntFlagType(TypeDecorator[TIntFlag]):
    impl = Integer
    cache_ok = True

    def __init__(self, enum_cls: type[TIntFlag]) -> None:
        super().__init__()
        self._enum_cls = enum_cls

    def process_bind_param(
        self,
        value: TIntFlag | None,
        dialect: Dialect,  # noqa: ARG002
    ) -> object | None:
        if value is None:  # pragma: no cover
            return None
        return value.value

    def process_result_value(
        self,
        value: int | None,
        dialect: Dialect,  # noqa: ARG002
    ) -> TIntFlag | None:
        if value is None:  # pragma: no cover
            return None
        return self._enum_cls(value)


class UTCDateTime(TypeDecorator[datetime]):
    impl = DateTime(timezone=True)
    cache_ok = True

    def process_result_value(  # pragma: no cover
        self,
        value: datetime | None,
        dialect: Dialect,
    ) -> datetime | None:
        if value is None:
            return None

        if isinstance(dialect, SQLiteDialect):
            return value.replace(tzinfo=UTC)

        return value
