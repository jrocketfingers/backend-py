"""
Add level and member category to profile

Revision ID: e4ec46a0d0b9
Revises: 7242a1fdb2d2
Create Date: 2024-05-24 01:12:43.391450

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy import Integer

# revision identifiers, used by Alembic.
revision = "e4ec46a0d0b9"
down_revision: str | None = "7242a1fdb2d2"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    with op.batch_alter_table("profile", schema=None) as batch_op:
        batch_op.add_column(sa.Column("level", sa.Integer(), nullable=True))
        batch_op.add_column(
            sa.Column("member_category", Integer, nullable=True),
        )

    op.execute("update profile set member_category = 0, level = 1")

    with op.batch_alter_table("profile", schema=None) as batch_op:
        batch_op.alter_column("level", nullable=False)
        batch_op.alter_column("member_category", nullable=False)


def downgrade() -> None:
    with op.batch_alter_table("profile", schema=None) as batch_op:
        batch_op.drop_column("member_category")
        batch_op.drop_column("level")
