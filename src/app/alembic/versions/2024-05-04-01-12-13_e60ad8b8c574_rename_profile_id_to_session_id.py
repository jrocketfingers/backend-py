"""
Rename profile_id to session_id

Revision ID: e60ad8b8c574
Revises: 574e299398d5
Create Date: 2024-05-04 01:12:13.428917

"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "e60ad8b8c574"
down_revision: str | None = "574e299398d5"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    with op.batch_alter_table("account", schema=None) as batch_op:
        batch_op.alter_column("profile_id", new_column_name="session_id")

        batch_op.drop_constraint("uq_account_profile_id", type_="unique")
        batch_op.create_unique_constraint(
            batch_op.f("uq_account_session_id"),
            ["session_id"],
        )


def downgrade() -> None:
    with op.batch_alter_table("account", schema=None) as batch_op:
        batch_op.alter_column("session_id", new_column_name="profile_id")

        batch_op.drop_constraint(batch_op.f("uq_account_session_id"), type_="unique")
        batch_op.create_unique_constraint("uq_account_profile_id", ["profile_id"])
