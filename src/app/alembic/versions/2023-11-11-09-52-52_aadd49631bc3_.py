"""
empty message

Revision ID: aadd49631bc3
Revises: bf720089770f
Create Date: 2023-11-11 09:52:52.461711

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "aadd49631bc3"
down_revision: str | None = "bf720089770f"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "profile",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("account_id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.ForeignKeyConstraint(
            ["account_id"],
            ["account.id"],
            name=op.f("fk_profile_account_id_account"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_profile")),
        sa.UniqueConstraint("name", name=op.f("uq_profile_name")),
    )


def downgrade() -> None:
    op.drop_table("profile")
