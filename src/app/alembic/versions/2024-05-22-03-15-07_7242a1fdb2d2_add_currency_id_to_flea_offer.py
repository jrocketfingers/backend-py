"""
Add currency_id to flea offer

Revision ID: 7242a1fdb2d2
Revises: d996ac90296c
Create Date: 2024-05-22 03:15:07.468860

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7242a1fdb2d2"
down_revision: str | None = "d996ac90296c"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    with op.batch_alter_table("flea_market_offer", schema=None) as batch_op:
        batch_op.add_column(sa.Column("currency_id", sa.String(), nullable=True))


def downgrade() -> None:
    with op.batch_alter_table("flea_market_offer", schema=None) as batch_op:
        batch_op.drop_column("currency_id")
