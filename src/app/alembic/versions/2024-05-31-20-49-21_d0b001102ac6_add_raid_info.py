"""
Add raid info

Revision ID: d0b001102ac6
Revises: f25d484272ec
Create Date: 2024-05-31 20:49:21.698667

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d0b001102ac6"
down_revision: str | None = "f25d484272ec"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "profile_raid_info",
        sa.Column("profile_id", sa.Uuid(), nullable=False),
        sa.Column("locations", sa.String(), nullable=False),
        sa.ForeignKeyConstraint(
            ["profile_id"],
            ["profile.id"],
            name=op.f("fk_profile_raid_info_profile_id_profile"),
        ),
        sa.PrimaryKeyConstraint("profile_id", name=op.f("pk_profile_raid_info")),
    )


def downgrade() -> None:
    op.drop_table("profile_raid_info")
