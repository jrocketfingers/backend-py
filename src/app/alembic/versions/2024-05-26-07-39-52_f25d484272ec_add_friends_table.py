"""
Add friends table

Revision ID: f25d484272ec
Revises: b793500cccdf
Create Date: 2024-05-26 07:39:52.290969

"""

import sqlalchemy as sa
from alembic import op

from app.db.types import UTCDateTime

# revision identifiers, used by Alembic.
revision = "f25d484272ec"
down_revision: str | None = "b793500cccdf"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "friends",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column(
            "created_at",
            UTCDateTime(timezone=True),
            nullable=False,
        ),
        sa.Column("sender_id", sa.Uuid(), nullable=False),
        sa.Column("recipient_id", sa.Uuid(), nullable=False),
        sa.ForeignKeyConstraint(
            ["recipient_id"],
            ["profile.id"],
            name=op.f("fk_friends_recipient_id_profile"),
        ),
        sa.ForeignKeyConstraint(
            ["sender_id"],
            ["profile.id"],
            name=op.f("fk_friends_sender_id_profile"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_friends")),
        sa.UniqueConstraint("sender_id", "recipient_id", name="friends_users_uq"),
    )


def downgrade() -> None:
    op.drop_table("friends")
