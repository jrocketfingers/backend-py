"""
empty message

Revision ID: d996ac90296c
Revises: cb141be6f97d
Create Date: 2024-05-15 10:40:51.006949

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d996ac90296c"
down_revision: str | None = "cb141be6f97d"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    with op.batch_alter_table("flea_market_offer", schema=None) as batch_op:
        batch_op.add_column(sa.Column("category_ids", sa.JSON(), nullable=False))


def downgrade() -> None:
    with op.batch_alter_table("flea_market_offer", schema=None) as batch_op:
        batch_op.drop_column("category_ids")
