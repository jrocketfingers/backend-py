"""
empty message

Revision ID: b7673b1cba60
Revises:
Create Date: 2023-11-10 11:07:11.227805

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "b7673b1cba60"
down_revision: str | None = None
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "account",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("username", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_account")),
    )


def downgrade() -> None:
    op.drop_table("account")
