"""
Add flea offer

Revision ID: 9f078cfd9985
Revises: e60ad8b8c574
Create Date: 2024-05-14 03:40:58.410205

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy import JSON

# revision identifiers, used by Alembic.
revision = "9f078cfd9985"
down_revision: str | None = "e60ad8b8c574"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "flea_market_offer",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("int_id", sa.Integer(), nullable=False),
        sa.Column("items", JSON, nullable=False),
        sa.Column("requirements", JSON, nullable=False),
        sa.Column("root_item_id", sa.String(), nullable=False),
        sa.Column("items_cost", sa.Integer(), nullable=False),
        sa.Column("requirement_items_cost", sa.Integer(), nullable=False),
        sa.Column("start_time", sa.DateTime(timezone=True), nullable=False),
        sa.Column("end_time", sa.DateTime(timezone=True), nullable=False),
        sa.Column("sell_in_one_piece", sa.Boolean(), nullable=False),
        sa.Column("required_loyalty_level", sa.Integer(), nullable=False),
        sa.Column("summary_cost", sa.Integer(), nullable=False),
        sa.Column("count", sa.Integer(), nullable=False),
        sa.Column("priority", sa.Boolean(), nullable=False),
        sa.Column("condition", sa.Float(), nullable=False),
        sa.Column("user_id", sa.String(), nullable=False),
        sa.Column("user_type", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_flea_market_offer")),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    op.drop_table("flea_market_offer")
