"""
empty message

Revision ID: c3bbf16c36f1
Revises: 9f078cfd9985
Create Date: 2024-05-14 06:53:43.925650

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c3bbf16c36f1"
down_revision: str | None = "9f078cfd9985"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    with op.batch_alter_table("flea_market_offer", schema=None) as batch_op:
        batch_op.add_column(sa.Column("category_id", sa.String(), nullable=False))


def downgrade() -> None:
    with op.batch_alter_table("flea_market_offer", schema=None) as batch_op:
        batch_op.drop_column("category_id")
