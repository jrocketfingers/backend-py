"""
Add friend request

Revision ID: b793500cccdf
Revises: 7f66e15ed337
Create Date: 2024-05-25 06:14:31.006288

"""

import sqlalchemy as sa
from alembic import op

from app.db.types import UTCDateTime

# revision identifiers, used by Alembic.
revision = "b793500cccdf"
down_revision: str | None = "7f66e15ed337"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "friend_request",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column(
            "created_at",
            UTCDateTime(timezone=True),
            nullable=False,
        ),
        sa.Column("sender_id", sa.Uuid(), nullable=False),
        sa.Column("recipient_id", sa.Uuid(), nullable=False),
        sa.ForeignKeyConstraint(
            ["recipient_id"],
            ["profile.id"],
            name=op.f("fk_friend_request_recipient_id_profile"),
        ),
        sa.ForeignKeyConstraint(
            ["sender_id"],
            ["profile.id"],
            name=op.f("fk_friend_request_sender_id_profile"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_friend_request")),
        sa.UniqueConstraint("sender_id", "recipient_id", name="sender_recipient_uq"),
    )


def downgrade() -> None:
    op.drop_table("friend_request")
