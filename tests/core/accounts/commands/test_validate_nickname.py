import pytest
from aioinject import InjectionContext

from app.core.domain.accounts.commands import ValidateNicknameCommand
from app.core.domain.accounts.errors import UsernameIsTaken, UsernameTooShort
from app.db.models import AccountProfile

pytestmark = [pytest.mark.usefixtures("session")]


@pytest.fixture
async def command(inject_context: InjectionContext) -> ValidateNicknameCommand:
    return await inject_context.resolve(ValidateNicknameCommand)


@pytest.mark.parametrize(
    "name",
    ["", "_", "aa"],
)
async def test_nickname_is_too_short(
    command: ValidateNicknameCommand,
    name: str,
) -> None:
    result = await command.execute(name=name)
    assert result.unwrap_err() == UsernameTooShort()


async def test_nickname_is_taken(
    command: ValidateNicknameCommand,
    account_profile: AccountProfile,
) -> None:
    result = await command.execute(name=account_profile.name)
    assert result.unwrap_err() == UsernameIsTaken()


async def test_ok(
    command: ValidateNicknameCommand,
) -> None:
    result = await command.execute(name="username")
    assert result.unwrap() == "username"
