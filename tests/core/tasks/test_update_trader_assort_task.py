from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.core.domain.flea_market.services import FleaMarketService
from app.core.domain.traders.tasks import UpdateTraderAssortTask
from app.db.models import FleaOffer, Trader
from lib.tarkov.trading.trader import Traders


async def test_ok(
    update_traders_assort_task: UpdateTraderAssortTask,
    session: AsyncSession,
    traders: Traders,
    flea_service: FleaMarketService,
) -> None:
    await update_traders_assort_task.run_once()
    traders_in_db = {
        trader.id: trader
        for trader in await session.scalars(
            select(Trader).options(selectinload(Trader.assort)),
        )
    }
    assert len(traders_in_db) == len(traders)
    sellable_items_count = 0
    for trader_model in traders:
        trader = traders_in_db[trader_model.base.id]
        for key in trader_model.assort.barter_scheme:
            assort_item = next(a for a in trader.assort if a.item_id == key)
            item = next(i for i in trader_model.assort.items if i.id == key)
            if flea_service.can_sell_on_flea(item):
                sellable_items_count += 1

            assert assort_item.count == item.upd["StackObjectsCount"]

    flea_offers_count = await session.scalar(select(func.count(FleaOffer.id))) or 0
    assert flea_offers_count == sellable_items_count
