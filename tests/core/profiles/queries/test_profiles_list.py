import pytest
from aioinject import InjectionContext

from app.core.domain.profiles.queries import ProfileListQuery
from app.db.models import Account
from lib.tarkov.profiles.dto import ProfileContext

pytestmark = [pytest.mark.usefixtures("session")]


@pytest.fixture
async def query(inject_context: InjectionContext) -> ProfileListQuery:
    return await inject_context.resolve(ProfileListQuery)


async def test_empty(
    query: ProfileListQuery,
    account: Account,
) -> None:
    assert await query.execute(account=account) == ()


async def test_ok(
    query: ProfileListQuery,
    account: Account,
    profile_context: ProfileContext,
) -> None:
    result = await query.execute(account=account)
    assert result == (profile_context.scav, profile_context.pmc)
