import random
import typing
import uuid

import pytest
from aioinject import InjectionContext
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.domain.items.services import CustomizationDB
from app.core.domain.profiles.commands import ProfileCreateCommand
from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import Account
from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.profiles.templates import ProfileTemplates

pytestmark = [pytest.mark.usefixtures("session")]


@pytest.fixture
async def command(inject_context: InjectionContext) -> ProfileCreateCommand:
    return await inject_context.resolve(ProfileCreateCommand)


async def test_ok(  # noqa: PLR0913
    command: ProfileCreateCommand,
    account: Account,
    profile_storage: ProfileStorage,
    customization_db: CustomizationDB,
    profile_templates: ProfileTemplates,
    session: AsyncSession,
) -> None:
    for game_edition, profile_template in profile_templates.templates.items():
        async with session.begin_nested() as transaction:
            account.game_edition = game_edition

            voice = random.choice(list(customization_db.customization.values()))
            dto = ProfileCreateDTO(
                name=str(uuid.uuid4()),
                side=random.choice(typing.get_args(PMCSide)),
                voice_id=voice.id,
                head_id=str(uuid.uuid4()),
            )
            account_profile = await command.execute(account=account, dto=dto)
            assert account_profile.name == dto.name
            assert account_profile.side == dto.side

            profile = await profile_storage.readonly(str(account_profile.id))

            assert profile.pmc.id == account_profile.profile_id

            assert profile.pmc.info.nickname == dto.name
            assert profile.pmc.info.lower_nickname == str(dto.name)
            assert profile.pmc.info.side == dto.side
            assert profile.pmc.savage is not None
            assert profile.pmc.info.voice == voice.name
            assert profile.pmc.customization.head == dto.head_id
            assert all(
                info.standing == profile_template.traders.initial_standing
                for info in profile.pmc.traders_info.values()
            )

            await transaction.rollback()
