import pytest
from aioinject import InjectionContext

from app.core.domain.profiles.commands import ProfileLogoutCommand
from app.db.models import Account


@pytest.fixture
async def command(inject_context: InjectionContext) -> ProfileLogoutCommand:
    return await inject_context.resolve(ProfileLogoutCommand)


async def test_no_profile(
    command: ProfileLogoutCommand,
    account: Account,
) -> None:
    assert account.profile is None

    await command.execute(account)
