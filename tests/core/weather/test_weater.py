from datetime import UTC, datetime
from unittest.mock import patch

import freezegun

from app.core.domain.weather.services import WeatherService


def test_ok(weather_service: WeatherService) -> None:
    time = datetime(2023, 1, 1, 6, 21, 1, tzinfo=UTC)

    with (
        patch.object(weather_service.config, "acceleration", 7),
        freezegun.freeze_time(time),
    ):
        result = weather_service.generate_weather(time)

    expected_time = "20:27:07"
    assert result.time == expected_time
    assert result.date == result.weather.date == "2023-01-01"
    assert result.weather.time == f"2023-01-01 {expected_time}"
