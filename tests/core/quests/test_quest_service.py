import random
from datetime import datetime

import pytest

from app.core.domain.quests.services import QuestsService
from lib.tarkov.config import QuestConfig
from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.profiles.models import Quest
from lib.tarkov.quests.models import (
    CompareMethod,
    LevelCondition,
    QuestStatus,
    QuestStatusCondition,
    QuestTemplate,
    TraderLoyaltyCondition,
    TraderStandingCondition,
)
from lib.tarkov.quests.storage import QuestStorage
from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.utils import generate_mongodb_id


@pytest.mark.parametrize(
    ("condition", "required_level", "level", "should_include"),
    [
        (">=", 15, 10, False),
        (">=", 15, 15, True),
        (">", 15, 15, False),
        (">", 15, 16, True),
        ("<=", 15, 16, False),
        ("<=", 15, 15, True),
        ("<", 15, 15, False),
        ("<", 15, 14, True),
    ],
)
async def test_min_level_condition(  # noqa: PLR0913
    *,
    profile_context: ProfileContext,
    quest_template: QuestTemplate,
    quest_service: QuestsService,
    condition: CompareMethod,
    required_level: int,
    level: int,
    should_include: bool,
) -> None:
    quest_template.conditions.available_for_start.append(
        LevelCondition(
            compare_method=condition,
            value=required_level,
            id=generate_mongodb_id(),
            index=0,
            parent_id="",
            dynamic_locale=False,
            global_quest_counter_id="",
        ),
    )
    profile_context.pmc.info.level = level
    assert (
        await quest_service.is_quest_available(
            profile=profile_context.pmc,
            quest=quest_template,
        )
        is should_include
    )


async def test_trader_min_standing(
    profile_context: ProfileContext,
    quest_template: QuestTemplate,
    quest_service: QuestsService,
    random_trader: TraderModel,
) -> None:
    value = random.random()
    profile_context.pmc.traders_info[random_trader.base.id].standing = value - 0.01
    quest_template.conditions.available_for_start.append(
        TraderStandingCondition(
            compare_method=">=",
            value=value,
            target=random_trader.base.id,
            id=generate_mongodb_id(),
            index=0,
            parent_id="",
            dynamic_locale=False,
            global_quest_counter_id="",
        ),
    )
    assert not await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )

    profile_context.pmc.traders_info[random_trader.base.id].standing = value
    assert await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )


async def test_quest_is_already_in_profile(
    profile_context: ProfileContext,
    quest_template: QuestTemplate,
    quest_service: QuestsService,
    now: datetime,
) -> None:
    quest_template.conditions.available_for_start.append(
        LevelCondition(
            compare_method=">",
            value=profile_context.pmc.info.level,
            id=generate_mongodb_id(),
            index=0,
            parent_id="",
            dynamic_locale=False,
            global_quest_counter_id="",
        ),
    )

    assert not await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )
    profile_context.pmc.quests.append(
        Quest(id=quest_template.id, start_time=now, status=QuestStatus.Fail),
    )

    assert await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )


async def test_quest_trader_loyalty_level(
    profile_context: ProfileContext,
    quest_template: QuestTemplate,
    quest_service: QuestsService,
    traders: Traders,
) -> None:
    trader = traders.get(TraderEnum.prapor.value)
    assert trader
    level = 2
    quest_template.conditions.available_for_start.append(
        TraderLoyaltyCondition(
            compare_method=">=",
            value=level,
            target=trader.base.id,
            id=generate_mongodb_id(),
            index=0,
            parent_id="",
            dynamic_locale=False,
            global_quest_counter_id="",
        ),
    )

    assert not await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )

    standing = trader.base.loyalty_levels[level - 1]
    trader_info = profile_context.pmc.traders_info[trader.base.id]
    trader_info.sales_sum = standing.min_sales_sum
    trader_info.standing = standing.min_standing
    profile_context.pmc.info.level = standing.min_level

    assert await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )


async def test_quest_status_condition(
    profile_context: ProfileContext,
    quest_template: QuestTemplate,
    quest_service: QuestsService,
    quest_storage: QuestStorage,
    now: datetime,
) -> None:
    required_quest = random.choice(list(quest_storage.quests.values()))
    required_status = QuestStatus.Started
    quest_template.conditions.available_for_start.append(
        QuestStatusCondition(
            target=required_quest.id,
            id=generate_mongodb_id(),
            index=0,
            parent_id="",
            dynamic_locale=False,
            global_quest_counter_id="",
            available_after=0,
            status=[required_status],
            dispersion=0,
        ),
    )

    assert not await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )
    profile_context.pmc.quests.append(
        Quest(id=required_quest.id, start_time=now, status=required_status),
    )

    assert await quest_service.is_quest_available(
        profile=profile_context.pmc,
        quest=quest_template,
    )


@pytest.mark.parametrize(
    "side",
    ["Usec", "Bear"],
)
async def test_quest_unavailable_to_side(
    profile_context: ProfileContext,
    quest_service: QuestsService,
    quest_storage: QuestStorage,
    quest_config: QuestConfig,
    side: PMCSide,
) -> None:
    profile_context.pmc.info.side = side
    deny_list = quest_config.bear_only if side == "Usec" else quest_config.usec_only

    for quest_id in deny_list:
        quest = quest_storage.quests[quest_id]
        assert not await quest_service.is_quest_available(
            profile=profile_context.pmc,
            quest=quest,
        )
