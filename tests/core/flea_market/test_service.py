from datetime import datetime, timedelta

import pytest

from app.core.domain.flea_market.services import FleaMarketService
from app.core.domain.traders.tasks import UpdateTraderAssortTask
from app.db.models import FleaOffer
from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import Traders


async def test_create_flea_offers(
    flea_service: FleaMarketService,
    traders: Traders,
    now: datetime,
    update_traders_assort_task: UpdateTraderAssortTask,
) -> None:
    await update_traders_assort_task.execute(now=now, update_flea=False)

    trader = traders.get(TraderEnum.prapor.value)
    assert trader

    next_resupply = now + timedelta(hours=1)
    offers = [
        offer
        async for offer in flea_service.make_trader_offers(
            trader_model=trader,
            now=now,
            next_resupply_time=next_resupply,
        )
    ]
    assert offers
    for offer in offers:
        assert offer.priority is False
        assert offer.user_id == trader.base.id
        assert offer.start_time == now
        assert offer.end_time == next_resupply

        assert offer.root_item_id in trader.assort.barter_scheme


async def test_buy_offer_count_error(
    flea_offer: FleaOffer,
    flea_service: FleaMarketService,
) -> None:
    with pytest.raises(ValueError):  # noqa: PT011
        await flea_service.buy_offer(offer=flea_offer, count=flea_offer.count + 1)


@pytest.mark.parametrize("count", [0, -1])
async def test_buy_flea_offer_negative_count_error(
    flea_offer: FleaOffer,
    flea_service: FleaMarketService,
    count: int,
) -> None:
    with pytest.raises(ValueError):  # noqa: PT011
        await flea_service.buy_offer(offer=flea_offer, count=count)
