import random

import pytest
from sqlalchemy.orm import InstrumentedAttribute

from app.core.domain.flea_market.dto import FleaMarketSearchDTO, FleaSearchResult
from app.core.domain.flea_market.repository import FleaOffersRepository
from app.core.dto import MinMaxDTO
from app.db.models import FleaOffer
from lib.db import DBContext
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.trading.models import CurrencyIds
from tests.factories import FleaOfferFactory
from tests.lib.tarkov.conftest import M4A1_ID
from tests.utils import random_currency_id


async def test_empty(
    flea_offers_repository: FleaOffersRepository,
    search_dto: FleaMarketSearchDTO,
) -> None:
    result = await flea_offers_repository.search(
        search=search_dto,
    )
    assert result == FleaSearchResult(
        items=[],
        total_count=0,
        categories={},
    )


@pytest.mark.parametrize(
    ("categories", "search_id", "should_include"),
    [
        (["a", "b", "c"], "a", True),
        (["a"], "a", True),
        ([], "a", False),
        (["b"], "a", False),
    ],
)
async def test_filter_by_category_id(  # noqa: PLR0913
    *,
    categories: list[str],
    search_id: str,
    should_include: bool,
    db_context: DBContext,
    flea_offers_repository: FleaOffersRepository,
    search_dto: FleaMarketSearchDTO,
) -> None:
    offer = FleaOfferFactory.build()
    offer.category_ids = categories
    db_context.add(offer)
    await db_context.flush()

    search_dto.handbook_id = search_id
    result = await flea_offers_repository.search(search_dto)
    assert result.items == ([offer] if should_include else [])


@pytest.mark.parametrize(
    (
        "field",
        "value",
        "min_value",
        "max_value",
        "search_property_name",
        "should_include",
    ),
    [
        (FleaOffer.condition, 50, 49, 51, "condition", True),
        (FleaOffer.condition, 20, 80, 100, "condition", False),
        (FleaOffer.summary_cost, 100, 80, 120, "price", True),
        (FleaOffer.summary_cost, 100, 80, 99, "price", False),
    ],
)
async def test_min_max_filters(  # noqa: PLR0913
    *,
    field: InstrumentedAttribute[float | int],
    value: float,
    min_value: float,
    max_value: float,
    search_property_name: str,
    should_include: bool,
    db_context: DBContext,
    flea_offers_repository: FleaOffersRepository,
    search_dto: FleaMarketSearchDTO,
) -> None:
    offer = FleaOfferFactory.build()
    setattr(offer, field.prop.key, value)
    db_context.add(offer)
    await db_context.flush()

    setattr(search_dto, search_property_name, MinMaxDTO(min=min_value, max=max_value))

    result = await flea_offers_repository.search(search_dto)
    assert result.items == ([offer] if should_include else [])


async def test_exclude_barter_offers(
    db_context: DBContext,
    flea_offers_repository: FleaOffersRepository,
    search_dto: FleaMarketSearchDTO,
) -> None:
    offer = FleaOfferFactory.build()
    offer.is_bartering = True
    db_context.add(offer)
    await db_context.flush()

    search_dto.exclude_bartering_offers = True
    result = await flea_offers_repository.search(search_dto)
    assert result.items == []


async def test_currency_filter(
    db_context: DBContext,
    flea_offers_repository: FleaOffersRepository,
    search_dto: FleaMarketSearchDTO,
) -> None:
    offers = []
    for currency in CurrencyIds:
        batch = FleaOfferFactory.build_batch(
            size=10,
            currency_id=currency.value,
        )
        offers.extend(batch)
    db_context.add_all(offers)

    currency_id = random_currency_id()
    search_dto.currency_id = currency_id
    expected_offers = [offer for offer in offers if offer.currency_id == currency_id]

    result = await flea_offers_repository.search(search_dto)
    assert result.items == sorted(expected_offers, key=lambda o: o.id, reverse=True)


async def test_linked_search(
    db_context: DBContext,
    flea_offers_repository: FleaOffersRepository,
    search_dto: FleaMarketSearchDTO,
    item_db: ItemDB,
) -> None:
    item = item_db.get(M4A1_ID)
    linked_items = item_db.linked_item_ids(item)

    random_linked_item_ids = random.sample(linked_items, k=len(linked_items) // 2)
    assert random_linked_item_ids

    offers = [
        FleaOfferFactory.build(root_item_template_id=linked_item_id)
        for linked_item_id in random_linked_item_ids
    ]
    db_context.add_all(offers)

    db_context.add_all(FleaOfferFactory.build_batch(size=len(offers) * 2))

    search_dto.linked_search_id = item.id
    search_dto.page_size = len(linked_items)
    result = await flea_offers_repository.search(search_dto)
    assert result.items == sorted(offers, key=lambda o: o.id, reverse=True)
    assert result.total_count == len(offers)
