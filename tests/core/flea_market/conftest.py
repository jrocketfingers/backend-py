import pytest

from app.core.domain.flea_market.dto import FleaMarketSearchDTO
from app.core.domain.models import FleaMarketSort, FleaMarketSortDirection
from app.core.dto import MinMaxDTO


@pytest.fixture
def search_dto() -> FleaMarketSearchDTO:
    return FleaMarketSearchDTO(
        condition=MinMaxDTO(min=None, max=None),
        price=MinMaxDTO(min=None, max=None),
        quantity=MinMaxDTO(min=None, max=None),
        handbook_id="",
        exclude_bartering_offers=False,
        page=0,
        page_size=15,
        sort=FleaMarketSort.id,
        sort_direction=FleaMarketSortDirection.asc,
        include_categories=True,
        currency_id=None,
        linked_search_id=None,
    )
