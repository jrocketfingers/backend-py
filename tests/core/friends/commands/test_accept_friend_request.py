import pytest
from aioinject import InjectionContext

from app.core.domain.friends.commands import FriendAcceptRequestCommand
from app.core.domain.friends.errors import FriendRequestNotFound, ProfileNotFound
from app.db.models import AccountProfile, FriendRequest
from lib.db import DBContext
from lib.tarkov.chat import ChatMember
from lib.tarkov.notifications.models import (
    FriendListRequestAcceptNotification,
    NotificationType,
)
from tests.factories import AccountProfileFactory
from tests.plugins.notifications import CaptureNotifications


@pytest.fixture
async def command(
    inject_context: InjectionContext,
    capture_notifications: CaptureNotifications,  # noqa: ARG001
) -> FriendAcceptRequestCommand:
    return await inject_context.resolve(FriendAcceptRequestCommand)


async def test_profile_not_found(
    command: FriendAcceptRequestCommand,
    account_profile: AccountProfile,
) -> None:
    result = await command.execute(
        recipient=account_profile,
        sender_profile_id="some id",
    )
    assert result.unwrap_err() == ProfileNotFound()


async def test_friend_request_not_found(
    command: FriendAcceptRequestCommand,
    account_profile: AccountProfile,
) -> None:
    result = await command.execute(
        recipient=account_profile,
        sender_profile_id=str(account_profile.profile_id),
    )
    assert result.unwrap_err() == FriendRequestNotFound()


async def test_ok(
    command: FriendAcceptRequestCommand,
    account_profile: AccountProfile,
    db_context: DBContext,
    capture_notifications: CaptureNotifications,
) -> None:
    sender = AccountProfileFactory.build()
    request = FriendRequest(sender=sender, recipient=account_profile)
    db_context.add(request)

    async with capture_notifications.capture(
        profile_id=str(sender.id),
    ) as notifications:
        result = await command.execute(
            recipient=account_profile,
            sender_profile_id=sender.profile_id,
        )

    friends = result.unwrap()
    assert friends.sender == sender
    assert friends.recipient == account_profile

    assert notifications == [
        FriendListRequestAcceptNotification(
            type=NotificationType.FRIEND_LIST_REQUEST_ACCEPT,
            profile=ChatMember.from_profile(account_profile),
        ),
    ]
