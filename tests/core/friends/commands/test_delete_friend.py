import pytest
from aioinject import InjectionContext

from app.core.domain.friends.commands import FriendDeleteCommand
from app.core.domain.friends.repository import FriendRepository
from app.db.models import AccountProfile
from lib.tarkov.chat import ChatMember
from lib.tarkov.notifications.models import (
    FriendListRemovetNotification,
    NotificationType,
)
from tests.plugins.notifications import CaptureNotifications


@pytest.fixture
async def command(
    inject_context: InjectionContext,
    capture_notifications: CaptureNotifications,  # noqa: ARG001
) -> FriendDeleteCommand:
    return await inject_context.resolve(FriendDeleteCommand)


async def test_ok(
    command: FriendDeleteCommand,
    account_profile: AccountProfile,
    friend: AccountProfile,
    friend_repository: FriendRepository,
    capture_notifications: CaptureNotifications,
) -> None:
    assert await friend_repository.friends(account_profile)

    async with capture_notifications.capture(
        profile_id=str(friend.id),
    ) as notifications:
        result = await command.execute(
            profile=account_profile,
            friend_profile_id=friend.profile_id,
        )

    assert result.is_ok()

    assert not await friend_repository.friends(account_profile)
    assert notifications == [
        FriendListRemovetNotification(
            type=NotificationType.FRIEND_LIST_REMOVE,
            profile=ChatMember.from_profile(account_profile),
        ),
    ]
