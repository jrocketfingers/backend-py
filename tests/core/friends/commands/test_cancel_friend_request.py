from datetime import datetime

import freezegun
import pytest
from aioinject import InjectionContext

from app.core.domain.friends.commands import FriendCancelRequestCommand
from app.core.domain.friends.errors import ProfileNotFound
from app.db.models import AccountProfile, FriendRequest
from lib.db import DBContext
from lib.tarkov.chat import ChatMember
from lib.tarkov.notifications.models import (
    FriendListRequestCancelledNotification,
    NotificationType,
)
from tests.factories import AccountProfileFactory
from tests.plugins.notifications import CaptureNotifications

pytestmark = pytest.mark.usefixtures("capture_notifications")


@pytest.fixture
async def command(
    inject_context: InjectionContext,
    capture_notifications: CaptureNotifications,  # noqa: ARG001
) -> FriendCancelRequestCommand:
    return await inject_context.resolve(FriendCancelRequestCommand)


async def test_profile_not_found(
    account_profile: AccountProfile,
    command: FriendCancelRequestCommand,
) -> None:
    result = await command.execute(
        sender=account_profile,
        recipient_profile_id="not-found",
    )
    assert result.unwrap_err() == ProfileNotFound()


async def test_should_not_send_notification_if_already_deleted(
    capture_notifications: CaptureNotifications,
    account_profile: AccountProfile,
    command: FriendCancelRequestCommand,
    db_context: DBContext,
) -> None:
    recipient = AccountProfileFactory.build()
    db_context.add(recipient)

    async with capture_notifications.capture(
        profile_id=str(recipient.id),
    ) as notifications:
        result = await command.execute(
            sender=account_profile,
            recipient_profile_id=recipient.profile_id,
        )

    assert result.unwrap() is None
    assert notifications == []


async def test_ok(
    capture_notifications: CaptureNotifications,
    account_profile: AccountProfile,
    command: FriendCancelRequestCommand,
    db_context: DBContext,
    now: datetime,
) -> None:
    recipient = AccountProfileFactory.build()
    request = FriendRequest(recipient=recipient, sender=account_profile)
    db_context.add(request)

    async with capture_notifications.capture(
        profile_id=str(recipient.id),
    ) as notifications:
        with freezegun.freeze_time(now):
            result = await command.execute(
                sender=account_profile,
                recipient_profile_id=recipient.profile_id,
            )

    assert result.unwrap() is None
    assert notifications == [
        FriendListRequestCancelledNotification(
            type=NotificationType.FRIEND_LIST_REQUEST_CANCEL,
            profile=ChatMember.from_profile(account_profile),
        ),
    ]
