import random

import pytest
from aioinject import InjectionContext

from app.core.domain.friends.queries import (
    FriendRequestOutboxQuery,
)
from app.db.models import AccountProfile, FriendRequest
from lib.db import DBContext
from tests.factories import AccountProfileFactory


@pytest.fixture
async def query(inject_context: InjectionContext) -> FriendRequestOutboxQuery:
    return await inject_context.resolve(FriendRequestOutboxQuery)


async def test_empty(
    account_profile: AccountProfile,
    query: FriendRequestOutboxQuery,
) -> None:
    assert await query.execute(user=account_profile) == []


async def test_ok(db_context: DBContext, query: FriendRequestOutboxQuery) -> None:
    accounts = AccountProfileFactory.build_batch(size=10)
    db_context.add_all(accounts)

    account = random.choice(accounts)
    requests = [
        FriendRequest(sender=account, recipient=other)
        for other in accounts
        if random.choice([True, False])
    ]
    requests.sort(key=lambda req: req.id)
    db_context.add_all(requests)

    result = await query.execute(user=account)
    assert result == requests
