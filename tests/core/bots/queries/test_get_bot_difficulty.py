import pytest
from aioinject import InjectionContext

from app.core.domain.bots.queries import BotDifficultyQuery
from lib.tarkov.bots.db import BotDB


@pytest.fixture
async def query(inject_context: InjectionContext) -> BotDifficultyQuery:
    return await inject_context.resolve(BotDifficultyQuery)


async def test_ok(bot_db: BotDB, query: BotDifficultyQuery) -> None:
    for bot_name, bot in bot_db.bots.items():
        for difficulty_name, difficulty in bot.difficulty.items():
            assert (
                await query.execute(bot_type=bot_name, bot_difficulty=difficulty_name)
                == difficulty
            )


async def test_core(bot_db: BotDB, query: BotDifficultyQuery) -> None:
    assert await query.execute(bot_type="core", bot_difficulty="core") == bot_db.core
