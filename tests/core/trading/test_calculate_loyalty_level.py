from app.core.domain.traders.dto import LoyaltyLevelDTO
from app.core.domain.traders.services import calculate_loyalty_level
from lib.tarkov.profiles.models import TraderInfo
from lib.tarkov.trading.models import LoyaltyLevel


def test_calculate_loyalty_level_negative_rep() -> None:
    loyalty_level = LoyaltyLevel(
        buy_price_coef=1,
        exchange_price_coef=1,
        heal_price_coef=1,
        insurance_price_coef=1,
        min_level=1,
        min_sales_sum=0,
        min_standing=0,
        repair_price_coef=0,
    )
    expected = LoyaltyLevelDTO(number=1, loyalty=loyalty_level)
    assert (
        calculate_loyalty_level(
            player_level=1,
            current_standing=TraderInfo(standing=-1, disabled=False, unlocked=False),
            loyalty_levels=[loyalty_level],
        )
        == expected
    )
