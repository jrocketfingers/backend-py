import random

import pytest
from aioinject import InjectionContext
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.domain.traders.queries import TraderAssortQuery
from app.core.domain.traders.repositories import TraderRepository
from app.db.models import AccountProfile, TraderPlayerRestriction
from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import TraderModel, Traders


@pytest.fixture
async def query(inject_context: InjectionContext) -> TraderAssortQuery:
    return await inject_context.resolve(TraderAssortQuery)


@pytest.fixture
async def trader(traders: Traders) -> TraderModel:
    trader = traders.get(TraderEnum.prapor.value)
    assert trader
    return trader


@pytest.mark.usefixtures("_seed_traders", "profile_context")
async def test_no_restrictions(
    trader: TraderModel,
    account_profile: AccountProfile,
    query: TraderAssortQuery,
) -> None:
    result = await query.execute(
        profile=account_profile,
        trader_id=trader.base.id,
    )
    dto = result.unwrap()
    for item in dto.items:
        assert item.upd.get("BuyRestrictionCurrent", 0) == 0


@pytest.mark.usefixtures("_seed_traders", "profile_context")
async def test_player_restrictions(
    trader: TraderModel,
    trader_repository: TraderRepository,
    account_profile: AccountProfile,
    query: TraderAssortQuery,
    session: AsyncSession,
) -> None:
    trader_db = await trader_repository.get(trader.base.id)
    assert trader_db

    restrictions = {}
    for item in trader.assort.items:
        max_count = item.upd.get("BuyRestrictionMax")
        if max_count is None:
            continue

        restrictions[item.id] = TraderPlayerRestriction(
            trader=trader_db,
            profile=account_profile,
            item_id=item.id,
            bought_count=random.randint(0, max_count),
        )
    session.add_all(restrictions.values())
    await session.flush()

    result = await query.execute(
        profile=account_profile,
        trader_id=trader.base.id,
    )
    dto = result.unwrap()
    for item in dto.items:
        if item.id in restrictions:
            assert (
                item.upd["BuyRestrictionCurrent"] == restrictions[item.id].bought_count
            )
