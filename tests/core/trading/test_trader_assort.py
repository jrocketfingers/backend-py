import uuid
from datetime import datetime

import pytest

from app.core.domain.traders.services import filter_assort_items
from lib.tarkov.items.models import Item
from lib.tarkov.profiles.models import Quest, TraderInfo
from lib.tarkov.quests.models import QuestStatus
from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.types import ItemId, ItemTemplateId, QuestId
from lib.utils import utc_now


@pytest.fixture
def trader(traders: Traders) -> TraderModel:
    trader = traders.get(TraderEnum.prapor)
    assert trader
    return trader


@pytest.fixture
def trader_info() -> TraderInfo:
    return TraderInfo(
        disabled=False,
        standing=0.0,
        unlocked=True,
    )


@pytest.fixture
def item() -> Item:
    return Item(
        id=ItemId(str(uuid.uuid4())),
        template_id=ItemTemplateId(str(uuid.uuid4())),
    )


def test_empty(
    trader: TraderModel,
    trader_info: TraderInfo,
) -> None:
    result = filter_assort_items(
        level=1,
        traders_info={trader.base.id: trader_info},
        trader=trader,
        items=[],
        quests=[],
    )
    assert result == []


def test_filter_by_loyalty_level(
    trader: TraderModel,
    trader_info: TraderInfo,
) -> None:
    item = Item(
        id=ItemId(str(uuid.uuid4())),
        template_id=ItemTemplateId(str(uuid.uuid4())),
    )
    trader.assort.loyal_level_items[item.id] = 2
    required_loyalty = trader.base.loyalty_levels[1]

    trader_info.standing = required_loyalty.min_standing
    trader_info.sales_sum = required_loyalty.min_sales_sum
    result = filter_assort_items(
        level=required_loyalty.min_level,
        traders_info={trader.base.id: trader_info},
        trader=trader,
        items=[item],
        quests=[],
    )
    assert list(result) == [item]


@pytest.mark.parametrize(
    ("sales_sum_delta", "level_delta", "standing_delta"),
    [
        (-1, 0, 0),
        (0, -1, 0),
        (0, 0, -0.01),
    ],
)
def test_filter_by_loyalty_level_exclude(  # noqa: PLR0913
    trader: TraderModel,
    trader_info: TraderInfo,
    item: Item,
    sales_sum_delta: int,
    level_delta: int,
    standing_delta: float,
) -> None:
    required_level = 2
    trader.assort.loyal_level_items[item.id] = required_level
    required_loyalty = trader.base.loyalty_levels[required_level - 1]

    trader_info.standing = required_loyalty.min_standing + standing_delta
    trader_info.sales_sum = required_loyalty.min_sales_sum + sales_sum_delta
    result = filter_assort_items(
        level=required_loyalty.min_level + level_delta,
        traders_info={trader.base.id: trader_info},
        trader=trader,
        items=[item],
        quests=[],
    )
    assert result == []


def test_quest_requirement_filter(
    trader: TraderModel,
    trader_info: TraderInfo,
    item: Item,
) -> None:
    quest_id = QuestId(str(uuid.uuid4()))

    for required_status, quest_assort in [
        (QuestStatus.Fail, trader.quest_assort.fail),
        (QuestStatus.Success, trader.quest_assort.success),
    ]:
        quest_assort[item.id] = quest_id

        result = filter_assort_items(
            level=1,
            traders_info={trader.base.id: trader_info},
            trader=trader,
            items=[item],
            quests=[Quest(id=quest_id, start_time=utc_now(), status=required_status)],
        )
        assert result == [item]


def test_child_items_should_be_excluded_too(
    trader: TraderModel,
    trader_info: TraderInfo,
    item: Item,
) -> None:
    child = Item(
        id=ItemId(str(uuid.uuid4())),
        parent_id=item.id,
        template_id=ItemTemplateId(""),
    )

    trader.quest_assort.success[item.id] = QuestId("")
    result = filter_assort_items(
        level=1,
        traders_info={trader.base.id: trader_info},
        trader=trader,
        items=[item, child],
        quests=[],
    )
    assert result == []


def test_quest_requirement_filter_exclude(
    trader: TraderModel,
    trader_info: TraderInfo,
    item: Item,
) -> None:
    quest_id = QuestId(str(uuid.uuid4()))
    trader.quest_assort.success[item.id] = quest_id

    result = filter_assort_items(
        level=1,
        traders_info={trader.base.id: trader_info},
        trader=trader,
        items=[item],
        quests=[Quest(id=quest_id, start_time=utc_now(), status=QuestStatus.Locked)],
    )
    assert result == []


_not_ok_statuses = (QuestStatus.Locked, QuestStatus.AvailableForStart)


@pytest.mark.parametrize(
    ("quest_status", "should_include"),
    [
        *((status, True) for status in QuestStatus if status not in _not_ok_statuses),
        *((status, False) for status in _not_ok_statuses),
    ],
)
async def test_quest_started_requirement(
    *,
    trader: TraderModel,
    trader_info: TraderInfo,
    item: Item,
    quest_status: QuestStatus,
    should_include: bool,
) -> None:
    quest_id = QuestId(str(uuid.uuid4()))
    trader.quest_assort.started[item.id] = quest_id

    expected = [item] if should_include else []

    result = filter_assort_items(
        level=1,
        traders_info={trader.base.id: trader_info},
        trader=trader,
        items=[item],
        quests=[Quest(id=quest_id, status=quest_status, start_time=datetime.min)],
    )
    assert result == expected


async def test_started_quest_not_found(
    trader: TraderModel,
    trader_info: TraderInfo,
    item: Item,
) -> None:
    trader.quest_assort.started[item.id] = QuestId(str(uuid.uuid4()))

    result = filter_assort_items(
        level=1,
        traders_info={trader.base.id: trader_info},
        trader=trader,
        items=[item],
        quests=[],
    )
    assert result == []
