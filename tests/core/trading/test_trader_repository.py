import uuid

import pytest

from app.core.domain.traders.repositories import TraderRepository
from lib.tarkov.trading.models import TraderEnum


async def test_get_not_found(
    trader_repository: TraderRepository,
) -> None:
    assert await trader_repository.get(str(uuid.uuid4())) is None


@pytest.mark.usefixtures("_seed_traders")
async def test_get(
    trader_repository: TraderRepository,
) -> None:
    trader = await trader_repository.get(TraderEnum.prapor)
    assert trader is not None
    assert trader.id == TraderEnum.prapor
