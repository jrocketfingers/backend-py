import uuid

import pytest
from aioinject import InjectionContext

from app.core.domain.aki_compat.commands import RaidConfigurationCommand
from app.core.domain.bots.schema import (
    RaidBotSettings,
    RaidConfigurationSchema,
    RaidTimeAndWeatherSchema,
    RaidWavesSettings,
)
from app.db.models import AccountProfile
from lib.tarkov.customization import Side


@pytest.fixture
async def command(inject_context: InjectionContext) -> RaidConfigurationCommand:
    return await inject_context.resolve(RaidConfigurationCommand)


async def test_ok(
    command: RaidConfigurationCommand,
    account_profile: AccountProfile,
) -> None:
    location = str(uuid.uuid4())
    configuration = RaidConfigurationSchema(
        bot_settings=RaidBotSettings(is_scav_wars=False, bot_amount=""),
        can_show_group_preview=True,
        key_id="",
        location=location,
        max_group_count=1,
        metabolism_disabled=False,
        players_spawn_place="",
        raid_mode="",
        side=Side.pmc,
        time_and_weather_settings=RaidTimeAndWeatherSchema(
            is_random_time=False,
            is_random_weather=False,
            cloudiness_type="",
            rain_type="",
            wind_type="",
            fog_type="",
            time_flow_type="",
            hour_of_day=0,
        ),
        time_variant="",
        waves_settings=RaidWavesSettings(
            bot_amount="",
            bot_difficulty="",
            is_bosses=True,
            is_tagged_and_cursed=False,
        ),
    )

    await command.execute(account_profile=account_profile, configuration=configuration)

    assert account_profile.raid_info
    assert account_profile.raid_info.locations == location
