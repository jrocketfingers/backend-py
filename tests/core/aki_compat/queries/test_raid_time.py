from datetime import timedelta

import pytest
from aioinject import InjectionContext

from app.core.domain.aki_compat.queries import RaidTimeQuery
from app.core.domain.aki_compat.services import RaidService
from lib.tarkov.customization import Side
from lib.tarkov.locations.db import LocationDB


@pytest.fixture
async def query(inject_context: InjectionContext) -> RaidTimeQuery:
    return await inject_context.resolve(RaidTimeQuery)


async def test_pmc(
    query: RaidTimeQuery,
    location_db: LocationDB,
    raid_service: RaidService,
) -> None:
    for location in location_db.locations.values():
        result = await query.execute(location_id=location.id, side=Side.pmc)
        assert result.time_left == timedelta(minutes=location.escape_time_limit)
        assert (
            result.original_required_survival_time
            == raid_service.survival_time_requirement
        )
        assert (
            result.new_required_survival_time == result.original_required_survival_time
        )
        assert result.exit_changes == []


async def test_scav(
    query: RaidTimeQuery,
    location_db: LocationDB,
    raid_service: RaidService,
) -> None:
    for location in location_db.locations.values():
        if location.escape_time_limit == 0:
            continue

        result = await query.execute(location_id=location.id, side=Side.scav)
        assert result.time_left <= timedelta(minutes=location.escape_time_limit)
        assert (
            result.original_required_survival_time
            == raid_service.survival_time_requirement
        )
        assert result.new_required_survival_time <= result.time_left
        assert result.exit_changes == []
