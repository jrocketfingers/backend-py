import pytest

from app.core.domain.hideout.services import HideoutService, find_area
from lib.tarkov.hideout.models.bonuses import HideoutBonusStashSize
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.profiles.models import HideoutAreaType


@pytest.fixture
async def account_game_edition() -> str:
    return "standard"


async def test_upgrade_stash(
    profile_context: ProfileContext,
    hideout_service: HideoutService,
) -> None:
    area = find_area(profile_context.pmc.hideout.areas, HideoutAreaType.stash)
    area_tpl = hideout_service.area_template(type=area.type)

    stash = profile_context.inventory.get(profile_context.pmc.inventory.stash)
    assert stash

    for level, stage in enumerate(
        list(area_tpl.stages.values())[area.level + 1 :],
        start=area.level + 1,
    ):
        assert area.level == level - 1
        await hideout_service.upgrade_complete(
            area=area,
            inventory=profile_context.inventory,
            profile=profile_context.pmc,
        )
        assert area.level == level

        stash_bonus = next(
            bonus for bonus in stage.bonuses if isinstance(bonus, HideoutBonusStashSize)
        )
        assert stash.template_id == stash_bonus.template_id
