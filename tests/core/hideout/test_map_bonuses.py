from app.core.domain.hideout.services import HideoutService, map_bonus


def test_map_bonuses(hideout_service: HideoutService) -> None:
    for area in hideout_service.areas:
        for stage in area.stages.values():
            for bonus in stage.bonuses:
                assert map_bonus(bonus)
