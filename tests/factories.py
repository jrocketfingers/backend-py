from __future__ import annotations

import random
from datetime import UTC, datetime
from typing import TYPE_CHECKING, Any, Generic, TypeVar

import factory  # type: ignore[import-untyped]
from factory.fuzzy import FuzzyDateTime  # type: ignore[import-untyped]

from app.db.models import Account, AccountProfile, FleaOffer
from lib.tarkov.chat import MemberCategory
from tests.utils import random_currency_id

T = TypeVar("T")


class GenericFactory(factory.Factory, Generic[T]):  # type: ignore[misc]
    if TYPE_CHECKING:

        @classmethod
        def build(cls, **kwargs: Any) -> T:  # noqa: ANN401
            ...

        @classmethod
        def build_batch(cls, size: int, **kwargs: Any) -> list[T]:  # noqa: ANN401
            ...

    def __class_getitem__(cls, item: type[T] | TypeVar) -> GenericFactory[T]:
        if isinstance(item, TypeVar):
            return cls  # type: ignore[return-value]

        class Meta:
            model = item

        return type(  # type: ignore[return-value]
            f"{cls.__qualname__}[{item.__qualname__}]",
            (cls,),
            {
                "Meta": Meta,
            },
        )


fuzzy_date_time = FuzzyDateTime(start_dt=datetime(1970, 1, 1, tzinfo=UTC))


class FleaOfferFactory(GenericFactory[FleaOffer]):
    root_item_id = factory.Faker("pystr")
    root_item_template_id = factory.Faker("pystr")
    category_ids = factory.Faker("pylist", value_types=[int])
    items = factory.LazyFunction(list)
    currency_id = factory.LazyFunction(random_currency_id)
    requirements = factory.LazyFunction(list)
    items_cost = factory.Faker("pyint")
    requirement_items_cost = factory.Faker("pyint")
    start_time = fuzzy_date_time
    end_time = fuzzy_date_time
    is_bartering = factory.Faker("pybool")
    sell_in_one_piece = factory.Faker("pybool")
    required_loyalty_level = factory.Faker("pyint", max_value=4)
    summary_cost = factory.Faker("pyint")
    count = factory.Faker("pyint")
    priority = factory.Faker("pybool")
    condition = factory.Faker("pyfloat", min_value=0, max_value=100)
    user_id = factory.Faker("pystr")
    user_type = factory.Faker("enum", enum_cls=MemberCategory)


class AccountFactory(GenericFactory[Account]):
    session_id = factory.Faker("pystr")
    username = factory.Faker("pystr")
    game_edition = factory.Faker("pystr")


class AccountProfileFactory(GenericFactory[AccountProfile]):
    name = factory.Faker("pystr")
    side = factory.LazyFunction(lambda: random.choice(["Usec", "Bear"]))
    level = factory.Faker("pyint")
    member_category = factory.Faker("enum", enum_cls=MemberCategory)
    profile_id = factory.Faker("pystr")
    account = factory.SubFactory(AccountFactory)
