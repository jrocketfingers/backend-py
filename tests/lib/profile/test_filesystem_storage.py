import pytest

from app.db.models import AccountProfile
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.profiles.storage import ProfileStorageImpl


class _TestError(Exception):
    pass


async def test_evicts_on_error(
    profile_storage: ProfileStorageImpl,
    profile_context: ProfileContext,
    account_profile: AccountProfile,
) -> None:
    with pytest.raises(_TestError):  # noqa: PT012
        async with profile_storage.profile(profile_id=str(account_profile.id)) as ctx:
            assert ctx is profile_context
            raise _TestError
    assert profile_context.pmc.id not in profile_storage._profiles  # noqa: SLF001
