import uuid
from pathlib import Path

import pytest

from lib.utils import safe_rename


class _TestError(Exception):
    pass


def test_file_does_not_exist(
    tmp_path: Path,
) -> None:
    src = tmp_path.joinpath("src")
    dest = tmp_path.joinpath("dest")

    with pytest.raises(FileNotFoundError), safe_rename(src, dest):
        pass
    assert not src.exists()
    assert not dest.exists()


def test_exception_raised(
    tmp_path: Path,
) -> None:
    src = tmp_path.joinpath("src")
    src.touch()

    dest = tmp_path.joinpath("dest")
    dest.touch()

    with pytest.raises(_TestError), safe_rename(src, dest):
        raise _TestError
    assert not src.exists()
    assert dest.exists()


def test_ok(
    tmp_path: Path,
) -> None:
    text = str(uuid.uuid4())
    src = tmp_path.joinpath("src")
    dest = tmp_path.joinpath("dest")

    with safe_rename(src, dest):
        src.write_text(text)
    assert not src.exists()
    assert dest.read_text() == text
