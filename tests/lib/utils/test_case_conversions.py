from pathlib import Path

import orjson
import pytest

from lib.utils import read_json, snake_to_camel, snake_to_pascal


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        ("a", "a"),
        ("a_b", "aB"),
        ("a_bb", "aBb"),
    ],
)
def test_snake_to_camel(value: str, expected: str) -> None:
    assert snake_to_camel(value) == expected


@pytest.mark.parametrize(
    ("value", "expected"),
    [
        ("a", "A"),
        ("a_b", "AB"),
        ("a_bb", "ABb"),
    ],
)
def test_snake_to_pascal(value: str, expected: str) -> None:
    assert snake_to_pascal(value) == expected


async def test_read_json(tmp_path: Path) -> None:
    filepath = tmp_path.joinpath("test.json")
    data = ["example", {"data": "foo"}]
    filepath.write_bytes(orjson.dumps(data))

    assert await read_json(filepath) == data
