import pytest

from lib.utils import scale


@pytest.mark.parametrize(
    "value",
    [-0.001, 1.001],
)
def test_invalid_value(
    value: float,
) -> None:
    with pytest.raises(ValueError):  # noqa: PT011
        scale(value, 0, 1)


@pytest.mark.parametrize(
    ("value", "a", "b", "expected"),
    [
        (0, 10, 20, 10),
        (1, 10, 20, 20),
        (0, 0, 1, 0),
        (0.25, 10, 20, 12.5),
        (1 / 3, -10, 20, 0),
    ],
)
def test_ok(
    value: float,
    a: float,
    b: float,
    expected: float,
) -> None:
    assert scale(value, a, b) == expected
