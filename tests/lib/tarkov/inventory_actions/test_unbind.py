import pytest

from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import UnbindHandler
from lib.tarkov.items.actions import UnbindAction
from lib.tarkov.types import ItemId


@pytest.fixture
def handler() -> UnbindHandler:
    return UnbindHandler()


@pytest.mark.parametrize(
    ("state", "key", "item_id", "expected"),
    [
        ({"a": "b"}, "a", "b", {}),
        ({"a": "b"}, "b", "a", {"a": "b"}),
        ({"a": "b"}, "a", "a", {"a": "b"}),
    ],
)
async def test_ok(  # noqa: PLR0913
    handler: UnbindHandler,
    action_context: ActionContext,
    state: dict[str, ItemId],
    key: str,
    item_id: ItemId,
    expected: dict[str, ItemId],
) -> None:
    action_context.profile.inventory.fast_panel = state
    result = await handler.handle(
        action=UnbindAction(
            action="Unbind",
            item=item_id,
            index=key,
        ),
        context=action_context,
    )
    assert result.unwrap() is None

    assert action_context.profile.inventory.fast_panel == expected
