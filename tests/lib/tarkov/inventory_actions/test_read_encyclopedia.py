import uuid

import pytest

from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import ReadEncyclopediaHandler
from lib.tarkov.items.actions import ReadEncyclopediaAction
from lib.tarkov.types import ItemTemplateId


@pytest.fixture
async def handler() -> ReadEncyclopediaHandler:
    return ReadEncyclopediaHandler()


async def test_ok(
    handler: ReadEncyclopediaHandler,
    action_context: ActionContext,
) -> None:
    key = ItemTemplateId(str(uuid.uuid4()))
    action_context.profile.encyclopedia[key] = False

    result = await handler.handle(
        context=action_context,
        action=ReadEncyclopediaAction(ids=[key], action="ReadEncyclopedia"),
    )
    assert result.is_ok()
    assert action_context.profile.encyclopedia[key] is True
