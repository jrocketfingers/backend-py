import random

import pytest

from lib.tarkov.inventory import ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import ExamineHandler
from lib.tarkov.inventory_actions.services import ItemLocator
from lib.tarkov.items.actions import ExamineAction, ItemOwner
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import Traders
from lib.tarkov.types import ItemId
from tests.lib.tarkov.conftest import PATRON_9x19_PST_GZH


@pytest.fixture
async def handler(item_db: ItemDB, item_locator: ItemLocator) -> ExamineHandler:
    return ExamineHandler(item_db=item_db, item_locator=item_locator)


async def test_ok(
    traders: Traders,
    item_db: ItemDB,
    handler: ExamineHandler,
    action_context: ActionContext,
) -> None:
    trader = traders.get(TraderEnum.prapor.value)
    assert trader
    item = random.choice(trader.assort.items)
    item_tpl = item_db.get(item.template_id)

    result = await handler.handle(
        context=action_context,
        action=ExamineAction(
            action="Examine",
            item=item.id,
            from_owner=ItemOwner(type="Trader", id=trader.base.id),
        ),
    )
    assert result.is_ok()
    assert action_context.profile.info.experience == item_tpl.props.examine_experience
    assert action_context.profile.encyclopedia[item.template_id] is False


async def test_trader_not_found(
    handler: ExamineHandler,
    action_context: ActionContext,
) -> None:
    item_id = ItemId("item.id")
    result = await handler.handle(
        context=action_context,
        action=ExamineAction(
            action="Examine",
            item=item_id,
            from_owner=ItemOwner(type="Trader", id="trader"),
        ),
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_item_not_found(
    handler: ExamineHandler,
    action_context: ActionContext,
) -> None:
    item_id = ItemId("item.id")
    result = await handler.handle(
        context=action_context,
        action=ExamineAction(
            action="Examine",
            item=item_id,
            from_owner=ItemOwner(type="Trader", id=TraderEnum.prapor.value),
        ),
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_examine_from_hideout_production_ok(
    handler: ExamineHandler,
    action_context: ActionContext,
) -> None:
    item_id = PATRON_9x19_PST_GZH
    result = await handler.handle(
        context=action_context,
        action=ExamineAction(
            action="Examine",
            item=ItemId(item_id),
            from_owner=ItemOwner(id="...", type="HideoutProduction"),
        ),
    )
    assert result.is_ok()
    assert action_context.profile.encyclopedia[PATRON_9x19_PST_GZH] is False
