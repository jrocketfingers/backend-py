import uuid

import pytest
from aioinject import InjectionContext

from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import AddToWishlistHandler
from lib.tarkov.items.actions import AddToWishlistAction
from lib.tarkov.types import ItemTemplateId


@pytest.fixture
async def handler(inject_context: InjectionContext) -> AddToWishlistHandler:
    return await inject_context.resolve(AddToWishlistHandler)


async def test_ok(
    handler: AddToWishlistHandler,
    action_context: ActionContext,
) -> None:
    template_id = ItemTemplateId(str(uuid.uuid4()))

    result = await handler.handle(
        action=AddToWishlistAction(
            action="AddToWishList",
            template_id=template_id,
        ),
        context=action_context,
    )
    assert result.is_ok()
    assert template_id in action_context.profile.wish_list
