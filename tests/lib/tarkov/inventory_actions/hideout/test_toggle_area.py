import itertools
import random
from datetime import datetime

import pytest

from app.core.domain.hideout.services import find_area
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import HideoutToggleAreaHandler
from lib.tarkov.items.actions import HideoutToggleAreaAction
from lib.tarkov.profiles.models import HideoutAreaType


@pytest.fixture
async def handler() -> HideoutToggleAreaHandler:
    return HideoutToggleAreaHandler()


@pytest.mark.parametrize(
    ("area_type", "enabled"),
    itertools.product(random.choices(list(HideoutAreaType), k=5), [True, False]),
)
async def test_toggle_area(
    *,
    handler: HideoutToggleAreaHandler,
    action_context: ActionContext,
    now: datetime,
    area_type: HideoutAreaType,
    enabled: bool,
) -> None:
    action = HideoutToggleAreaAction(
        area_type=area_type,
        enabled=enabled,
        timestamp=now,
        action="HideoutToggleArea",
    )
    result = await handler.handle(action=action, context=action_context)
    assert result.is_ok()

    area = find_area(action_context.profile.hideout.areas, area_type=area_type)
    assert area.active == enabled
