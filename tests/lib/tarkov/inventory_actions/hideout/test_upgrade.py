import pytest

from app.core.domain.hideout.services import HideoutService, find_area
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import HideoutUpgradeHandler
from lib.tarkov.items.actions import HideoutUpgradeAction
from lib.tarkov.profiles.models import HideoutAreaType
from lib.utils import utc_min, utc_now


@pytest.fixture
async def handler(hideout_service: HideoutService) -> HideoutUpgradeHandler:
    return HideoutUpgradeHandler(hideout_service=hideout_service)


async def test_ok(
    action_context: ActionContext,
    handler: HideoutUpgradeHandler,
) -> None:
    area_type = HideoutAreaType.vents
    area = find_area(action_context.profile.hideout.areas, area_type=area_type)
    assert area.level == 0
    assert area.constructing is False
    assert area.complete_time == utc_min

    action = HideoutUpgradeAction(
        area_type=area_type,
        items=[],
        action="HideoutUpgrade",
        timestamp=utc_now(),
    )
    await handler.handle(action=action, context=action_context)
    assert area.level == 0
    assert area.constructing is True
    assert area.complete_time > utc_min  # type: ignore[unreachable]
