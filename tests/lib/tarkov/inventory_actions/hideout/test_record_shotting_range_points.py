import random

import pytest

from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions._hideout import (
    RecordShootingRangePointsHandler,
)
from lib.tarkov.items.actions import RecordShootingRangePointsAction


@pytest.fixture
async def handler() -> RecordShootingRangePointsHandler:
    return RecordShootingRangePointsHandler()


async def test_ok(
    handler: RecordShootingRangePointsHandler,
    action_context: ActionContext,
) -> None:
    value = random.randint(1, 100)
    result = await handler.handle(
        action=RecordShootingRangePointsAction(
            action="RecordShootingRangePoints",
            points=value,
        ),
        context=action_context,
    )
    assert result.is_ok()

    counter = next(
        v
        for v in action_context.profile.stats.eft.overall_counters.items
        if v.key == ["ShootingRangePoints"]
    )
    assert counter.value == value
