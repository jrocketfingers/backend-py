from datetime import datetime

import pytest

from app.core.domain.hideout.services import find_area
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import HideoutPutItemsHandler
from lib.tarkov.items.actions import HideoutPutItemsAction, RequiredItem
from lib.tarkov.items.models import Item
from lib.tarkov.profiles.models import HideoutAreaSlot, HideoutAreaType


@pytest.fixture
async def handler() -> HideoutPutItemsHandler:
    return HideoutPutItemsHandler()


async def test_ok(
    handler: HideoutPutItemsHandler,
    action_context: ActionContext,
    inventory_item: Item,
    now: datetime,
) -> None:
    area_type = HideoutAreaType.generator
    action = HideoutPutItemsAction(
        area_type=area_type,
        items={0: RequiredItem(id=inventory_item.id, count=1)},
        timestamp=now,
        action="HideoutPutItemsInAreaSlots",
    )
    result = await handler.handle(action=action, context=action_context)
    assert result.is_ok()

    assert action_context.inventory.get(inventory_item.id) is None

    area = find_area(action_context.profile.hideout.areas, area_type=area_type)
    assert area.slots == [HideoutAreaSlot(item=[inventory_item], location_index=0)]
