import random
import uuid

import pytest

from app.core.domain.hideout.services import HideoutService, find_area
from lib.tarkov.hideout.models.bonuses import HideoutBonusType, HideoutGenericBonus
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import (
    HideoutUpgradeCompleteHandler,
)
from lib.tarkov.items.actions import HideoutUpgradeCompleteAction
from lib.tarkov.profiles.models import HideoutAreaType
from lib.utils import utc_min, utc_now


@pytest.fixture
async def handler(hideout_service: HideoutService) -> HideoutUpgradeCompleteHandler:
    return HideoutUpgradeCompleteHandler(hideout_service=hideout_service)


async def test_ok(
    action_context: ActionContext,
    handler: HideoutUpgradeCompleteHandler,
) -> None:
    area_type = HideoutAreaType.vents
    area = find_area(action_context.profile.hideout.areas, area_type=area_type)
    area.level = 0
    area.constructing = True
    area.complete_time = utc_now()

    action = HideoutUpgradeCompleteAction(
        area_type=area_type,
        action="HideoutUpgradeComplete",
        timestamp=utc_now(),
    )
    await handler.handle(action=action, context=action_context)

    assert area.level == 1
    assert area.constructing is False
    assert area.complete_time == utc_min


async def test_maximum_energy_increase(
    action_context: ActionContext,
    handler: HideoutUpgradeCompleteHandler,
    hideout_service: HideoutService,
) -> None:
    previous_max_energy = action_context.profile.health.energy.maximum
    energy_increase = random.randint(10, 100)
    area_type = HideoutAreaType.vents
    area_tpl = hideout_service.area_template(type=area_type)
    area_tpl.stages[1].bonuses.append(
        HideoutGenericBonus(
            id=str(uuid.uuid4()),
            value=energy_increase,
            passive=True,
            production=True,
            visible=True,
            type=HideoutBonusType.maximum_energy,
        ),
    )
    area = find_area(action_context.profile.hideout.areas, area_type=area_type)
    area.level = 0
    action = HideoutUpgradeCompleteAction(
        area_type=area_type,
        action="HideoutUpgradeComplete",
        timestamp=utc_now(),
    )
    await handler.handle(action=action, context=action_context)

    assert (
        action_context.profile.health.energy.maximum
        == previous_max_energy + energy_increase
    )
