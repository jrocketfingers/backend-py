import pytest

from lib.tarkov.inventory import (
    ItemNotFoundError,
    ItemOutOfBoundsError,
)
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import MoveHandler
from lib.tarkov.items.actions import Location, MoveAction
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation
from lib.tarkov.utils import generate_mongodb_id


@pytest.fixture
def handler() -> MoveHandler:
    return MoveHandler()


async def test_item_not_found(
    handler: MoveHandler,
    action_context: ActionContext,
    inventory_root: Item,
) -> None:
    initial_changes_state = action_context.changes.model_copy(deep=True)

    id_ = generate_mongodb_id()
    result = await handler.handle(
        action=MoveAction(
            action="Move",
            item=id_,
            to=Location(
                id=inventory_root.id,
                container="hideout",
                location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
            ),
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=id_)
    assert action_context.changes == initial_changes_state


async def test_container_not_found(
    handler: MoveHandler,
    action_context: ActionContext,
    inventory_item: Item,
) -> None:
    initial_changes_state = action_context.changes.model_copy(deep=True)

    container_id = generate_mongodb_id()
    result = await handler.handle(
        action=MoveAction(
            action="Move",
            item=inventory_item.id,
            to=Location(
                id=container_id,
                container="hideout",
                location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
            ),
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=container_id)
    assert action_context.changes == initial_changes_state


async def test_move_error(
    handler: MoveHandler,
    action_context: ActionContext,
    inventory_item: Item,
    inventory_root: Item,
) -> None:
    result = await handler.handle(
        action=MoveAction(
            action="Move",
            item=inventory_item.id,
            to=Location(
                id=inventory_root.id,
                container="hideout",
                location=ItemLocation(x=11, y=0, rotation=ItemRotation.Horizontal),
            ),
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemOutOfBoundsError()


async def test_ok(
    handler: MoveHandler,
    inventory_root: Item,
    inventory_item: Item,
    action_context: ActionContext,
) -> None:
    loc = ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal)

    result = await handler.handle(
        action=MoveAction(
            action="Move",
            item=inventory_item.id,
            to=Location(
                id=inventory_root.id,
                container="hideout",
                location=loc,
            ),
        ),
        context=action_context,
    )
    assert result.unwrap() is None
    assert inventory_item not in action_context.changes.items.changed
    assert inventory_item.location == loc
    assert inventory_item.parent_id == inventory_root.id
    assert inventory_item.slot_id == "hideout"
