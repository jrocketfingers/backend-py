import uuid

import pytest
from aioinject import InjectionContext

from app.core.domain.flea_market.repository import FleaOffersRepository
from app.db.models import FleaOffer
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import FleaBuyHandler
from lib.tarkov.inventory_actions.errors import (
    FleaOfferNotFoundError,
    NoSpaceForItemError,
)
from lib.tarkov.items.actions import RagFairBuyOfferAction, RagFairOfferBuy
from lib.tarkov.items.models import Item
from lib.tarkov.items.props.container import StashProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.utils import generate_mongodb_id
from tests.lib.tarkov.conftest import PATRON_9x19_PST_GZH


@pytest.fixture
async def handler(inject_context: InjectionContext) -> FleaBuyHandler:
    return await inject_context.resolve(FleaBuyHandler)


async def test_offer_not_found(
    handler: FleaBuyHandler,
    action_context: ActionContext,
) -> None:
    offer_id = str(uuid.uuid4())
    action = RagFairBuyOfferAction(
        action="RagFairBuyOffer",
        offers=[
            RagFairOfferBuy(
                id=offer_id,
                count=1,
                items=[],
            ),
        ],
    )
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == FleaOfferNotFoundError(id=offer_id)


async def test_no_space_for_item(
    handler: FleaBuyHandler,
    action_context: ActionContext,
    flea_offer: FleaOffer,
    item_db: ItemDB,
) -> None:
    stash = action_context.inventory.get(action_context.profile.inventory.stash)
    assert stash
    stash_tpl = item_db.get(stash.template_id)
    assert isinstance(stash_tpl.props, StashProps)
    slot = stash_tpl.props.grids[0]
    for _ in range(slot.props.cells_width * slot.props.cells_height):
        inventory_item = Item(id=generate_mongodb_id(), template_id=PATRON_9x19_PST_GZH)
        location = action_context.inventory.find_location(
            item=inventory_item,
            children=[],
            parent=stash,
            slot_id="hideout",
        )
        assert location
        action_context.inventory.add(
            item=inventory_item,
            children=[],
            location=location,
        ).unwrap()

    action = RagFairBuyOfferAction(
        action="RagFairBuyOffer",
        offers=[
            RagFairOfferBuy(
                id=str(flea_offer.id),
                count=1,
                items=[],
            ),
        ],
    )
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NoSpaceForItemError()


@pytest.mark.parametrize(
    ("count", "count_to_buy"),
    [
        (100, 1),
        (1, 1),
    ],
)
async def test_ok(  # noqa: PLR0913
    handler: FleaBuyHandler,
    action_context: ActionContext,
    flea_offer_item: Item,
    flea_offer: FleaOffer,
    flea_offers_repository: FleaOffersRepository,
    count: int,
    count_to_buy: int,
) -> None:
    flea_offer.count = count

    action = RagFairBuyOfferAction(
        action="RagFairBuyOffer",
        offers=[
            RagFairOfferBuy(
                id=str(flea_offer.id),
                count=count_to_buy,
                items=[],
            ),
        ],
    )
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap() is None
    assert any(
        i.template_id == flea_offer_item.template_id
        for i in action_context.inventory.items.values()
    )

    assert (
        await flea_offers_repository.get(id=flea_offer.id) is None
        if count_to_buy >= count
        else flea_offer
    )
