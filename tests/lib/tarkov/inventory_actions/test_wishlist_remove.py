import pytest
from aioinject import InjectionContext

from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import RemoveFromWishlistHandler
from lib.tarkov.items.actions import RemoveFromWishlistAction
from lib.tarkov.types import ItemTemplateId


@pytest.fixture
async def handler(inject_context: InjectionContext) -> RemoveFromWishlistHandler:
    return await inject_context.resolve(RemoveFromWishlistHandler)


@pytest.mark.parametrize(
    ("state", "item", "expected"),
    [
        ([], "a", []),
        (["a"], "a", []),
        (["a"], "b", ["a"]),
    ],
)
async def test_ok(
    handler: RemoveFromWishlistHandler,
    action_context: ActionContext,
    state: list[ItemTemplateId],
    item: ItemTemplateId,
    expected: list[ItemTemplateId],
) -> None:
    action_context.profile.wish_list = set(state)

    result = await handler.handle(
        action=RemoveFromWishlistAction(
            action="RemoveFromWishList",
            template_id=item,
        ),
        context=action_context,
    )
    assert result.is_ok()
    assert action_context.profile.wish_list == set(expected)
