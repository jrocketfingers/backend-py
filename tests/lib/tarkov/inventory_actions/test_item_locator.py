import uuid

import pytest

from app.db.models import FleaOffer
from lib.db import DBContext
from lib.tarkov.inventory_actions.services import ItemLocator
from lib.tarkov.items.actions import ItemOwner
from lib.tarkov.items.models import Item
from lib.tarkov.types import ItemId, ItemTemplateId
from tests.factories import FleaOfferFactory


@pytest.fixture
def item() -> Item:
    return Item(
        id=ItemId(str(uuid.uuid4())),
        template_id=ItemTemplateId(str(uuid.uuid4())),
    )


@pytest.fixture
async def offer(item: Item, db_context: DBContext) -> FleaOffer:
    offer = FleaOfferFactory.build()
    offer.items = [item]
    offer.root_item_id = item.id
    db_context.add(offer)
    return offer


async def test_flea_offer_not_found(item_locator: ItemLocator) -> None:
    result = await item_locator.item_from_owner(
        id="",
        owner=ItemOwner(
            id=str(uuid.uuid4()),
            type="RagFair",
        ),
        require_real_item=True,
    )
    assert result is None


async def test_flea_offer_item_not_found(
    item_locator: ItemLocator,
    offer: FleaOffer,
) -> None:
    result = await item_locator.item_from_owner(
        id="Wrong Id",
        owner=ItemOwner(
            id=str(offer.id),
            type="RagFair",
        ),
        require_real_item=True,
    )
    assert result is None


async def test_flea_offer(
    item_locator: ItemLocator,
    offer: FleaOffer,
    item: Item,
) -> None:
    result = await item_locator.item_from_owner(
        id=item.id,
        owner=ItemOwner(
            id=str(offer.id),
            type="RagFair",
        ),
        require_real_item=True,
    )
    assert result is item
