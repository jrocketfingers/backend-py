import random
import uuid
from datetime import datetime

import pytest
from aioinject import InjectionContext

from lib.tarkov.inventory import Inventory, ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import EatHandler
from lib.tarkov.inventory_actions.errors import InvalidItemError
from lib.tarkov.items.actions import EatAction
from lib.tarkov.items.models import Item
from lib.tarkov.items.props.item import FoodDrinkProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.types import ItemId
from tests.lib.tarkov.conftest import WATER_ID


@pytest.fixture
async def handler(inject_context: InjectionContext) -> EatHandler:
    return await inject_context.resolve(EatHandler)


@pytest.fixture
def action(now: datetime) -> EatAction:
    return EatAction(
        action="Eat",
        item=ItemId("item_id"),
        count=1,
        time=now,
    )


@pytest.fixture
def food_item(inventory_item: Item) -> Item:
    inventory_item.template_id = WATER_ID
    return inventory_item


@pytest.fixture
def food_item_template_props(food_item: Item, item_db: ItemDB) -> FoodDrinkProps:
    tpl = item_db.get(food_item.template_id)
    assert isinstance(tpl.props, FoodDrinkProps)
    return tpl.props


async def test_item_not_found(
    handler: EatHandler,
    action_context: ActionContext,
    action: EatAction,
) -> None:
    action.item = ItemId(str(uuid.uuid4()))
    result = await handler.handle(action=action, context=action_context)
    assert result.unwrap_err() == ItemNotFoundError(id=action.item)


async def test_invalid_item(
    handler: EatHandler,
    action_context: ActionContext,
    action: EatAction,
    inventory_item: Item,
) -> None:
    action.item = inventory_item.id

    result = await handler.handle(action=action, context=action_context)
    err = result.unwrap_err()
    assert isinstance(err, InvalidItemError)
    assert err.id == inventory_item.id


async def test_ok(  # noqa: PLR0913
    handler: EatHandler,
    action_context: ActionContext,
    action: EatAction,
    inventory: Inventory,
    food_item: Item,
    food_item_template_props: FoodDrinkProps,
) -> None:
    consume_percent = random.randint(1, int(food_item_template_props.max_resource))
    action.item = food_item.id
    action.count = consume_percent

    result = await handler.handle(action=action, context=action_context)
    assert result.unwrap() is None

    assert food_item.id in inventory.items
    assert (
        food_item.upd["FoodDrink"]["HpPercent"]
        == food_item_template_props.max_resource - consume_percent
    )


async def test_should_delete_consumed_item(  # noqa: PLR0913
    handler: EatHandler,
    action_context: ActionContext,
    action: EatAction,
    inventory: Inventory,
    food_item: Item,
    food_item_template_props: FoodDrinkProps,
) -> None:
    action.item = food_item.id
    action.count = int(food_item_template_props.max_resource)

    result = await handler.handle(action=action, context=action_context)
    assert result.unwrap() is None

    assert food_item.id not in inventory.items
    assert food_item.upd["FoodDrink"]["HpPercent"] == 0
