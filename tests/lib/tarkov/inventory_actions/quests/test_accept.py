import random
import uuid
from datetime import datetime

import pytest

from app.core.domain.quests.services import QuestsService
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import QuestAcceptHandler
from lib.tarkov.items.actions import QuestAcceptAction
from lib.tarkov.profiles.models import Quest
from lib.tarkov.quests.errors import (
    QuestIsAlreadyAccepted,
    QuestIsNotAvailableError,
    QuestNotFoundError,
)
from lib.tarkov.quests.models import LevelCondition, QuestConditions, QuestStatus
from lib.tarkov.quests.storage import QuestStorage
from lib.tarkov.types import QuestId


@pytest.fixture
async def handler(
    quest_storage: QuestStorage,
    quest_service: QuestsService,
) -> QuestAcceptHandler:
    return QuestAcceptHandler(
        quest_storage=quest_storage,
        quest_service=quest_service,
    )


async def test_quest_not_found(
    handler: QuestAcceptHandler,
    action_context: ActionContext,
) -> None:
    quest_id = QuestId(str(uuid.uuid4()))
    result = await handler.handle(
        context=action_context,
        action=QuestAcceptAction(
            action="QuestAccept",
            quest_id=quest_id,
        ),
    )
    assert result.unwrap_err() == QuestNotFoundError(quest_id=quest_id)


async def test_quest_already_accepted(
    handler: QuestAcceptHandler,
    action_context: ActionContext,
    quest_storage: QuestStorage,
    now: datetime,
) -> None:
    quest = random.choice(list(quest_storage.quests.values()))

    action_context.profile.quests.append(
        Quest(id=quest.id, start_time=now, status=QuestStatus.Started),
    )
    result = await handler.handle(
        context=action_context,
        action=QuestAcceptAction(
            action="QuestAccept",
            quest_id=quest.id,
        ),
    )
    assert result.unwrap_err() == QuestIsAlreadyAccepted(quest_id=quest.id)


async def test_quest_is_not_available(
    handler: QuestAcceptHandler,
    action_context: ActionContext,
    quest_storage: QuestStorage,
) -> None:
    quest = random.choice(list(quest_storage.quests.values()))
    quest.conditions = QuestConditions(
        available_for_start=[
            LevelCondition(
                value=15,
                compare_method=">=",
                id=str(uuid.uuid4()),
                index=0,
                parent_id="",
                dynamic_locale=False,
                global_quest_counter_id="",
            ),
        ],
        available_for_finish=[],
        fail=[],
    )
    result = await handler.handle(
        context=action_context,
        action=QuestAcceptAction(
            action="QuestAccept",
            quest_id=quest.id,
        ),
    )
    assert result.unwrap_err() == QuestIsNotAvailableError(quest_id=quest.id)


async def test_ok(
    handler: QuestAcceptHandler,
    action_context: ActionContext,
    quest_service: QuestsService,
) -> None:
    quests = await quest_service.available_quests(profile=action_context.profile)
    quest = random.choice(quests)

    assert action_context.profile.quests == []
    result = await handler.handle(
        context=action_context,
        action=QuestAcceptAction(
            action="QuestAccept",
            quest_id=quest.id,
        ),
    )
    assert result.unwrap() is None
    profile_quest = action_context.profile.quests[0]
    assert profile_quest.id == quest.id
    assert profile_quest.status == QuestStatus.Started
