import random
import uuid
from collections.abc import Callable, Sequence

import pytest
from aioinject import InjectionContext

from app.core.domain.traders.repositories import (
    TraderAssortRepository,
)
from lib.tarkov.inventory import Inventory, ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import TradingConfirmHandler
from lib.tarkov.inventory_actions.errors import (
    NotEnoughItemsForTradeError,
    TraderNotFoundError,
    TradingRestrictionLimitError,
)
from lib.tarkov.items.actions import (
    RequiredItem,
    TradingConfirmAction,
)
from lib.tarkov.items.models import Item
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.trading.models import CurrencyIds, TraderEnum
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.types import ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id
from tests.utils import add_items_required_for_trade, find_barter

pytestmark = [
    pytest.mark.usefixtures("_seed_traders"),
]


@pytest.fixture
async def handler(inject_context: InjectionContext) -> TradingConfirmHandler:
    return await inject_context.resolve(TradingConfirmHandler)


@pytest.fixture
def trader(traders: Traders) -> TraderModel:
    trader = traders.get(TraderEnum.prapor)
    assert trader
    return trader


@pytest.fixture
def trader_inventory(traders: Traders, trader: TraderModel) -> Inventory:
    return traders.inventory(trader)


@pytest.fixture
def item_to_buy(
    action: TradingConfirmAction,
    trader: TraderModel,
    trader_inventory: Inventory,
) -> Item:
    item_id, _ = find_barter(
        trader.assort.barter_scheme,
        template_id=CurrencyIds.RUB.value,
    )
    item = trader_inventory.get(item_id)
    action.item_id = item_id
    assert item
    return item


@pytest.fixture
def items_required_for_trade(
    action: TradingConfirmAction,
    inventory: Inventory,
    inventory_root: Item,
    trader: TraderModel,
) -> Sequence[Item]:
    action.scheme_items, items = add_items_required_for_trade(
        inventory=inventory,
        inventory_root=inventory_root,
        scheme=trader.assort.barter_scheme[action.item_id][action.scheme_id],
    )
    return items


@pytest.fixture
def action(trader: TraderModel, trader_inventory: Inventory) -> TradingConfirmAction:
    item_id = random.choice(list(trader.assort.barter_scheme.keys()))
    item = trader_inventory.get(item_id)
    assert item
    return TradingConfirmAction(
        action="TradingConfirm",
        type="buy_from_trader",
        trader_id=trader.base.id,
        item_id=item_id,
        count=1,
        scheme_id=0,
        scheme_items=[],
    )


async def test_not_enough_items(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsForTradeError()


async def test_trader_not_found(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    trader_id = str(uuid.uuid4())
    action.trader_id = trader_id
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == TraderNotFoundError(id=trader_id)


async def test_scheme_item_not_found(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    action.scheme_items = [RequiredItem(id=generate_mongodb_id(), count=42)]
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsForTradeError()


async def test_item_not_found(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    item_id = generate_mongodb_id()
    action.item_id = item_id
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_not_enough_in_stack(  # noqa: PLR0913
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
    inventory: Inventory,
    inventory_root: Item,
    trader: TraderModel,
) -> None:
    scheme_items, items = add_items_required_for_trade(
        inventory=inventory,
        inventory_root=inventory_root,
        scheme=trader.assort.barter_scheme[action.item_id][action.scheme_id],
    )
    action.scheme_items = scheme_items
    for item in items:
        item.upd["StackObjectsCount"] -= 1

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsForTradeError()


@pytest.mark.parametrize(
    "item_id_predicate",
    [
        CurrencyIds.RUB.value,
        lambda id_: id_ not in CurrencyIds.__members__.values(),
    ],
)
async def test_ok(  # noqa: PLR0913
    handler: TradingConfirmHandler,
    inventory: Inventory,
    inventory_root: Item,
    action_context: ActionContext,
    action: TradingConfirmAction,
    trader_inventory: Inventory,
    trader: TraderModel,
    profile_context: ProfileContext,
    trader_assort_repository: TraderAssortRepository,
    item_id_predicate: ItemTemplateId | Callable[[ItemTemplateId], bool],
) -> None:
    item_id, _ = find_barter(
        barter_scheme=trader.assort.barter_scheme,
        template_id=item_id_predicate,
    )
    item = trader_inventory.get(item_id)
    assert item
    action.item_id = item.id

    trader_assort = await trader_assort_repository.get(
        trader_id=trader.base.id,
        item_id=item.id,
    )
    assert trader_assort
    previous_assort_count = trader_assort.count

    initial_item_count = len(inventory.items)
    assert initial_item_count == 1

    scheme_items, _ = add_items_required_for_trade(
        inventory=inventory,
        inventory_root=inventory_root,
        scheme=trader.assort.barter_scheme[action.item_id][action.scheme_id],
    )
    action.scheme_items = scheme_items

    scheme_item = scheme_items[0]
    scheme_item_in_inventory = inventory.get(scheme_item.id)
    assert scheme_item_in_inventory

    result = await handler.handle(
        action=action,
        context=action_context,
    )

    assert result.is_ok()
    assert len(inventory.items) == initial_item_count + 1 + len(
        list(trader_inventory.children(item)),
    )
    assert len(scheme_items) == 1
    expected_sales_sum = (
        scheme_item.count
        if scheme_item_in_inventory.template_id
        == CurrencyIds[trader.base.currency].value
        else 0
    )
    assert (
        profile_context.pmc.traders_info[trader.base.id].sales_sum == expected_sales_sum
    )
    assert trader_assort.count == previous_assort_count - action.count


@pytest.mark.usefixtures("item_to_buy")
async def test_partial_money_stack(  # noqa: PLR0913
    handler: TradingConfirmHandler,
    inventory: Inventory,
    inventory_root: Item,
    action_context: ActionContext,
    action: TradingConfirmAction,
    trader: TraderModel,
) -> None:
    scheme_items, money_items = add_items_required_for_trade(
        inventory=inventory,
        inventory_root=inventory_root,
        scheme=trader.assort.barter_scheme[action.item_id][action.scheme_id],
    )
    action.scheme_items = scheme_items
    assert len(money_items) == 1
    diff = random.randint(1, 1000)
    money = money_items[0]
    money.upd["StackObjectsCount"] += diff

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.is_ok()
    assert money.upd["StackObjectsCount"] == diff
    assert inventory.get(money.id) is not None


@pytest.mark.parametrize(
    ("max_count", "count", "is_ok"),
    [
        (10, 1, True),
        (10, 10, True),
        (10, 11, False),
        (0, 1, False),
    ],
)
async def test_restriction_limit(  # noqa: PLR0913
    *,
    handler: TradingConfirmHandler,
    action_context: ActionContext,
    action: TradingConfirmAction,
    item_to_buy: Item,
    items_required_for_trade: Sequence[Item],
    max_count: int,
    count: int,
    is_ok: bool,
) -> None:
    action.count = count
    item_to_buy.upd["BuyRestrictionMax"] = max_count

    for scheme_item in action.scheme_items:
        scheme_item.count *= count
    for item in items_required_for_trade:
        item.upd["StackObjectsCount"] *= count

    result = await handler.handle(
        action=action,
        context=action_context,
    )

    if not is_ok:
        assert result.unwrap_err() == TradingRestrictionLimitError(
            item_id=item_to_buy.id,
            current_count=0,
            max_count=max_count,
            count=count,
        )
    else:
        assert result.is_ok()
