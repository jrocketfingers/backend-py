import math
import random
import uuid

import pytest

from lib.tarkov.inventory import Inventory, ItemNotFoundError, Size
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import TraderSellItemHandler
from lib.tarkov.inventory_actions.errors import (
    NoSpaceForItemError,
    NotEnoughItemsForTradeError,
    TraderNotFoundError,
)
from lib.tarkov.items.actions import SellItem, TradingSellToTraderAction
from lib.tarkov.items.models import Item
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.items.upd import Upd
from lib.tarkov.trading.models import CurrencyIds, TraderEnum
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.utils import generate_mongodb_id


@pytest.fixture
def handler(
    item_db: ItemDB,
    traders: Traders,
) -> TraderSellItemHandler:
    return TraderSellItemHandler(
        item_db=item_db,
        traders=traders,
    )


@pytest.fixture
def item_to_sell(
    inventory: Inventory,
    inventory_root: Item,
    item_db: ItemDB,
) -> Item:
    item = Item(
        id=generate_mongodb_id(),
        template_id=item_db.get_by_name("patron_9x19_PST_gzh").id,
    )
    location = inventory.find_location(
        item,
        children=(),
        parent=inventory_root,
        slot_id="hideout",
    )
    assert location
    inventory.add(
        item=item,
        children=(),
        location=location,
    ).unwrap()
    return item


@pytest.fixture
def trader(traders: Traders) -> TraderModel:
    trader = traders.get(TraderEnum.prapor)
    assert trader
    return trader


@pytest.fixture
def action(trader: TraderModel) -> TradingSellToTraderAction:
    return TradingSellToTraderAction(
        action="TradingConfirm",
        type="sell_to_trader",
        trader_id=trader.base.id,
        items=[],
        price=0,
    )


async def test_ok(  # noqa: PLR0913
    handler: TraderSellItemHandler,
    action_context: ActionContext,
    inventory: Inventory,
    item_to_sell: Item,
    action: TradingSellToTraderAction,
    trader: TraderModel,
    item_db: ItemDB,
) -> None:
    money_tpl = item_db.get(CurrencyIds[trader.base.currency].value)

    price = random.randint(
        money_tpl.props.stack_max_size,
        money_tpl.props.stack_max_size * 10,
    )
    action.price = price
    action.items = [
        SellItem(
            id=item_to_sell.id,
            scheme_id=0,
            count=item_to_sell.upd.get("StackObjectsCount", 1),
        ),
    ]

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.is_ok()
    assert inventory.get(item_to_sell.id) is None

    money_items = [
        item for item in inventory.items.values() if item.template_id == money_tpl.id
    ]
    assert sum(item.upd["StackObjectsCount"] for item in money_items) == price
    assert len(money_items) == math.ceil(price / money_tpl.props.stack_max_size)


async def test_adds_money_to_existing_stacks(  # noqa: PLR0913
    handler: TraderSellItemHandler,
    action_context: ActionContext,
    inventory: Inventory,
    item_to_sell: Item,
    action: TradingSellToTraderAction,
    trader: TraderModel,
    item_db: ItemDB,
    inventory_root: Item,
) -> None:
    money_tpl = item_db.get(CurrencyIds[trader.base.currency].value)
    price = money_tpl.props.stack_max_size * 2
    action.price = price
    action.items = [
        SellItem(
            id=item_to_sell.id,
            scheme_id=0,
            count=item_to_sell.upd.get("StackObjectsCount", 1),
        ),
    ]

    existing_money = []
    for count in (1, money_tpl.props.stack_max_size):
        money = Item(
            id=generate_mongodb_id(),
            template_id=money_tpl.id,
            upd=Upd(StackObjectsCount=count),
        )
        existing_money.append(money)
        location = inventory.find_location(
            item=money,
            children=(),
            parent=inventory_root,
            slot_id="hideout",
        )
        assert location
        inventory.add(
            item=money,
            children=(),
            location=location,
        ).unwrap()

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.is_ok()

    assert all(
        money.upd["StackObjectsCount"] == money_tpl.props.stack_max_size
        for money in existing_money
    )
    assert (
        sum(
            item.upd["StackObjectsCount"]
            for item in inventory.items.values()
            if item.template_id == money_tpl.id
        )
        == price + 1 + money_tpl.props.stack_max_size
    )


async def test_trader_not_found(
    handler: TraderSellItemHandler,
    action_context: ActionContext,
    action: TradingSellToTraderAction,
) -> None:
    action.trader_id = str(uuid.uuid4())

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == TraderNotFoundError(id=action.trader_id)


async def test_item_not_found(
    handler: TraderSellItemHandler,
    action_context: ActionContext,
    action: TradingSellToTraderAction,
) -> None:
    item_id = generate_mongodb_id()
    action.items = [SellItem(id=item_id, count=1, scheme_id=0)]

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_count_too_big(
    handler: TraderSellItemHandler,
    action_context: ActionContext,
    action: TradingSellToTraderAction,
    item_to_sell: Item,
) -> None:
    action.items = [
        SellItem(
            id=item_to_sell.id,
            count=item_to_sell.upd.get("StackObjectsCount", 1) + 1,
            scheme_id=0,
        ),
    ]

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsForTradeError()


async def test_no_stash_space(
    handler: TraderSellItemHandler,
    action_context: ActionContext,
    action: TradingSellToTraderAction,
    item_to_sell: Item,
    inventory_size: Size,
) -> None:
    action.price = inventory_size.width * inventory_size.height * 5_000_000
    action.items = [
        SellItem(
            id=item_to_sell.id,
            count=1,
            scheme_id=0,
        ),
    ]
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NoSpaceForItemError()
