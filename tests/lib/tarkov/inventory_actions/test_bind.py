import uuid

import pytest

from lib.tarkov.inventory import ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import BindHandler
from lib.tarkov.items.actions import BindAction
from lib.tarkov.items.models import Item
from lib.tarkov.utils import generate_mongodb_id


@pytest.fixture
def handler() -> BindHandler:
    return BindHandler()


async def test_item_not_found(
    handler: BindHandler,
    action_context: ActionContext,
) -> None:
    item_id = generate_mongodb_id()
    result = await handler.handle(
        action=BindAction(
            action="Bind",
            item=item_id,
            index="index",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_ok(
    handler: BindHandler,
    action_context: ActionContext,
    inventory_item: Item,
) -> None:
    index = str(uuid.uuid4())
    result = await handler.handle(
        action=BindAction(
            action="Bind",
            item=inventory_item.id,
            index=index,
        ),
        context=action_context,
    )
    assert result.unwrap() is None
    assert action_context.profile.inventory.fast_panel == {
        index: inventory_item.id,
    }


async def test_should_remove_duplicate(
    handler: BindHandler,
    action_context: ActionContext,
    inventory_item: Item,
) -> None:
    action_context.profile.inventory.fast_panel["existing"] = inventory_item.id

    index = str(uuid.uuid4())
    result = await handler.handle(
        action=BindAction(
            action="Bind",
            item=inventory_item.id,
            index=index,
        ),
        context=action_context,
    )
    assert result.unwrap() is None
    assert action_context.profile.inventory.fast_panel == {
        index: inventory_item.id,
    }
