import uuid

import pytest
from aioinject import InjectionContext

from app.core.domain.accounts.insurance.services import InsuranceService
from lib.tarkov.inventory import ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import InsureHandler
from lib.tarkov.inventory_actions.errors import NotEnoughItemsError, TraderNotFoundError
from lib.tarkov.items.actions import InsureAction
from lib.tarkov.items.models import Item
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.models import InsuredItem
from lib.tarkov.trading.models import CurrencyIds, TraderEnum
from lib.tarkov.trading.trader import Traders
from lib.tarkov.types import ItemId
from tests.lib.tarkov.conftest import M4A1_ID
from tests.utils import add_items


@pytest.fixture
async def handler(inject_context: InjectionContext) -> InsureHandler:
    return await inject_context.resolve(InsureHandler)


async def test_trader_not_found(
    handler: InsureHandler,
    action_context: ActionContext,
    inventory_item: Item,
) -> None:
    trader_id = str(uuid.uuid4())
    result = await handler.handle(
        action=InsureAction(
            items=[inventory_item.id],
            trader_id=trader_id,
            action="Insure",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == TraderNotFoundError(id=trader_id)


async def test_item_not_found(
    handler: InsureHandler,
    action_context: ActionContext,
) -> None:
    item_id = ItemId(str(uuid.uuid4()))
    result = await handler.handle(
        action=InsureAction(
            items=[item_id],
            trader_id=TraderEnum.prapor.value,
            action="Insure",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_not_enough_money(
    handler: InsureHandler,
    action_context: ActionContext,
    inventory_item: Item,
) -> None:
    inventory_item.template_id = M4A1_ID
    result = await handler.handle(
        action=InsureAction(
            items=[inventory_item.id],
            trader_id=TraderEnum.prapor.value,
            action="Insure",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsError()


async def test_ok(  # noqa: PLR0913
    handler: InsureHandler,
    action_context: ActionContext,
    inventory_item: Item,
    insurance_service: InsuranceService,
    traders: Traders,
    inventory_root: Item,
    item_db: ItemDB,
) -> None:
    trader = traders.get(TraderEnum.prapor.value)
    if trader is None:
        raise ValueError

    inventory_item.template_id = M4A1_ID
    prices = insurance_service.get_insurance_cost(
        items=[inventory_item],
        trader=trader,
        profile=action_context.profile,
    )
    total_cost = sum(prices.values())
    money_items = add_items(
        inventory=action_context.inventory,
        parent=inventory_root,
        item_template=item_db.get(CurrencyIds.RUB.value),
        count=total_cost,
    )

    result = await handler.handle(
        action=InsureAction(
            items=[inventory_item.id],
            trader_id=trader.base.id,
            action="Insure",
        ),
        context=action_context,
    )
    assert result.unwrap() is None
    assert action_context.profile.insured_items == [
        InsuredItem(
            item_id=inventory_item.id,
            trader_id=trader.base.id,
        ),
    ]
    for item in money_items:
        assert item.id not in action_context.inventory.items
        assert item in action_context.changes.items.deleted
