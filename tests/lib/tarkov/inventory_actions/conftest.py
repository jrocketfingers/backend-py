import uuid
from datetime import datetime

import pytest

from app.db.models import AccountProfile
from lib.tarkov.inventory import (
    Inventory,
)
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.items.actions import Location, ProfileChanges
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation, ItemTemplate
from lib.tarkov.items.props.base import StackableItemProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.profiles.models import (
    BodyPart,
    BodyParts,
    Health,
    ProfileHealth,
    Skills,
)
from lib.tarkov.utils import generate_mongodb_id
from tests.lib.tarkov.conftest import PATRON_9x19_PST_GZH


@pytest.fixture
async def changes(now: datetime) -> ProfileChanges:
    return ProfileChanges(
        id=str(uuid.uuid4()),
        health=ProfileHealth(
            hydration=Health(current=100, maximum=100),
            energy=Health(current=100, maximum=100),
            temperature=Health(current=100, maximum=100),
            update_time=now,
            body_parts=BodyParts(
                head=BodyPart(health=Health(current=100, maximum=100)),
                chest=BodyPart(health=Health(current=100, maximum=100)),
                stomach=BodyPart(health=Health(current=100, maximum=100)),
                left_arm=BodyPart(health=Health(current=100, maximum=100)),
                right_arm=BodyPart(health=Health(current=100, maximum=100)),
                left_leg=BodyPart(health=Health(current=100, maximum=100)),
                right_leg=BodyPart(health=Health(current=100, maximum=100)),
            ),
        ),
        skills=Skills(common=[], mastering=[], points=0),
    )


@pytest.fixture
def action_context(
    account_profile: AccountProfile,
    profile_context: ProfileContext,
    changes: ProfileChanges,
    profile_inventory: Inventory,
) -> ActionContext:
    return ActionContext(
        account_profile=account_profile,
        profile=profile_context.pmc,
        inventory=profile_inventory,
        changes=changes,
    )


@pytest.fixture
def stackable_template(item_db: ItemDB) -> ItemTemplate:
    return item_db.get_by_name("patron_9x19_PST_gzh")


@pytest.fixture
def stackable_max_stack_size(stackable_template: ItemTemplate) -> int:
    assert isinstance(stackable_template.props, StackableItemProps)
    return stackable_template.props.stack_max_size


@pytest.fixture
def stackable_source(
    stackable_template: ItemTemplate,
    inventory: Inventory,
    inventory_root: Item,
) -> Item:
    item = Item(
        id=generate_mongodb_id(),
        template_id=stackable_template.id,
    )
    inventory.add(
        item,
        children=(),
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
        ),
    )
    return item


@pytest.fixture
def stackable_target(
    stackable_template: ItemTemplate,
    inventory: Inventory,
    inventory_root: Item,
) -> Item:
    item = Item(
        id=generate_mongodb_id(),
        template_id=stackable_template.id,
    )
    inventory.add(
        item,
        children=(),
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=0, y=1, rotation=ItemRotation.Horizontal),
        ),
    )
    return item


@pytest.fixture
def inventory_item(
    inventory: Inventory,
    inventory_root: Item,
) -> Item:
    item = Item(id=generate_mongodb_id(), template_id=PATRON_9x19_PST_GZH)
    inventory.add(
        item=item,
        children=(),
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
        ),
    ).unwrap()
    return item
