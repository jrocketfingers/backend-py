from lib.tarkov.inventory import Inventory, ItemNotFoundError
from lib.tarkov.items.models import Item
from lib.tarkov.types import ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id


def test_not_found(inventory: Inventory) -> None:
    item = Item(id=generate_mongodb_id(), template_id=ItemTemplateId(""))
    result = inventory.remove(item)
    assert result.unwrap_err() == ItemNotFoundError(id=item.id)
