from lib.tarkov.inventory import Inventory
from lib.tarkov.items.actions import Location
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation
from lib.tarkov.items.props.base import CompoundItemProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.utils import generate_mongodb_id


async def test_empty(
    inventory: Inventory,
    inventory_root: Item,
    item_db: ItemDB,
) -> None:
    item = Item(
        id=generate_mongodb_id(),
        template_id=item_db.get_by_name("patron_9x19_PST_gzh").id,
    )
    result = inventory.find_location_in_container(
        item=item,
        children=(),
        parent=inventory_root,
        slot_id="hideout",
    )
    assert result == ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal)


async def test_no_space(
    inventory: Inventory,
    inventory_root: Item,
    item_db: ItemDB,
) -> None:
    root_tpl = item_db.get(inventory_root.template_id)
    assert isinstance(root_tpl.props, CompoundItemProps)
    grid = next(grid for grid in root_tpl.props.grids if grid.name == "hideout")

    item_template = item_db.get_by_name("patron_9x19_PST_gzh")
    for _ in range(grid.props.cells_height * grid.props.cells_width):
        item = Item(
            id=generate_mongodb_id(),
            template_id=item_template.id,
        )
        location = inventory.find_location_in_container(
            item=item,
            children=(),
            parent=inventory_root,
            slot_id="hideout",
        )
        assert location
        result = inventory.add(
            item=item,
            children=(),
            location=Location(
                id=inventory_root.id,
                container="hideout",
                location=location,
            ),
        )
        assert result.is_ok()

    item = Item(
        id=generate_mongodb_id(),
        template_id=item_template.id,
    )
    location = inventory.find_location_in_container(
        item=item,
        children=(),
        parent=inventory_root,
        slot_id="hideout",
    )
    assert location is None
