import random

import pytest

from lib.tarkov.inventory import ContainerMap, SlotAlreadyTakenError
from lib.tarkov.items.models import Item
from lib.tarkov.types import ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id
from tests.lib.tarkov.conftest import PATRON_9x19_PST_GZH


async def test_slot_taken(container_map: ContainerMap) -> None:
    weapon = Item(
        id=generate_mongodb_id(),
        template_id=ItemTemplateId("5cadc190ae921500103bb3b6"),
    )
    ammo = Item(
        id=generate_mongodb_id(),
        template_id=PATRON_9x19_PST_GZH,
        parent_id=weapon.id,
        slot_id="patron_in_weapon",
    )
    container_map.add(weapon).unwrap()
    container_map.add(ammo).unwrap()

    result = container_map.add(ammo).unwrap_err()
    assert result == SlotAlreadyTakenError(slot="patron_in_weapon", state=True)


async def test_slot_already_empty(container_map: ContainerMap) -> None:
    weapon = Item(
        id=generate_mongodb_id(),
        template_id=ItemTemplateId("5cadc190ae921500103bb3b6"),
    )

    container_map.add(weapon).unwrap()
    ammo = Item(
        id=generate_mongodb_id(),
        template_id=PATRON_9x19_PST_GZH,
        parent_id=weapon.id,
        slot_id="patron_in_weapon",
    )
    result = container_map.remove(ammo, children=()).unwrap_err()
    assert result == SlotAlreadyTakenError(slot="patron_in_weapon", state=False)


async def test_add_cartridge(container_map: ContainerMap) -> None:
    mag = Item(
        id=generate_mongodb_id(),
        template_id=ItemTemplateId("5cadc2e0ae9215051e1c21e7"),
    )
    container_map.add(mag).unwrap()
    for i in range(10):
        ammo = Item(
            id=generate_mongodb_id(),
            template_id=PATRON_9x19_PST_GZH,
            parent_id=mag.id,
            slot_id="cartridges",
            location=i,
        )
        container_map.add(ammo).unwrap()

    location = random.randint(0, 10 - 1)
    item = Item(
        id=generate_mongodb_id(),
        template_id=PATRON_9x19_PST_GZH,
        parent_id=mag.id,
        slot_id="cartridges",
        location=location,
    )
    result = container_map.add(item=item).unwrap_err()
    assert result == SlotAlreadyTakenError(slot=("cartridges", location), state=True)


async def test_no_slot_id_or_parent(container_map: ContainerMap) -> None:
    parent_id = generate_mongodb_id()
    item = Item(
        id=generate_mongodb_id(),
        template_id=ItemTemplateId("5cadc2e0ae9215051e1c21e7"),
        parent_id=parent_id,
    )
    msg = r"^Parent id is not added to map or item.slot_id is None$"
    # Parent id not found
    with pytest.raises(ValueError, match=msg):
        container_map.add(item)

    container_map.add(Item(id=parent_id, template_id=item.template_id))
    item.slot_id = None
    # Slot id is None
    with pytest.raises(ValueError, match=msg):
        container_map.add(item)
