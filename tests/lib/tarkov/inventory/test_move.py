import itertools
import random

import pytest

from lib.tarkov.inventory import (
    Inventory,
    ItemNotFoundError,
    ItemOutOfBoundsError,
    NotAContainerError,
    SlotAlreadyTakenError,
)
from lib.tarkov.items.actions import Location
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation
from lib.tarkov.utils import generate_mongodb_id
from tests.lib.tarkov.conftest import PATRON_9x19_PST_GZH


@pytest.fixture
def move_item(inventory: Inventory, inventory_root: Item) -> Item:
    item = Item(
        id=generate_mongodb_id(),
        template_id=PATRON_9x19_PST_GZH,
    )
    inventory.add(
        item=item,
        children=(),
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
        ),
    )

    return item


def test_item_not_found(inventory: Inventory, inventory_root: Item) -> None:
    item = Item(
        id=generate_mongodb_id(),
        template_id=PATRON_9x19_PST_GZH,
    )
    result = inventory.move(
        item,
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
        ),
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item.id)


def test_no_such_location(
    inventory: Inventory,
    move_item: Item,
) -> None:
    location_id = generate_mongodb_id()
    result = inventory.move(
        item=move_item,
        location=Location(
            id=location_id,
            container="main",
        ),
    )
    assert result.unwrap_err() == ItemNotFoundError(id=location_id)


def test_taken_slot(
    inventory: Inventory,
    inventory_root: Item,
    move_item: Item,
) -> None:
    location = Location(
        id=inventory_root.id,
        container="hideout",
        location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
    )
    inventory.move(
        item=move_item,
        location=location,
    ).unwrap()

    item = Item(id=generate_mongodb_id(), template_id=PATRON_9x19_PST_GZH)
    inventory.add(
        item=item,
        children=(),
        location=location.model_copy(
            update={
                "location": ItemLocation(x=0, y=10, rotation=ItemRotation.Horizontal),
            },
        ),
    ).unwrap()

    result = inventory.move(item=item, location=location)
    assert result.unwrap_err() == SlotAlreadyTakenError(slot=(0, 0), state=True)


def test_parent_is_not_a_container(
    inventory: Inventory,
    move_item: Item,
    inventory_root: Item,
) -> None:
    inventory_root.template_id = PATRON_9x19_PST_GZH
    result = inventory.move(
        item=move_item,
        location=Location(
            id=inventory_root.id,
            container="main",
        ),
    )
    assert result.unwrap_err() == NotAContainerError()


@pytest.mark.parametrize(
    ("x", "y"),
    [
        (11, 0),
        (0, 81),
    ],
)
def test_out_of_bounds(
    inventory: Inventory,
    move_item: Item,
    inventory_root: Item,
    x: int,
    y: int,
) -> None:
    result = inventory.move(
        item=move_item,
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=x, y=y, rotation=ItemRotation.Horizontal),
        ),
    )
    assert result.unwrap_err() == ItemOutOfBoundsError()


def test_roundtrip(
    inventory: Inventory,
    inventory_root: Item,
    move_item: Item,
) -> None:
    initial_state = move_item.model_copy(deep=True)

    for item_location in itertools.chain.from_iterable(
        (
            ItemLocation(x=1, y=0, rotation=ItemRotation.Horizontal),
            ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
        )
        for _ in range(5)
    ):
        result = inventory.move(
            item=move_item,
            location=Location(
                id=inventory_root.id,
                container="hideout",
                location=item_location,
            ),
        )

    assert result.unwrap() == initial_state


def test_ok(
    inventory: Inventory,
    inventory_root: Item,
    move_item: Item,
) -> None:
    location = Location(
        id=inventory_root.id,
        container="hideout",
        location=ItemLocation(
            x=random.randint(1, 9),
            y=random.randint(1, 10),
            rotation=ItemRotation.Horizontal,
        ),
    )
    result = inventory.move(
        item=move_item,
        location=location,
    )
    assert result.unwrap() is move_item
    assert move_item.location == location.location
    assert move_item.parent_id == location.id == inventory_root.id
    assert move_item.slot_id == location.container
