from pathlib import Path

from lib.tarkov.db import AssetsPath, create_resource_db


async def test_create_resource_db(assets_path: Path) -> None:
    result = await create_resource_db(resources_path=AssetsPath(assets_path))
    assert result.globals_
