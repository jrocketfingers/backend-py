import asyncio
import uuid

from lib.tarkov.notifications.backend import InMemoryBackend


async def test_send_recv(in_memory_backend: InMemoryBackend) -> None:
    channel_id = str(uuid.uuid4())
    received = []
    expected = ["{i}" for i in range(100)]
    async with asyncio.TaskGroup() as tg:

        async def recv() -> None:
            async with in_memory_backend.subscribe(channel_id) as channel:
                async for message in channel:
                    received.append(message.decode())
                    if len(received) == len(expected):
                        return

        async def send() -> None:
            for notification in expected:
                await in_memory_backend.send(
                    channel_id=channel_id,
                    message=notification,
                )

        tg.create_task(recv())
        tg.create_task(send())

    assert received == expected
