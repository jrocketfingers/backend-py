import uuid

from lib.tarkov.notifications.models import (
    FleaOfferSoldNotification,
    Notification,
    NotificationType,
)
from lib.tarkov.notifications.service import NotificationService
from tests.plugins.notifications import CaptureNotifications


async def test_send_recv(notification_service: NotificationService) -> None:
    profile_id = str(uuid.uuid4())
    expected = [
        Notification(type=NotificationType.PING),
        FleaOfferSoldNotification(
            type=NotificationType.RAGFAIR_OFFER_SOLD,
            offer_id="Offer id",
            count=42,
            handbook_id="str",
        ),
    ]
    async with CaptureNotifications(notification_service).capture(
        profile_id=profile_id,
    ) as received:
        for notification in expected:
            await notification_service.send(
                profile_id=profile_id,
                notification=notification,
            )

    assert received == expected
