import pytest

from lib.tarkov.inventory import Inventory, Size
from lib.tarkov.items.models import Item
from lib.tarkov.items.props.container import StashProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.types import ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id

STASH_28x10 = ItemTemplateId("566abbc34bdc2d92178b4576")
PATRON_9x19_PST_GZH = ItemTemplateId("56d59d3ad2720bdb418b4577")
WATER_ID = ItemTemplateId("5448fee04bdc2dbc018b4567")
M4A1_ID = ItemTemplateId("5447a9cd4bdc2dbd208b4567")


@pytest.fixture
def inventory_root() -> Item:
    return Item(
        id=generate_mongodb_id(),
        template_id=STASH_28x10,
    )


@pytest.fixture
def inventory_size(inventory_root: Item, item_db: ItemDB) -> Size:
    tpl = item_db.get(inventory_root.template_id)
    assert isinstance(tpl.props, StashProps)
    grid = next(g for g in tpl.props.grids if g.name == "hideout")
    return Size(width=grid.props.cells_width, height=grid.props.cells_height)


@pytest.fixture
def inventory(inventory_root: Item, item_db: ItemDB) -> Inventory:
    return Inventory.from_sequence(
        items=[inventory_root],
        templates=item_db.items,
    )


@pytest.fixture
def profile_inventory(
    inventory: Inventory,
    inventory_root: Item,
    profile_context: ProfileContext,
) -> Inventory:
    profile_context.pmc.inventory.stash = inventory_root.id
    return inventory
