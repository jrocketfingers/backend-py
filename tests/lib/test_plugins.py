import aioinject

from lib.plugins.registry import PluginRegistry
from lib.tarkov.profiles.dto import ProfileContext


async def test_profile_plugin() -> None:
    call_count = 0

    class TestPlugin:
        async def on_profile_load(
            self,
            profile_context: ProfileContext,  # noqa: ARG002
        ) -> None:
            nonlocal call_count
            call_count += 1

        async def on_profile_create(
            self,
            profile_context: ProfileContext,  # noqa: ARG002
        ) -> None:
            nonlocal call_count
            call_count += 1

    container = aioinject.Container()
    registry = PluginRegistry(container=container)
    registry.register(TestPlugin)

    async with container.context():
        assert call_count == 0
        await registry.profile.on_profile_create(context=None)  # type: ignore[arg-type]
        assert call_count == 1
        await registry.profile.on_profile_load(context=None)  # type: ignore[arg-type]
        assert call_count == 2  # noqa: PLR2004
