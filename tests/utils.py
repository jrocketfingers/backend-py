import random
from collections.abc import Callable, Sequence

from lib.tarkov.inventory import Inventory
from lib.tarkov.inventory_actions.utils import item_stack_sizes
from lib.tarkov.items.actions import RequiredItem
from lib.tarkov.items.models import Item, ItemTemplate
from lib.tarkov.items.upd import Upd
from lib.tarkov.trading.models import BarterScheme, CurrencyIds, TraderBarter
from lib.tarkov.types import ItemId, ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id


def add_items(
    *,
    inventory: Inventory,
    parent: Item,
    slot_id: str = "hideout",
    item_template: ItemTemplate,
    count: int,
) -> list[Item]:
    result = []
    for stack_count in item_stack_sizes(item_template=item_template, total=count):
        item = Item(
            id=generate_mongodb_id(),
            template_id=item_template.id,
            upd=Upd(StackObjectsCount=stack_count),
        )
        result.append(item)
        location = inventory.find_location(
            item=item,
            children=(),
            parent=parent,
            slot_id=slot_id,
        )
        if location is None:
            raise ValueError
        inventory.add(
            item=item,
            children=(),
            location=location,
        ).unwrap()
    return result


def add_items_required_for_trade(
    inventory: Inventory,
    inventory_root: Item,
    scheme: Sequence[TraderBarter],
    slot_id: str = "hideout",
) -> tuple[Sequence[RequiredItem], Sequence[Item]]:
    scheme_items = []
    items = []

    for scheme_item in scheme:
        money = Item(
            id=generate_mongodb_id(),
            template_id=scheme_item.template_id,
            upd=Upd(StackObjectsCount=scheme_item.count),
        )
        scheme_items.append(RequiredItem(id=money.id, count=scheme_item.count))
        items.append(money)
        location = inventory.find_location(
            item=money,
            children=(),
            parent=inventory_root,
            slot_id=slot_id,
        )
        assert location
        inventory.add(
            item=money,
            children=(),
            location=location,
        )

    return scheme_items, items


def find_barter(
    barter_scheme: BarterScheme,
    template_id: ItemTemplateId | Callable[[ItemTemplateId], bool],
) -> tuple[ItemId, TraderBarter]:
    template_id_original = template_id
    if isinstance(template_id, str):

        def template_id(id_: ItemTemplateId) -> bool:
            return id_ == template_id_original

    for item_id, barters in barter_scheme.items():
        for barter in barters:
            if len(barter) == 1 and all(
                template_id(item.template_id) for item in barter
            ):
                return item_id, barter[0]
    raise ValueError


def random_currency_id() -> ItemTemplateId:
    return random.choice(list(CurrencyIds.__members__.values())).value
