from typing import Any

import typeguard
from _pytest.python import Function
from _pytest.runner import CallInfo


def pytest_runtest_makereport(item: Function, call: CallInfo[Any]) -> None:
    if call.when == "call":
        for fixture_name, ty in item.obj.__annotations__.items():
            if fixture_name not in item.funcargs:
                continue
            try:
                typeguard.check_type(item.funcargs[fixture_name], ty)
            except typeguard.TypeCheckError as e:
                e.add_note(f"{fixture_name}: {ty}")
                raise
