import pytest
from aioinject import InjectionContext

from lib.tarkov.config import BotConfig


@pytest.fixture
async def bot_config(inject_context: InjectionContext) -> BotConfig:
    return await inject_context.resolve(BotConfig)
