import pytest
from aioinject import InjectionContext
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.di._modules.locales import create_locale_service
from app.core.domain.accounts.insurance.services import InsuranceService
from app.core.domain.aki_compat.services import RaidService
from app.core.domain.flea_market.repository import FleaOffersRepository
from app.core.domain.flea_market.services import FleaMarketService
from app.core.domain.friends.repository import FriendRepository, FriendRequestRepository
from app.core.domain.hideout.services import HideoutService
from app.core.domain.items.services import CustomizationDB, HandbookDB
from app.core.domain.locales.services import LocaleService
from app.core.domain.quests.services import QuestsService
from app.core.domain.traders.repositories import (
    TraderAssortRepository,
    TraderRepository,
)
from app.core.domain.traders.tasks import UpdateTraderAssortTask
from app.core.domain.weather.services import WeatherService
from lib.db import DBContext
from lib.tarkov.achievements.db import AchievementDB
from lib.tarkov.bots.db import BotDB
from lib.tarkov.config import QuestConfig
from lib.tarkov.db import AssetsPath
from lib.tarkov.inventory_actions.services import ItemLocator
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.locations.db import LocationDB
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.quests.storage import QuestStorage
from lib.tarkov.trading.trader import Traders


@pytest.fixture
async def db_context(inject_context: InjectionContext) -> DBContext:
    return await inject_context.resolve(DBContext)  # type: ignore[type-abstract]


@pytest.fixture
async def locale_service(assets_path: AssetsPath) -> LocaleService:
    return await create_locale_service(assets_path)


@pytest.fixture
async def traders(inject_context: InjectionContext) -> Traders:
    return await inject_context.resolve(Traders)


@pytest.fixture
async def achievement_db(inject_context: InjectionContext) -> AchievementDB:
    return await inject_context.resolve(AchievementDB)


@pytest.fixture
async def customization_db(inject_context: InjectionContext) -> CustomizationDB:
    return await inject_context.resolve(CustomizationDB)


@pytest.fixture
async def profile_storage(inject_context: InjectionContext) -> ProfileStorage:
    return await inject_context.resolve(
        ProfileStorage,  # type: ignore[type-abstract]
    )


@pytest.fixture
async def weather_service(inject_context: InjectionContext) -> WeatherService:
    return await inject_context.resolve(WeatherService)


@pytest.fixture
async def location_db(inject_context: InjectionContext) -> LocationDB:
    return await inject_context.resolve(LocationDB)


@pytest.fixture
async def quest_storage(inject_context: InjectionContext) -> QuestStorage:
    return await inject_context.resolve(QuestStorage)


@pytest.fixture
async def quest_config(inject_context: InjectionContext) -> QuestConfig:
    return await inject_context.resolve(QuestConfig)


@pytest.fixture
async def quest_service(inject_context: InjectionContext) -> QuestsService:
    return await inject_context.resolve(QuestsService)


@pytest.fixture
async def handbook_db(inject_context: InjectionContext) -> HandbookDB:
    return await inject_context.resolve(HandbookDB)


@pytest.fixture
async def update_traders_assort_task(
    inject_context: InjectionContext,
) -> UpdateTraderAssortTask:
    return await inject_context.resolve(UpdateTraderAssortTask)


@pytest.fixture
async def item_db(inject_context: InjectionContext) -> ItemDB:
    return await inject_context.resolve(ItemDB)


@pytest.fixture
async def item_locator(inject_context: InjectionContext) -> ItemLocator:
    return await inject_context.resolve(ItemLocator)


@pytest.fixture
async def trader_repository(session: AsyncSession) -> TraderRepository:
    return TraderRepository(session=session)


@pytest.fixture
async def trader_assort_repository(session: AsyncSession) -> TraderAssortRepository:
    return TraderAssortRepository(session=session)


@pytest.fixture
async def hideout_service(inject_context: InjectionContext) -> HideoutService:
    return await inject_context.resolve(HideoutService)


@pytest.fixture
async def flea_offers_repository(
    inject_context: InjectionContext,
) -> FleaOffersRepository:
    return await inject_context.resolve(FleaOffersRepository)


@pytest.fixture
async def flea_service(
    inject_context: InjectionContext,
) -> FleaMarketService:
    return await inject_context.resolve(FleaMarketService)


@pytest.fixture
async def friend_request_repository(
    inject_context: InjectionContext,
) -> FriendRequestRepository:
    return await inject_context.resolve(FriendRequestRepository)


@pytest.fixture
async def friend_repository(
    inject_context: InjectionContext,
) -> FriendRepository:
    return await inject_context.resolve(FriendRepository)


@pytest.fixture
async def bot_db(
    inject_context: InjectionContext,
) -> BotDB:
    return await inject_context.resolve(BotDB)


@pytest.fixture
async def insurance_service(inject_context: InjectionContext) -> InsuranceService:
    return await inject_context.resolve(InsuranceService)


@pytest.fixture
async def raid_service(inject_context: InjectionContext) -> RaidService:
    return await inject_context.resolve(RaidService)
