from datetime import datetime

import pytest

from app.core.domain.traders.tasks import UpdateTraderAssortTask


@pytest.fixture
async def _seed_traders(
    now: datetime,
    update_traders_assort_task: UpdateTraderAssortTask,
) -> None:
    await update_traders_assort_task.execute(
        now=now,
        update_flea=False,
    )
