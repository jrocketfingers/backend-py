import asyncio
import contextlib
from collections.abc import AsyncIterator

import aioinject
import pytest
from aioinject import Object

from lib.tarkov.config import NotificationsConfig
from lib.tarkov.notifications.backend import InMemoryBackend
from lib.tarkov.notifications.models import Notification
from lib.tarkov.notifications.service import NotificationService


class CaptureNotifications:
    def __init__(self, notification_service: NotificationService) -> None:
        self._notification_service = notification_service

    @contextlib.asynccontextmanager
    async def capture(self, profile_id: str) -> AsyncIterator[list[Notification]]:
        received = []
        timeout = asyncio.Timeout(None)
        stopped = asyncio.Event()

        async def recv() -> None:
            with contextlib.suppress(asyncio.TimeoutError):
                async with timeout:
                    async for message in self._notification_service.subscribe(
                        profile_id,
                    ):
                        received.append(message)  # noqa: PERF401
            stopped.set()

        async with asyncio.TaskGroup() as tg:
            tg.create_task(recv())
            yield received
            await asyncio.sleep(0)
            timeout.reschedule(-1)
            await stopped.wait()


@pytest.fixture
async def in_memory_backend() -> InMemoryBackend:
    return InMemoryBackend(config=NotificationsConfig.Broker(expiration_time=60))


@pytest.fixture
async def notification_service(
    in_memory_backend: InMemoryBackend,
    container: aioinject.Container,
) -> AsyncIterator[NotificationService]:
    service = NotificationService(backend=in_memory_backend)
    with container.override(Object(service)):
        yield service


@pytest.fixture
def capture_notifications(
    notification_service: NotificationService,
) -> CaptureNotifications:
    return CaptureNotifications(notification_service=notification_service)
