from collections.abc import AsyncIterator, Sequence
from datetime import datetime
from pathlib import Path

import aioinject
import dotenv
import httpx
import pytest
from aioinject import InjectionContext
from httpx import ASGITransport
from litestar import Litestar
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.di import create_container
from app.db.models import Account
from lib.tarkov.db import AssetsPath, ResourceDB
from lib.tarkov.paths import asset_dir_path
from lib.tarkov.profiles.models import ProfileStoragePath
from lib.utils import utc_now

dotenv.load_dotenv(".env")

pytest_plugins = [
    "anyio",
    "sqlalchemy_pytest.database",
    "tests.plugins.configs",
    "tests.plugins.database",
    "tests.plugins.entities",
    "tests.plugins.fixture_typecheck",
    "tests.plugins.notifications",
    "tests.plugins.seeding",
    "tests.plugins.services",
    "tests.plugins.settings",
]


@pytest.fixture(scope="session", autouse=True)
def anyio_backend() -> str:
    return "asyncio"


@pytest.fixture(scope="session", autouse=True)
async def http_app() -> AsyncIterator[Litestar]:
    from app.adapters.api.app import create_app

    app = create_app()
    async with app.lifespan():
        yield app


@pytest.fixture(scope="session")
async def http_base_url() -> str:
    return "http://test"


@pytest.fixture
async def http_client(
    http_app: Litestar,
    http_base_url: str,
) -> AsyncIterator[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        transport=ASGITransport(app=http_app),  # type: ignore[arg-type]
        base_url=http_base_url,
    ) as client:
        yield client


@pytest.fixture
async def user_http_client(
    http_app: Litestar,
    account: Account,
) -> AsyncIterator[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        transport=ASGITransport(app=http_app),  # type: ignore[arg-type]
        base_url="http://test",
        cookies={
            "PHPSESSID": account.session_id,
        },
    ) as client:
        yield client


@pytest.fixture(scope="session")
def worker_id() -> str:
    return "main"


@pytest.fixture(scope="session")
def assets_path() -> AssetsPath:
    return AssetsPath(asset_dir_path)


@pytest.fixture(scope="session")
def locales(assets_path: Path) -> Sequence[str]:
    return [d.stem for d in assets_path.joinpath("locales", "global").iterdir()]


@pytest.fixture
def now() -> datetime:
    return utc_now()


@pytest.fixture(scope="session")
def container() -> aioinject.Container:
    return create_container()


@pytest.fixture
async def inject_context(
    container: aioinject.Container,
) -> AsyncIterator[aioinject.InjectionContext]:
    async with container.context() as ctx:
        yield ctx


@pytest.fixture(autouse=True)
async def session(
    container: aioinject.Container,
    session: AsyncSession,
) -> AsyncIterator[AsyncSession]:
    with container.override(aioinject.Object(session, AsyncSession)):
        yield session


@pytest.fixture
async def resource_db(inject_context: InjectionContext) -> ResourceDB:
    return await inject_context.resolve(ResourceDB)


@pytest.fixture(autouse=True)
async def _configure_container(
    container: aioinject.Container,
    tmp_path: Path,
) -> AsyncIterator[None]:
    with container.override(
        aioinject.Object(tmp_path.joinpath("profiles"), ProfileStoragePath),
    ):
        yield
