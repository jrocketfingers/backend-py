from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Success


@pytest.fixture
async def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("match_group_exit_from_menu")


async def test_ok(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await user_http_client.post(endpoint_url)

    assert response.status_code == HTTPStatus.OK
    assert Success[None].model_validate_json(response.content) == Success(data=None)
