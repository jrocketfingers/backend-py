from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Success
from app.db.models import AccountProfile
from lib.tarkov.profiles.storage import ProfileStorageImpl


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_game_logout")


@pytest.mark.usefixtures("profile_context")
async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    profile_storage: ProfileStorageImpl,
    account_profile: AccountProfile,
) -> None:
    assert str(account_profile.id) in profile_storage._profiles  # noqa: SLF001

    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert response.json() == Success(data=None).model_dump(mode="json")

    assert str(account_profile.id) not in profile_storage._profiles  # noqa: SLF001


async def test_ok_no_active_profile(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    profile_storage: ProfileStorageImpl,
    account_profile: AccountProfile,
) -> None:
    assert str(account_profile.id) not in profile_storage._profiles  # noqa: SLF001

    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert response.json() == Success(data=None).model_dump(mode="json")

    assert str(account_profile.id) not in profile_storage._profiles  # noqa: SLF001
