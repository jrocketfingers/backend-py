import random
import uuid

import httpx
import pytest
from aioinject import InjectionContext
from litestar import Litestar

from app.adapters.api.schema import Success
from app.core.domain.accounts.queries import ProfileSearchQuery
from app.db.models import AccountProfile
from lib.db import DBContext
from lib.tarkov.chat import ChatMember
from tests.factories import AccountProfileFactory


@pytest.fixture
async def query(inject_context: InjectionContext) -> ProfileSearchQuery:
    return await inject_context.resolve(ProfileSearchQuery)


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("profile_search")


ResponseType = Success[list[ChatMember]]


async def test_does_not_include_self(
    account_profile: AccountProfile,
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await user_http_client.post(
        endpoint_url,
        json={"nickname": account_profile.name},
    )
    data = ResponseType.model_validate_json(response.content)
    assert data.data == []


@pytest.mark.usefixtures("account_profile")
async def test_ok(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
    db_context: DBContext,
) -> None:
    profiles = AccountProfileFactory.build_batch(size=10)
    target = random.choice(profiles)
    target.name = str(uuid.uuid4())
    db_context.add_all(profiles)

    response = await user_http_client.post(endpoint_url, json={"nickname": target.name})
    data = ResponseType.model_validate_json(response.content)
    assert data.data == [ChatMember.from_profile(target)]
