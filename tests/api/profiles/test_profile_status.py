from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.profiles._schema import ProfileStatusesSchema, ProfileStatusSchema
from app.db.models import AccountProfile
from lib.tarkov.profiles.abc import ProfileStorage


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("profile_status")


async def test_no_profile(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    expected = ProfileStatusesSchema(
        max_pve_count_exceeded=False,
        profiles=[],
    )
    assert ProfileStatusesSchema.model_validate(response.json()["data"]) == expected


@pytest.mark.usefixtures("profile_context")
async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    account_profile: AccountProfile,
    profile_storage: ProfileStorage,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    profile = await profile_storage.readonly(profile_id=str(account_profile.id))
    expected = ProfileStatusesSchema(
        max_pve_count_exceeded=False,
        profiles=[
            ProfileStatusSchema(
                profile_id=profile_id,
                profile_token=None,
                status="Free",
                sid="",
                ip="",
                port=0,
            )
            for profile_id in (profile.pmc.id, profile.scav.id)
        ],
    )

    assert ProfileStatusesSchema.model_validate(response.json()["data"]) == expected
