import random
import typing
import uuid
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.adapters.api.profiles._schema import ProfileCreateSchema
from app.core.domain.items.services import CustomizationDB
from app.db.models import AccountProfile
from lib.tarkov.customization import PMCSide


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("profile_create")


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    session: AsyncSession,
    customization_db: CustomizationDB,
) -> None:
    voice = random.choice(list(customization_db.customization.values()))
    schema = ProfileCreateSchema(
        side=random.choice(typing.get_args(PMCSide)),
        nickname=str(uuid.uuid4()),
        head_id=str(uuid.uuid4()),
        voice_id=voice.id,
    )

    response = await user_http_client.post(
        endpoint_url,
        json=schema.model_dump(mode="json", by_alias=True),
    )
    assert response.status_code == HTTPStatus.OK

    profile = (await session.scalars(select(AccountProfile))).one()
    assert profile.side == schema.side
    assert profile.name == schema.nickname
    assert response.json()["data"] == {"uid": str(profile.id)}
