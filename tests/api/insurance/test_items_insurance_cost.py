from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.insurance.schema import InsuranceCostRequestSchema
from app.adapters.api.schema import Success
from app.core.domain.accounts.insurance.services import InsuranceService
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import Traders


@pytest.fixture
async def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("insurance_items_cost")


@pytest.mark.usefixtures("profile_context", "account_profile")
async def test_empty(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.post(
        endpoint_url,
        json=InsuranceCostRequestSchema(traders=[], items=[]).model_dump(mode="json"),
    )
    assert response.json() == Success(data={}).model_dump(mode="json")
    assert response.status_code == HTTPStatus.OK


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    profile_context: ProfileContext,
    insurance_service: InsuranceService,
    traders: Traders,
) -> None:
    items = list(profile_context.inventory.items.values())
    trader_ids = [TraderEnum.prapor.value, TraderEnum.therapist.value]
    expected = {}
    for trader_id in trader_ids:
        trader = traders.get(trader_id)
        assert trader
        expected[trader_id] = insurance_service.get_insurance_cost(
            items=items,
            trader=trader,
            profile=profile_context.pmc,
        )

    request_schema = InsuranceCostRequestSchema(
        traders=trader_ids,
        items=[item.id for item in items],
    )
    response = await user_http_client.post(
        endpoint_url,
        json=request_schema.model_dump(mode="json"),
    )

    assert response.json() == Success(data=expected).model_dump(mode="json")
    assert response.status_code == HTTPStatus.OK
