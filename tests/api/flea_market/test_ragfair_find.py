from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.flea_market._schema import FleaMarketSearchSchema
from app.adapters.api.schema import Success
from app.core.domain.flea_market.dto import FleaMarketSearchResultSchema
from app.core.domain.models import FleaMarketSort, FleaMarketSortDirection
from lib.tarkov.chat import MemberCategory
from lib.tarkov.types import ItemTemplateId


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_ragfair_find")


async def test_empty(http_client: httpx.AsyncClient, endpoint_url: str) -> None:
    payload = FleaMarketSearchSchema(
        condition_from=None,
        condition_to=None,
        price_from=None,
        price_to=None,
        quantity_from=None,
        quantity_to=None,
        page=0,
        limit=15,
        sort_type=FleaMarketSort.id,
        sort_direction=FleaMarketSortDirection.asc,
        currency=None,
        one_hour_expiration=False,
        remove_bartering=False,
        offer_owner_type=MemberCategory.default,
        only_functional=False,
        update_offer_count=True,
        handbook_id="",
        linked_search_id=ItemTemplateId(""),
        needed_search_id="",
        build_items={},
        build_count=0,
        tm=0,
        reload=0,
    )
    response = await http_client.post(
        url=endpoint_url,
        json=payload.model_dump(mode="json"),
    )
    assert response.status_code == HTTPStatus.OK

    expected = Success(
        data=FleaMarketSearchResultSchema(
            categories={},
            offers=[],
            offers_count=0,
            selected_category=payload.handbook_id,
        ),
    )
    assert response.json() == expected.model_dump(
        mode="json",
        by_alias=True,
        exclude_none=True,
    )
