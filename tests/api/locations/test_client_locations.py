from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar
from pydantic import TypeAdapter

from lib.tarkov.locations.db import LocationDB
from lib.tarkov.locations.models import LocationsBase


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_locations")


async def test_ok(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    location_db: LocationDB,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    adapter = TypeAdapter(LocationsBase)
    locations = adapter.validate_python(response.json()["data"])
    assert locations == location_db.full_base()
