from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from lib.tarkov.locations.db import LocationDB
from lib.tarkov.locations.models import Location


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("locations_get_loot")


async def test_ok(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    location_db: LocationDB,
) -> None:
    for location in location_db.locations:
        response = await http_client.post(
            endpoint_url,
            json={"locationId": location, "variantId": 0},
        )

        assert response.status_code == HTTPStatus.OK
        result = Location.model_validate(response.json()["data"])
        assert result.id == location
