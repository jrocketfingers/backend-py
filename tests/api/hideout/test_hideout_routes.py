from collections.abc import Sequence
from http import HTTPStatus
from typing import Any

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Success
from lib.tarkov.hideout.models.area import HideoutAreaTemplate
from lib.tarkov.hideout.models.qte import HideoutQuickTimeEvents
from lib.tarkov.hideout.models.recipes import HideoutRecipe, ScavCaseRecipe
from lib.tarkov.hideout.models.settings import HideoutSettings


@pytest.mark.parametrize(
    ("endpoint_name", "result_type"),
    [
        ("client_hideout_areas", Sequence[HideoutAreaTemplate]),
        ("client_hideout_cte_list", Sequence[HideoutQuickTimeEvents]),
        ("client_hideout_settings", HideoutSettings),
        ("client_hideout_production_recipes", Sequence[HideoutRecipe]),
        ("client_hideout_production_scavcase_recipes", Sequence[ScavCaseRecipe]),
    ],
)
async def test_ok(
    http_app: Litestar,
    http_client: httpx.AsyncClient,
    endpoint_name: str,
    result_type: Any,  # noqa: ANN401
) -> None:
    response = await http_client.post(http_app.route_reverse(endpoint_name))
    assert response.status_code is HTTPStatus.OK

    assert Success[result_type].model_validate_json(response.content, strict=False)
