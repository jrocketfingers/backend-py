from collections.abc import Sequence
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("locales_available_languages")


async def test_available_languages(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    locales: Sequence[str],
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    data = response.json()
    assert set(data["data"]) == set(locales)
