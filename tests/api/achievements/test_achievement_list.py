from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar
from pydantic import TypeAdapter

from app.adapters.api.schema import Elements, Success
from lib.tarkov.achievements.db import AchievementDB
from lib.tarkov.achievements.models import Achievement


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("achievement_list")


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    achievement_db: AchievementDB,
) -> None:
    adapter = TypeAdapter(Success[Elements[list[Achievement]]])

    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    content = adapter.validate_json(response.content)
    assert content.data
    assert content.data.elements == achievement_db.achievements
