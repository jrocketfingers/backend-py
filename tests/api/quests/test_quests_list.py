from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar
from pydantic import TypeAdapter

from app.core.domain.quests.services import QuestsService
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.quests.models import QuestTemplate
from lib.tarkov.quests.storage import QuestStorage


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("quest_list")


async def test_ok(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
    quest_storage: QuestStorage,
    quest_service: QuestsService,
    profile_context: ProfileContext,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    expected = [
        q
        for q in quest_storage.quests.values()
        if await quest_service.is_quest_available(quest=q, profile=profile_context.pmc)
    ]
    adapter = TypeAdapter(list[QuestTemplate])
    assert adapter.validate_python(response.json()["data"], strict=False) == expected
