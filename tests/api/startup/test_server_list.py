from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar
from pydantic import TypeAdapter

from app.adapters.api.startup._schema import ServerSchema


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("server_list")


async def test_ok(
    http_base_url: str,
    endpoint_url: str,
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    adapter = TypeAdapter(list[ServerSchema])
    result = adapter.validate_python(response.json()["data"])
    assert result == [ServerSchema(ip=http_base_url.removeprefix("http://"), port=80)]
