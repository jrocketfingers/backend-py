from datetime import datetime
from http import HTTPStatus

import freezegun
import httpx
import pytest
from litestar import Litestar

from app.adapters.api.startup._schema import GameStartResponseSchema
from app.db.models import Account


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_game_start")


async def test_menu_locale(
    endpoint_url: str,
    account: Account,
    user_http_client: httpx.AsyncClient,
    now: datetime,
) -> None:
    with freezegun.freeze_time(now):
        response = await user_http_client.post(endpoint_url)

    assert response.status_code == HTTPStatus.OK

    expected = GameStartResponseSchema(utc_time=int(now.timestamp()))
    assert GameStartResponseSchema.model_validate(response.json()["data"]) == expected
    assert response.cookies["PHPSESSID"] == str(account.session_id)
