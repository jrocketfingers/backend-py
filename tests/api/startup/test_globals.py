from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from lib.tarkov.db import ResourceDB


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_globals")


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    resource_db: ResourceDB,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert response.json()["data"] == resource_db.globals_.model_dump(
        by_alias=True,
        mode="json",
    )
