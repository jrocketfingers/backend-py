from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Success


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_version_validate")


async def test_version_validate(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert Success.model_validate(response.json()) == Success(data=None)
