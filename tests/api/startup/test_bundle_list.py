from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("bundle_list")


async def test_bundle_list(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.get(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert response.json() == []
