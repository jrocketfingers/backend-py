from http import HTTPStatus
from pathlib import Path

import httpx
import pytest
from litestar import Litestar

from lib.utils import read_json


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_settings")


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    assets_path: Path,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert response.json()["data"] == await read_json(
        assets_path.joinpath("core", "client.settings.json"),
    )
