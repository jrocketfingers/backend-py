from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.friends.schema import SendFriendRequestResponse
from app.adapters.api.schema import Error, Success
from app.core.domain.friends.repository import FriendRequestRepository
from app.db.models import AccountProfile, FriendRequest
from lib.db import DBContext
from lib.tarkov.errors import ErrorCode
from tests.factories import AccountProfileFactory

pytestmark = pytest.mark.usefixtures("account_profile")


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("friend_request_cancel")


async def test_profile_not_found(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.post(
        endpoint_url,
        json={"profileId": "id"},
    )

    assert response.status_code == HTTPStatus.OK
    data = Error.model_validate_json(response.content)
    assert data.error_code == ErrorCode.player_profile_not_found


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    db_context: DBContext,
    friend_request_repository: FriendRequestRepository,
    account_profile: AccountProfile,
) -> None:
    recipient = AccountProfileFactory.build()
    request = FriendRequest(sender=account_profile, recipient=recipient)
    db_context.add(recipient)
    db_context.add(request)

    response = await user_http_client.post(
        endpoint_url,
        json={"profileId": recipient.profile_id},
    )
    assert response.status_code == HTTPStatus.OK
    Success[SendFriendRequestResponse].model_validate_json(response.content)

    assert (
        await friend_request_repository.find(
            sender=account_profile,
            recipient=recipient,
        )
        is None
    )
