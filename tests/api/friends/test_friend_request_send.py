from http import HTTPStatus
from uuid import UUID

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.friends.schema import SendFriendRequestResponse
from app.adapters.api.schema import Error, Success
from app.core.domain.friends.repository import FriendRequestRepository
from lib.db import DBContext
from lib.tarkov.errors import ErrorCode
from tests.factories import AccountProfileFactory

pytestmark = pytest.mark.usefixtures("account_profile")


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("friend_request_send")


async def test_profile_not_found(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.post(
        endpoint_url,
        json={"to": "id"},
    )

    assert response.status_code == HTTPStatus.OK
    data = Error.model_validate_json(response.content)
    assert data.error_code == ErrorCode.player_profile_not_found


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    db_context: DBContext,
    friend_request_repository: FriendRequestRepository,
) -> None:
    recipient = AccountProfileFactory.build()
    db_context.add(recipient)

    response = await user_http_client.post(
        endpoint_url,
        json={"to": recipient.profile_id},
    )

    assert response.status_code == HTTPStatus.OK
    data = Success[SendFriendRequestResponse].model_validate_json(response.content)
    assert data.data
    assert data.data.status == ErrorCode.none
    assert await friend_request_repository.get(id=UUID(data.data.request_id))
