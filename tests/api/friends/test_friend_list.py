import random
from collections.abc import Sequence
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Success
from app.core.domain.friends.repository import FriendRepository
from app.db.models import AccountProfile, Friends
from lib.tarkov.chat import ChatMember
from lib.tarkov.friends.models import FriendList
from tests.factories import AccountProfileFactory


def _create_friendships(
    profiles: Sequence[AccountProfile],
    friend_repository: FriendRepository,
) -> Sequence[Friends]:
    result: list[Friends] = []
    for profile in profiles:
        friends = random.sample(
            [p for p in profiles if p is not profile],
            k=len(profiles) // 2,
        )
        for friend in friends:
            # Is already a friend
            if any(friendship.sender == friend for friendship in result):
                continue
            result.append(
                friend_repository.create(
                    sender=profile,
                    recipient=friend,
                ),
            )
    return result


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("friend_list")


@pytest.mark.usefixtures("account_profile")
async def test_empty(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert FriendList.model_validate(response.json()["data"]) == FriendList(
        friends=[],
        ignore=[],
        in_ignore_list=[],
    )


@pytest.mark.usefixtures("account_profile")
async def test_other_friendships(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
    friend_repository: FriendRepository,
) -> None:
    profiles = AccountProfileFactory.build_batch(size=10)
    _create_friendships(profiles=profiles, friend_repository=friend_repository)

    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert Success[FriendList].model_validate_json(response.content).data == FriendList(
        friends=[],
        ignore=[],
        in_ignore_list=[],
    )


async def test_ok(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
    account_profile: AccountProfile,
    friend_repository: FriendRepository,
) -> None:
    profiles = AccountProfileFactory.build_batch(size=10)
    _create_friendships(profiles=profiles, friend_repository=friend_repository)

    expected = [
        friend_repository.create(sender=account_profile, recipient=friend)
        for friend in random.sample(profiles, k=len(profiles) // 2)
    ]
    expected.sort(key=lambda f: f.recipient.name.lower())

    response = await user_http_client.post(endpoint_url)

    assert response.status_code == HTTPStatus.OK
    result = Success[FriendList].model_validate_json(response.content).data
    assert result
    assert result.friends == [
        ChatMember.from_profile(friend.recipient) for friend in expected
    ]
