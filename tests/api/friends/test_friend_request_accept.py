from http import HTTPStatus
from unittest.mock import patch

import httpx
import pytest
from litestar import Litestar
from result import Err

from app.adapters.api.schema import Error, Success
from app.core.domain.friends.commands import FriendAcceptRequestCommand
from app.core.domain.friends.errors import ProfileNotFound
from app.db.models import AccountProfile, FriendRequest
from lib.db import DBContext
from lib.tarkov.errors import ErrorCode
from tests.factories import AccountProfileFactory


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse(name="friend_request_accept")


@pytest.fixture
async def friend_request(
    account_profile: AccountProfile,
    db_context: DBContext,
) -> FriendRequest:
    request = FriendRequest(
        recipient=account_profile,
        sender=AccountProfileFactory.build(),
    )
    db_context.add(request)
    return request


async def test_ok(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
    friend_request: FriendRequest,
) -> None:
    response = await user_http_client.post(
        endpoint_url,
        json={"profileId": friend_request.sender.profile_id},
    )

    assert response.status_code == HTTPStatus.OK
    assert Success[bool].model_validate_json(response.content) == Success(data=True)


async def test_err(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
    friend_request: FriendRequest,
) -> None:
    with patch.object(
        FriendAcceptRequestCommand,
        FriendAcceptRequestCommand.execute.__name__,
        return_value=Err(ProfileNotFound()),
    ):
        response = await user_http_client.post(
            endpoint_url,
            json={"profileId": friend_request.sender.profile_id},
        )

    assert response.status_code == HTTPStatus.OK
    assert Error.model_validate_json(response.content) == Error(
        error_code=ErrorCode.player_profile_not_found,
    )
