from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.core.domain.friends.repository import FriendRepository
from app.db.models import AccountProfile


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("friend_delete")


async def test_ok(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
    account_profile: AccountProfile,
    friend: AccountProfile,
    friend_repository: FriendRepository,
) -> None:
    response = await user_http_client.post(
        endpoint_url,
        json={"friend_id": friend.profile_id},
    )
    assert response.status_code == HTTPStatus.OK
    assert response.json()["data"] is True

    assert await friend_repository.friends(account_profile) == []
