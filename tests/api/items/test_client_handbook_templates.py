from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Success
from lib.tarkov.handbook import Handbook


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_handbook_templates")


async def test_ok(http_client: httpx.AsyncClient, endpoint_url: str) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code is HTTPStatus.OK

    assert Success[Handbook].model_validate_json(response.content)
