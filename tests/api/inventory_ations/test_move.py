import random

import httpx
import pytest

from app.adapters.api.items._schema import (
    ItemsMovingRequestSchema,
    ItemsMovingResponseSchema,
)
from app.adapters.api.schema import Success
from lib.tarkov.items.actions import ItemChanges, Location, MoveAction
from lib.tarkov.items.models import ItemLocation, ItemRotation
from lib.tarkov.profiles.dto import ProfileContext


@pytest.mark.filterwarnings(
    # Pydantic creates a warning because we pass string into
    # ItemLocation.rotation, this is normal
    "ignore:Pydantic serializer warnings",
)
async def test_ok(
    user_http_client: httpx.AsyncClient,
    profile_context: ProfileContext,
    endpoint_url: str,
) -> None:
    root_id = profile_context.pmc.inventory.stash
    item = random.choice(list(profile_context.inventory.items.values()))
    assert item
    action = MoveAction(
        action="Move",
        item=item.id,
        to=Location(
            id=root_id,
            container="hideout",
            location=ItemLocation.model_construct(
                x=0,
                y=40,
                # Tests enum coercion
                rotation=ItemRotation.Horizontal.name,  # type: ignore[arg-type]
            ),
        ),
    )

    json = ItemsMovingRequestSchema(data=[action], tm=0, reload=0).model_dump(
        mode="json",
    )
    response = await user_http_client.post(endpoint_url, json=json)
    response.raise_for_status()
    data = Success[ItemsMovingResponseSchema].model_validate_json(
        response.content,
        strict=False,
    )
    assert data.data
    changes = data.data.profile_changes[profile_context.pmc.id].items
    assert changes == ItemChanges()
