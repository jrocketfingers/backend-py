import pytest
from litestar import Litestar


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("profile_items_moving")
