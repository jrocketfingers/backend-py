import zlib
from http import HTTPStatus

import httpx
import orjson
import pytest
from litestar import Litestar


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("sp_config_bot_difficulty")


async def test_zlib_request(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    content = zlib.compress(orjson.dumps({"name": "bear"}))
    response = await http_client.post(endpoint_url, content=content)
    assert response.status_code == HTTPStatus.OK
