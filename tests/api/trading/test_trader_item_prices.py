import uuid
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from lib.tarkov.trading.trader import Traders

pytestmark = [
    pytest.mark.usefixtures("profile_context", "_seed_traders"),
]


_ENDPOINT_NAME = "trader_item_prices"


async def test_not_found(
    user_http_client: httpx.AsyncClient,
    http_app: Litestar,
) -> None:
    response = await user_http_client.post(
        http_app.route_reverse(name=_ENDPOINT_NAME, trader_id=str(uuid.uuid4())),
    )
    assert response.status_code == HTTPStatus.NOT_FOUND


async def test_ok(
    user_http_client: httpx.AsyncClient,
    http_app: Litestar,
    traders: Traders,
) -> None:
    for trader in traders:
        response = await user_http_client.post(
            http_app.route_reverse(name=_ENDPOINT_NAME, trader_id=trader.base.id),
        )
        assert response.status_code == HTTPStatus.OK
