from collections.abc import Sequence
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Success
from lib.tarkov.trading.models import TraderBase


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("trading_trader_settings")


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert Success[Sequence[TraderBase]].model_validate(response.json(), strict=False)
