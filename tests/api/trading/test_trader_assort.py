import random
import uuid
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from lib.tarkov.trading.models import TraderEnum
from lib.tarkov.trading.trader import Traders

pytestmark = [
    pytest.mark.usefixtures("profile_context"),
]

_ENDPOINT_NAME = "trader_assort"


async def test_not_found(
    user_http_client: httpx.AsyncClient,
    http_app: Litestar,
) -> None:
    response = await user_http_client.post(
        http_app.route_reverse(name=_ENDPOINT_NAME, trader_id=str(uuid.uuid4())),
    )
    assert response.status_code == HTTPStatus.NOT_FOUND


async def test_not_present_in_db(
    user_http_client: httpx.AsyncClient,
    http_app: Litestar,
    traders: Traders,
) -> None:
    response = await user_http_client.post(
        http_app.route_reverse(
            name=_ENDPOINT_NAME,
            trader_id=random.choice(tuple(traders)).base.id,
        ),
    )
    assert response.status_code == HTTPStatus.NOT_FOUND


@pytest.mark.usefixtures("_seed_traders")
async def test_ok(
    user_http_client: httpx.AsyncClient,
    http_app: Litestar,
    traders: Traders,
) -> None:
    trader = traders.get(TraderEnum.prapor)
    assert trader
    response = await user_http_client.post(
        http_app.route_reverse(name=_ENDPOINT_NAME, trader_id=trader.base.id),
    )
    assert response.status_code == HTTPStatus.OK
