import uuid
from http import HTTPStatus

import httpx
from litestar import Litestar

from lib.tarkov.config import BotConfig

_ROUTE_NAME = "aki_bot_generation_limit"


async def test_ok(
    http_app: Litestar,
    user_http_client: httpx.AsyncClient,
    bot_config: BotConfig,
) -> None:
    for role, amount in bot_config.preset_batch.items():
        response = await user_http_client.get(
            http_app.route_reverse(_ROUTE_NAME, role=role),
        )
        assert response.status_code == HTTPStatus.OK
        assert response.json() == amount


async def test_not_found(
    http_app: Litestar,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.get(
        http_app.route_reverse(_ROUTE_NAME, role=str(uuid.uuid4())),
    )
    assert response.status_code == HTTPStatus.OK
    assert response.json() == 30  # noqa: PLR2004
