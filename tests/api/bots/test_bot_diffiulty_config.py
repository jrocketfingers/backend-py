import uuid
from collections.abc import Sequence
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.startup._schema import (
    BotDifficultyRequestSchema,
)
from lib.tarkov.bots.db import BotDB
from lib.utils import snake_to_pascal


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("sp_config_bot_difficulty")


async def test_not_found(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(
        endpoint_url,
        json=BotDifficultyRequestSchema(name=str(uuid.uuid4())).model_dump(),
    )
    assert response.status_code == HTTPStatus.NOT_FOUND


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    bot_types: Sequence[str],
    bot_db: BotDB,
) -> None:
    for bot_type in bot_types:
        response = await http_client.post(
            endpoint_url,
            json=BotDifficultyRequestSchema(name=bot_type).model_dump(),
        )
        assert response.status_code == HTTPStatus.OK
        assert response.json() == {
            snake_to_pascal(k): v for k, v in bot_db.bots[bot_type].difficulty.items()
        }
