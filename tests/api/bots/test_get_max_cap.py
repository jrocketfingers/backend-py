from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.db.models import AccountProfile, ProfileRaidInfo
from lib.tarkov.config import BotConfig


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("aki_bot_max_cap")


@pytest.mark.usefixtures("account_profile")
async def test_no_active_raid(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.get(endpoint_url)
    assert response.status_code == HTTPStatus.BAD_REQUEST


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    bot_config: BotConfig,
    account_profile: AccountProfile,
) -> None:
    for location, bot_cap in bot_config.max_bot_cap.items():
        account_profile.raid_info = ProfileRaidInfo(locations=location)

        response = await user_http_client.get(endpoint_url)

        assert response.status_code == HTTPStatus.OK
        assert response.json() == bot_cap
