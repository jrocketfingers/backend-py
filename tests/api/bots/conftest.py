from collections.abc import Sequence

import pytest

from lib.tarkov.bots.db import BotDB


@pytest.fixture
def bot_types(bot_db: BotDB) -> Sequence[str]:
    return list(bot_db.bots.keys())
