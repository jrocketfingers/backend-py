from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.core.domain.items.services import CustomizationDB


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("account_customization")


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    customization_db: CustomizationDB,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    assert response.json()["data"] == [
        c.id for c in customization_db.customization.values() if c.props.side
    ]
