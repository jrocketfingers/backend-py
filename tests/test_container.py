import aioinject
from aioinject import validation


def test_validate_container(container: aioinject.Container) -> None:
    validation.validate_container(
        container=container,
        validators=validation.DEFAULT_VALIDATORS,
    )
