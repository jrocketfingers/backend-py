import dotenv
import uvicorn

from app.adapters.api.app import create_app

if __name__ == "__main__":
    dotenv.load_dotenv(".env")
    uvicorn.run(create_app())
