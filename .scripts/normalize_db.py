import asyncio
from pathlib import Path
from typing import Any

import orjson

from lib.tarkov.paths import asset_dir_path
from lib.utils import read_json


def _looks_like_response(obj: dict[str, Any]) -> bool:
    return "err" in obj and "errmsg" in obj and "data" in obj


async def _normalize_locale(path: Path) -> None:
    locale = await read_json(path)
    if not _looks_like_response(locale):
        return
    path.write_bytes(
        orjson.dumps(locale["data"], option=orjson.OPT_INDENT_2),
    )


async def _normalize_menu(path: Path) -> None:
    menu = await read_json(path)
    if _looks_like_response(menu):
        menu = menu["data"]
    if "menu" in menu and len(menu) == 1:
        menu = menu["menu"]
    path.write_bytes(orjson.dumps(menu, option=orjson.OPT_INDENT_2))


async def normalize_locales(path: Path) -> None:

    for locale_file in path.joinpath("global").iterdir():
        await _normalize_locale(locale_file)

    for menu_file in path.joinpath("menu").iterdir():
        await _normalize_menu(menu_file)


async def main() -> None:
    await normalize_locales(asset_dir_path.joinpath("locales"))


if __name__ == "__main__":
    asyncio.run(main())
