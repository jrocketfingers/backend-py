# Installation
## Requirements
- [Python 3.11](https://www.python.org/downloads/) or higher
- [PDM](https://pdm-project.org/en/stable/) package manager
- [Taskfile](https://taskfile.dev/installation/) as a local task runner

## Installation
Run `pdm sync --clean` to install dependencies, if there's no 
active virtual environment pdm should create one.

Project nests all files in `/src` directory, so you have to add
`PYTHONPATH=src` to your environment variables.

Copy `.env.example` to `.env` and set up a postgresql or sqlite database

Run database migrations with `alembic upgrade head`

## Running the game
Server currently does not work with AKI Launcher, account can be
created by inserting a row into `accounts` table and 
game can be launched with a batch file
```sh
EscapeFromTarkov.exe -bC5vLmcuaS5u={'email':'email','password':'password','toggle':true,'timestamp':0} -token=SESSION_ID_TOKEN -config={'BackendUrl':'http://127.0.0.1:8000','Version':'live'}
```
